<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    // SET HEADER
    header("Content-Type: application/json; charset=UTF-8");

    // INCLUDING DATABASE AND MAKING OBJECT
    include('../db/database.php');
    // MAKE SQL QUERY
    $personData = json_decode($_REQUEST['data']);
    $dvalue = $personData->dvalue;
    $lifestage = $personData->lifestage;
    $resourcetypes = $personData->rtypes;
    $tags = $personData->tags;
    $dvalue = $personData->dvalue;
    $keywords = explode(',', $dvalue);
    $advancedkeywords = implode("', '", $keywords);

    $search = $personData->search;
    $searchQuery = str_replace('\\', "", $search);
    $unquotedQuery = str_replace('"', "", $search);
    $sort = $personData->sort;
    if (empty($sort)) {
        $sort = '0';
    }

    if ($search == '0') {
        $searchQuery = '';
        $unquotedQuery = '';
    }

    if ($resourcetypes === 0 OR $resourcetypes == '0') {
        $resourcetypes = '';
    }
    if($tags == '0'){
        $tags = '';
    }
    if (empty($dvalue)) {
        $dvalue = '0';
    }
    if ($lifestage == 0) {
        $lifestage = '';
    }
    $page = $personData->pager;

    $limit = '9';
    if ($page) {
        $start = ($pvalue - 1) * $limit;
    } else {
        $start = 0;
    }
    $list = 'true';
    $level = '100';
    $pgorder = '1';

    /**
     * Optimise code
     */
    $select = "select distinct RS.wp_post_id, P.post_title as title, P.post_date_gmt as `date`, P.post_type, P.post_name, RS.slug, match (RS.html, RS.title) against ('{$unquotedQuery}') AS relevancy, (P.post_title = '{$unquotedQuery}') AS title_match, (P.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, wc.view_count, RS.page_order, P.post_date_gmt";
    $join = " join wp_posts P on P.ID = RS.wp_post_id ";

    // if ($lifestage != 0) {
    //     $join .= " join life_stage_type AS l ON (l.lifestagetype in ({$lifestage}) AND l.postid = RS.ID) ";
    // }

    $join .= "  left join wp_postmeta PM on PM.post_id = RS.wp_post_id AND PM.meta_value <='{$searchQuery}' AND PM.meta_key = 'access_level' AND PM.meta_key = 'list_in_search' ";
    $join .= "  left join wp_postmeta MM on MM.post_id = RS.wp_post_id AND MM.meta_value = 'true'";
    $join .= "  left join wp_postmeta MD on MD.post_id = RS.wp_post_id AND MD.meta_value = '_page_edit_data'";
    $join .= "  left join wp_resources_view_count as wc on wc.wp_post_id = RS.wp_post_id ";

    $from = " from wp_resources as RS";
    $where = " where P.post_status = 'publish' AND RS.level_of_access = '$level' AND RS.list_in_search = '$list' AND RS.page_order = '$pgorder'" ;
    if ($searchQuery != '') {
        $where .= " and (match (RS.html, RS.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
    }
    if($resourcetypes !== ''){
        $where .= " and P.post_type='{$resourcetypes}' ";
    }
    //    echo $tags;
    if($tags != 0){
        $tags = rtrim($tags,',');
        $where .= " and  P.ID in (select TR.object_id from wp_term_taxonomy as TT  join wp_term_relationships as TR on TR.term_taxonomy_id = TT.term_taxonomy_id where TT.taxonomy = 'resource_tag' and TT.term_id in ({$tags})) ";
    }
    //Custom Pagination
    $order = '';
    // if ($sort === 0 || $sort == '0') {
    //     $order .= " order by title_match desc, title_rough_match desc, relevancy desc";
    // } else 
    
    if ($sort == 'views') {
        $order .= " order by wc.view_count DESC";
    } else if ($sort == 'relevance') {
        $order .= " AND RS.page_order = '$pgorder' ";
    } else if ($sort == 'date') {
        $order .= " order by date desc";
    }
    $order .= " limit {$start},{$limit}";
    //echo $order;

    $where .= mainLifeStages($lifestage,$tags,$db);
    $sql = $select . $from . $join . $where . $order;
//    echo $sql;die;

    $getwppost = $db->prepare($sql);
    $getwppost->execute();
    if ($resourcetypes == '0') {
        $rtypesquery = '';
    } else {
        $rtypesquery = " AND post_type = '$resourcetypes'";
    }
    $poststatus = " ";
    $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    $escaped_url = htmlspecialchars($url, ENT_QUOTES, 'UTF-8');
    $table_name = "wp_term_relationships";
    $table_name2 = "wp_terms";

    while ($row = $getwppost->fetch(PDO::FETCH_ASSOC)) {
        $title = $row['title'];
        $postid = $row['wp_post_id'];
        $postname = $row['title'];
        $posttype = $row['post_type'];
        $slug = $row['slug'];
        if ($posttype == 'article') {
            $seo_dvalue = $base_site_url . '/resources/articles/';
        } else if ($posttype == 'calculator') {
            $seo_dvalue = $base_site_url . '/resources/calculators/';
        } else if ($posttype == 'video') {
            $seo_dvalue = $base_site_url . '/resources/videos/';
        } else if ($posttype == 'newsletter') {
            $seo_dvalue = $base_site_url . '/resources/newsletters/';
        } else if ($posttype == 'podcast') {
            $seo_dvalue = $base_site_url . '/resources/podcasts/';
        } else if ($posttype == 'toolkit') {
            $seo_dvalue = $base_site_url . '/resources/toolkits/';
        } else if ($posttype == 'booklet') {
            $seo_dvalue = $base_site_url . '/resources/booklets/';
        }else if($dvaluen == 'worksheet'){
            $seo_dvalue = $base_site_url.'/resources/worksheets/';
        }else if($dvaluen == 'checklist'){
            $seo_dvalue = $base_site_url.'/resources/checklists/';
        } else {
        }
        $output = '';
        //$output .= '<div class="resource-column same-height-holder content-inner-page">';
        $output .= '<!-- resource resource in resources starts -->
            <div class="col-sm-6 col-md-4">
             <!-- resource block starts -->
             <div class="resource-block">';
        $output .= '<div class="img-holder same-height"><span class="icon-' . $posttype . '"></span></div>
           <div class="text-holder"><p class="dot-holder">' . $title . '</p>';
        $output .= '<div class="btn-tag same-height  same-height-left same-height-right" style="height: 103px;" dvalue="' . $countthem . '">';
        $resultss = $db->prepare("SELECT * FROM $table_name WHERE object_id='$postid' limit 3");
        $resultss->execute();
        while ($resultn = $resultss->fetch(PDO::FETCH_ASSOC)) {
            $tagid = $resultn['term_taxonomy_id'];
            $output .= $tagid;
            $gettagname = $db->prepare("SELECT * FROM $table_name2 WHERE term_id='$tagid'");
            $gettagname->execute();
            while ($resultnn = $gettagname->fetch(PDO::FETCH_ASSOC)) {
                $output .= '<a class="tag ng-binding ng-scope tag-click" url="' . $escaped_url . '" tagname="' . $resultnn['name'] . '" pager="' . $pager . '" type="' . $type . '" dvalue="' . $tagid . '" tagid="' . $dvalue . '" tags="' . $dvalue . '">';
                $output .= $resultnn['name'];
                $output .= '</a>';
            }
        }
        $output .= '</div>';
        $output .= '<a href="' . $seo_dvalue . $slug . '" target="_self" class="text-view btn btn-warning btn-block">VIEW</a>';
        $output .= '</div>';
        $output .= '<span class="icon-lock" style="display: none;"></span>';
        $output .= '</div>
                    <!-- resource block ends -->
                    </div>
                    <!-- resource resource in resources starts -->';
        //$output .= '</div>';
        $return_arr[] = array("message" => $output);
        //$return_arr[] = array("title" => $title, "postid" => $postid, "postname" => $postname, "posttype" => $posttype);
    }
    //}
    // tag search ends

    // Encoding array in JSON format
    echo json_encode($return_arr);
?>
