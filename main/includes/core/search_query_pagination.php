<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
// SET HEADER
    header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
    include('../db/database.php');
// MAKE SQL QUERY
    $personData = json_decode($_REQUEST['data']);
    $dvaluen = $personData->dvalue;
    $page = $personData->page;
    $resourcetypes = $personData->resourcetypes;
    $lifestage = $personData->lifestage;
    if (!isset($personData->dvalue)) {
        $search = $personData->dvalue;
    }
    $newsearch = '';
    if (strpos($dvaluen, '(') != '' && strpos($dvaluen, ')') == '') {
        $searchx = explode('(', $dvaluen);
        $newsearch = trim($searchx[0]);
    } else if (strpos($dvaluen, '(') != '' && strpos($dvaluen, ')') != '') {
        $dvaluen = trim(preg_replace('/\s*\([^)]*\)/', '', $dvaluen));
    }
    if ($newsearch != '') {
        $dvaluen = $newsearch;
    }
    if($search == '0'){
        $searchQuery = '';
        $unquotedQuery = '';
    }
    $searchQuery = str_replace('\\', "", $dvaluen);
    $searchQuery = str_replace("'", "", $searchQuery);
    $searchQuery = preg_replace('/[#\@\.\*\%\;\$\&\^]+-/', '', $searchQuery);

    $unquotedQuery = str_replace('"', "", $dvaluen);
    $unquotedQuery = str_replace("'", "", $unquotedQuery);
    $unquotedQuery = preg_replace('/[#\@\.\*\%\;\$\&\^]+-/', '', $unquotedQuery);

    if($searchQuery == ''){
        $searchQuery = '0';
    }

    $return_arr = array();
    $sort = $personData->sort;
    if (empty($sort)) {
        $sort = '0';
    }
    if ($page) {
        $pvalue = $page;
    } else {
        $pvalue = '0';
    }
    $limit = 9;
    if ($page) {
        $start = ($pvalue - 1) * $limit;
    } else {
        $start = 0;
    }
    $tags = $personData->tags;
    if (empty($tags) or $tags == "") {
        $tags = '0';
    }
    $keywords = explode(',', $tags);
    $advancedkeywords = implode("', '", $keywords);

    if (empty($tags) or $tags == "") {
        $tags = '0';
    }
    $list = 'true';
    $level = '100';
    $pgorder = '1';
    $limit = ' limit 9';

    if ($dvaluen == '') {
        $searchvalue = '0';
    } else {
        $searchvalue = $dvaluen;
    }
    $dtype = $personData->resourcetypes;
    if($dtype === 0){
        $dtype = '';
    }
    $list = 'true';
    $level = '100';
    $pgorder = '1';
    /**
     * Optimise code
     */
    $select = "select distinct RS.wp_post_id, P.post_title as title, P.post_date_gmt as `date`, P.post_type, P.post_name, RS.slug, match (RS.html, RS.title) against ('{$unquotedQuery}') AS relevancy, (P.post_title = '{$unquotedQuery}') AS title_match, (P.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, wc.view_count, RS.page_order,P.post_date_gmt";
    $selectCount = "select Count(DISTINCT P.id) AS pages";
    $join = " join wp_posts P on P.ID = RS.wp_post_id ";

    // if ($lifestage != 0) {
    //     $join .= " join life_stage_type AS l ON (l.lifestagetype in ({$lifestage}) AND l.postid = RS.ID) ";
    // }

    $join .= "  left join wp_postmeta PM on PM.post_id = RS.wp_post_id AND PM.meta_value <='{$searchQuery}' AND PM.meta_key = 'access_level' AND PM.meta_key = 'list_in_search' ";
    $join .= "  left join wp_postmeta MM on MM.post_id = RS.wp_post_id AND MM.meta_value = 'true'";
    $join .= "  left join wp_postmeta MD on MD.post_id = RS.wp_post_id AND MD.meta_value = '_page_edit_data'";
    $join .= "  left join wp_resources_view_count as wc on wc.wp_post_id = RS.wp_post_id ";

    $from = " from wp_resources as RS";
    $where = " where P.post_status = 'publish' AND RS.level_of_access = '$level' AND RS.list_in_search = '$list' AND RS.page_order = '$pgorder'";
    if ($searchQuery != '0') {
        $where .= " and (match (RS.html, RS.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
    }
    if ($dtype !== '0') {
        $where .= " and P.post_type='{$dtype}' ";
    }
//    echo $tags;
    if ($tags != 0) {
        $tags = rtrim($tags, ',');
        $where .= " and  P.ID in (select TR.object_id from wp_term_taxonomy as TT  join wp_term_relationships as TR on TR.term_taxonomy_id = TT.term_taxonomy_id where TT.taxonomy = 'resource_tag' and TT.term_id in ({$tags})) ";
    }
    //Custom Pagination
    $order = '';
    // if($sort === 0 || $sort == '0'){
    //     $order .= " order by title_match desc, title_rough_match desc, relevancy desc";
    // }
    // else 
    
    if ($sort == 'views') {
        $order .= " order by wc.view_count DESC";
    }
    else if  ($sort == 'relevance') {
        $order .= " AND RS.page_order = '$pgorder' ";
    }
    else if ($sort == 'date') {
        $order .= " order by P.post_date_gmt desc";
    }
//    $order .=  " limit {$start},{$limit}";
    $where .= mainLifeStages($lifestage,$tags,$db);
    $sql = $select . $from . $join . $where . $order;
    //echo $sql; die;
    $result = $db->prepare($sql);
    $result->execute();
    $rcount = $result->rowCount();
    $limit = '9';
    $totalpages = ceil($rcount / $limit);
    $output .= '<nav aria-label="balance pager m14-m15" balance-pager="" class="paging-holder clear">
    <ul class="pagination">';
    if ($pvalue > 1) {
        if ($totalpages != 1) {
            $output .= '<li>
	<div class="search-prv-click" query="' . $searchvalue . '" pager="' . ($pvalue - 1) . '" aria-label="Next"  resourcetypes="' . $resourcetypes . '" lifestage="' . $lifestage . '"   sort="' . $sort . '">
		<span class="btn-prev"></span>
	       <span class="hidden-xs">Prev</span>
	</div>
	</li>';
        }
    }
    if (empty($pvalue) or $pvalue == '' or $pvalue == 0 or $pvalue == '0') {
        $pvalue = 1;
    }
    if ($pvalue == $totalpages) {
        //for ($i=1; $i <= min($totalpages,10); $i++) {

        if ($totalpages == 1) {
            $output .= '<li class="active" style="padding:5px 6px; cursor: pointer"  totalpages="' . $totalpages . '"  resourcetypes="' . $resourcetypes . '" lifestage="' . $lifestage . '" queryvalue="' . $searchvalue . '" pagerv="1"  sort="' . $sort . '">1</li>';
        } else if ($totalpages <= 6) {
            for ($i = 1; $i <= $totalpages; $i++) {
                $output .= '<li class="pg-btn-search ' . ($pvalue == $i ? 'active' : '') . '" style="padding:5px 6px; cursor: pointer"  resourcetypes="' . $resourcetypes . '" lifestage="' . $lifestage . '" queryvalue="' . $searchvalue . '" pagerv="' . $i . '"  sort="' . $sort . '">' . $i . '</li>';
            }
        } else if ((6 + $pvalue - 1) < $totalpages) {
            for ($i = (1 + $pvalue - 1); $i <= (6 + $pvalue - 1); $i++) {
                $output .= '<li class="pg-btn-search ' . ($pvalue == $i ? 'active' : '') . '" style="padding:5px 6px; cursor: pointer"  resourcetypes="' . $resourcetypes . '" lifestage="' . $lifestage . '" queryvalue="' . $searchvalue . '" pagerv="' . $i . '"  sort="' . $sort . '">' . $i . '</li>';
            }
        } else {
            for ($i = ($totalpages - 5); $i <= $totalpages; $i++) {
                $output .= '<li class="pg-btn-search ' . ($pvalue == $i ? 'active' : '') . '" style="padding:5px 6px; cursor: pointer"  resourcetypes="' . $resourcetypes . '" lifestage="' . $lifestage . '" queryvalue="' . $searchvalue . '" pagerv="' . $i . '"  sort="' . $sort . '">' . $i . '</li>';
            }
        }

        // 	for ($i= max(1, $pvalue); $i <= min($pvalue + 5, $totalpages); $i++) {
        // $output .='<li class="pg-btn-search '.($pvalue == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer"  resourcetypes="'.$resourcetypes.'" lifestage="'.$lifestage.'" queryvalue="'.$searchvalue.'" pagerv="'.$i.'"  sort="'.$sort.'">'.$i.'</li>';
        // }


        if ($pvalue < $totalpages) {
            $output .= '<li style="padding-top: 4px; padding-left: 17px;">
		<div class="search-nxt-click" style="cursor: pointer" query="' . $searchvalue . '" pager="' . ($pvalue + 1) . '" resourcetypes="' . $resourcetypes . '" lifestage="' . $lifestage . '" sort="' . $sort . '">
			<span class="hidden-xs" style="float: left;">Next</span>
			<span class="btn-next" style="float: left; margin-top: 6px; margin-left: 10px;"></span>
		</div>
		</li>';
        }

        if ($totalpages > 1) {
            $output .= '</ul>
			<p>
				<span>of&nbsp;</span>
				<span class="ng-binding">' . $totalpages . '</span>
				<span>&nbsp;pages</span>
			</p>
	</nav>';
        } else {
            $output .= '</ul>
			<p>
				<span>of&nbsp;</span>
				<span class="ng-binding">' . $totalpages . '</span>
				<span>&nbsp;page</span>
			</p>
	</nav>';
        }
    } else {
        //for ($i=1; $i <= min($totalpages,10); $i++) {

        if ($totalpages == 1) {
            $output .= '<li class="active" style="padding:5px 6px; cursor: pointer"  totalpages="' . $totalpages . '"  resourcetypes="' . $resourcetypes . '" lifestage="' . $lifestage . '" queryvalue="' . $searchvalue . '" pagerv="1"  sort="' . $sort . '">1</li>';
        } else if ($totalpages <= 6) {
            for ($i = 1; $i <= $totalpages; $i++) {
                $output .= '<li class="pg-btn-search ' . ($pvalue == $i ? 'active' : '') . '" style="padding:5px 6px; cursor: pointer"  resourcetypes="' . $resourcetypes . '" lifestage="' . $lifestage . '" queryvalue="' . $searchvalue . '" pagerv="' . $i . '"  sort="' . $sort . '">' . $i . '</li>';
            }
        } else if ((6 + $pvalue - 1) < $totalpages) {
            for ($i = (1 + $pvalue - 1); $i <= (6 + $pvalue - 1); $i++) {
                $output .= '<li class="pg-btn-search ' . ($pvalue == $i ? 'active' : '') . '" style="padding:5px 6px; cursor: pointer"  resourcetypes="' . $resourcetypes . '" lifestage="' . $lifestage . '" queryvalue="' . $searchvalue . '" pagerv="' . $i . '"  sort="' . $sort . '">' . $i . '</li>';
            }
        } else {
            for ($i = ($totalpages - 5); $i <= $totalpages; $i++) {
                $output .= '<li class="pg-btn-search ' . ($pvalue == $i ? 'active' : '') . '" style="padding:5px 6px; cursor: pointer"  resourcetypes="' . $resourcetypes . '" lifestage="' . $lifestage . '" queryvalue="' . $searchvalue . '" pagerv="' . $i . '"  sort="' . $sort . '">' . $i . '</li>';
            }
        }

        // 	for ($i= max(1, $pvalue ); $i <= min($pvalue + 5, $totalpages); $i++) {
        // $output .='<li class="pg-btn-search '.($pvalue == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer"  resourcetypes="'.$resourcetypes.'" lifestage="'.$lifestage.'" queryvalue="'.$searchvalue.'" pagerv="'.$i.'"  sort="'.$sort.'">'.$i.'</li>';
        // }
        if ($pvalue < $totalpages) {
            $output .= '<li style="padding-top: 4px; padding-left: 17px;">
	<div class="search-nxt-click" style="cursor: pointer" query="' . $searchvalue . '" pager="' . ($pvalue + 1) . '" resourcetypes="' . $resourcetypes . '" lifestage="' . $lifestage . '" sort="' . $sort . '">
		<span class="hidden-xs" style="float: left;">Next</span>
		<span class="btn-next" style="float: left; margin-top: 6px; margin-left: 10px;"></span>
	</div>
	</li>';
        }
        if ($totalpages > 1) {
            $output .= '</ul>
			<p>
				<span>of&nbsp;</span>
				<span class="ng-binding">' . $totalpages . '</span>
				<span>&nbsp;pages</span>
			</p>
	</nav>';
        } else {
            $output .= '</ul>
			<p>
				<span>of&nbsp;</span>
				<span class="ng-binding">' . $totalpages . '</span>
				<span>&nbsp;page</span>
			</p>
	</nav>';
        }
    }
//if($rcount==0)	$output='';
    $return_arr['message'] = $output;
    echo json_encode($return_arr);
?>