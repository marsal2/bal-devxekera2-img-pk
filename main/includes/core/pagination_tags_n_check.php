<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
// SET HEADER
    header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
    include('../db/database.php');
// MAKE SQL QUERY
    $personData = json_decode($_REQUEST['data']);
    $dvaluen = $personData->dvalue;
    $page = $personData->page;
    $dvalue = $personData->tagid;
    $keywords = explode(',', $dvalue);
    $advancedkeywords = implode("', '", $keywords);
    $limit = 9;
    if ($page) {
        $start = ($page - 1) * $limit;
        $newpage = $page;
    } else {
        $start = 0;
        $newpage = 1;
    }

    if (empty($page) or $page == '') {
        $page = 1;
    }
    $search = $personData->search;
    if ($search == '0') {
        $search = '';
    }
    $searchQuery = str_replace('\\', "", $search);
    $unquotedQuery = str_replace('"', "", $search);
    $sort = $personData->sort;
    $lifestage = $personData->lifestage;
    $resourcetypes = $personData->rtypes;

    $rt = $personData->rtype;

    if (!empty($rt)) {
        $resourcetypes = $personData->rtype;
    }
    if (empty($sort)) {
        $sort = '0';
    }

    if (empty($search)) {
        $search = '0';
    }
    if ($resourcetypes === 0 OR $resourcetypes == '0') {
        $resourcetypes = '';
    }
    if ($dvalue == 0) {
        $dvalue = '';
    }
    if ($lifestage == 0) {
        $lifestage = '';
    }
    $list = 'true';
    $level = '100';
    $pgorder = '1';
    $resources = 'resources/';

    /**
     * Optimise code
     */
    $select = "select distinct RS.wp_post_id, P.post_title as title, P.post_date_gmt as `date`, P.post_type, P.post_name,  RS.slug,match (RS.html, RS.title) against ('{$unquotedQuery}') AS relevancy, (P.post_title = '{$unquotedQuery}') AS title_match, (P.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, wc.view_count,RS.page_order,P.post_date_gmt";
    $join = " join wp_posts P on P.ID = RS.wp_post_id ";

    // if($lifestage != ''){
    //     $join .=" join life_stage_type AS l ON (l.lifestagetype in ({$lifestage}) AND l.postid = RS.ID) ";
    // }


    $join .= "  left join wp_postmeta PM on PM.post_id = RS.wp_post_id AND PM.meta_value <='{$searchQuery}' AND PM.meta_key = 'access_level' AND PM.meta_key = 'list_in_search' ";
    $join .= "  left join wp_postmeta MM on MM.post_id = RS.wp_post_id AND MM.meta_value = 'true'";
    $join .= "  left join wp_postmeta MD on MD.post_id = RS.wp_post_id AND MD.meta_value = '_page_edit_data'";
    $join .= "  left join wp_resources_view_count as wc on wc.wp_post_id = RS.wp_post_id ";

    $from = " from wp_resources as RS";
    $where = " where P.post_status = 'publish' AND RS.level_of_access = '$level' AND RS.list_in_search = '$list' AND RS.page_order = '$pgorder'";
    if ($searchQuery != '') {
        $where .= " and (match (RS.html, RS.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
    }
    if ($resourcetypes != '') {
        $where .= " and P.post_type='{$resourcetypes}' ";
    }
    //    echo $tags;
    if ($dvalue != '') {
        $dvalue = rtrim($dvalue, ',');
        $where .= " and  P.ID in (select TR.object_id from wp_term_taxonomy as TT  join wp_term_relationships as TR on TR.term_taxonomy_id = TT.term_taxonomy_id where TT.taxonomy = 'resource_tag' and TT.term_id in ({$dvalue})) ";
    }
    //Custom Pagination
    $order = '';
    // if($sort === 0 || $sort == '0'){
    //     $order .= " order by title_match desc, title_rough_match desc, relevancy desc";
    // }
    // else 
    
    if ($sort == 'views') {
        $order .= " order by wc.view_count DESC";
    }
    else if  ($sort == 'relevance') {
        $order .= " AND RS.page_order = '$pgorder' ";
    }
    else if ($sort == 'date') {
        $order .= " order by P.post_date_gmt desc";
    }
    //$order .=  " limit {$start},{$limit}";
    //echo $order;
    $where .= mainLifeStages($lifestage,$tags,$db);
    $sql = $select . $from . $join . $where . $order;
//        echo $sql;die;

    $checkn = $db->prepare($sql);

    $checkn->execute();
//now count row 
    $checkcountn = $checkn->rowCount();

    $totalpages = ceil($checkcountn / $limit);

    $ttvalue = 'tags';

    $output = '';
//$output = $newpage.'--this is new page--this is total page'.$totalpages;  
    $output .= '<nav aria-label="balance pager m14-m15" balance-pager="" class="paging-holder clear">
    <ul class="pagination">';
    if ($page > 1) {
        if ($totalpages != 1) {
            $output .= '<li>
			<div class="prv-btn-tags" search="0" sort="0" tagid="' . $tagids . '" type="' . $ttvalue . '" pager="' . ($page - 1) . '" search="0" sort="0">
				<div style="float:left;margin-right: 5px;margin-left: 10px;margin-top: 11px; cursor: pointer;">
					<span class="btn-prev"></span>
				</div>
				<div style="float:left;margin-top: 7px;  cursor: pointer; margin-right: 22px;">
					<span class="hidden-xs">Prev</span>
				</div>
			</div>
		 </li>';
        }
    }

    if ($page == $totalpages) {

        if ($totalpages == 1) {
            $output .= '<li class="active" style="padding:5px 6px; cursor: pointer"  totalpages="' . $totalpages . '"  resourcetypes="' . $resourcetypes . '" lifestage="' . $lifestage . '" queryvalue="' . $searchvalue . '" pagerv="1"  sort="' . $sort . '">1</li>';
        } else if ($totalpages <= 6) {
            for ($i = 1; $i <= $totalpages; $i++) {
                $output .= '<li class="pg-btn-tags ' . ($page == $i ? 'active' : '') . '" style="padding:5px 6px; cursor: pointer" tagid="' . $tagids . '" typevalue="' . $ttvalue . '" pagerv="' . $i . '" search="0" sort="0">' . $i . '</li>';
            }
        } else if ((6 + $page - 1) < $totalpages) {
            for ($i = (1 + $page - 1); $i <= (6 + $page - 1); $i++) {
                $output .= '<li class="pg-btn-tags ' . ($page == $i ? 'active' : '') . '" style="padding:5px 6px; cursor: pointer" tagid="' . $tagids . '" typevalue="' . $ttvalue . '" pagerv="' . $i . '" search="0" sort="0">' . $i . '</li>';
            }
        } else {
            for ($i = ($totalpages - 5); $i <= $totalpages; $i++) {
                $output .= '<li class="pg-btn-tags ' . ($page == $i ? 'active' : '') . '" style="padding:5px 6px; cursor: pointer" tagid="' . $tagids . '" typevalue="' . $ttvalue . '" pagerv="' . $i . '" search="0" sort="0">' . $i . '</li>';
            }
        }

        // for ($i= max(1, $page ); $i <= min($page + 5, $totalpages); $i++)
        // {
        // 	$output .='<li class="pg-btn-tags '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" tagid="'.$tagids.'" typevalue="'.$ttvalue.'" pagerv="'.$i.'" search="0" sort="0">'.$i.'</li>';
        // }

        if ($page < $totalpages) {
            $output .= '<li><div class="next-btn-tags" tagid="' . $tagids . '" type="' . $ttvalue . '" pager="' . ($page + 1) . '" search="0" sort="0">
				<div style="float:left;margin-right: 5px;margin-left: 22px;margin-top: 4px; cursor: pointer;"><span class="hidden-xs">Next</span></div>
				<div style="float:left;margin-top: 10px;  cursor: pointer;"><span class="btn-next"></span></div>
			</div></li>';
        }
        if ($totalpages > 1) {
            $output .= '</ul>
			<p>
				<span>of&nbsp;</span>
				<span class="ng-binding">' . $totalpages . '</span>
				<span>&nbsp;pages</span>
			</p>
	</nav>';
        } else {
            $output .= '</ul>
			<p>
				<span>of&nbsp;</span>
				<span class="ng-binding">' . $totalpages . '</span>
				<span>&nbsp;page</span>
			</p>
	</nav>';
        }
    } else {

        if ($totalpages == 1) {
            $output .= '<li class="active" style="padding:5px 6px; cursor: pointer"  totalpages="' . $totalpages . '"  resourcetypes="' . $resourcetypes . '" lifestage="' . $lifestage . '" queryvalue="' . $searchvalue . '" pagerv="1"  sort="' . $sort . '">1</li>';
        } else if ($totalpages <= 6) {
            for ($i = 1; $i <= $totalpages; $i++) {
                $output .= '<li class="pg-btn-tags ' . ($page == $i ? 'active' : '') . '" style="padding:5px 6px; cursor: pointer" tagid="' . $tagids . '" typevalue="' . $ttvalue . '" pagerv="' . $i . '" search="0" sort="0">' . $i . '</li>';
            }
        } else if ((6 + $page - 1) < $totalpages) {
            for ($i = (1 + $page - 1); $i <= (6 + $page - 1); $i++) {
                $output .= '<li class="pg-btn-tags ' . ($page == $i ? 'active' : '') . '" style="padding:5px 6px; cursor: pointer" tagid="' . $tagids . '" typevalue="' . $ttvalue . '" pagerv="' . $i . '" search="0" sort="0">' . $i . '</li>';
            }
        } else {
            for ($i = ($totalpages - 5); $i <= $totalpages; $i++) {
                $output .= '<li class="pg-btn-tags ' . ($page == $i ? 'active' : '') . '" style="padding:5px 6px; cursor: pointer" tagid="' . $tagids . '" typevalue="' . $ttvalue . '" pagerv="' . $i . '" search="0" sort="0">' . $i . '</li>';
            }
        }

        // for ($i= max(1, $page ); $i <= min($page + 5, $totalpages); $i++) {
        // 	$output .='<li class="pg-btn-tags '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" tagid="'.$tagids.'" typevalue="'.$ttvalue.'" pagerv="'.$i.'" search="0" sort="0">'.$i.'</li>';
        // }

        if ($page < $totalpages) {
            $output .= '<li><div class="next-btn-tags" tagid="' . $tagids . '" type="' . $ttvalue . '" pager="' . ($page + 1) . '" search="0" sort="0">
				<div style="float:left;margin-right: 5px;margin-left: 22px;margin-top: 4px; cursor: pointer;"><span class="hidden-xs">Next</span></div>
				<div style="float:left;margin-top: 10px;  cursor: pointer;"><span class="btn-next"></span></div>
			</div></li>';
        }
        if ($totalpages > 1) {
            $output .= '</ul>
			<p>
				<span>of&nbsp;</span>
				<span class="ng-binding">' . $totalpages . '</span>
				<span>&nbsp;pages</span>
			</p>
	</nav>';
        } else {
            $output .= '</ul>
			<p>
				<span>of&nbsp;</span>
				<span class="ng-binding">' . $totalpages . '</span>
				<span>&nbsp;page</span>
			</p>
	</nav>';
        }
    }
    $return_arr['message'] = $output;
    echo json_encode($return_arr);
?>