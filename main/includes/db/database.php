<?php
$balKeyIv 	    = '../../../../../../../usr/share/balancepro/bal_key_iv.txt'; // /usr/share/balancepro/
$balEnv         = '../../../../../balancepro/bal_env.txt'; // /var/www/ and bal_env_backup
require_once('common.php');
error_reporting(1);

//include('../../wp-config.php');
// $db_host		= DB_HOST;
// $db_user		= DB_USER;
// $db_pass		= DB_PASSWORD;
// $db_database	= DB_NAME;
// $db_encoding    = DB_CHARSET; 
/* End config */

// $db_host		= 'localhost';
// $db_user		= 'root';
// $db_pass		= '';
// $db_database	= 'dev_balancepro';
// $db_encoding    = 'utf8'; 

$db = new PDO('mysql:host='.$db_host.';dbname='.$db_database, $db_user, $db_pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES ".$db_encoding));
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);



// =============== old code
/*
error_reporting(0);

$db_host		= 'mysql-balancepro-20191213.cjf7xkcjtoge.us-west-2.rds.amazonaws.com';
$db_user		= 'balancepro';
$db_pass		= '"V{4PLmv6X@]$euxE;T8`';
$db_database	= 'wp_balance_20200201';
$db_encoding    = 'utf8'; 


$db = new PDO('mysql:host='.$db_host.';dbname='.$db_database, $db_user, $db_pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES ".$db_encoding));
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//$path = 'http://35.168.237.147/';

$path = "https://balancepro.org/wordpress/";
*/

function mainLifeStages($lifestage,$tags='',$dbss=''){
    global $db;
		$lsWhere  = '';
		if($lifestage == '0' OR $lifestage == 0 OR $lifestage == ''){
			$lifestage = '';
		}
	 if($lifestage != ''){
        $lifestage = getSubLifeStages($lifestage);
    $lsSelectSql =" SELECT  GROUP_CONCAT(ag.`post_type`) AS post_type,GROUP_CONCAT(ag.`term__in`) AS term__in,GROUP_CONCAT(ag.`term__not_in`) AS term__not_in,
    GROUP_CONCAT(ag.`post__in`) AS post__in,GROUP_CONCAT(ag.`post__not_in`) AS post__not_in FROM `wp_postmeta` wpm INNER JOIN `wp_access_groups` ag WHERE wpm.`post_id` in ({$lifestage}) AND wpm.`meta_key`='_page_edit_data' 
        AND FIND_IN_SET(ag.`wp_post_id`,(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( wpm.`meta_value`, '\"accessgroups\"' , -1), '\";}}s:',1),'\"',-1))) ";

            $lsResult = $db->prepare($lsSelectSql);
            $lsResult->execute();
            $lsRow =$lsResult->fetch(PDO::FETCH_ASSOC);
            if($lsRow){
                
                $post_type = "'".str_replace(",", "','",delComma($lsRow['post_type'])). "'";
                $term__in = delComma($lsRow['term__in']);
                $term__in = ($term__in)?$term__in:0;
                $term__not_in = delComma($lsRow['term__not_in']);
                $term__not_in = ($term__not_in)?$term__not_in:0;
                $post__in = delComma($lsRow['post__in']);
                $post__in = ($post__in)?$post__in:0;
                $post__not_in = delComma($lsRow['post__not_in']);
                $post__not_in = ($post__not_in)?$post__not_in:0;

                $lsWhere .= " AND (";
                $lsWhere .= " (P.post_type in ({$post_type}) ";
                $lsWhere .= "  OR P.ID in (SELECT object_id FROM `wp_term_relationships` WHERE `term_taxonomy_id` in ({$term__in})) ";
                $lsWhere .= " OR P.ID in ({$post__in}) )  ";

                $lsWhere .= " AND (P.ID not in (SELECT object_id FROM `wp_term_relationships` WHERE `term_taxonomy_id` in ({$term__not_in})) ";
                $lsWhere .= " and P.ID not in ({$post__not_in})) ";
                $lsWhere .= " )";

            }
        }
    return $lsWhere;
}

function getParentLifeStages(){
    global $db;
    $lsSelectSqlParent = "SELECT  GROUP_CONCAT(P.ID) AS id FROM wp_posts AS P WHERE P.post_type = 'life_stage' AND P.post_status = 'publish' AND P.`post_parent` = 0 ";

    $lsResultParent = $db->prepare($lsSelectSqlParent);
    $lsResultParent->execute();
    $lsRowParent =$lsResultParent->fetch(PDO::FETCH_ASSOC);
    //$lifestage = '';
    if($lsRowParent){
        $lifestage .=  ','.$lsRowParent['id'];
    }
    $lifestage = trim($lifestage,',');
    return $lifestage;
}

function getSubLifeStages($lifestage){
    global $db;
    $lsSelectSqlSub = "SELECT  GROUP_CONCAT(P.ID) AS id FROM wp_posts AS P WHERE P.post_type = 'life_stage' AND P.post_status = 'publish' AND P.`post_parent` in ($lifestage) ";
    $lsResultSub = $db->prepare($lsSelectSqlSub);
    $lsResultSub->execute();
    $lsRowSub =$lsResultSub->fetch(PDO::FETCH_ASSOC);
    if($lsRowSub){
        $lifestage .=  ','.$lsRowSub['id'];
    }
    $lifestage = trim($lifestage,',');

    return $lifestage;
}

function delComma($strVal){
    $string = preg_replace("/,+/", ",", $strVal);
    return trim($string,',');
}
?>
