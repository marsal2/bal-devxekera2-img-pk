<?php

if($fileKeyIv = file($balKeyIv)){
	$fileKey = isset($fileKeyIv[0])?trim(substr($fileKeyIv[0], 0, strpos($fileKeyIv[0], '//'))):'';
	$fileIv = isset($fileKeyIv[1])?trim(substr($fileKeyIv[1], 0, strpos($fileKeyIv[1], '//'))):'';
}

$ifgetkeyiv = 1;
if(isset($_GET['encryptionkey'])){
	$key = $_GET['encryptionkey'];
	$ifgetkeyiv= 0;
}else{
	$key = trim($fileKey);
}

if(isset($_GET['encryptioniv'])){
	$iv = $_GET['encryptioniv'];
	$ifgetkeyiv= 0;
}else{
	$iv = trim($fileIv);
}


if(empty($key) || empty($iv)){
	echo "Encryption Key and IV not defined, Kindly contact with administrator.";
	exit;
}

// AES Encryption with AES-128-CTR algorithm string
function aesEcrypt($value,$key='',$iv='',$type='string'){
	$ciphering = "AES-128-CTR"; 
	$options = 0;
	if($type=='array'){
		$jsonFileValue = json_encode($value);
		$value = base64_encode($jsonFileValue);
	}
 	$result = openssl_encrypt($value, $ciphering,$key, $options, $iv);
	return $result;		
}

// AES Decryption with AES-128-CTR algorithm string
function aesDecrypt($value,$key='',$iv='',$type='string'){
	$ciphering = "AES-128-CTR"; 
	$options = 0;
	$result=openssl_decrypt($value, $ciphering,$key, $options, $iv);
	if($type=='array'){
		$base64fileValue = base64_decode($result);
		$result = json_decode($base64fileValue);
	}
	return $result;		
}

function fileContentGetPut($rename_file,$old_value,$new_value){
	file_put_contents($rename_file,file_get_contents($old_value));
	file_put_contents($old_value,$new_value);
	return true;
}

if($ifgetkeyiv){
	$balEnvFile = file($balEnv);
	if(isset($balEnvFile[0])){
		$aesDecrypt = aesDecrypt($balEnvFile[0],$key,$iv,$type='array');
		foreach ($aesDecrypt as $key => $aesDec) {
		$aesDecryptData = explode(':::',$aesDec);
			$_ENV[$aesDecryptData[0]]=trim($aesDecryptData[1]);
		}
		// print_r($ENV);
		// exit;
	}
}

?>