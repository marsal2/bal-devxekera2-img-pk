#Balance Devxekera Site
App utilizes Wordpress and core PHP functionality where core PHP functionality used for RESOURCES filteration.

## Setup
Make the `public_html` directory web accessible.
Create includes/config.php file in the root of the install directory and fill in your values, you can use and replace it as needed

## .config.php
```
  define('WP_DEBUG', true);
  define('WP_DEBUG_LOG', false);
  define('WP_DEBUG_DISPLAY', false);
  define('THEME_VERSION', '1.10.0');
  define('DISABLE_WP_CRON', true);
  define('COPY_TO_MS_SQL', true);
  define('TRANSFER_QUIZ_RESULTS_TO_MSSQL', true);
  define('MS_PROTOCOL', 'dblib');
  define('MS_DB_HOST', '231.6.90');
  define('MS_DB_DATABASE','PersonalFinance');
  define('MS_DB_USERNAME', 'roidna');
  define('MS_DB_PASSWORD', '__GET_IT_FROM_PASSWORD_MANAGER__');
```

### LIVE SERVER DEPLOYMENT

In `/var/www/devxekera` directory you as your user do the following:
- `git pull` to get the latest version from github
- `git checkout TAG` to set the latest TAG
Then from anywhere you can do the following to deploy the latest updates  
- `deploy` to start gulp build with user `app` in `/var/www/devxekera`  
