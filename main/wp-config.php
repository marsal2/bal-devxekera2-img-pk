<?php

if(strstr($_SERVER['REQUEST_URI'],'wp-admin') && !strstr($_SERVER['REQUEST_URI'],'wp-login')){
	 $balKeyIv 	    = '../../../../../../usr/share/balancepro/bal_key_iv.txt'; // /usr/share/balancepro/
 	 $balEnv         = '../../../../balancepro/bal_env.txt'; // /var/www/ and bal_env_backup
}else{
 	 $balKeyIv 	    = '../../../../../usr/share/balancepro/bal_key_iv.txt'; // /usr/share/balancepro/
 	 $balEnv         = '../../../balancepro/bal_env.txt'; // /var/www/ and bal_env_backup
}

require_once('includes/db/common.php');
// error_reporting(0);
// @ini_set('display_errors', 0);
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
//    error_reporting(0);
//    @ini_set('display_errors', 0);
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define( 'DB_NAME', 'wp_balance_20200201' );
/** MySQL database username */
//define( 'DB_USER', 'balancepro' );
/** MySQL database password */
//define( 'DB_PASSWORD', '"V{4PLmv6X@]$euxE;T8`' );
/** MySQL hostname */
//define( 'DB_HOST', 'mysql-balancepro-20191213.cjf7xkcjtoge.us-west-2.rds.amazonaws.com' );
 
/** The name of the database for WordPress */
define( 'DB_NAME', $db_database );
/** MySQL database username */
define( 'DB_USER', $db_user );
/** MySQL database password */
define( 'DB_PASSWORD', $db_pass );
/** MySQL hostname */
define( 'DB_HOST', $db_host );
/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', $db_encoding );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define( 'AUTH_KEY',          $auth_key );
define( 'SECURE_AUTH_KEY',   $secure_auth_key );
define( 'LOGGED_IN_KEY',     $logged_in_key );
define( 'NONCE_KEY',         $nonce_key );
define( 'AUTH_SALT',         $auth_salt );
define( 'SECURE_AUTH_SALT',  $secure_auth_salt );
define( 'LOGGED_IN_SALT',    $logged_in_salt );
define( 'NONCE_SALT',        $nonce_salt );
define( 'WP_CACHE_KEY_SALT', $wp_cache_key_salt );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

define( 'DBI_AWS_ACCESS_KEY_ID', $dbi_aws_access_key_id );
define( 'DBI_AWS_SECRET_ACCESS_KEY', $dbi_aws_secret_key );

define('WP_DEBUG', false);
define('WP_DEBUG_LOG', false);
define('WP_DEBUG_DISPLAY', false);
define('THEME_VERSION', '1.10.0');
define('DISABLE_WP_CRON', true);
define('COPY_TO_MS_SQL', true);
define('TRANSFER_QUIZ_RESULTS_TO_MSSQL', true);

define('MS_PROTOCOL', 'dblib');
//define('MS_DB_HOST', '4.71.115.149');
define('MS_DB_HOST', $ms_db_host);
define('MS_DB_DATABASE',$ms_db_database);
define('MS_DB_USERNAME', $ms_db_user);
define('MS_DB_PASSWORD', $ms_db_pass);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

//define('BASE_SIT_URL','http://bal-devxekera2-img-pk.test');
define('BASE_SIT_URL',site_url());
define('SITE_KEY',$site_key);
