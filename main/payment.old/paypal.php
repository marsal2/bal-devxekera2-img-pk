<?php

require_once('email.php');

// Suppress DateTime warnings, if not set already
date_default_timezone_set(@date_default_timezone_get());
// Adding Error Reporting for understanding errors properly
error_reporting(E_ALL);
ini_set('display_errors', '1');

$WAS_ERROR = false;
$WAS_ERROR_ADMIN = false;
$SHOW_ERROR = '';

function wasError() {
  global $WAS_ERROR;
  return $WAS_ERROR;
}

// parsePayflowString: Parses a response string from Payflow and returns an
// associative array of response parameters.
function parsePayflowString($str) {
    $workstr = $str;
    $out = array();

    while(strlen($workstr) > 0) {
        $loc = strpos($workstr, '=');
        if($loc === FALSE) {
            // Truncate the rest of the string, it's not valid
            $workstr = "";
            continue;
        }

        $substr = substr($workstr, 0, $loc);
        $workstr = substr($workstr, $loc + 1); // "+1" because we need to get rid of the "="

        if(preg_match('/^(\w+)\[(\d+)]$/', $substr, $matches)) {
            // This one has a length tag with it.  Read the number of characters
            // specified by $matches[2].
            $count = intval($matches[2]);

            $out[$matches[1]] = substr($workstr, 0, $count);
            $workstr = substr($workstr, $count + 1); // "+1" because we need to get rid of the "&"
        } else {
            // Read up to the next "&"
            $count = strpos($workstr, '&');
            if($count === FALSE) { // No more "&"'s, read up to the end of the string
                $out[$substr] = $workstr;
                $workstr = "";
            } else {
                $out[$substr] = substr($workstr, 0, $count);
                $workstr = substr($workstr, $count + 1); // "+1" because we need to get rid of the "&"
            }
        }
    }

    return $out;
}

// runPayflowCall: Runs a Payflow API call.  $params is an associative array of
// Payflow API parameters.  Returns FALSE on failure, or an associative array of response
// parameters on success.
function runPayflowCall($params) {
    $paypalconf = include('paypal.conf.php');

    $params['PARTNER'] = $paypalconf['partner'];
    $params['VENDOR'] = $paypalconf['vendor'];
    $params['USER'] = $paypalconf['user'];
    $params['PWD'] = $paypalconf['password'];
    $params['TRXTYPE'] = $paypalconf['transaction_type'];
    $params['CURRENCY'] = $paypalconf['currency'];
    $params['CREATESECURETOKEN'] = 'Y';
    $params['SECURETOKENID'] = uniqid('MySecTokenID-'); //Should be unique; never used before
    $params['RETURNURL'] = $paypalconf['return_url'];
    $params['CANCELURL'] = $paypalconf['cancel_url'];
    $params['ERRORURL'] = $paypalconf['error_url'];

    $paramList = array();
    foreach($params as $index => $value) {
        $paramList[] = $index . "[" . strlen($value) . "]=" . $value;
    }

    $apiStr = implode("&", $paramList);

    // Initialize our cURL handle.
    $curl = curl_init($paypalconf['endpoint']);

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

    // If you get connection errors, it may be necessary to uncomment
    // the following two lines:
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

    curl_setopt($curl, CURLOPT_POST, TRUE);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $apiStr);

    $result = curl_exec($curl);
    if($result === FALSE) {
      echo curl_error($curl);
      return FALSE;
    }
    else return parsePayflowString($result);
}

// Using this method to call others
// as same err handling would otherwise
// need to be done
function doPaypal($method, $params) {
  global $WAS_ERROR;
  global $WAS_ERROR_ADMIN;
  try {
    return call_user_func($method, $params);
  } catch ( Exception $e ) {
    setError($e);
  }
}

function setError($e, $use_message=false) {
  global $WAS_ERROR;
  global $WAS_ERROR_ADMIN;

  if ( empty( $e ) ) {
    $e = new Exception();
  }

  $trace = explode("\n", $e->getTraceAsString());
  // reverse array to make steps line up chronologically
  $trace = array_reverse($trace);
  array_shift($trace); // remove {main}
  array_pop($trace); // remove call to this method
  $length = count($trace);
  $result = array();

  for ($i = 0; $i < $length; $i++) {
    $result[] = ($i + 1)  . ')' . substr($trace[$i], strpos($trace[$i], ' ')); // replace '#someNum' with '$i)', set the right ordering
  }

  $trace_output = "<br>Stack Trace:<br>" . implode("<br>", $result);

  if(empty($use_message) === false) {
    $WAS_ERROR = $use_message;
    $WAS_ERROR_ADMIN = $use_message . $trace_output;
    return;
  }

  $code = $e->getCode();
  $data = $e->getData();
  $message = $e->getMessage();
  $WAS_ERROR = "$code";
  $WAS_ERROR_ADMIN = "$code $data $message . $trace_output";

  $config = include('config.php');
  sendEmail(
    $config['PAYMENT_FAILURES_RECIPIENTS'],
    'There was an error with the payment process (Paypal)',
    getCcFailureEmail()
  );
}

function renderError($use_message=false) {
  global $SHOW_ERROR;
  $SHOW_ERROR = wasError();
  if(empty($use_message) === false) {
    $SHOW_ERROR = $use_message;
  }
  include('error.php');
  exit();
}

function getCcFailureEmail() {
  global $WAS_ERROR_ADMIN;
  ob_start();
  require_once('email.error.admin.php');
  return ob_get_clean();
}
