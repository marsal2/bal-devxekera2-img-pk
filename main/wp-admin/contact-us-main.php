

<style type="text/css">
    .form-table th, .form-table td { padding: 10px 10px; border:1px solid #e7e7e7; }
    .form-table .no-border th, .form-table .no-border td { border:0px !important }
    input.toggleCheck {
        padding:14px 27px !important;
        -webkit-appearance: none !important;
        appearance: none !important;
        border-radius: 16px !important;
        background: radial-gradient(circle 12px, white 100%, transparent calc(100% + 1px)) #ccc -14px !important;
        transition: 0.3s ease-in-out !important;
    }

    input.toggleCheck:checked {
        background-color: dodgerBlue !important;
        background-position: 14px !important;
    }
</style>
<div class="wrap">
    <div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">
        <div class="inside">
            <h2> <?php  echo __( 'Contact Us', 'balance_theme' )  ?> </h2>
            <form class="form-table" method="post" action="options.php">
                <?php settings_fields( 'contactus-settings-group' ); ?>

                <p><label><b><?php echo __( 'Do you want to enable Contact page', 'balance_theme' ); ?></b></label><br>

                    <?php echo checkbox_field( get_option('main_contact_us_page'), 'main_contact_us_page',false,'toggleCheck'); ?>
                </p>

                <p><label><b><?php echo __( 'Do you want to enable Contact form', 'balance_theme' ); ?></b></label><br>

                    <?php echo checkbox_field( get_option('main_contact_us_form') , 'main_contact_us_form',false,'toggleCheck'); ?>
                </p>

                <p><label><b><?php echo __( 'Do you want to enable Contact Number', 'balance_theme' ); ?></b></label><br>

                    <?php echo checkbox_field( get_option('main_contact_us_number') , 'main_contact_us_number',false,'toggleCheck'); ?>
                </p>


                <?php submit_button(); ?>
            </form>

        </div>
    </div>
</div>

<link rel='stylesheet' href='<?php echo get_template_directory_uri() ?>/css/admin/jquery.timepicker.min.css' type="text/css"  />
