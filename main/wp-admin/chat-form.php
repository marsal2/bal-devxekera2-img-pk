

<style type="text/css">
.form-table th, .form-table td { padding: 10px 10px; border:1px solid #e7e7e7; }
.form-table .no-border th, .form-table .no-border td { border:0px !important }
  input.toggleCheck { 
  	padding:14px 27px !important;
  	-webkit-appearance: none !important;
    appearance: none !important;
    border-radius: 16px !important;
    background: radial-gradient(circle 12px, white 100%, transparent calc(100% + 1px)) #ccc -14px !important;
    transition: 0.3s ease-in-out !important;
  }

  input.toggleCheck:checked {
    background-color: dodgerBlue !important;
    background-position: 14px !important;
  }
</style>
<div class="wrap">
	<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">
		<div class="inside">
		<h2> <?php  echo __( 'Chat', 'balance_theme' )  ?> </h2>
		<form class="form-table" method="post" action="options.php">
			<?php settings_fields( 'chat-settings-group' ); ?>
			
<!--                <p><label><b>--><?php //echo __( 'Enable disable contact number and contact form :', 'balance_theme' ); ?><!--</b></label><br>-->
<!--			-->
<!--					--><?php //echo checkbox_field( get_option('main_chat_contact_enable_disable'), 'main_chat_contact_enable_disable',false,'toggleCheck'); ?>
<!--				</p>-->

				<p><label><b><?php echo __( 'Dynamic chat enable disable:', 'balance_theme' ); ?></b></label><br>
					
						<?php echo checkbox_field( get_option('main_chat_enable_disable') , 'main_chat_enable_disable',false,'toggleCheck'); ?>
				</p>
				<br>
			<table class="form-table">
				<tr valign="top">
					<th scope="row">
						<?php echo __( 'Days', 'balance_theme' ); ?>
					</th>
					<th scope="row">
						<?php echo __( 'ON/OFF', 'balance' ); ?>
					</th>
					<th scope="row">
						 <?php echo __( 'Start Time', 'balance_theme' ); ?>
					</th>
					<th scope="row">
						<?php echo __( 'End Time', 'balance_theme' ); ?>
					</th>
				</tr>
				
				<tr valign="top">
					<th scope="row">
						<?php echo __( 'Sunday', 'balance_theme' ); ?>
					</th>
					<th scope="row">
						<?php echo checkbox_field( get_option('main_chat_checkbox_sun'), 'main_chat_checkbox_sun',false,'toggleCheck','sun'); ?>
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_sun_start'), 'main_chat_sun_start','','main_chat_sun_start'); ?>
						
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_sun_end'), 'main_chat_sun_end','','main_chat_sun_end'); ?>
					</th>
				</tr>
				
				<tr valign="top">
					<th scope="row">
						<?php echo __( 'Monday', 'balance_theme' ); ?>
					</th>
					<th scope="row">
						<?php echo checkbox_field( get_option('main_chat_checkbox_mon'), 'main_chat_checkbox_mon',false,'toggleCheck','sun'); ?>
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_mon_start'), 'main_chat_mon_start','','main_chat_mon_start'); ?>
						
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_mon_end'), 'main_chat_mon_end','','main_chat_mon_end'); ?>
					</th>
					
				</tr>

				<tr valign="top">
					<th scope="row">
						<?php echo __( 'Tuesday', 'balance_theme' ); ?>
					</th>

					<th scope="row">
						<?php echo checkbox_field( get_option('main_chat_checkbox_tue'), 'main_chat_checkbox_tue',false,'toggleCheck','sun'); ?>
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_tue_start'), 'main_chat_tue_start','','main_chat_tue_start'); ?>
						
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_tue_end'), 'main_chat_tue_end','','main_chat_tue_end'); ?>
					</th>
					
				</tr>
				<tr valign="top">
					<th scope="row">
						<?php echo __( 'Wednesday', 'balance_theme' ); ?>
					</th>

					<th scope="row">
						<?php echo checkbox_field( get_option('main_chat_checkbox_wed'), 'main_chat_checkbox_wed',false,'toggleCheck','sun'); ?>
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_wed_start'), 'main_chat_wed_start','','main_chat_wed_start'); ?>
						
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_wed_end'), 'main_chat_wed_end','','main_chat_wed_end'); ?>
					</th>
					
				</tr>
				<tr valign="top">
					<th scope="row">
						<?php echo __( 'Thursday', 'balance_theme' ); ?>
					</th>
					<th scope="row">
						<?php echo checkbox_field(get_option('main_chat_checkbox_thu') , 'main_chat_checkbox_thu',false,'toggleCheck','sun'); ?>
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_thu_start'), 'main_chat_thu_start','','main_chat_thu_start'); ?>
						
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_thu_end'), 'main_chat_thu_end','','main_chat_thu_end'); ?>
					</th>
					
				</tr>
				<tr valign="top">
					<th scope="row">
						<?php echo __( 'Friday', 'balance_theme' ); ?>
					</th>
					<th scope="row">
						<?php echo checkbox_field(get_option('main_chat_checkbox_fri') , 'main_chat_checkbox_fri',false,'toggleCheck','sun'); ?>
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_fri_start'), 'main_chat_fri_start','','main_chat_fri_start'); ?>
						
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_fri_end'), 'main_chat_fri_end','','main_chat_fri_end'); ?>
					</th>

				</tr>
				<tr valign="top">
					<th scope="row">
						<?php echo __( 'Saturday', 'balance_theme' ); ?>
					</th>
					<th scope="row">
						<?php echo checkbox_field( get_option('main_chat_checkbox_sat') , 'main_chat_checkbox_sat',false,'toggleCheck','sun'); ?>
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_sat_start'), 'main_chat_sat_start','','main_chat_sat_start'); ?>
						
					</th>
					<th scope="row">
						<?php echo text_field( get_option('main_chat_sat_end'), 'main_chat_sat_end','','main_chat_sat_end'); ?>
					</th>

					
				</tr>
			</table>

			<br>
				<!-- Title tooltip This text will appear as hover on Chat menu -->
				<p><label><b>
						<?php echo __( 'Title tooltip :', 'balance_theme' ); ?>
					</b></label><br>
						<?php echo text_field( get_option('main_chat_title'), 'main_chat_title','','main_chat_title'); ?>
			    </p>

				<p><label><b>
						<?php echo __( 'Description :', 'balance_theme' ); ?>
					</b></label><br>
						<?php echo textarea_field( get_option('main_chat_description'), 'main_chat_description', false, 5,'<p>BALANCE Pro chat is currently unavailable. Our normal business hours are in (UTC-08:00) Pacific Time (US),<br> %timesettings%</p><p> You may call us at 1-888-456-2227 during normal business hours.</p>' ); ?>
					</p>


			<?php submit_button(); ?>
		</form>
	</div>
	</div>
</div>

 <script src="<?php echo get_template_directory_uri() ?>/js/admin/jquery.timepicker.min.js"></script>
<link rel='stylesheet' href='<?php echo get_template_directory_uri() ?>/css/admin/jquery.timepicker.min.css' type="text/css"  />
<script type="text/javascript">
	jQuery(document).ready(function(){
	jQuery('.main_chat_sun_start,.main_chat_sun_end').timepicker();
    jQuery('.main_chat_mon_start,.main_chat_mon_end').timepicker();
    jQuery('.main_chat_tue_start,.main_chat_tue_end').timepicker();
    jQuery('.main_chat_wed_start,.main_chat_wed_end').timepicker();
    jQuery('.main_chat_thu_start,.main_chat_thu_end').timepicker();
    jQuery('.main_chat_fri_start,.main_chat_fri_end').timepicker();
    jQuery('.main_chat_sat_start,.main_chat_sat_end').timepicker();
});
</script>