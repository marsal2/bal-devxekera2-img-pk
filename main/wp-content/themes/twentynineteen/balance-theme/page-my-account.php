<?php
/*
My Account page
*/
?>
<?php
global $additional_body_class, $my_account_action;
$additional_body_class = 'woocommerce page-my-account';

// get header based on user type
$plain = getHeaderType();
get_header( $plain );

$user_id = get_current_user_id();
$logged_in_account_actions = array( 'view-account-details', 'view-billing-address', 'view-my-orders' );
$not_logged_in_account_actions = array( 'reset-password', 'lost-password', 'login', 'register', 'verify-account' );
$account_actions = array_merge( $logged_in_account_actions, $not_logged_in_account_actions );
$my_account_action = !empty( $_GET['action'] ) && in_array( $_GET['action'], $account_actions ) ? $_GET['action'] : 'login';
$my_account_page_permalink = get_the_permalink ( get_option('woocommerce_myaccount_page_id') );
if ( $user_id == 0 && in_array( $my_account_action, $logged_in_account_actions ) ) {
	echo '<script>window.location="' . $my_account_page_permalink . '?action=login";</script>';
	exit();
} else if ( $user_id != 0 && $my_account_action != 'verify-account' &&  !in_array( $my_account_action, $logged_in_account_actions ) ) {
	echo '<script>window.location="' . $my_account_page_permalink . '?action=view-account-details";</script>';
	exit();
}
?>
<section aria-label="<?=$my_account_action?>" class="container background-white">
  <div class="row">
          <div class="main-tab tab-holder col-sm-12">
      <?php
switch ( $my_account_action ) {
	case 'reset-password':
		include 'woocommerce/my-account/reset-password.php';
		break;
	case 'lost-password':
		include 'woocommerce/my-account/lost-password.php';
		break;
	case 'verify-account':
		include 'woocommerce/my-account/verified-account.php';
		break;
	case 'login':
	case 'register':
		include 'woocommerce/my-account/login-register.php';
		break;
	case 'view-my-orders':
	case 'view-billing-address':
	default:
		include 'woocommerce/my-account/view-account-details.php';
		break;
}
?>
    </div>
  </div>
</section>
<?php
// strip footer markup from theme to display in iframe
get_footer( $plain );
