<?php
/*
Modules: {"m1[0]":{"name":"M1: Hero"},"m34[0]":{"name":"M34: Section - title, copy"}}
*/
?>
<?php
global $additional_body_class, $data;
$additional_body_class = 'woocommerce page-shop';
$data = get_custom_data( get_page_by_path( 'shop' )->ID );

// get header based on user type
$plain = getHeaderType();
get_header( $plain );

/* Hero */
if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0], 'medium' );
}

/* SECTION - TITLE, COPY */
if (!empty($data['m34_module']) && !empty($data['m34_module'][0])) {
    echo render_m34_section_title_copy($data['m34_module'][0]);
}
?>
<article class="text-block article default-content-style" aria-label="article archive product">
	<div class="container">
		<div class="row">
			<?php //var_dump( wp_get_current_user() ); ?>
			<?php do_action( 'woocommerce_archive_description' );	?>
			<?php if ( have_posts() ) : ?>
				<?php do_action( 'woocommerce_before_shop_loop' ); ?>
				<?php woocommerce_product_loop_start(); ?>
					<?php woocommerce_product_subcategories(); ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php wc_get_template_part( 'content', 'product' ); ?>
					<?php endwhile; // end of the loop. ?>
				<?php woocommerce_product_loop_end(); ?>
				<?php do_action( 'woocommerce_after_shop_loop' ); ?>
			<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
				<?php wc_get_template( 'loop/no-products-found.php' ); ?>
			<?php endif; ?>
			<?php do_action( 'woocommerce_sidebar' );	?>
		</div>
	</div>
</article>
<?php
get_footer( $plain );
