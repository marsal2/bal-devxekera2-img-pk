<?php
/*
Modules: {"m34[0]":{"name":"Copy"},"calculator[0]":{"name":"Settings"}}
*/
?>
<?php
global $additional_body_class, $post, $data, $plain_html;
Balance_Resources_Public::increment_resource_view_count( $post->ID, 1, 0 );
$additional_body_class = 'single-calculator';
get_custom_data();

if ( !empty( $_GET['plainhtml'] ) && $_GET['plainhtml'] == 'true' ) {
	get_header( 'plain' );
} else {
	get_header();
}

get_template_part( 'partials/calculator' );

if ( !empty( $_GET['plainhtml'] ) && $_GET['plainhtml'] == 'true' ) {
	get_footer( 'plain' );
} else {
	echo render_m23a_may_also_like_resources();
	get_footer();
}
