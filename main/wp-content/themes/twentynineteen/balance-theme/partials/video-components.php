<?php
global $data;
/* M34 renderer */
if ( !empty( $data['m34_module'] ) && !empty( $data['m34_module'][0] ) ) {
	echo render_m34_section_title_copy( $data['m34_module'][0] );
}

if ( !empty( $data['m12_module'] ) && !empty( $data['m12_module'][0] ) ){
  echo render_m12_video_overlay( $data['m12_module'][0] );
}
