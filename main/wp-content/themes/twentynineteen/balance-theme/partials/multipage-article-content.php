<?php
global $data, $post, $article_page, $total_article_pages, $quiz_data;

echo '<div id="intro" class="intro-block background-white">';

if ( !empty( $data['multipage_module_module'] ) && !empty( $data['multipage_module_module'][0] ) ) {
  $article_data = $data['multipage_module_module'][0];
  if ( !empty( $article_data['pages'][$article_page-1] )){

    $page = $article_data['pages'][$article_page-1];
    $wide = false;
    if ( !empty($page['side_related_resources']) && !empty($page['side_related_resources'][0]) && !empty($page['side_related_resources'][0]['link_text']) && !empty($page['side_related_resources'][0]['side_related_resources'])) {
      echo render_m68_chapter_associated_resources( $page['side_related_resources_title'], get_related_resource_info($page['side_related_resources']) );
    }else{
      $wide = true;
    }

    /* M34 intro */
    if ( !empty( $data['m34_module'] ) && !empty( $data['m34_module'][0] ) && $article_page == 1 ) {
      echo render_m34_article_content( $data['m34_module'][0], 'section-01', $wide );
    }

    /* M34 Article content */
    echo render_m34_article_content( $page, '', $wide );

    if ( !empty( $page['quiz'] ) && !empty( $page['quiz'] )) {
      echo render_article_short_quiz($page['quiz'], $page['quiz_success_message'], $page['quiz_fail_message'], $wide);
    }

  }else if($article_page == count($article_data['pages']) + 1 && !empty( $article_data['last_page_quiz'] ) ) {
    $quiz_data = get_custom_data($article_data['last_page_quiz']);
    get_template_part('partials/quiz');
  }
}
echo '</div>';
