<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.1.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/** @global WC_Checkout $checkout */

?>
<div class="woocommerce-billing-fields">
	<?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<h4><?php _e( 'Billing &amp; Shipping', 'woocommerce' ); ?></h4>

	<?php else : ?>

		<h4><?php _e( 'Billing Details', 'woocommerce' ); ?></h4>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<?php foreach ( $checkout->checkout_fields['billing'] as $key => $field ) : ?>

		<?php 
		if ( !empty($field['type']) && $field['type'] === 'state' ) {
			$states = WC()->countries->get_states( 'US' );
			?>
			<p class="form-row form-row form-row-first address-field validate-state validate-required" id="billing_state_field" data-o_class="form-row form-row form-row-last address-field validate-required validate-state">
				<label for="billing_state" class="">State <abbr class="required" title="required">*</abbr></label>
				<select name="billing_state" class="select2 form-control inner-select">
					<option value=""><?php _e( '-- Select State/County --', 'woocommerce' ); ?></option>
					<?php foreach ($states as $key => $value) { ?>
						<option value="<?php echo $key; ?>" <?php selected( !empty( $_POST['billing_state'] ) ? wc_clean( $_POST['billing_state'] ) : $key, $key, 1 ); ?>><?php echo $value; ?></option>
					<?php } ?>
				</select>
			</p>
		<?php } else {
			woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );	
		} ?>
	<?php endforeach; ?>

	<?php do_action('woocommerce_after_checkout_billing_form', $checkout ); ?>

	<?php if ( ! is_user_logged_in() && $checkout->enable_signup ) : ?>

		<?php if ( $checkout->enable_guest_checkout ) : ?>

			<p class="form-row form-row-wide create-account">
				<input class="input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true) ?> type="checkbox" name="createaccount" value="1" /> <label for="createaccount" class="checkbox"><?php _e( 'Create an account?', 'woocommerce' ); ?></label>
			</p>

		<?php endif; ?>

		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

		<?php if ( ! empty( $checkout->checkout_fields['account'] ) ) : ?>

			<div class="create-account">

				<p><?php _e( 'Create an account by entering the information below. If you are a returning customer please login at the top of the page.', 'woocommerce' ); ?></p>

				<?php foreach ( $checkout->checkout_fields['account'] as $key => $field ) : ?>

					<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

				<?php endforeach; ?>

				<div class="clear"></div>

			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>

	<?php endif; ?>
</div>
