<?php

function render_m41b_left_right_image( $module_data ) {
	$output = '';

	if ( empty( $module_data['sections'][0]['title'] ) && empty( $module_data['sections'][0]['copy'] ) && empty( $module_data['sections'][0]['uploadimage']['image']['filename'] ) && !is_linkable( $module_data['sections'][0]['linkfield'] ) ) {
		return '';
	}

	foreach ( $module_data['sections'] as $section ) {
        $alt = !empty($section['title']) ? $section['title'] : 'Report image';
		if ( !empty( $section['title'] ) || !empty( $section['copy'] ) || !empty( $section['uploadimage']['image']['filename'] ) || ( is_linkable( $section['linkfield'] && !empty( $section['buttontext'] ) ) ) ) {
			$output .=  '<div class="block report-holder background-white default-content-style">';
			$output .=    '<section aria-label="'. $section['title'] .'" class="report-block" style="min-height: 452px;">';
			$output .=      '<div class="container">';
			$output .=        '<div class="row">';
			$output .=          '<div class="img-holder'. ( $section['leftright'] == 'left' ? ' goofy' : '' ) .'">';
			$output .=           '<img alt="' . $alt . '" src="'. ( !empty( $section['image']['image']['fullpath'] ) ? $section['image']['image']['fullpath'] : get_stylesheet_directory_uri().'/images/icon-bg.jpg' )  .'" '. ( $section['leftright'] == 'right' ? ' width="695" height="410" ' : ' width="700" height="410" ' ) .' class="height-calc">';
			$output .=          '</div>';
			$output .=          '<div class="text-holder height-calc'. ( $section['leftright'] == 'left' ? ' goofy' : '' ) .'">';
			$output .=            '<div class="text">';
			$output .=              !empty( $section['title'] ) ? '<h1 class="text-info">'.$section['title'].'</h1>' : '';
			$output .=              !empty( $section['copy'] ) ? apply_filters( 'the_content', html_entity_decode( $section['copy'] ) ) : '';
			$output .=              !empty( $section['buttontext'] ) ? sprintf( build_link( $section['linkfield'], '', true ), "but btn btn-primary", $section['buttontext'] ) : '';
			$output .=            '</div>';
			$output .=          '</div>';
			$output .=        '</div>';
			$output .=      '</div>';
			$output .=    '</section>';
			$output .=  '</div>';
		}

	}

	return stripslashes( $output );

}
