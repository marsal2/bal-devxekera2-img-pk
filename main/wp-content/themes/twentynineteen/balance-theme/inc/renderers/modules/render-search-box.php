<?php 
	function search_box_form(){
		$output = '';
		$output .='<form class="sort-form">
                  <fieldset>
                    <div class="col-sm-8">
                      <div class="input-group">
                        <input type="search" aria-label="Enter Keywords field. Content section" placeholder="Enter Keywords" ng-model="filters.query" class="form-control" id="keyword-search">
                        <button type="submit" class="search-btn"><span class="icon-search"></span></button>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="input-group">
                        <select aria-label="Sort by" ng-change="apply()" ng-model="filters.sortBy" class="form-control sort-form-select">
                          <option value="" class="hide-me">' . __('Sort By', 'balance').'</option>
                          <option value="null" ng-selected="filters.sortBy==\'null\'">' . __('Relevance', 'balance').'</option>
                          <option value="-views" ng-selected="filters.sortBy==\'-views\'">' . __('Most Popular', 'balance').'</option>
                          <option value="-date" ng-selected="filters.sortBy==\'-date\'">' . __('Most Recent', 'balance').'</option>
                        </select>
                      </div>
                    </div>
                  </fieldset>
                </form>';
        return stripslashes( $output );
	}
?>