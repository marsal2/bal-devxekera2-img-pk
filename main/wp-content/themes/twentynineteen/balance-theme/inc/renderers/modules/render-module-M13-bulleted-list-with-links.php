<?php

function render_m13_bulleted_list_with_links( $module_data ) {
	$output = '';
	if ( !empty( $module_data['bullets'] ) ) {
		$output .= '<section aria-label="links list" class="list-block background-white">';
		$output .= '  <div class="container">';
		$output .= '    <div class="row">';
		$output .= '      <ul>';
		foreach ( $module_data['bullets'] as $key => $value ) {
			$output .= '        <li><span class="text">'.( !empty( $value['copy'] ) ? wpautop( $value['copy'] ) : '' ).'</span>';
			$output .= sprintf( build_link( $value['linkfield'] ), 'text-read-more text-warning', __( 'READ MORE', 'balance' ) );
			$output .= '</li>';
		}
		$output .= '      </ul>';
		$output .= '    </div>';
		$output .= '  </div>';
		$output .= '</section>';
	}

	return stripslashes( $output );
}
