<?php

function render_m81_form_select_field( $field, $form_id = '', $value = '' ) {
  $logic_event = get_field_conditional_logic( $form_id, $field, 'keyup' );
  $output_field = '';
  $output_field .=  '<select id="input_' . $form_id . '_' . $field['id'] . '" name="input_' . $field['id'] . '" ' . $logic_event . '>';
  foreach ($field['choices'] as $key => $choice) {
    $output_field .=  '<option value="' . $choice['value'] . '" ' . ( $choice['value'] == $value ? ' selected="selected" ' : '' ) . '>';
    $output_field .=      $choice['text'];
    $output_field .=  '</option>';
  }
  $output_field .=  '</select>';
  $output = '';
  $output = sprintf( get_form_field_wrap( $field, $form_id ), $output_field );

  return stripslashes( $output );
}
