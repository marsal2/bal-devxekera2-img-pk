<?php

function render_m73_m74_m75_m77_m78_form_table_field( $field, $form_id = '', $value = '' ) {

  $output = '';
  if ( !empty( $field['row_choices'] ) && !empty( $field['column_choices'] ) ) {
    $special = [];
    $table_text_fields = ['description', 'name of creditor', 'interest rate'];
    $form = GFAPI::get_form( $form_id );
    $output .= '<div class="ginput_container ginput_container_' . $field['type'] . ' input-group-frame three-cols">';
    $output .= '<input type="text" id="input_' . $form_id . '_' . $field['id'] . '_shim" style="position:absolute;left:-999em;">';
    $output .= '<table class="colored">';
    $output .= '  <thead>';
    $output .= '    <tr>';
    $output .= '      <th>&nbsp;</th>';
    foreach ( $field['column_choices'] as $column_key => $column ) {
      $output .= '      <th>' . $column['text'] . '</th>';
      // description field input can have max 34 characters, or 22 characters for tables on page 2 of Client worksheet
      if ( $column['text'] == 'Description' ) {
        if ( $field['pageNumber'] == 2 && strtolower( rgar( $form, 'title') ) == 'client worksheet' ) {
          $special[$column_key] = 'maxlength="22" ';
        } else {
        $special[$column_key] = 'maxlength="34" ';
      }
      }
    }
    $output .= '    </tr>';
    $output .= '  </thead>';
    $output .= '  <tbody>';
    $count_inputs = 0;
    foreach ( $field['row_choices'] as $row_key => $row ) {
      $output .= '    <tr class="' . ( !empty( $row['price'] ) ? 'no-bottom-border' : '' ) . ' ' . ( $row_key % 2 == 0 ? 'even' : 'odd' ) . '">';
      $output .= '      <td class="text-left" data-header-title="' . $row['text'] . '"><span class="hidden-xs">' . $row['text'] . '</span>&nbsp;</td>';
      foreach ( $field['column_choices'] as $column_key => $column ) {
        $input_id = 'input_' . $form_id . '_' . $field['id'] . '_' . $row_key . '_' . $column_key;
        $input_name = 'input_' . $field['id'] . '_' . $count_inputs;
        $input_value = !empty($value[$field['id'] . '.' . $count_inputs]) ? $value[$field['id'] . '.' . $count_inputs] : '';
        $output .= '      <td data-header-title="' . $column['text'] . '"><input ' . ( in_array( strtolower( $column['text'] ), $table_text_fields ) ? '' : 'class="gf_currency" ' ) . 'id="' . $input_id . '" name="' . $input_name . '" type="text" placeholder="' . ( !empty( $column['price'] ) ? $column['price'] : '' ) . '" value="' . $input_value . '"' . ( ! empty( $special[$column_key] ) ? $special[$column_key] : '' ) . '></td>';
        $count_inputs++;
      }
      $output .= '    </tr>';
      if ( !empty( $row['price'] ) ) {
        $output .= '    <tr class="' . ( $row_key % 2 == 0 ? 'even' : 'odd' ) . '">';
        $output .= '      <td class="text-left" colspan="' . ( count( $field['column_choices'] ) + 1 ) . '">' . $row['price'] . '</td>';
        $output .= '    </tr>';
      }
    }

    $output .= '  </tbody>';
    $output .= '</table>';

    $output .= '<div class="input-group">';

    if ( !empty( $field['description'] ) ) {
      $output .= '<div class="note">';
      $output .= '  <p>' . $field['description'] . '</p>';
      $output .= '</div>';
    }
    if ( $field['failed_validation'] && $field['validation_message'] != '' ) {
      $output .= '<div class="validation_message note">';
      $output .= $field['validation_message'];
      $output .= '</div>';
    }

    $output .= '  </div>';

    $output .= '</div>';
  }

  return stripslashes( $output );
}
