<?php

function render_m41b_programs() {
	$output = '';

	$programs_args = array(
		'posts_per_page' => -1,
		'post_type' => 'program',
		'post_status' => 'publish',
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);

	$programs = get_posts( $programs_args );

	$count = 0;

	foreach ( $programs as $program ) {

		$visible = get_post_meta($program->ID)['meta-box-checkbox'][0];

		if( empty($visible) ){

			$title = $program->post_title;
			$excerpt = $program->post_excerpt;
			$link = get_relative_permalink( $program->ID );
			$featured_src = wp_get_attachment_url( get_post_thumbnail_id( $program->ID ) );
            $alt = !empty($title) ? $title : 'Program image';
			// use the default image
			if (empty( $featured_src )) {
				$featured_src = get_stylesheet_directory_uri() . '/images/program_default_featured.jpg';
			}

			if ( !empty( $title ) || !empty( $excerpt ) || !empty( $featured_src ) ) {
				$output .=  '<div class="block report-holder background-white default-content-style">';

				$output .=    '<section aria-label="report block '. $title .'" class="report-block" style="min-height: 370px;">';
				$output .=      '<div class="container">';
				$output .=        '<div class="row">';
			
				$output .=          '<div class="img-holder'. ( $count % 2 == 1 ? ' goofy' : '' ) .'">';
				$output .=            !empty( $featured_src ) ? '<img alt = "' . $alt . '" src="'. $featured_src .'" '. ( $count % 2 == 1 ? ' width="695" height="410" ' : ' width="700" height="410" ' ) .' class="height-calc">' : '';
				$output .=          '</div>';
				$output .=          '<div class="text-holder height-calc'. ( $count % 2 == 1 ? ' goofy' : '' ) .'">';
				$output .=            '<div class="text">';
				$output .=              !empty( $title ) ? '<h1 class="text-info">'.$title.'</h1>' : '';
				$output .=              !empty( $excerpt ) ? apply_filters( 'the_content', html_entity_decode( $excerpt ) ) : '';
				//echo $link;
				if($link == '/programs/rental-counseling/'){
					$newurl = '/resources/articles/counseling-programs-rental-counseling/';
					$output .=              '<a href="'.$newurl.'" aria-label="'.$title.'" class="but btn btn-primary">Learn More</a>';
				}else if($link == '/programs/counseling-programs-student-loan-counseling/'){
					$newurl = '/resources/articles/counseling-programs-student-loan-counseling/';
					$output .=              '<a href="'.$newurl.'" aria-label="'.$title.'" class="but btn btn-primary">Learn More</a>';
				}else{
					//$newurl == $link;
					$output .=              '<a href="'.$link.'" aria-label="'.$title.'" class="but btn btn-primary">Learn More</a>';	
				}
				//$output .=              '<a href="'.$newurl.'" aria-label="'.$title.'" class="but btn btn-primary">Learn More</a>';
				$output .=            '</div>';
				$output .=          '</div>';
				$output .=        '</div>';
				$output .=      '</div>';
				$output .=    '</section>';
				$output .=  '</div>';

				$count++;
			}

		}

	}

	return stripslashes( $output );

}
