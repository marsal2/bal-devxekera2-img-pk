<?php
/*
 *  WP Edit module: White Label Websites Programs
 */
function white_label_websites_programs_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
	global $data;

	if ( empty( $data['wlw_programs_module'] ) ) {
		$data = init_array_on_var_and_array_key($data, 'wlw_programs_module');
		$data['wlw_programs_module'][ $key ] = array(
			'programs' => '',
			'programs_protected' => 'individually'
		);
	}

	$autocomplete_args_programs = array(
		'post_type' => 'program',
		'posts_per_page' => -1,
		'post_status' => 'publish'
	);

	//Left right image choices
	$programs_protected_choices = array(
		'individually'  => __( 'Programs Individually Protected', 'balance' ),
		'all' => __( 'All Programs Protected', 'balance' ),
	);

	if ( empty( $data['wlw_programs_module'][ $key ]['programs_protected'] ) ) {
		$data['wlw_programs_module'][ $key ]['programs_protected'] = 'individually';
	}

	$output = '';
	$output .= '<a name="wlw-programs-module-wrapper-'. $key .'"></a>';
	$output .= '<div class="module-wrapper wlw-programs-module-wrapper-'. $key .' hidden" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
	$output .= '  <div class="postbox postbox-custom wlw-programs-module-list-wrapper-'. $key .'">';
	$output .= '    <h3>'. $module_title . ( intval( $key ) > 0 ? ' #'.( intval( $key )+1 ) : '' ) .'<a class="description fright section-expander is-expanded" data-toggle-title="'. __( 'Expand', 'balance' ) .'" href="javascript:;">'. __( 'Collapse', 'balance' ) .'</a></h3>';
	$output .= '    <div class="inside">';

	$output .= '     <p>';
	$output .= '        <label><b>'. __( 'Programs Protection', 'balance' ) .':</b></label><br>';
	$output .=          radiobuttonlist_field( $data['wlw_programs_module'][ $key ]['programs_protected'], 'wlw_programs_module['.$key.'][programs_protected]', $programs_protected_choices, 'individually', true );
	$output .= '      </p>';

	$output .= '        <p>';
	$output .= '          <label><b>'. __( 'Programs linked to Partner website', 'balance' ) .':</b></label>';
	$output .=              multi_autocomplete_field( $data['wlw_programs_module'][ $key ]['programs'], 'wlw_programs_module['.$key.'][programs]', $autocomplete_args_programs );
	$output .= '        </p>';

	$output .= '    </div>';
	$output .= '  </div>';
	$output .= '</div>';
	return $output;
}
?>
