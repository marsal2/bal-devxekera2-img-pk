<?php
/*
 *  WP Edit module: M18
 *  Description: Module simple form. M18 – Simple form
 */

function m18_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data;

  if( empty( $data['m18_module'][ $key ] ) ){
    $data = init_array_on_var_and_array_key($data, 'm18_module');
    $data['m18_module'][ $key ] = array(
      'formhtml' => '',
    );
  }

  $output = '';
  $output .= '<a name="m18-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper m18-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom m18-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside hidden">';

  $output .= '            <p>';
  $output .= '              <label><b>'. __( 'Form html/shortcode', 'balance' ) .':</b></label><br/>';
  $output .=                textarea_field( $data['m18_module'][ $key ]['formhtml'], 'm18_module['.$key.'][formhtml]', false );
  $output .= '            </p>';

  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;

}

?>
