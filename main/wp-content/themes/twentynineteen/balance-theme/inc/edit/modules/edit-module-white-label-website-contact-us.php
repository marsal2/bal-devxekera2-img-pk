<?php
/*
 *  WP Edit module: White Label Websites Contact Us
 */
function white_label_websites_contact_us_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
  global $data;

  if( !empty( $data['wlw_contact_us_module'] ) ){
    $data['wlw_contact_us_module'][ $key ] = array(
      'title' => '',
    );
  }

  $output = '';
  $output .= '<a name="wlw-contact_us-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper wlw-contact_us-module-wrapper-'. $key .' hidden" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom wlw-contact_us-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-expanded" data-toggle-title="'. __( 'Expand', 'balance' ) .'" href="javascript:;">'. __( 'Collapse', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside">';
  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;
}
?>
