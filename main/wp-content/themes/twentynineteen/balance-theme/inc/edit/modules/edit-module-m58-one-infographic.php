<?php
/*
 *  WP Edit module: M58
 *  Description: Module infographic. M58 - One infographic
 */

function m58_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data;

  if( empty( $data['m58_module'][ $key ] ) ){
    $data = init_array_on_var_and_array_key($data, 'm58_module');
    $data['m58_module'][ $key ] = array(
      'image' => '',
    );
  }

  $output = '';
  $output .= '<a name="m58-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper m58-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom m58-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside hidden">';

  $output .= '    <p>';
  $output .= '      <label><b>'. __( 'Infographic image', 'balance' ) .':</b></label><br/>';
  $output .=        upload_field( $data['m58_module'][ $key ]['image'], 'm58_module['.$key.'][image]' );
  $output .= '    </p>';

  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;

}

?>
