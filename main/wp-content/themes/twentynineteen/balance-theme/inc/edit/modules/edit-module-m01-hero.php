<?php
/*
 *  WP Edit module: M1
 *  Description: Module hero. M01 – Hero module & M57 - Hero with CTA, no carousel
 */

function m1_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data;

  if( empty( $data['m1_module'][ $key ] ) ){
    $data = init_array_on_var_and_array_key($data, 'm1_module');
    $data['m1_module'][ $key ] = array(
      'title' => '',
      'copy' => '',
      'bgimage' => array(
        'attachment_id' => '',
        'filename' => '',
        'fullpath' => ''
      ),
      'buttontext' => '',
      'linkfield' => array(
        'url_type' => '1',
        'url' => '',
        'url_page_id' => '',
        'url_onclick_js' => ''
      ),
      'style' => 'short'
    );
  }

  if( empty($data['m1_module'][ $key ]['bottomradius']) ){
    $data['m1_module'][ $key ]['bottomradius'] = '';
  }

  if( empty($data['m1_module'][ $key ]['hidehero']) ){
    $data['m1_module'][ $key ]['hidehero'] = '';
  }

  if( empty($data['m1_module'][ $key ]['style']) ){
    $data['m1_module'][ $key ]['style'] = 'short';
  }

  $hero_style_choices = array(
    'short' => __( 'Short', 'balance' ),
    'medium' => __( 'Medium', 'balance' ),
    'tall'  => __( 'Tall', 'balance' ),
  );

  $output = '';
  $output .= '<a name="m1-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper m1-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom m1-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside hidden">';

  $output .= '        <p>';
  $output .= '          <label><b>'. __( 'Hero image', 'balance' ) .':</b></label><br/>';
  $output .=              upload_field( $data['m1_module'][ $key ]['bgimage'], 'm1_module['.$key.'][bgimage]' );
  $output .= '        </p>';

  $output .= '        <p>';
  $output .= '          <label><b>'. __( 'Title', 'balance' ) .':</b></label>';
  $output .=              text_field( $data['m1_module'][ $key ]['title'], 'm1_module['.$key.'][title]');
  $output .= '        </p>';

  $output .= '        <p>';
  $output .= '          <label><b>'. __( 'Subtitle', 'balance' ) .':</b></label>';
  $output .=              text_field( $data['m1_module'][ $key ]['copy'], 'm1_module['.$key.'][copy]');
  $output .= '        </p>';

  $output .= '      <p>';
  $output .= '       <label><b>'. __( 'Button text', 'balance' ) .':</b></label>';
  $output .=          text_field( $data['m1_module'][ $key ]['buttontext'], 'm1_module['.$key.'][buttontext]');
  $output .= '      </p>';

  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Button link', 'balance' ) .':</b></label><br/>';
  $output .=          links_to_field( $data['m1_module'][ $key ]['linkfield'], 'm1_module['.$key.'][linkfield]');
  $output .= '      </p>';

  $output .= '      <p>';
  $output .=          checkboxlist_field( $data['m1_module'][ $key ]['bottomradius'], 'm1_module['.$key.'][bottomradius]', array( 'yes' => __( 'Bottom Radius', 'balance' ) ) );
  $output .= '      </p>';

  $output .= '      <p>';
  $output .=          checkboxlist_field( $data['m1_module'][ $key ]['hidehero'], 'm1_module['.$key.'][hidehero]', array( 'no' => __( 'Hide Hero', 'balance' ) ) );
  $output .= '      </p>';

  $output .= '     <p>';
  $output .= '        <label><b>'. __( 'Hero height style', 'balance' ) .':</b></label><br>';
  $output .=          radiobuttonlist_field( $data['m1_module'][ $key ]['style'], 'm1_module['.$key.'][style]', $hero_style_choices, 'short', true);
  $output .= '      </p>';

  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;

}

?>
