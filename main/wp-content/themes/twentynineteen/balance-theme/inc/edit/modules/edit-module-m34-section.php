<?php
/*
 *  WP Edit module: M34
 *  Description: Module section: M34 - Title, Copy
 */

function m34_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data;

  if( empty( $data['m34_module'][ $key ] ) ){
    $data = init_array_on_var_and_array_key($data, 'm34_module');
    $data['m34_module'][ $key ] = array(
      'title' => '',
      'copy' => '',
    );
  }

  if ( empty( $custom_settings ) ) {
  	$custom_settings = array('media_buttons' => false, 'quicktags' => true );
  }

  $output = '';
  $output .= '<a name="m34-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper m34-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom m34-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside hidden">';

  $output .= '      <p>';
  $output .= '       <label><b>'. __( 'Title', 'balance' ) .':</b></label>';
  $output .=          text_field( $data['m34_module'][ $key ]['title'], 'm34_module['.$key.'][title]');
  $output .= '      </p>';

  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Copy', 'balance' ) .':</b></label><br/><br/>';
  $output .=          textarea_field( $data['m34_module'][ $key ]['copy'], 'm34_module['.$key.'][copy]', true, 20, '', '', $custom_settings );
  $output .= '      </p>';

  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;

}

?>
