<?php
/*
 *  WP Edit module: M20
 *  Description: Module with split carousel. M20 – Split Carousel
 */

function m20_module_form( $key, $visible_on = 'all', $module_title = '' ) {
	global $data;

	$output = simple_edit_module( $key, 'm20', array(
			array(
				'type' => 'text',
				'name' => 'title',
				'default_value' => '',
				'label' => 'Title'
			),
			array(
				'type' => 'repeater',
				'name' => 'slides',
				'label' => 'Slide',
				'default_value' => array(),
				'fields' => array(
					array(
						'type' => 'upload',
						'name' => 'uploadimage',
						'label' => 'Image',
						'default_value' => array(),
						'additional_params' => array(
							'',
							'Image',
							'image'
						)
					),
					array(
						'type' => 'text',
						'name' => 'title',
						'label' => 'Title',
						'default_value' => ''
					),
					array(
						'type' => 'text',
						'name' => 'description',
						'label' => 'Description',
						'default_value' => ''
					)
				)
			)
		), $data, $module_title, $visible_on );

	return $output;

}

?>
