<?php
/*
 *  WP Edit module: resourcesections
 *  Description: Module for Program Resource Sections
 */

function resourcesections_module_form( $key, $visible_on = 'all', $module_title = '') {
  global $data;
  global $resources_cpts;

  $output = simple_edit_module($key, 'resourcesections', array(
    array(
      'type' => 'repeater',
      'name' => 'sections',
      'default_value' => array(),
      'label' => 'Resource section',
      'fields' => array(
        array(
          'type' => 'text',
          'name' => 'title',
          'label' => 'Title',
          'default_value' => ''
        ),
        array(
          'type' => 'multi_autocomplete',
          'name' => 'list',
          'label' => 'Resources',
          'default_value' => '',
          'additional_params' => array(
            array(
              'post_type' => $resources_cpts,
              'posts_per_page' => -1
            ),
            -1
          )
        )
      )
    )
  ), $data, $module_title, $visible_on);

  return $output;
}