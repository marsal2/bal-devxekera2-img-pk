if (typeof window.url_path === 'undefined' || !window.url_path) {
	window.url_path = {
		ajax_url: '',
		images_url: 'images/',
		home_url: ''
	};
}

// hide the page template label
jQuery('.post-type-page #pageparentdiv select#parent_id').next().replaceWith('<br><br>');

jQuery(document).ready(function() {
  jQuery(".loading-modules").appendTo('#wpbody-content .wrap h1').show();
  jQuery('.custom-datepicker').datepicker();
  jQuery(document).on('focusin', '.custom-datepicker', function(e) {
    jQuery(this).datepicker();
  });

  var page_template_selector = jQuery("#page_template");

  //Hide some commands from Nested Pages Plugin
  jQuery(".nestedpages .action-buttons a.np-btn i.np-icon-bubble").parent().hide();
  jQuery(".nestedpages .nestedpages-tools .subsubsub li a.np-toggle-hidden").parent().hide();


  if ( jQuery('body').hasClass('post-new-php') || jQuery('body').hasClass('post-php') ) {
    reloadModuleForms();
    resetTMCEHeight();
  }

  jQuery(document).on("change", "select.custom-templates-selector", function() {
    var $this = jQuery(this);
    page_template_selector.val($this.val());
    reloadModuleForms();
    resetTMCEHeight();
  });

	// find each tinyMCE editor and recalculate its height by multiply its textarea rows and lineheight properties
	function resetTMCEHeight() {
		window.setTimeout(function() {
			jQuery(".mce-tinymce").each(function() {
				var textarea = jQuery(this).parent().find('.wp-tmce');
				var line_height = 0,
					rows = 0,
					height = 0;
				if (textarea.css('line-height')) {
					line_height = parseFloat(textarea.css('line-height'));
				}
				if (line_height > 0 && textarea.attr('rows')) {
					rows = textarea.attr('rows');
				}
				if (line_height > 0 && rows > 0) {
					height = rows * line_height;
				}
				if (height > 0) {
					jQuery(this).find('.mce-edit-area').css('min-height', height + 'px');
				}
			});
		}, 20);
	}

	// function which load modules that are specified in selected template header
	function reloadModuleForms() {
		jQuery.ajax({
			url: url_path.ajaxurl,
			method: 'POST',
			cache: false,
			async: false,
			dataType: 'json',
			data: {
				action: 'get_edit_form',
				post_id: jQuery('#post_ID').val(),
				post_type: jQuery('#post_type').val(),
				page_template: page_template_selector.val(),
			},
			beforeSend: function(event, ui) {
				jQuery(".loading-modules").css({
					'opacity': '1'
				});
				// remove all initialized tinyMCE editors
				var i, t = tinyMCE.editors;
				for (i in t) {
					if (t.hasOwnProperty(i)) {
						t[i].remove();
					}
				}

				// remove all current modules
				if (jQuery('#post-body-content .custom-modules-wrapper')) {
					jQuery('#post-body-content .custom-modules-wrapper').html("");
				}

				// remove all tinyMCE markup
				jQuery('.mce-tinymce').remove();
				jQuery('.quicktags-toolbar').remove();
			},
			success: function(response) {

				if (response.post_type_show_modules != 0) {
					// add modules wrapper if not exists
					if (!jQuery('#post-body-content .custom-modules-wrapper').length > 0) {
						jQuery('#post-body-content').append('<div class="custom-modules-wrapper"></div>');
					}
					// output modules
					if (response.modules) {
						jQuery('#post-body-content .custom-modules-wrapper').append(response.modules);
						// if there is only one module then expand it
						if (response.count_modules == 1) {
							setTimeout(function() {
								// if collapsed, expand it
								if (jQuery(".postbox-custom:eq(0) h3").find('.section-expander').hasClass('is-collapsed')) {
									jQuery(".postbox-custom:eq(0) h3").find('.section-expander').trigger('click');
								}
							}, 0);
						}
						// check if the tabs selector is appended as a module
						if (jQuery('.nav-tab-wrapper').length > 0) {
							reloadTabsModules();
							// attach click event when clicking on tabs (change active state and reload modules state)
							jQuery('.nav-tab-wrapper a').on('click', function() {
								jQuery('.nav-tab-wrapper a').removeClass('nav-tab-active');
								jQuery(this).addClass('nav-tab-active');
								reloadTabsModules();
							});
						}

						// check if type selector is appended as a module
						if (jQuery('.types-selector-wrapper').length > 0) {
							reloadTypesModules();
							// attach click event when clicking on tabs (change active state and reload modules state)
							jQuery('.types-selector').on('change', function() {
								reloadTypesModules();
							});
						}
					}
					// output error if present
					if (response.error) {
						jQuery('#post-body-content .custom-modules-wrapper').append(response.error);
					}
				}

			},
			complete: function() {
				setTimeout(function() {
					// reset template select field
					jQuery("select.custom-templates-selector").val(page_template_selector.val());
					//Enable repeatables
					registerRepeatables();
					// initialize needed tinyMCE editors
					enableEditor(jQuery('#post-body-content .custom-modules-wrapper'));
					initializeUrlSuggest();
					initializeTokenize();
					//Done
					jQuery(".loading-modules").css({
						'opacity': '0'
					});

				}, 100);
			},
			error: function(jqXHR) {
				jQuery('#post-body-content').append('<br/><br/><br/><br/><p align="center" style="font-size:20px;">Ooops, something went wrong!</p><p align="center" style="font-size:14px;">There is an error during modules load, please try to load this page again by accessing it through the menu.</p>');
			}
		});

		//Initialize autocomplete tokenize
	}

	function enableEditor($target, resetId) {
		jQuery(".wp-tmce", $target).each(function() {
			//get settings from texarea
			var $this = jQuery(this);
			if (resetId) {
				$this.attr('id', 'wp-edit-' + (new Date().getTime()));
			}
			var settings;
			settings = jQuery(this).attr('data-settings');
			if (settings) {
				settings = JSON.parse(settings);
			}
			var mode = "tmce";
			if (settings && settings.mode) {
				mode = settings.mode;
			}

			$this.wp_editor(settings);

			// switch to editor mode set in settings
			switchEditors.go(jQuery(this).attr('id'), mode);
		});
	}
	// function which handles the state of the modules
	// loops through all the tabs and makes active the ones that are related to the selected tab (through data-tab attribute)
	function reloadTabsModules() {
		jQuery('.nav-tab-wrapper a').each(function() {
			var $this = jQuery(this);
			if ($this.hasClass('nav-tab-active')) {
				jQuery('[data-visible-on="' + $this.attr('data-tab') + '"]').removeClass('hidden');
			} else {
				jQuery('[data-visible-on="' + $this.attr('data-tab') + '"]').addClass('hidden');
			}
		});
		$activeTab = jQuery('.nav-tab-wrapper a.nav-tab-active');
		jQuery('[data-visible-on="' + $activeTab.attr('data-tab') + '"]:eq(0)').find('.section-expander:not(.is-expanded)').trigger('click');;
	}

	// function which handles the state of the modules depending on the type selection
	function reloadTypesModules() {
		var $selected_type = jQuery('.types-selector').val();
		jQuery('.types-selector option').each(function() {
			$this = jQuery(this);
			jQuery('[data-visible-on*="' + $this.attr('value') + '"]').addClass('hidden');
		});
		jQuery('[data-visible-on*="' + $selected_type + '"]').removeClass('hidden');
	}

	/**
	 * [registerSortable description]
	 * @param  {} _item                               [description]
	 * @param  {[type]} _items_wrapper                [description]
	 * @param  {[type]} _item_is_child_sortable       [description]
	 * @return {[type]}                               [description]
	 */
	function registerSortable(_item, _items_wrapper, _item_is_child_sortable) {
		var handleForSorting;
		if (!_item_is_child_sortable) {
			handleForSorting = 'h4:not(.child)';
		} else {
			handleForSorting = 'h4.child';
		}
		var radio_checked = [];
		jQuery(_items_wrapper).find(handleForSorting).css({
			"cursor": "move"
		}).addClass("sortable-item").attr("title", theme.move);
		jQuery(_items_wrapper).sortable({
			items: '.item',
			placeholder: "sortable-placeholder",
			axis: "y",
			forceHelperSize: true,
			forcePlaceholderSize: true,
			handle: handleForSorting,
			start: function(event, ui) {

				var $sender = jQuery(this);

				jQuery(ui.item).find('.wp-tmce').each(function() {
					var textareaID = jQuery(this).attr('id');
					tinyMCE.EditorManager.execCommand('mceRemoveEditor', false, textareaID);
					jQuery(this).parent().find('.mce-tinymce').remove();
				});


				jQuery(_items_wrapper).find("input[type='radio']").each(function() {
					if (jQuery(this).is(":checked")) {
						radio_checked.push(jQuery(this));
					}
				});
			},
			stop: function(event, ui) {
				jQuery(ui.item).find('.wp-tmce').each(function() {
					var textareaID = jQuery(this).attr('id');
					window.setTimeout(function() {
						settings = jQuery(this).attr('data-settings');
						if (settings) {
							settings = JSON.parse(settings);
						}
						var mode = "tmce";
						if (settings && settings.mode) {
							mode = settings.mode;
						}
						jQuery(this).wp_editor();
						tinyMCE.EditorManager.execCommand('mceAddEditor', true, textareaID);
						switchEditors.go(textareaID, mode);
						resetTMCEHeight();
					}, 20);
				});
				var $sender = jQuery(this);
				var $counter = 0,
					$this,
					$item;
				// we need to rename the fields when we sort them (so they are properly indexed in array we send in $_POST)
				$sender.find(_item).each(function(key, value) {
					$this = jQuery(this);
					$item = ui.item;
					if (!_item_is_child_sortable) {
						jQuery(this).find("span.counter:not(.child-counter)").html($counter + 1);
					} else {
						jQuery(this).find("span.counter").html($counter + 1);
					}
					jQuery(this).find("input,textarea,select.single-drp").each(function() {
						if (jQuery(this).attr("name") !== undefined) {
							var $this = jQuery(this);
							var $this_value = jQuery(this).val();
							var $this_is_checked = jQuery(this).is(":checked");
							var name = jQuery(this).attr('name');

							// Matching inputs index. \d{0,2}\ is matching 2 digit number
							var ocurrences_count = name.match(/\[[0-9]\d{0,2}\]/gi);
							var j = 0;
							jQuery(this).attr('name', name.replace(/\[[0-9]\d{0,2}\]/gi, function(match, pos, original) {
								j++;
								if ($this.hasClass('child') && !_item_is_child_sortable) {
									return (j == 1) ? "[" + $counter + "]" : match;
								} else {
									return (ocurrences_count.length == j) ? "[" + $counter + "]" : match;
								}

							}));
							// ugly hack
							// radio button group become unchecked on sortable so we need to store the selected value in a variable and recheck it after renaming

							jQuery(_items_wrapper).find("input[type='radio']").each(function() {
								jQuery(this).attr("checked", false);
							});

							for (var i = 0; i < radio_checked.length; ++i) {
								radio_checked[i].attr("checked", true);
							}
						}
					});
					$counter++;
				});

			}
		});
	}

	function registerRemove(_button_selector, _item_selector, _items_wrapper, _isParent) {
		jQuery(_items_wrapper).on('click', _button_selector, function(e) {
			e.stopPropagation();
			e.preventDefault();
			var $sender = jQuery(this);
			var itemForm = jQuery(this).parents(_item_selector);
			var nmb_of_slides = itemForm.parent().find(_item_selector).size();
			var parent = itemForm.parent();
			if (nmb_of_slides == 1) {
				alert(theme.min_one_slide_error);
				return false;
			}
			if (confirm(theme.confirm_delete)) {
				$tmp_id = itemForm.find('.wp-editor-area').attr('id');
				tinyMCE.EditorManager.execCommand('mceRemoveEditor', false, $tmp_id);
				itemForm.find('.mce-tinymce').remove();
				itemForm.remove();
				var $counter = 0;

				parent.find(_item_selector).each(function(key, value) {
					if (_isParent) {
						jQuery(this).find("span.counter:not(.child-counter)").html($counter + 1);
					} else {
						jQuery(this).find("span.counter").html($counter + 1);
					}
					jQuery(this).find("input,textarea,select.single-drp").each(function() {
						if (jQuery(this).attr("name") !== undefined) {
							var $this = jQuery(this);
							var $this_value = jQuery(this).val();
							var name = jQuery(this).attr('name');
							// Matching inputs index. \d{0,2}\ is matching 2 digit number
							var ocurrences_count = name.match(/\[[0-9]\d{0,2}\]/gi);
							var j = 0;
							jQuery(this).attr('name', name.replace(/\[[0-9]\d{0,2}\]/gi, function(match, pos, original) {
								j++;
								if ($this.hasClass('child') && _isParent) {
									return (j == 1) ? "[" + $counter + "]" : match;
								} else {
									return (ocurrences_count.length == j) ? "[" + $counter + "]" : match;
								}
							}));
							// ugly hack
							// radio button group become unchecked on sortable so we need to store the selected value in a variable and recheck it after renaming
							if (jQuery(this).is(":checked") && jQuery(this).attr("type") == "radio") {
								jQuery("input[name='" + jQuery(this).attr("name") + "'][value='" + $this_value + "']").attr('checked', 'checked');
							}
						}
					});
					$counter++;
				});
			}

		});
	}

	function registerRepeatables() {
		//Select all edit modules and apply listeners
		jQuery('.custom-modules-wrapper').find('.inside').each(function() {
			//Array of all repeatable field sets
			var $module = jQuery(this);
			mapRepeatables($module);
		});
	}
	/**
	Recursivly travers the DOM elements to find repeatables and map them
	**/
	function mapRepeatables($module) {
		$module.children('.items-list').each(function() {
			var $root = jQuery(this);
			var children = [];

			var repeatable = {
				root: $root,
				clonable: null,
				children: children
			};
			//Event bindings
			bindRepeatableActions(repeatable);
			enableSortable(repeatable.root);
			//Continue recursion
			mapRepeatables($root.children(), children);
			//Set clonable after it received all its bindings
			repeatable.clonable = getClonableFieldset($root);

		});
	}
	/**
	Binds click handlers for repeatable buttons
	**/
	function bindRepeatableActions(repeatable) {
		repeatable.root.children('.add-new-item').on('click', function(e) {
			e.preventDefault();
			e.stopPropagation();
			var $button = jQuery(this);
			if ($button.parent().attr('data-max')) {
				var num = $button.parent().children('.item').size();
				if (num > parseInt($button.parent().attr('data-max'))) {
					return false;
				}
			}
			var $newItem = repeatable.clonable.clone(true, true);
			var num = setFieldsIndex($newItem, -1);
			$newItem.insertBefore($button);
			repeatable.root.trigger('stack-change', [$button.parent()]);
			setFieldsIndex($newItem, num);
			bindSpecialFields($newItem);
		});
		repeatable.root.on('click', '.remove-item', function(e) {
			e.stopPropagation();
			e.preventDefault();
			$parent = jQuery(this).closest('.item').parent();
			jQuery(this).closest('.item').remove();
			if ($parent.children('.item').size() == 0) {
				$parent.children('.add-new-item').click();
			} else {
				$parent.trigger('stack-change', [$parent]);
			}
		});
		repeatable.root.on('stack-change', function(e, $root) {
			e.stopPropagation();
			e.preventDefault();
			if($root.parents('.items-list').length) {
				$root = $root.parents('.items-list').last();
			}
			setNameKeys($root, []);

			if ($root.attr('data-max')) {
				var num = $root.children('.item').size();
				if (num >= parseInt($root.attr('data-max'))) {
					$root.children('.add-new-item').prop('disabled', true);
				} else {
					$root.children('.add-new-item').prop('disabled', false);
				}
			}
		});

	}
	/**
	Takes care of bindings for special fields like wp editor
	**/
	function bindSpecialFields($target) {
		enableEditor($target, true);
		initializeTokenize($target);
		initializeUrlSuggest($target);
	}
	/**
	Enables sorting of repeatable items
	**/
	function enableSortable($target) {
		var radio_checked = [];
		$target.find('> h4').css({
			"cursor": "move"
		}).addClass("sortable-item").attr("title", theme.move);
		$target.sortable({
			items: '> .item',
			placeholder: "sortable-placeholder",
			axis: "y",
			forceHelperSize: true,
			forcePlaceholderSize: true,
			handle: '> h4',
			containment: 'parent',
			start: function(event, ui) {
				var $sender = jQuery(this);

				jQuery(ui.item).find('.wp-tmce').each(function() {
					var textareaID = jQuery(this).attr('id');
					tinyMCE.EditorManager.execCommand('mceRemoveEditor', false, textareaID);
					jQuery(this).parent().find('.mce-tinymce').remove();
				});

				$target.find("input[type='radio']").each(function() {
					if (jQuery(this).is(":checked")) {
						radio_checked.push(jQuery(this));
					}
				});
			},
			stop: function(event, ui) {
				setNameKeys($target)
				jQuery(ui.item).find('.wp-tmce').each(function() {

					var textareaID = jQuery(this).attr('id');
					window.setTimeout(function() {
						settings = jQuery(this).attr('data-settings');
						if (settings) {
							settings = JSON.parse(settings);
						}
						var mode = "tmce";
						if (settings && settings.mode) {
							mode = settings.mode;
						}
						jQuery(this).wp_editor();
						tinyMCE.EditorManager.execCommand('mceAddEditor', true, textareaID);
						switchEditors.go(textareaID, mode);
						resetTMCEHeight();
					}, 20);
				});

			}
		});
	}
	/**
	Sets appropriate keys after a change of the item stack
	**/
	function setNameKeys($root, indexArray) {
		indexArray = indexArray ? indexArray : [];
		$root.children('div.item').each(function(index) {
			var indexes = indexArray.slice();
			indexes.push(index);
			var $item = jQuery(this);
			//Set corect names
			jQuery('h4 > .counter', $item).html(index + 1);
			$item.children('p,div.upload-wrapper').find("input,textarea,select.single-drp").each(function() {
				var $field = jQuery(this);
				var fieldName = $field.attr('name');
				if (fieldName && fieldName.length) {
					fieldName = fieldName.split(']');
					var startAt = fieldName.length - 1 - (indexes.length * 2);
					startAt = startAt - (startAt % 2);
					if ( !jQuery.isNumeric(fieldName[startAt].replace('[','')) ) {
						startAt = startAt - 2;
					}
					for (var i = 0; i < indexes.length; i++) {
						var idx = startAt + i * 2;
						fieldName[idx] = '[' + indexes[i];
					}
					$field.attr('name', fieldName.join(']'));
				}
			});
			$item.children('.items-list').each(function() {
				setNameKeys(jQuery(this), indexes);
			});
		});
	}
	/**
	Returns a clean clone of the repeatable item
	**/
	function getClonableFieldset($root) {
		var $fieldset = $root.children(':first').clone(true, true);
		$fieldset.children(':not(.items-list)').each(function() {
			cleanFields(jQuery(this));
		});
		$fieldset.find('.items-list').each(function() {
			jQuery(this).children('.item:not(:first)').remove();
		});
		return $fieldset;
	}
	function setFieldsIndex($target, index){
		var oldVal = null;
		$target.find("input,textarea,select.single-drp").each(function() {
			if(jQuery(this).attr('name') !== undefined){
				var name = jQuery(this).attr('name');
				if(/^[a-zA-Z0-9_-]+\[[-0-9]+\]/.test(name)){
					var newName = name.split('[');
					if(!oldVal){
						oldVal = newName[1].replace(']','');
					}
					newName[1] = index+']';
					jQuery(this).attr('name', newName.join('['));
				}
			}
		});
		console.log(oldVal);
		return oldVal;
	}
	/**
	Resets field values to empty for clonables.
	**/
	function cleanFields($fieldContainer) {
		// Remove gelocation module map
		$fieldContainer.find(".geolocator-box .error").remove();
		$fieldContainer.find(".geolocator-preview .inner .map").attr('style', '').html("");
		$fieldContainer.find(".geolocator-preview .inner .message").removeClass('hidden');

		// toggle becomes current
		$fieldContainer.find(".btn-upload").val(theme.upload_select);
		// current becomes toggle
		$fieldContainer.find(".btn-upload").data("toggle-title", theme.change);
		$fieldContainer.find(".btn-upload-remove").hide();
		$fieldContainer.find(".upload-img-data").html('').addClass('hidden');
		$fieldContainer.find("img").attr('src', $fieldContainer.find("img").attr('data-dummy-src'));
		//Clean up value

		$fieldContainer.find("input,textarea,select.single-drp").each(function() {
			var $this = jQuery(this);
			if ($this.attr("name") !== undefined) {
				if ($this.attr("type") == 'text' || $this.attr("type") == 'email' || $this.attr("type") == 'hidden' || $this.is("textarea")) {
					$this.val("");
				}
			}
		});
		$fieldContainer.find("select.multiautosuggestfield").each(function() {
			jQuery(this).html("");
		});
		if (jQuery('input[type=radio]', this).length > 0) {
			jQuery('input[type=radio]', this).attr('checked', false);
			jQuery('.url_onclick_js', this).val("");
			if (jQuery('input[type=radio]:first', this).hasClass('url_selector')) {
				jQuery('input[type=radio]:last', this).attr('checked', true);
				jQuery(".page_url", this).addClass("hidden-important");
				jQuery("select.overlay_url", this).addClass("hidden");
				jQuery("input.custom_url", this).removeClass("hidden");
				jQuery(".url_overlay_id_holder", this).val("");
				jQuery("input.custom_url", this).val("");
			} else {
				jQuery('input[type=radio]:first', this).attr('checked', true);
			}
		}
	}

	var custom_uploader;

	jQuery(document).on('click', '.btn-upload-remove', function(e) {
		e.preventDefault();
		var $this = jQuery(this);
		var $input_parent = jQuery(this).parent();
		var $input_upload = jQuery(this).parent().find(".btn-upload");
		var $input_field_attachment_id = $input_parent.find('input[name*="attachment_id"]');
		var $input_field_filename = $input_parent.find('input[name*="filename"]');
		var $input_field_fullpath = $input_parent.find('input[name*="fullpath"]');
		var $input_field_filesize = $input_parent.find('input[name*="filesize"]');
		var $input_field_editurl = $input_parent.find('input[name*="editurl"]');
		var $input_field_type = $input_parent.find('input[name*="type"]');
		var $input_field_subtype = $input_parent.find('input[name*="subtype"]');
		var $image = $input_parent.find('img');
		var $pre_upload_img_data = $input_parent.find('.upload-img-data');

		$input_parent.removeClass("has-image");
		$image.attr("src", $image.data("dummy-src"));
		$input_field_attachment_id.val("");
		$input_field_filename.val("");
		$input_field_fullpath.val("");
		$input_field_filesize.val("");
		$input_field_editurl.val("");
		$input_field_type.val("");
		$input_field_subtype.val("");
		$pre_upload_img_data.html("").addClass('hidden');

		// current title
		var $input_upload_text = $input_upload.val();
		// toggle title
		var $input_upload_toggle_text = $input_upload.data("toggle-title");
		// toggle becomes current
		$input_upload.val($input_upload_toggle_text);
		// current becomes toggle
		$input_upload.data("toggle-title", $input_upload_text);
	});

	jQuery(document).on('click', '.btn-upload', function(e) {
		e.preventDefault();
		var $this = jQuery(this);
		var $input_parent = jQuery(this).parent();
		var $input_field_attachment_id = $input_parent.find('input[name*="attachment_id"]');
		var $input_field_filename = $input_parent.find('input[name*="filename"]');
		var $input_field_fullpath = $input_parent.find('input[name*="fullpath"]');
		var $input_field_filesize = $input_parent.find('input[name*="filesize"]');
		var $input_field_editurl = $input_parent.find('input[name*="editurl"]');
		var $input_field_type = $input_parent.find('input[name*="type"]');
		var $input_field_subtype = $input_parent.find('input[name*="subtype"]');

		var $image = $input_parent.find('img');
		var $pre_upload_img_data = $input_parent.find('.upload-img-data');
		var library = {};
		if ($input_parent.attr('data-library-type')) {
			library.type = $input_parent.attr('data-library-type');
		}
		custom_uploader = wp.media.frames.file_frame = wp.media({
			title: theme.choose,
			button: {
				text: theme.select
			},
			library: library,
			multiple: false
		});
		custom_uploader.on('open', function() {
			if ($input_parent.hasClass("has-image")) {
				var selection = wp.media.frames.file_frame.state().get('selection');
				ids = $input_field_attachment_id.val().split(',');
				ids.forEach(function(id) {
					attachment = wp.media.attachment(id);
					attachment.fetch();
					selection.add(attachment ? [attachment] : []);
				});
			}
		});
		custom_uploader.on('select', function() {
			var attachment = custom_uploader.state().get('selection').first().toJSON();
			if ($input_field_attachment_id.val() === "") { // if previous is not set switch titles
				$input_parent.addClass("has-image");
				// current title
				var $this_text = $this.val();
				// toggle title
				var $this_toggle_text = $this.data("toggle-title");
				// toggle becomes current
				$this.val($this_toggle_text);
				// current becomes toggle
				$this.data("toggle-title", $this_text);
			}

			$input_field_attachment_id.val(attachment.id);
			$input_field_filename.val(attachment.filename);
			$input_field_fullpath.val(attachment.url);
			$input_field_filesize.val(attachment.filesizeHumanReadable);
			$input_field_editurl.val(attachment.editLink);
			$input_field_type.val(attachment.type);
			$input_field_subtype.val(attachment.subtype);

			$pre_upload_img_data.html(
				'File: ' + attachment.filename + '<br>Path: ' + attachment.url + '<br>Size: ' + attachment.filesizeHumanReadable + '<br>Type: ' + attachment.type + ' (' + attachment.subtype + ')' + '<br><a href="' + attachment.editLink + '" target="blank">Edit</a>'
			).removeClass('hidden');
			if (attachment.type != 'image') {
				$image.attr("src", attachment.icon);
			} else {
				$image.attr("src", attachment.url);
			}

			$input_parent.find(".btn-upload-remove").show();
		});
		custom_uploader.open();
	});

	jQuery(document).on("click", ".section-helper", function(e) {
		e.preventDefault();
		if (jQuery(this).parents(".postbox").find(".section-helper-wrapper").hasClass("hidden")) { // display help
			jQuery(this).parents(".postbox").find(".section-helper-wrapper").hide().removeClass("hidden").slideDown();
		} else { // hide help
			jQuery(this).parents(".postbox").find(".section-helper-wrapper").slideUp().addClass("hidden").show();
		}
		// current title
		var $this_text = jQuery(this).html();
		// toggle title
		var $this_toggle_text = jQuery(this).data("toggle-title");
		// toggle becomes current
		jQuery(this).html($this_toggle_text);
		// current becomes toggle
		jQuery(this).data("toggle-title", $this_text);
	});

	jQuery(document).on("click", ".postbox-custom h3", function() {
		jQuery(this).find('.section-expander').trigger('click');
	});

	jQuery(document).on("click", ".section-expander", function(e) {
		e.preventDefault();
		e.stopPropagation();

		var last_selected_section_class_array = jQuery(this).parents('.postbox').attr('class').split(' ');
		var last_selected_section = last_selected_section_class_array[last_selected_section_class_array.length - 1];

		if (jQuery(this).parents(".postbox").find(".inside").hasClass("hidden")) { // display
			jQuery(this).parents(".postbox").find(".inside").hide().removeClass("hidden").slideDown();
			loadGeoboxes(jQuery(this).parents(".postbox").find(".inside .geolocator-box"));
			window.location.hash = last_selected_section;
			var post_action = jQuery("#post").attr("action").split("#");
			jQuery("#post").attr("action", post_action[0] + '#' + last_selected_section);
			jQuery(this).removeClass('is-collapsed').addClass('is-expanded');
		} else { // hide
			jQuery(this).removeClass('is-expanded').addClass('is-collapsed');
			jQuery(this).parents(".postbox").find(".inside").slideUp().addClass("hidden").show();
			window.location.hash = '';
		}
		// current title
		var $this_text = jQuery(this).html();
		// toggle title
		var $this_toggle_text = jQuery(this).data("toggle-title");
		// toggle becomes current
		jQuery(this).html($this_toggle_text);
		// current becomes toggle
		jQuery(this).data("toggle-title", $this_text);

	});
	jQuery(document).on("click", ".all-sections-collapser", function(e) {
		e.preventDefault();
		e.stopPropagation();
		jQuery('.section-expander').each(function() {
			if (jQuery(this).hasClass('is-expanded')) {
				jQuery(this).removeClass('is-expanded').addClass('is-collapsed');
				jQuery(this).parents(".postbox").find(".inside").slideUp().addClass("hidden").show();
				// current title
				var $this_text = jQuery(this).html();
				// toggle title
				var $this_toggle_text = jQuery(this).data("toggle-title");
				// toggle becomes current
				jQuery(this).html($this_toggle_text);
				// current becomes toggle
				jQuery(this).data("toggle-title", $this_text);
			}
		});
	});

	jQuery(document).on("click", ".all-sections-expander", function(e) {
		e.preventDefault();
		e.stopPropagation();
		jQuery('.section-expander').each(function() {
			if (jQuery(this).hasClass('is-collapsed')) {
				jQuery(this).removeClass('is-collapsed').addClass('is-expanded');
				jQuery(this).parents(".postbox").find(".inside").hide().removeClass("hidden").slideDown();
				loadGeoboxes(jQuery(this).parents(".postbox").find(".inside .geolocator-box"));
				// current title
				var $this_text = jQuery(this).html();
				// toggle title
				var $this_toggle_text = jQuery(this).data("toggle-title");
				// toggle becomes current
				jQuery(this).html($this_toggle_text);
				// current becomes toggle
				jQuery(this).data("toggle-title", $this_text);
			}
		});
	});

	jQuery(document).on("change", ".url_selector", function() {
		if (jQuery(this).is(":checked")) {
			if (jQuery(this).val() == "1") {
				//url from page selected, show dropdown and hide input
				jQuery(this).parent('label').parent("p").find(".page_url").removeClass("hidden-important");
				jQuery(this).parent('label').parent("p").find("input.custom_url").addClass("hidden");
				jQuery(this).parent('label').parent("p").find(".page_url").val("");
				jQuery(this).parent('label').parent("p").find("input.custom_url").val("");
			} else { // custom url selected, hide dropdown, show input and empty current saved values
				jQuery(this).parent('label').parent("p").find(".page_url").addClass("hidden-important");
				jQuery(this).parent('label').parent("p").find("input.custom_url").removeClass("hidden");
				jQuery(this).parent('label').parent("p").find("input.custom_url").val("");
				// jQuery(this).parent('label').parent("p").find("input.url_id_holder").val("");
				// jQuery(this).parent('label').parent("p").find("input.url_value_holder").val("");
				// jQuery(this).parent('label').parent("p").find(".Tokenize").tokenize().tokenRemove();
			}
		}
	});

	// jQuery(document).on("change", ".page_url", function() {
	//   jQuery(this).parent("p").find(".url_value_holder").val(jQuery(this).val());
	//   jQuery(this).parent("p").find(".url_id_holder").val(jQuery(this).val());
	// });

	jQuery(document).on("keyup input", "input.custom_url", function() {
		jQuery(this).parent("p").find(".url_value_holder").val(jQuery(this).val());
	});

	if (window.location.hash !== '') {
		var hash = window.location.hash;
		hash = hash.substring(1, hash.length);
		jQuery("." + hash).find('.section-expander').trigger('click');
	}

	jQuery(document).on("click", "input[type=submit]", function() {
		var regexReplace = [
			[/<div>/gi, '<br>'], //replace div to br
			[/<\/div>/gi, ''] //replace close div tag with empty
		];
		return true;
	});

	var viewPortSize = jQuery(window).height();
	jQuery(window).scroll(function() {
		if (jQuery(document).scrollTop() > viewPortSize) {
			jQuery(".submitdiv:last").attr("style", "");
			jQuery(".submitdiv:last").css({
				"position": "fixed",
				"top": '50px'
			});
		} else {
			jQuery(".submitdiv:last").attr("style", "");
			jQuery(".submitdiv:last").css({
				"position": "relative"
			});
		}
	});

	jQuery(document).on('click', '.collapse-item', function(e) {
		e.preventDefault();
		e.stopPropagation();

		var item_wrap = jQuery(this).parent().parent();

		if (jQuery(this).hasClass("collapsed")) { //show
			jQuery(this).removeClass("collapsed");
			jQuery(this).parent().css({
				"margin": "0 0 10px 0"
			});

			if (item_wrap.hasClass('parent-node')) {
				item_wrap.find("p").not('.child-list p').slideDown();
				item_wrap.find(".upload-wrapper").not('.child-list .upload-wrapper').slideDown();
				item_wrap.find(".geolocator-box").not('.child-list .geolocator-box').slideDown();
				item_wrap.find('.wp-editor-container').not('.child-list .wp-editor-container').parent().slideDown();
				loadGeoboxes(item_wrap.find(".geolocator-box").not('.child-list .geolocator-box'));
			} else {
				item_wrap.find("p").slideDown();
				item_wrap.find(".upload-wrapper").slideDown();
				item_wrap.find(".geolocator-box").slideDown();
				item_wrap.find('.wp-editor-container').parent().slideDown();
				loadGeoboxes(item_wrap.find(".geolocator-box"));
			}
		} else { //hide
			jQuery(this).addClass("collapsed");
			jQuery(this).parent().css({
				"margin": "0"
			});

			item_wrap.find('.child-list .collapse-item').addClass("collapsed");
			item_wrap.find("p").slideUp();
			item_wrap.find(".upload-wrapper").slideUp();
			item_wrap.find(".geolocator-box").slideUp();
			item_wrap.find('.wp-editor-container').parent().slideUp();
		}
		// current title
		var $this_text = jQuery(this).html();
		// toggle title
		var $this_toggle_text = jQuery(this).data("toggle-title");
		// toggle becomes current
		jQuery(this).html($this_toggle_text);
		// current becomes toggle
		jQuery(this).data("toggle-title", $this_text);

		//toggle child items if parent collapsed
		if (jQuery(this).hasClass("collapsed")) {
			jQuery(this).parent().parent().find('.child-list .collapse-item').html($this_toggle_text);
			jQuery(this).parent().parent().find('.child-list .collapse-item').data("toggle-title", $this_text);
		}
	});
	// setTimeout(function() {
	//   jQuery('.mce-i-blockquote, .mce-i-strikethrough, .mce-i-alignleft, .mce-i-alignright, .mce-i-aligncenter, .mce-i-fullscreen').parent().parent().hide();
	// }, 100);

	/**
	 *   GEOLOCATION MODULE
	 *
	 *   Loads exiting maps from latitutde and longtitude and geocodes address
	 **/

	function setMarker(latlng, map, markersArray) {
		clearMarkers(markersArray);
		var marker = new google.maps.Marker({
			map: map,
			position: latlng
		});
		markersArray.push(marker);
	}

	function clearMarkers(markersArray) {
		for (var i = 0; i < markersArray.length; i++) {
			markersArray[i].setMap(null);
		}
		markersArray.length = 0;
	}

	// Load maps
	function loadGeoboxes(wrapper) {
		wrapper.each(function(i, event) {
			var geolocator_box, map, lat, lng, markersArray; //Because of add more funcionality, declare params each time

			map = null;
			latlng = null;
			markersArray = [];
			geolocator_box = document.getElementsByClassName('geolocator-box')[i];

			lat = geolocator_box.getElementsByClassName('gmap_geolocator_lat')[0].getAttribute("value");
			lng = geolocator_box.getElementsByClassName('gmap_geolocator_long')[0].getAttribute("value");
			if (lat && lng) {
				latlng = {
					lat: parseFloat(lat),
					lng: parseFloat(lng)
				};

				var map_DOM = geolocator_box.getElementsByClassName('geolocator-preview')[0]
					.getElementsByClassName('inner')[0]
					.getElementsByClassName('map')[0];

				jQuery(geolocator_box).find('.geolocator-preview .inner .message').addClass('hidden');

				//mapDOM must be DOM element. Not jQuery object!
				map = new google.maps.Map(map_DOM, {
					mapTypeControl: false,
					zoom: 11,
					center: latlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});
				setMarker(latlng, map, markersArray);
			}
		});
	}
	visible_geoboxes = jQuery('.geolocator-box:visible');
	loadGeoboxes(visible_geoboxes);

	// Geocode address
	jQuery(document).on('click', '.geolocator-button', function() {
		var geocoder, geolocator_box, map, latlng, address, markersArray;

		geocoder = new google.maps.Geocoder();
		geolocator_box = jQuery(this).parents('.geolocator-box');
		address = geolocator_box.find('.gmap_geolocator_address').val();
		markersArray = [];

		geocoder.geocode({
			'address': address
		}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				geolocator_box.find('.error').addClass('hidden');
				geolocator_box.find('.geolocator-preview .inner .message').addClass('hidden');
				latlng = results[0].geometry.location;
				map_DOM = geolocator_box.find('.geolocator-preview .inner .map').get(0);
				map = new google.maps.Map(map_DOM, {
					mapTypeControl: false,
					zoom: 11,
					center: latlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});
				map.setCenter(latlng);
				setMarker(latlng, map, markersArray);

				//fill lat and lng form fields
				geolocator_box.find('.gmap_geolocator_lat').val(latlng.k);
				geolocator_box.find('.gmap_geolocator_long').val(latlng.D);
			} else {
				//return error
				geolocator_box.find('.error').remove();
				geolocator_box.append('<div class="error">Geocode was not successful for the following reason: ' + status + '</div>');
			}
		});
	});

	function initializeUrlSuggest($target) {
		if (!$target) {
			$target = jQuery('body');
		}
		jQuery(".page_url:not(.multiautosuggestfield)", $target).each(function() {
			jQuery(this).parent("p").find(".url_value_holder").val(jQuery(this).val());
			jQuery(this).parent("p").find(".url_id_holder").val(jQuery(this).val());
		});

		jQuery("input.custom_url").each(function() {
			jQuery(this).parent("p").find(".url_value_holder").val(jQuery(this).val());
		});
	}

	function initializeTokenize($target) {
		if (!$target) {
			$target = jQuery('body');
		}
		//initialize autocomplete fields with multiple select option
		jQuery('select.multiautosuggestfield', $target).each(function(i, el) {
			el = jQuery(el);
			el.tokenize({
				datas: url_path.ajaxurl + '?action=autocomplete_field_function',
				maxElements: el.attr('data-maxelements'),
				sortable: true,
				nbDropdownElements: 50
			});
		});
	}

});
