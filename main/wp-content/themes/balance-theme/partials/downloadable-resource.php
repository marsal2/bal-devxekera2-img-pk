<?php
global $data;
/* M34 renderer */


if ( !empty( $data['m33_module'] ) && !empty( $data['m33_module'][0] ) ) {
  echo render_m33_section_title_copy_button( $data['m33_module'][0]);
}


if ( !empty( $data['m5_module'] ) && !empty( $data['m5_module'][0] ) ) {
  echo render_m34_article_content( $data['m5_module'][0], '', true, 'article default-content-style', 'text-center');

  if ( !empty( $data['m5_module'][0]['resource'] ) ){
    echo render_button_row( $data['m5_module'][0]['resource']['audio/mpeg']['fullpath'] , $data['m5_module'][0]['cta']);
  }
}
if ( !empty( $data['m34_module'] ) && !empty( $data['m34_module'][0] ) ) {
  echo render_m34_article_content( $data['m34_module'][0], '', true, 'article default-content-style', 'text-center');
}
if ( !empty( $data['m12a_module'] ) && !empty( $data['m12a_module'][0] && !empty($data['m12a_module'][0]['pdf'])) ){
  echo render_button_row( $data['m12a_module'][0]['pdf']['application/pdf']['fullpath'] , 'Download');
}
