$(document).ready(function(){
function navigateLink(form) {
var destination = form.selectNav[form.selectNav.selectedIndex].value;
if (destination != '*') {location.href = destination; }
else {  return false; }
}

if (KJE.IE7and8) KJE.init();

 KJ=new Object();
   KJ.KJECalcNavigation = document.getElementById("KJECalcNavigation");
   KJ.KJECalcNavigation2 = document.getElementById("KJECalcNavigation2");
   KJ.KJENavButton = document.getElementById("KJENavButton");
   KJ.ShowNav = false;

   KJ.KJECalcNav = function(){
     if (KJ.ShowNav) {
       KJ.KJECalcNavigation.style.display='none';
       if (window.innerWidth < 500) KJ.KJECalcNavigation2.style.display='none';
       KJ.KJENavButton.innerHTML='<br><br>More&darr;';
       KJ.ShowNav = false;
     }
     else    {
       KJ.KJECalcNavigation.style.display='block';
       KJ.KJECalcNavigation2.style.display='inline-block';
       KJ.KJENavButton.innerHTML='<br><br>Less&uarr;';
       KJ.ShowNav = true;
     }
   };


   KJ.KJECalcNavResize = function(){
     var iNavOldWidth = window.innerWidth;
     if (iNavOldWidth > 900)   KJ.KJECalcNavigation.style.display='block';
     else  KJ.KJECalcNavigation.style.display='none';

     if (iNavOldWidth > 500)   KJ.KJECalcNavigation2.style.display='inline-block';
     else  KJ.KJECalcNavigation2.style.display='none';

     if (iNavOldWidth < 900) {KJ.ShowNav = false;  KJ.KJENavButton.innerHTML='<br><br>More&darr;';}

   };

   if (window.addEventListener) {
     window.addEventListener('resize', KJ.KJECalcNavResize ,false);
   }

  function KJESetRecents(rvt_div){
    if (!rvt_div) return;

    var aList = new Array();
    for (var i = 0; i < localStorage.length; i++){
      aList[i] = localStorage.key(i) ;
    }
    aList = aList.sort();

    var iList = rvt_div;
    var sList = "";
    for (var i = 0; i < aList.length; i++){
      //  only care about entries without a "_" in them.
      var i_ = aList[i].indexOf("_");
      if (i_ < 0) {
        var name = (aList[i]+"_name");
        var iCutOff = aList[i].indexOf('#');
        if (iCutOff>0) sIndex = aList[i].substring(0,iCutOff);
        else sIndex =  aList[i];

        var sHref = sIndex +"_href";
        var sTitle = sIndex +"_title";
  if (localStorage.getItem(sTitle)!=null) {
    sList += "<dt style='line-height:0px'><div class='rvt_line' style='line-height:0px'><div class='rvt_line_lead' style='line-height:0px'>&#9829;</div></dt><dd><a href='"+ localStorage.getItem(sHref)+"?KJEData="+ localStorage.getItem(aList[i]) +"'>"+localStorage.getItem(sTitle)+(iCutOff>0?" : "+localStorage.getItem(name) :"")+"</a></div></dd>";
     }
      }
    }
    iList.innerHTML = '<dl><div class="rvt_title">Recently saved:</div>'+sList+'</dl>';
  }
  KJESetRecents(document.getElementById("KJERecents"));

});