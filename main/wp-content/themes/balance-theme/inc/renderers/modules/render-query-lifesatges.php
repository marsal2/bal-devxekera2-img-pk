<?php 


function mainLifeStages($lifestage){
    global $wpdb;
    $lsWhere  = '';
    if($lifestage == '0' OR $lifestage == 0 OR $lifestage == ''){
      $lifestage = '';
    }
   if($lifestage != ''){
        $lifestage = getSubLifeStages($lifestage);
    $lsSelectSql =" SELECT  GROUP_CONCAT(ag.`post_type`) AS post_type,GROUP_CONCAT(ag.`term__in`) AS term__in,GROUP_CONCAT(ag.`term__not_in`) AS term__not_in,
    GROUP_CONCAT(ag.`post__in`) AS post__in,GROUP_CONCAT(ag.`post__not_in`) AS post__not_in FROM `wp_postmeta` wpm INNER JOIN `wp_access_groups` ag WHERE wpm.`post_id` in ({$lifestage}) AND wpm.`meta_key`='_page_edit_data' 
        AND FIND_IN_SET(ag.`wp_post_id`,(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( wpm.`meta_value`, '\"accessgroups\"' , -1), '\";}}s:',1),'\"',-1))) ";

            // $lsResult = $wpdb->prepare($lsSelectSql);
            // $lsResult->execute();
            // $lsRow =$lsResult->fetch(PDO::FETCH_ASSOC);
            $lsResult = $wpdb->get_row($lsSelectSql,ARRAY_A);
            if($lsResult){
                $lsRow = $lsResult;
                $post_type = "'".str_replace(",", "','",delComma($lsRow['post_type'])). "'";
                $term__in = delComma($lsRow['term__in']);
                $term__in = ($term__in)?$term__in:0;
                $term__not_in = delComma($lsRow['term__not_in']);
                $term__not_in = ($term__not_in)?$term__not_in:0;
                $post__in = delComma($lsRow['post__in']);
                $post__in = ($post__in)?$post__in:0;
                $post__not_in = delComma($lsRow['post__not_in']);
                $post__not_in = ($post__not_in)?$post__not_in:0;

                $lsWhere .= " AND (";
                $lsWhere .= " (P.post_type in ({$post_type}) ";
                $lsWhere .= "  OR P.ID in (SELECT object_id FROM `wp_term_relationships` WHERE `term_taxonomy_id` in ({$term__in})) ";
                $lsWhere .= " OR P.ID in ({$post__in}) )  ";

                $lsWhere .= " AND (P.ID not in (SELECT object_id FROM `wp_term_relationships` WHERE `term_taxonomy_id` in ({$term__not_in})) ";
                $lsWhere .= " and P.ID not in ({$post__not_in})) ";
                $lsWhere .= " )";

            }
        }
    return $lsWhere;
}

// function getParentLifeStages(){
//     global $wpdb;
//     $lsSelectSqlParent = "SELECT  GROUP_CONCAT(P.ID) AS id FROM wp_posts AS P WHERE P.post_type = 'life_stage' AND P.post_status = 'publish' AND P.`post_parent` = 0 ";

//     $lsRowParent = $wpdb->get_row($lsSelectSqlParent,ARRAY_A);
//     if($lsRowParent){
//         $lifestage .=  ','.$lsRowParent['id'];
//     }
//     $lifestage = trim($lifestage,',');
//     return $lifestage;
// }

function getSubLifeStages($lifestage){
    global $wpdb;
    $lsSelectSqlSub = "SELECT  GROUP_CONCAT(P.ID) AS id FROM wp_posts AS P WHERE P.post_type = 'life_stage' AND P.post_status = 'publish' AND P.`post_parent` in ({$lifestage}) ";
    $lsRowSub = $wpdb->get_row($lsSelectSqlSub,ARRAY_A);
  
    if($lsRowSub){
        $lifestage .=  ','.$lsRowSub['id'];
    }
    $lifestage = trim($lifestage,',');

    return $lifestage;
}

function delComma($strVal){
    $string = preg_replace("/,+/", ",", $strVal);
    return trim($string,',');
}
?>