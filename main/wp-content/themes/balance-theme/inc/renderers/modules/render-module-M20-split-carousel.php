<?php

function render_m20_split_carousel( $module_data ) {
	$output = '';

	if( empty( $module_data['slides'][0]['uploadimage']['image']['fullpath'] ) ){
		return '';
	}

	if ( !empty( $module_data['slides'] ) && count( $module_data['slides'] ) > 0 ) {
		$output .= '<!-- M20: SPLIT CAROUSEL -->';
		$output .= '<section aria-label="CAROUSEL" class="slide-block background-grey">';
		$output .= ' <div class="container">';
		$output .= '   <div class="row">';
		$output .= '     <div class="slider-slick">';
		if ( !empty( $module_data['title'] ) ) {
			$output .= '       <h1 class="text-info text-center">' . $module_data['title'] . '</h1>';
		}
		$output .= '       <div class="slick-mask">';
		$output .= '         <div class="slick-slideset">';
		foreach ( $module_data['slides'] as $key => $slide ) {
			$output .= '           <div class="slick-slide">';
			if ( !empty( $slide['uploadimage']['image']['fullpath'] ) ) {
				$output .= '             <div class="img-holder"><img src="' . $slide['uploadimage']['image']['fullpath'] . '" alt="image description" width="220" height="220"></div>';
			}
			$output .= '             <div class="caption-holder">';
			if ( !empty( $slide['title'] ) ) {
				$output .= '               <h2>' . $slide['title'] . '</h2>';
			}
			if ( !empty( $slide['description'] ) ) {
				$output .= '               <p>' . $slide['description'] . '</p>';
			}
			$output .= '             </div>';
			$output .= '           </div>';
		}

		$output .= '          </div>';
		$output .= '        </div>';
		$output .= '      </div>';
		$output .= '    </div>';
		$output .= '  </div>';
		$output .= '</section>';
		$output .= '<!-- end M20: SPLIT CAROUSEL -->';
	}
	return stripslashes( $output );
}
