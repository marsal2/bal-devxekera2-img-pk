<?php

function render_m85_form_email_field( $field, $form_id = '', $value = '' ) {
  // assign correct field html type

  $field_id = 'input_' . $form_id . '_' . $field['id'];

  $output_field = '';
  $logic_event = get_field_conditional_logic( $form_id, $field, 'keyup' );
  if ( $field['emailConfirmEnabled'] ) {

    $email_value = is_array( $value ) ? $value[0] : $value;
    $email_value = esc_attr( $email_value );
    $confirmation_value = is_array( $value ) ? $value[1] : rgpost( 'input_' . $field['id'] . '_2' );
    $confirmation_value = esc_attr( $confirmation_value );

    $enter_email_field_input = GFFormsModel::get_input( $field, $field['id'] . '' );
    $confirm_field_input     = GFFormsModel::get_input( $field, $field['id'] . '.2' );

    $enter_email_placeholder = rgar( $enter_email_field_input, 'customLabel' ) != '' ? $enter_email_field_input['customLabel'] : esc_html__( 'Enter Email', 'balance' );
    $confirm_email_placeholder = rgar( $confirm_field_input, 'customLabel' ) != '' ? $confirm_field_input['customLabel'] : esc_html__( 'Confirm Email', 'balance' );

    $output_field .= '<span id="' . $field_id . '_1_container" class="ginput_left">';
    $output_field .= '  <input class="" type="text" name="input_' . $field['id'] . '" id="' . $field_id . '" value="' . $email_value . '" placeholder="' . $enter_email_placeholder . '" ' . $logic_event . '>';
    $output_field .= '</span>';
    $output_field .= '<span id="' . $field_id . '_2_container" class="ginput_right">';
    $output_field .= '  <input class="" type="text" name="input_' . $field['id'] . '_2" id="' . $field_id . '_2" value="' . $confirmation_value . '" placeholder="' . $confirm_email_placeholder . '" >';
    $output_field .= '</span>';
  } else {
    $output_field .= '  <input id="input_' . $form_id . '_' . $field['id'] . '" name="input_' . $field['id'] . '" type="email" placeholder="' . $field['placeholder'] . '" value="' . $value . '" ' . $logic_event . '>';
  }

  $output = '';
  $output = sprintf( get_form_field_wrap( $field, $form_id ), $output_field );
  
  return stripslashes( $output );
}
