<?php
/*
 * Custom Taxonomy: Program Category
 *
 */

add_action( 'init', 'create_program_taxonomy' );

function create_program_taxonomy() {
	$post_types = array( 'program' );
	$labels = array(
		'name'              => _x( 'Category', 'taxonomy general name' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
		'search_items'      => __( 'Search' ),
		'all_items'         => __( 'All' ),
		'parent_item'       => __( 'Parent' ),
		'parent_item_colon' => __( 'Parent:' ),
		'edit_item'         => __( 'Edit' ),
		'update_item'       => __( 'Update' ),
		'add_new_item'      => __( 'Add New' ),
		'new_item_name'     => __( 'New Name' ),
		'menu_name'         => __( 'Category' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'show_in_menu' => false
	);
	register_taxonomy( 'program_category', $post_types, $args );
}
?>
