<?php
/*
 *  WP Edit module: M34
 *  Description: Module section: M34 - Title, Copy
 */

function m34_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data;

  if( empty( $data['m34_module'][ $key ] ) ){
    $data = init_array_on_var_and_array_key($data, 'm34_module');
    $data['m34_module'][ $key ] = array(
      'title' => '',
      'copy' => ''
    );
  }

  if ( empty( $custom_settings ) ) {
  	$custom_settings = array('media_buttons' => false, 'quicktags' => true );
  }
    $getContactPageVal = "input[name='m34_module[1][whitelabel_contact_page_enable]']";
    $getContactFormVal = "input[name='m34_module[1][whitelabel_contact_form_enable]']";

  $output = '';
    $output .= '<style>
  .box-div-settings {
    border: solid 3px #cccccc;
    padding: 10px 20px 20px 20px;
    margin-bottom: 10px;
  }
</style>';
  $output .= '<a name="m34-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper m34-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom m34-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside hidden">';
  if($visible_on == 'tab-2'){
      $output .= '  <div class="box-div-settings">';

      $output .= '     <p>';
      $output .= '          <label><b>'. __( 'Do you want to enable Contact page?', 'balance' ) .'</b></label><br><br>';
      $output .=  checkbox_field( $data['m34_module'][$key]['whitelabel_contact_page_enable'], 'm34_module['.$key.'][whitelabel_contact_page_enable]',false,'toggleCheck formshowhide');

      //$output .=              radiobuttonlist_field( $data['m34_module'][$key]['whitelabel_contact_page_enable'], 'm34_module['.$key.'][whitelabel_contact_page_enable]', array( 'Yes' => __( 'Yes', 'balance' ), 'No' => __( 'No', 'balance' ) ), 'y', true);
      $output .= '      </p>';
      $output .= '     <p>';
      $output .= '          <label><b>'. __( 'Do you want to enable Contact form?', 'balance' ) .'</b></label><br><br>';
      $output .=  checkbox_field($data['m34_module'][$key]['whitelabel_contact_form_enable'], 'm34_module['.$key.'][whitelabel_contact_form_enable]',false,'toggleCheck formshowhide');
      //$output .=              radiobuttonlist_field( $data['m34_module'][$key]['whitelabel_contact_form_enable'], 'm34_module['.$key.'][whitelabel_contact_form_enable]', array( 'Yes' => __( 'Yes', 'balance' ), 'No' => __( 'No', 'balance' ) ), 'n', true);

      $output .= '      </p>';
      $output .= '     <p>';
      $output .= '          <label><b>'. __( 'Do you want to enable Contact Number?', 'balance' ) .'</b></label><br><br>';
      $output .=  checkbox_field($data['m34_module'][$key]['whitelabel_contact_number_enable'], 'm34_module['.$key.'][whitelabel_contact_number_enable]',false,'toggleCheck');
      //$output .=              radiobuttonlist_field( $data['m34_module'][$key]['whitelabel_contact_number_enable'], 'm34_module['.$key.'][whitelabel_contact_number_enable]', array( 'Yes' => __( 'Yes', 'balance' ), 'No' => __( 'No', 'balance' ) ), 'y', true);

      $output .= '      </p>';
      $output .= ' </div>';
  }

  $output .= '      <p>';
  $output .= '       <label><b>'. __( 'Title', 'balance' ) .':</b></label>';
  $output .=          text_field( $data['m34_module'][ $key ]['title'], 'm34_module['.$key.'][title]');
  $output .= '      </p>';

  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Copy', 'balance' ) .':</b></label><br/><br/>';
  $output .=          textarea_field( $data['m34_module'][ $key ]['copy'], 'm34_module['.$key.'][copy]', true, 20, '', '', $custom_settings );
  $output .= '      </p>';

  $output .= '    </div>';
  $output .= '  </div>';
    $output .= '<script type="text/javascript">';
    $output .= 'jQuery(document).ready(function(){';
    $output .= 'valueOld = jQuery("'.$getContactPageVal.'", ".inside").is(":checked");';
    $output .= 'jQuery(".inside .formshowhide:checkbox").addClass("toggleCheck");';
    $output .= 'checkFormStatus();';
    $output .= '});';
    $output .= '</script>';
    $output .= '<script>function checkFormStatus(){
   
        if(valueOld === false){
            jQuery("'.$getContactFormVal.'", ".inside").attr("disabled",true);
        }else{
          jQuery("'.$getContactFormVal.'", ".inside:checkbox").attr("disabled",false);
        }
        jQuery(".inside input.formshowhide").on("change" , function (){
            valueIs = jQuery("'.$getContactPageVal.':checked", ".inside").is(":checked");
            if(valueIs === false){
               valueIs = jQuery("'.$getContactFormVal.'", ".inside").is(":checked");
               jQuery("'.$getContactFormVal.'", ".inside").attr({"disabled":true,"checked":false});
            }
            else {
                jQuery("'.$getContactFormVal.'", ".inside").attr("disabled",false);
            }
        });   
    }
    </script>';

   /**** for register form****/
  
   $array_time = array();
   for($i=1;$i<=12;$i++){
	   if($i<10) $i='0'.$i;
	   $array_time[] =  array(
      'id' => $i,
      'title' => $i);    
   }
   $array_time_am = array();   
   $array_time_am[] =  array(
  'id' => 'am',
  'title' => 'AM');
	
	$array_time_am[] =  array(
  'id' => 'pm',
  'title' => 'PM');	
   
  $total_register_hidden =0;
  //$total_register_hidden_1 =0;
  
  if($data['m34_module'][ $key ]['title']=='Workshops'){
  $output .= '      <div class="postbox postbox-custom">';
  $output .= '      <div class="inside">';
  $output .= '      <div id="form-register" class="items-list ui-sortable">';
    $total_register = count($data['workshop']);
	if($total_register>1)	
    $total_register_hidden = $total_register-1;
    //$total_register_hidden_1 = $total_register_hidden;
	if($total_register>0){
	 //for($c=0;$c<$total_register;$c++){
	 $k=0;		
	 $c=0;	
	 $output .='<table class="wp-list-table widefat fixed striped posts" style="margin-bottom: 5px;">';	
	  $output .= '      <tr><th style="font-weight: bold;">Title</th><th style="font-weight: bold;">Date</th><th style="font-weight: bold;">Time(PST)</th><th style="font-weight: bold;">Registration Open Date</th><th style="font-weight: bold;">Link to Register</th><th style="font-weight: bold;">Action</th></tr>';
	  $output .= '      </table>';
	 foreach($data['workshop'] as $m=>$v){	
	  $srno = $k+1;	
	  $evdate = date('D, F d, Y',strtotime($v['workshop_end_date']));
	  $end_minute = trim($v['workshop_end_min']);
		if(strlen($end_minute)==1) $end_minute='0'.$end_minute;
		//$end_minute = date("i", strtotime($end_minute));
		
		$start_minute = trim($v['workshop_start_min']);
		if(strlen($start_minute)==1) $start_minute='0'.$start_minute;
		//$start_minute = date("i", strtotime($start_minute));
		
		$start_time = $v['workshop_start_hour'].':'.$start_minute.' '.$v['workshop_start_am'];
		$end_time = $v['workshop_end_hour'].':'.$end_minute.' '.$v['workshop_end_am'];
		
	  $output .= '      <div class="" id="item-show-'.$srno.'">';
	  $output .='<table class="wp-list-table widefat fixed striped posts" style="margin-bottom: 5px;">';
	  $output .= '<tr>';
      $output .= '<td data-label="Title">'.$v['workshop_register_title'].'</td>';
	  $output .= '<td data-label="Date">'.$evdate.'</td>';
	  $output .= '<td data-label="Time">'.$start_time.' to '.$end_time.'</td>';
	  $output .= '<td data-label="Registration">'.$v['workshop_open_date'].'</td>';
	  if(trim($v['workshop_Link'])!=''){
		  $has_link = stristr($v['workshop_Link'], 'http://') ?: stristr($v['workshop_Link'], 'https://');
		  if($has_link){
			 $output .= '<td data-label="Link"><a href="'.$v['workshop_Link'].'" target="_blank">Link</a></td>';
		  }
		  else $output .= '<td data-label="Link">'.$v['workshop_Link'].'</td>';
	  }	 
	  else $output .= '<td data-label="Link">-</td>';
	  $output .= '<td data-label="Link"><input type="button" class="button" value="Show" id="button-'.$srno.'" onclick="showDiv('.$srno.')"> <input type="button" class="button" value="Delete" id="delbutton-'.$srno.'" onclick="DeleteRegister('.$srno.')"></td>';
	  $output .= '</tr>';
	  $output .= '      </table>';
	  $output .= '      </div>';
	  $output .= '      <div class="item hidden" id="item-'.$srno.'">';
	  $output .= '      <h4>Register '.$srno.'#</h4>';
	 
	  $output .= '      <p>';
	  $output .= '        <label><b>'. __( 'Title', 'register' ) .':</b></label>';
	  $output .=          text_field( $v['workshop_register_title'], 'workshop['.$c.'][workshop_register_title]');
	  $output .= '      </p>';
	  
	  $output .= '      <p>';
	  $output .= '        <label><b>'. __( 'Registration Open Date', 'register' ) .':</b></label><BR>';
	  $output .=          text_field( $v['workshop_open_date'], 'workshop['.$c.'][workshop_open_date]','','custom-datepicker');
	  $output .= '      <br><span>[Hint: Registration open date should be smaller than event date]</span>';
	  $output .= '      </p>';
	  
	  $output .= '      <p>';
	  $output .= '        <label><b>'. __( 'Event Date', 'register' ) .':</b></label><BR>';
	  $output .=          datepicker_field( $v['workshop_end_date'], 'workshop['.$c.'][workshop_end_date]');
	 
	  $output .= '      </p>';
	  $output .= '      <p>';	
 	  $output .=         ' From '. dropdown_field( $v['workshop_start_hour'], 'workshop['.$c.'][workshop_start_hour]', '', $array_time, '', false ).'Hours &nbsp;&nbsp;';
	  $output .=          number_field($v['workshop_start_min'], 'workshop['.$c.'][workshop_start_min]',0,'','',0,59).'Mins ';
	  $output .=          dropdown_field( $v['workshop_start_am'], 'workshop['.$c.'][workshop_start_am]', '', $array_time_am, '', false );
	  
	  $output .='&nbsp;&nbsp;To &nbsp;&nbsp;';
	  $output .=          dropdown_field( $v['workshop_end_hour'], 'workshop['.$c.'][workshop_end_hour]', '', $array_time, '', false ).'Hours &nbsp;&nbsp;';
	  $output .=          number_field($v['workshop_end_min'], 'workshop['.$c.'][workshop_end_min]',0,'','',0,59).'Mins';
	 
	  $output .=          dropdown_field( $v['workshop_end_am'], 'workshop['.$c.'][workshop_end_am]', '', $array_time_am, '', false );
	  $output .= '      </p>';
	 
	  
	  $output .= '      <p>';
	  $output .= '        <label><b>'. __( 'Link to Register', 'register' ) .':</b></label>';
	  $output .=          text_field( $v['workshop_Link'], 'workshop['.$c.'][workshop_Link]');
	  $output .= '      <span>[Hint: https://outlook.live.com]</span>';
	  $output .= '      </p>';
	 
	  $output .= '      </div>';
	  $k++;
	  $c++;
	 }	
	}	
  else {
 
  $output .= '      <div class="item" id="item-1">';
  $output .= '      <h4>Register 1#</h4>';
 
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Title', 'register' ) .':</b></label>';
  $output .=          text_field( '', 'workshop[0][workshop_register_title]');
  $output .= '      </p>';
  
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Registration Open Date', 'register' ) .':</b></label><BR>';
  $output .=          text_field( '', 'workshop[0][workshop_open_date]','','custom-datepicker');
  $output .= '      <br><span>[Hint: Registration open date should be smaller than event date]</span>';
  $output .= '      </p>';
  
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Event Date', 'register' ) .':</b></label><BR>';
  $output .=          datepicker_field('', 'workshop[0][workshop_end_date]');
  $output .= '      </p>';
  
 $output .= '      <p>';
 // $output .= '        <label><b>'. __( 'Time', 'register' ) .':</b></label><BR>';
  $output .=          ' From '.dropdown_field( '', 'workshop[0][workshop_start_hour]', '', $array_time, '', false ).'Hours &nbsp;&nbsp;';
  $output .=          number_field('', 'workshop[0][workshop_start_min]',0,'','',0,59).'Mins';

  $output .= 		  dropdown_field( '', 'workshop[0][workshop_start_am]', '', $array_time_am, '', false );
  $output .=' &nbsp;&nbsp;To &nbsp;&nbsp;';
  $output .=          dropdown_field( '', 'workshop[0][workshop_end_hour]', '', $array_time, '', false ).'Hours &nbsp;&nbsp;';
  $output .=          number_field('', 'workshop[0][workshop_end_min]',0,'','',0,59).'Mins';
//  $output .= '      </p>';
  $output .= 		  dropdown_field( '', 'workshop[0][workshop_end_am]', '', $array_time_am, '', false );
  $output .= '      </p>';
  
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Link to Register', 'register' ) .':</b></label>';
  $output .=          text_field( $data['workshop_link'], 'workshop[0][workshop_Link]');
  $output .= '      <span>[Hint: https://outlook.live.com]</span>';
  $output .= '      </p>';
 
  $output .= '      </div>';
  
  }
   $output .= '      </div>';
   
  $output .= '      <div class="">&nbsp;<p><input type="button" class="button add-new-item" value="Add New" onclick="addRegister()"> &nbsp;<input type="button" class="button add-new-item hidden" value="Remove" onclick="removeRegister()" id="removebutton"></p>';
 
  $output .= '      <input type="hidden" id="workshop_count" value="'.$total_register_hidden.'">';
  $output .= '      <input type="hidden" id="workshop_count_1" value="'.$total_register_hidden.'">';
  $output .= '      </div>';
  $output .= '      </div>';
  $output .= '      </div>';

  $output .= '  
  <script>function addRegister(){
	var workshop_count = jQuery("#workshop_count").val();
	var num= parseInt(workshop_count)+1;
	//if(num==2){
		jQuery("#removebutton").show();
	//}	
	var srnum = num+1;
	jQuery("#workshop_count").val(num);
	let html=`<div class="item" id="item-${srnum}"><h4 class="ui-sortable-handle">Register ${srnum}#</h4>      <p>        <label><b>Title:</b></label><input type="text" class="" name="workshop[${num}][workshop_register_title]" value="">      </p>    
<p>        <label><b>Registration Open Date:</b></label><br>'.text_field( "", "workshop[".'${num}'."][workshop_open_date]","","custom-datepicker").'<br><span>[Hint: Registration open date should be smaller than event date]</span></p>  	<p>        <label><b>Event Date:</b></label><br>'.datepicker_field( "", "workshop[".'${num}'."][workshop_end_date]").'</p><p> From'.dropdown_field( '', "workshop[".'${num}'."][workshop_start_hour]", '', $array_time, '', false ).'Hours &nbsp;&nbsp;'.number_field("","workshop[".'${num}'."][workshop_start_min]",0,"","",0,59).'Mins '.dropdown_field( 'am', "workshop[".'${num}'."][workshop_start_am]", '', $array_time_am, '', false ).'&nbsp;&nbsp;To &nbsp;&nbsp;'.dropdown_field( '', "workshop[".'${num}'."][workshop_end_hour]", '', $array_time, '', false ).'Hours &nbsp;&nbsp;'.number_field("","workshop[".'${num}'."][workshop_end_min]",0,"","",0,59).'Mins'.dropdown_field( 'pm', "workshop[".'${num}'."][workshop_end_am]", '', $array_time_am, '', false ).'      </p>          <p>        <label><b>Link to Register:</b></label>'.text_field("", "workshop[".'${num}'."][workshop_Link]").'    <span>[Hint: https://outlook.live.com]</span> </p>      </div>`;	
	jQuery("#form-register").append(html);
}
    function removeRegister(){	
    var workshop_count = jQuery("#workshop_count").val();
    var workshop_count_1 = jQuery("#workshop_count_1").val();
    var removeid = 	parseInt(workshop_count)+1;
    
    jQuery("#item-"+removeid).remove();
    jQuery("#item-show-"+removeid).remove();
        var num= parseInt(workshop_count)-1;
        jQuery("#workshop_count").val(num);
        if(num==workshop_count_1){
        jQuery("#removebutton").hide();
    } else jQuery("#removebutton").show();
        
    }
    function showDiv(divid){
        jQuery("#item-"+divid).toggle();
        if(jQuery("#button-"+divid).attr("value") == "Show") {
            jQuery("#button-"+divid).attr("value", "Hide");
        } else {
            jQuery("#button-"+divid).attr("value","Show");
        }
    }
    function DeleteRegister(removeid){
        jQuery("#item-"+removeid).remove();
        jQuery("#item-show-"+removeid).remove();
    }

</script>';
  
  } 
 /**** end for register form****/
  
  $output .= '</div>';
  return $output;

}

?>
