<?php
/*
 *  WP Edit module: M55
 *  Description: Module with 5 columns on desktop. M55 - Listed life stages
 */

function m55_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data;

  if( empty( $data['m55_module'][ $key ] ) ){
    $data = init_array_on_var_and_array_key($data, 'm55_module');
    $data['m55_module'][ $key ] = array(
      'title' => '',
      'copy' => '',
    );
  }

  if( empty($data['m55_module'][ $key ]['showlifestages']) ){
    $data['m55_module'][ $key ]['showlifestages'] = '';
  }

  $showlogin = array( 
    'showlifestages' => __( 'Hide listed Life Stages', 'balance' ),
  );

  if ( empty( $custom_settings ) ) {
    $custom_settings = array('media_buttons' => false, 'quicktags' => true );
  }

  $output = '';
  $output .= '<a name="m55-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper m55-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom m55-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside hidden">';

  $output .= '        <p>';
  $output .= '          <label>'. __( 'Title', 'balance' ) .':</label>';
  $output .=            text_field( $data['m55_module'][ $key ]['title'], 'm55_module['.$key.'][title]');
  $output .= '        </p>';

  $output .= '        <p>';
  $output .= '          <label>'. __( 'Copy', 'balance' ) .':</label>';
  $output .=            textarea_field( $data['m55_module'][ $key ]['copy'], 'm55_module['.$key.'][copy]', true, 10, '', '', $custom_settings );
  $output .= '        </p>';

  $output .= '        <p>';
  $output .=            checkboxlist_field( $data['m55_module'][ $key ]['showlifestages'], 'm55_module['.$key.'][showlifestages]', $showlogin);
  $output .= '        </p>';

  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;

}

?>
