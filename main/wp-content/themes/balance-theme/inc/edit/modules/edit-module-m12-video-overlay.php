<?php
/*
 *  WP Edit module: M12
 *  Description: Module hero. M12 – Overlay with video
 */

function m12_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data;

$output = simple_edit_module($key, 'm12', array(
    array(
          'type' => 'text',
          'name' => 'video',
          'label' => 'Video link',
          'default_value' => array()
        ),
  ), $data, $module_title, $visible_on);


  return $output;

}

?>
