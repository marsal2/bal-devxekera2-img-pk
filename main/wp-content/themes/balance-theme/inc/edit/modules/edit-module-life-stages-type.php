<?php
/*
 *  WP Edit module: Life Stage Type selector
 *  Description: Module with type selector for life stage or life experience. On change only the modules of the selected type gets displayed
 */
function life_stages_type_module_form( $key ) {
  global $data;
  global $post_id;

  if ( empty($data['life_stage_type'][ $key ]) ) {
    $data = init_array_on_var_and_array_key($data, 'life_stage_type');
    $data['life_stage_type'][ $key ] = array(
      'type' => 'life_stage',
      'multiautocompletefield' => ''
    );
  }
  $array_of_types = array(
    0 => array(
      'id' => 'life_stage',
      'title' => 'Life Stage',
    ),
    1 => array(
      'id' => 'life_experience',
      'title' => 'Life Experience'
    )
  );

  $parent_args = array(
    'post_type' => 'life_stage',
    'posts_per_page' => -1,
    'post_status' => 'publish',
    'post_parent' => 0,
    'post__not_in' => array( $post_id ), //prevent including curent post
    'meta_query' => array( //prevent including children that are unassigned
      array(
        'key' => '_wp_page_template',
        'value' => 'life_experience',
        'compare' => '!=',
      ),
    ),
  );

  $output = '';
  $output .= '<div class="postbox types-selector-wrapper">';
  $output .= '  <div class="inside">';
  $output .= '    <p>';
  $output .= '      <label>';
  $output .= '        <span class="text">'. __( 'Select type: ') .'</span>';
  $output .= '      </label>';
  $output .=        dropdown_field( $data['life_stage_type'][ $key ]['type'], 'life_stage_type['.$key.'][type]', '', $array_of_types, 'types-selector button button-secondary button-large', false );
  $output .= '      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
  $output .= '      <label class="hidden" data-visible-on="life_experience">';
  $output .= '        <span class="text">'. __( 'Select Parent: ') .'</span>';
  $output .=          multi_autocomplete_field( $data['life_stage_type'][ $key ]['multiautocompletefield'], 'life_stage_type['.$key.'][multiautocompletefield]', $parent_args, 1, '', 'medium');
  $output .= '      </label>';
  $output .= '    </p>';
  $output .= '  </div>';
  $output .= '</div>';

  return $output;
}
?>
