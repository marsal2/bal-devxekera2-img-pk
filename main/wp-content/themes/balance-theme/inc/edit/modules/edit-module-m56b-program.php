<?php
/*
 *  WP Edit module: M56b
 *  Description: Module for single program settings
 */

function m56b_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data, $resources_cpts;

  if( empty( $data['m56b_module'][ $key ] ) ){
    $data = init_array_on_var_and_array_key($data, 'm56b_module');
    
    $data['m56b_module'][ $key ] = array(
      'program_type' => '',
      'link_to_old_site' => '',
      'program_resources' => array(
        0 => array(
          'title' => '',
          'list' => ''
        )
      ),
      'resource_as_program' => ''
    );
  }
  // arguments for autocomplete field
  $resources_args = array(
    'post_type' => $resources_cpts,
    'posts_per_page' => -1
  );

  if ( empty( $data['m56b_module'][ $key ]['program_resources'] ) ) {
  	$data['m56b_module'][ $key ]['program_resources'] = array(
        0 => array(
          'title' => '',
          'list' => ''
        )
      );
  }

  $value_holder = !empty( $data['m56b_module'][ $key ]['program_type'] )  ? $data['m56b_module'][ $key ]['program_type'] :  'link_to_old_site';
  $output = '';
  $output .= '<input type="radio" id="'.$data['m56b_module'][ $key ]['program_type'].'-'.$key.'" name="'.$data['m56b_module'][ $key ]['program_type'].'" value="'.$key.'" ' . checked( ( !empty( $value_holder )  ? $value_holder :  $default_value ), $key, false ) . ' />';

  $output = '';
  $output .= '<a name="m56b-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper m56b-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom m56b-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside hidden">';

  $output .= '        <p>';
  $output .= '          <input type="radio" name="m56b_module['.$key.'][program_type]" id="m56b_module['.$key.'][program_type][0]" value="link_to_old_site" ' . checked( $value_holder, 'link_to_old_site', false ) . '>';
  $output .= '          <label for="m56b_module['.$key.'][program_type][0]">' . __( 'Link to old site', 'balance' ) . '</label>';
  $output .= '        </p>';

  $output .= '        <p>';
  $output .=              text_field( $data['m56b_module'][ $key ]['link_to_old_site'], 'm56b_module['.$key.'][link_to_old_site]', '', '', __( 'Enter full URL', 'balance' ) );
  $output .= '        </p>';

  $output .= '        <p>';
  $output .= '          <input type="radio" name="m56b_module['.$key.'][program_type]" id="m56b_module['.$key.'][program_type][1]" value="program_resources" ' . checked( $value_holder, 'program_resources', false ) . '>';
  $output .= '          <label for="m56b_module['.$key.'][program_type][1]">' . __( 'List of resources (M1 and M38 copies from below will be used to display this program)', 'balance' ) . '</label>';
  $output .= '        </p>';

  //Repeated fields
  $output .= '        <div class="items-list">';

  foreach ( $data['m56b_module'][ $key ]['program_resources'] as $item_key => $item ) {
    //Single item in repeater
    $output .= '          <div class="item">';
    $output .= '            <h4>';
    $output .=                __( 'SECTION', 'balance' );
    $output .= '              #<span class="counter">'. ( $item_key + 1 ) .'</span>';
    $output .= '              <a class="remove-item description fright">'. __( 'Remove Item', 'balance' ) .'</a>&nbsp;';
    $output .= '              <a class="collapse-item description fright" data-toggle-title="'. __( 'Expand Item', 'balance' ) .'">'. __( 'Collapse Item', 'balance' ) .'</a>';
    $output .= '            </h4>';

    //Text field
    $output .= '            <p>';
    $output .= '              <label><b>'. __( 'Title of section', 'balance' ) .':</b></label><br/>';
    $output .=                text_field( $item['title'], 'm56b_module['.$key.'][program_resources]['.$item_key.'][title]' );
    $output .= '            </p>';

    $output .= '        <p>';
    $output .= '              <label><b>'. __( 'Section resources', 'balance' ) .':</b></label><br/>';
    $output .=              multi_autocomplete_field( $item['list'], 'm56b_module['.$key.'][program_resources]['.$item_key.'][list]', $resources_args);
    $output .= '        </p>';

    $output .= '          </div>';
  }

  $output .= '          <input type="button" class="button add-new-item" value="'. __( 'Add New', 'balance' ) .'" />';
  $output .= '      </div>';
  $output .= '      <br/>';

  $output .= '        <p>';
  $output .= '          <input type="radio" name="m56b_module['.$key.'][program_type]" id="m56b_module['.$key.'][program_type][2]" value="resource_as_program" ' . checked( $value_holder, 'resource_as_program', false ) . '>';
  $output .= '          <label for="m56b_module['.$key.'][program_type][2]">' . __( 'Single resource (M1 and M38 copies from below will NOT be used to display this program)', 'balance' ) . '</label>';
  $output .= '        </p>';

  $output .= '        <p>';
  $output .=              multi_autocomplete_field( $data['m56b_module'][ $key ]['resource_as_program'], 'm56b_module['.$key.'][resource_as_program]', $resources_args);
  $output .= '        </p>';

  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;

}

?>
