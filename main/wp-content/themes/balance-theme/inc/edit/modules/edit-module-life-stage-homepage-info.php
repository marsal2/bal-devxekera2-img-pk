<?php
/*
 *  WP Edit module: Life Stage Homepage info
 */
function life_stage_homepage_info_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
	global $data;

	$output = simple_edit_module( $key, 'life_stage_homepage_info', array(
			array(
				'type' => 'text',
				'name' => 'title',
				'label' => 'Title',
				'default_value' => ''
			),
			array(
				'type' => 'upload',
				'name' => 'image',
				'label' => 'Image',
				'default_value' => '',
				'additional_params' => array(
					'',
					'Image',
					'image'
				)
			),
			array(
				'type' => 'repeater',
				'name' => 'bullets',
				'label' => 'Item',
				'default_value' => array(),
				'fields' => array(
					array(
						'type' => 'text',
						'name' => 'copy',
						'label' => 'Bullet text',
						'default_value' => ''
					)
				)
			),
			array(
				'type' => 'text',
				'name' => 'buttontext',
				'label' => 'Button Text',
				'default_value' => ''
			)
		), $data, $module_title, $visible_on );

	return $output;
}
?>
