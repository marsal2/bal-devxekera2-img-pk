<?php
/*
 *  WP Edit module: M56, M17
 *  Description: Module with three or four columns on desktop. M56 - Three messages with names, M17 – Four messages with names
 */

function m56_m17_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
	global $data;

	$output = simple_edit_module( $key, 'm56', array(
			array(
				'type' => 'radiobuttonlist',
				'name' => 'radiochoice',
				'default_value' => '',
				'label' => 'Number of columns',
				'additional_params' => array(
					array(
						'three' => __( 'Three columns', 'balance' ),
						'four'  => __( 'Four columns', 'balance' ),
					),
					'three',
					true
				)
			),
			array(
				'type' => 'text',
				'name' => 'title',
				'default_value' => '',
				'label' => 'Title'
			),
			array(
				'type' => 'upload',
				'name' => 'bgimage',
				'label' => 'Background image',
				'default_value' => array(),
				'additional_params' => array(
					'',
					'Image',
					'image'
				)
			),
			array(
				'type' => 'repeater',
				'name' => 'messages',
				'label' => 'Message',
				'title' => 'Messages',
				'default_value' => array(),
				'fields' => array(
					array(
						'type' => 'upload',
						'label' => 'Message image',
						'name' => 'uploadimage',
						'default_value' => '',
						'additional_params' => array(
							'',
							'Image',
							'image'
						)
					),
					array(
						'type' => 'text',
						'label' => 'Message title',
						'name' => 'title',
						'default_value' => ''
					),
					array(
						'type' => 'text',
						'label' => 'Message description',
						'name' => 'description',
						'default_value' => ''
					),
					array(
						'type' => 'links_to',
						'label' => 'Button link',
						'name' => 'link',
						'default_value' => array(),
						'additional_params' => array(
							'',
							'Page',
							array(
								'post_type' => 'page',
								'posts_per_page' => -1,
								'post_status' => 'publish'
							)
						)
					)

      )
    ),
    array(
      'type' => 'text',
      'name' => 'buttontext',
      'label' => '<br>Optional button text',
      'default_value' => ''
    ),
    array(
      'type' => 'links_to',
      'name' => 'linkfield',
      'label' => 'Optional Button link',
      'default_value' => ''
    )
  ), $data, $module_title, $visible_on);
  return $output;

}

?>
