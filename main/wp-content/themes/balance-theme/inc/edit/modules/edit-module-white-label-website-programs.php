<?php
    /*
     *  WP Edit module: White Label Websites Programs
     */
    function white_label_websites_programs_module_form($key, $visible_on = 'all', $module_title = '', $custom_settings = array())
    {
        global $data;

        if (empty($data['wlw_programs_module'])) {
            $data = init_array_on_var_and_array_key($data, 'wlw_programs_module');
            $data['wlw_programs_module'][$key] = array(
                'programs' => '',
                'programs_protected' => 'individually'
            );
        }

        $autocomplete_args_programs = array(
            'post_type' => 'program',
            'posts_per_page' => -1,
            'post_status' => 'publish'
        );

        //Left right image choices
        $programs_protected_choices = array(
            'individually' => __('Programs Individually Protected', 'balance'),
            'all' => __('All Programs Protected', 'balance'),
        );

        if (empty($data['wlw_programs_module'][$key]['programs_protected'])) {
            $data['wlw_programs_module'][$key]['programs_protected'] = 'individually';
        }


        /*START OF SETTINGS*/

        if (empty($data['test_times'][$key])) {
            $data = init_array_on_var_and_array_key($data, 'test_times');
            $data['test_times'] = array(
                'type' => 'life_stage'
            );
        }


        $array_of_types = array(
            0 => array(
                'id' => '0',
                'title' => 'Select',
            ),
            1 => array(
                'id' => '1',
                'title' => '1',
            ),
            2 => array(
                'id' => '2',
                'title' => '2'
            ),
            3 => array(
                'id' => '3',
                'title' => '3'
            ),
            4 => array(
                'id' => '4',
                'title' => '4'
            ),
            5 => array(
                'id' => '5',
                'title' => '5'
            ),
            6 => array(
                'id' => '6',
                'title' => '6'
            ),
            7 => array(
                'id' => '7',
                'title' => '7'
            ),
            8 => array(
                'id' => '8',
                'title' => '8'
            ),
            9 => array(
                'id' => '9',
                'title' => '9'
            ),
            10 => array(
                'id' => '10',
                'title' => '10'
            ),
            11 => array(
                'id' => '11',
                'title' => '11'
            ),
            12 => array(
                'id' => '12',
                'title' => '12'
            ),
            13 => array(
                'id' => '13',
                'title' => '13'
            ),
            14 => array(
                'id' => '14',
                'title' => '14'
            ),
            15 => array(
                'id' => '15',
                'title' => '15'
            ),
            16 => array(
                'id' => '16',
                'title' => '16'
            ),
            17 => array(
                'id' => '17',
                'title' => '17'
            ),
            18 => array(
                'id' => '18',
                'title' => '18'
            ),
            19 => array(
                'id' => '19',
                'title' => '19'
            ),
            20 => array(
                'id' => '20',
                'title' => '20'
            ),
            21 => array(
                'id' => '21',
                'title' => '21'
            ),
            22 => array(
                'id' => '22',
                'title' => '22'
            ),
            23 => array(
                'id' => '23',
                'title' => '23'
            ),
            24 => array(
                'id' => '24',
                'title' => '24'
            ),
            25 => array(
                'id' => '25',
                'title' => '25'
            ),
            26 => array(
                'id' => '26',
                'title' => '26'
            ),
            27 => array(
                'id' => '27',
                'title' => '27'
            ),
            28 => array(
                'id' => '28',
                'title' => '28'
            ),
            29 => array(
                'id' => '29',
                'title' => '29'
            ),
            30 => array(
                'id' => '30',
                'title' => '30'
            )
        );

        $passing_percent = array(
            0 => array(
                'id' => '',
                'title' => 'Select',
            ),
            1 => array(
                'id' => '50',
                'title' => '50',
            ),
            2 => array(
                'id' => '55',
                'title' => '55',
            ),
            3 => array(
                'id' => '60',
                'title' => '60'
            ),
            4 => array(
                'id' => '65',
                'title' => '65'
            ),
            5 => array(
                'id' => '70',
                'title' => '70'
            ),
            6 => array(
                'id' => '75',
                'title' => '75'
            ),
            7 => array(
                'id' => '80',
                'title' => '80'
            ),
            8 => array(
                'id' => '85',
                'title' => '85'
            ),
            9 => array(
                'id' => '90',
                'title' => '90'
            ),
            10 => array(
                'id' => '95',
                'title' => '95'
            ),
            11 => array(
                'id' => '100',
                'title' => '100'
            )
        );


        if (empty($data['test_duration'])) {
            $data = init_array_on_var_and_array_key($data, 'test_duration');
            $data['test_duration'] = array(
                'yes_no' => ''
            );
        }

        if (empty($data['test_duration'])) {
            $data = init_array_on_var_and_array_key($data, 'test_duration');
            $data['test_duration'] = array(
                'duration_test' => ''
            );
        }
        //options for html checkboxes
        $checkboxhtml_choices = array(
            array('24Hr', '<span>24 Hr</span>'),
            array('48Hr', '<span>48 Hr</span>'),
            array('Unlimited', '<span>No Limit</span>'),

        );

//

        /*END OF SETTINGS*/
        $output = '';
        $output .= '<a name="wlw-programs-module-wrapper-' . $key . '"></a>';
        $output .= '<div class="module-wrapper wlw-programs-module-wrapper-' . $key . ' hidden" ' . ($visible_on != "all" ? "data-visible-on='" . $visible_on . "'" : "") . '>';
        $output .= '  <div class="postbox postbox-custom wlw-programs-module-list-wrapper-' . $key . '">';
        $output .= '    <h3>' . $module_title . (intval($key) > 0 ? ' #' . (intval($key) + 1) : '') . '<a class="description fright section-expander is-expanded" data-toggle-title="' . __('Expand', 'balance') . '" href="javascript:;">' . __('Collapse', 'balance') . '</a></h3>';
        $output .= '  <div class="box-div-settings">';
        $output .= '    <div class="inside">';

        $output .= '     <p>';
        $output .= '        <label><b>' . __('Programs Protection', 'balance') . ':</b></label><br><br>';
        $output .= radiobuttonlist_field($data['wlw_programs_module'][$key]['programs_protected'], 'wlw_programs_module[' . $key . '][programs_protected]', $programs_protected_choices, 'individually', true);
        $output .= '      </p>';

        $output .= '        <p>';
        $output .= '          <label><b>' . __('Programs linked to Partner website', 'balance') . ':</b></label>';
        $output .= multi_autocomplete_field($data['wlw_programs_module'][$key]['programs'], 'wlw_programs_module[' . $key . '][programs]', $autocomplete_args_programs);
        $output .= '        </p>';

        $output .= '    </div>';
        $output .= '    </div>';
        $output .= '  </div>';
        $output .= '</div>';


        /*START OF SETTINGS*/
        $output .= '<style>
  .box-div-settings {
    border: solid 3px #cccccc;
    padding: 10px 20px 20px 20px;
    margin-bottom: 10px;
  }
</style>';

        $output .= '<div class="module-wrapper wlw-programs-module-wrapper-0 hidden" data-visible-on="tab-7">';

        $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';
        $output .= '<h3>Settings<a class="description fright section-expander is-expanded" data-toggle-title="Expand" href="javascript:;">Collapse</a></h3> ';
        $output .= '  <div class="inside">';
        $output .= '  <div class="box-div-settings">';
        $output .= '     <p>';
        $output .= '          <label><b>' . __('Send Email to Siteadmin and Superadmin', 'balance') . ':</b></label>';
        $output .= '      &nbsp;';
        $output .=              radiobuttonlist_field( $data['siteadmin_superadmin_mail'], 'siteadmin_superadmin_mail', array( 'Yes' => __( 'Yes', 'balance' ), 'No' => __( 'No', 'balance' ) ), 'y', true);
        $output .= '      </p>';
        $output .= '</div>';

        $output .= '  <div class="box-div-settings">';
        $output .= '    <p>';
        $output .= '          <label><b>' . __('Quiz Duration', 'balance') . ':</b></label><br>';
        //$output .=          checkboxlist_arrayed_field( $data['test_duration'], 'test_duration', $checkboxhtml_choices);
        $output .= '<br>';

        $output .=              radiobuttonlist_field( $data['test_duration'], 'test_duration', array( '24' => __( '24 hours', 'balance' ), '48' => __( '48 hours', 'balance' ), 'NO' => __( 'No Limit', 'balance' ) ), 'y', true);
//        $output .= '          <label><b>' . __('24 Hour', 'balance') . ':</b></label>';
//        $output .= '      &nbsp;';
//
//      $output .= checkbox_field($data['test_duration_24'], 'test_duration_24', false, 'toggleCheck', '24Hr');
//      $output .= '      &nbsp;';
//
//      $output .= '          <label><b>' . __('48 Hour', 'balance') . ':</b></label>';
//        $output .= checkbox_field($data['test_duration_48'], 'test_duration_48', false, 'toggleCheck', '48Hr');
//        $output .= '          <label><b>' . __('Unlimited', 'balance') . ':</b></label>';
//        $output .= checkbox_field($data['test_duration_noLimit'], 'test_duration_noLimit', false, 'toggleCheck', 'noLimit');
        $output .= '      <hr>';
        $output .= '      <label>';
        $output .= '          <label><b>' . __('Number of attempts', 'balance') . ':</b></label>';
        $output .= '      </label>';
        $output .= dropdown_field($data['test_times'], 'test_times', '', $array_of_types, '', false);
        $output .= '    </p>';

        $output .= '      <hr>';
        $output .= '    <p>';
        $output .= '          <label><b>' . __('Passing Percentage Criteria', 'balance') . ':</b></label>';
        $output .= '      <label>';
        // $output .= '      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        //$output .= '        <span class="text">' . __('Passing Percentage: ') . '</span>';
        $output .= '      </label>';
        $output .= dropdown_field($data['passing_percent'], 'passing_percent', '', $passing_percent, '', false);
        $output .= '    </p>';
        $output .= '      <hr>';

        $output .= '     <p>';
        $output .= '          <label><b>' . __('Do you want to show Question and Answer after Quiz attempt?', 'balance') . '</b></label>';
        $output .= '      &nbsp;';
        $output .= radiobuttonlist_field( $data['show_quiz_results'], 'show_quiz_results', array( 'Yes' => __( 'Yes', 'balance' ), 'No' => __( 'No', 'balance' ) ), 'y', true);
        //$output .= checkbox_field($data['show_quiz_results'], 'show_quiz_results', false, 'toggleCheck', 'quiz_result');
        $output .= '      </p>';
        $output .= '</div>';


        $output .= '  </div>';
        $output .= '</div>';
        $output .= '</div>';

        /*END OF SETTINGS*/

        /*START OF MESSAGES*/
        $output .= '<div class="module-wrapper wlw-programs-module-wrapper-0 hidden" data-visible-on="tab-9">';
        $output .= '     <style>
                  input.toggleCheck {
                  -webkit-appearance: none !important;
                  appearance: none !important;
                  padding: 16px 30px !important;
                  border-radius: 16px !important;
                  background: radial-gradient(circle 12px, white 100%, transparent calc(100% + 1px)) #ccc -14px !important;
                  transition: 0.3s ease-in-out !important;
                }

                input.toggleCheck:checked {
                  background-color: dodgerBlue !important;
                  background-position: 14px !important;
				  
				 
                }
				 #messagetext_ifr,#thankyoutext_ifr{
					  height:400px !important;
				  }
				  #certificatetext_ifr,#certificateadmintext_ifr{
					  height:200px !important;
				  }
				   
                </style>
';

//START OF Registration MESSAGE
        $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';

        $output .= '<h3>Registration Mail Message<a class="description fright section-expander is-expanded" data-toggle-title="Expand" href="javascript:;">Collapse</a></h3> ';
        $output .= '  <div class="inside">';
        $output .= '     <p>';
        $output .= '          <label><b>Subject</b></label><br>';
        if (isset($data['register_subject']) && trim($data['register_subject']) != '') $register_subject = $data['register_subject'];
        else $register_subject = 'BALANCE Account Email Verification!';
        $output .= '             <input type="text" class="" name="register_subject" value="' . $register_subject . '">';
        $output .= '            <span><b>[Note: %username%,%site_title% are variables used for user name, your website title]</b>';
        $output .= '      </p>';


        $output .= '     <p>';
        $output .= '          <label><b>Enable Dynamic Message</b></label><br>';
        if (isset($data['register_message_status']) && $data['register_message_status'] == 'on') {
            $output .= '             <input type="checkbox" class="toggleCheck" name="register_message_status" checked>';
        } else {
            $output .= '             <input type="checkbox" class="toggleCheck" name="register_message_status">';
        }
        $output .= '      </p>';
        //Textarea field

        $default_register_message_text = '<div><p>Thank you %username% for creating your new profile with BALANCE, in partnership with %site_title%. Please click the link below to verify your ownership of %user_email%.</p>

		<p>CLICK THIS LINK TO VERIFY: <BR><a href="%link%">%link%</a> <BR>
		<BR><BR>
		Best,<BR>

		BALANCE in partnership with %site_title%</p></div>';

        $output .= '            <p>';
        $output .= '              <label><b>' . __('Message Text', 'balance') . ':</b></label><br/>';
        $output .= textarea_field($data['register_message_text'], 'register_message_text', false, 10, $default_register_message_text, '', array('media_buttons' => false, 'quicktags' => true));
        $output .= '            <span><b>[Note: %username%,%user_email%,%site_title%,%site_url%,%link% are variables used for user name, user email, your website title, website url and verification link]</b>';
        $output .= '            </p>';
        $output .= '  </div>';
        $output .= '</div>';
        //END OF Registration Mail Message

//START OF THANK YOU MESSAGE
        $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';
        $output .= '<h3>Thank You Message<a class="description fright section-expander" data-toggle-title="Expand" href="javascript:;">Collapse</a></h3> ';
        $output .= '  <div class="inside">';

        $output .= '     <p>';
        $output .= '          <label><b>Enable Dynamic Message</b></label><br>';
        if (isset($data['thankYou_status']) && $data['thankYou_status'] == 'on') {
            $output .= '             <input type="checkbox" class="toggleCheck" name="thankYou_status" checked>';
        } else {
            $output .= '             <input type="checkbox" class="toggleCheck" name="thankYou_status">';
        }
        $output .= '      </p>';
        //Textarea field

        $default_thankYou_text = '<h1 class="text-info text-center">Thank you for verifying your email address</h1>
				<p>Thank you for completing your registration with BALANCE, in partnership with <span class="text-info"><b>%site_title%</b></span>. You’ve taken a step to financial fitness by being an active participant in your own financial wellness.</p>
				<p>Your registration gives you access to more BALANCE online education programs available on this website. Use your email address and BALANCE password to log in to a program when prompted.</p>
				<p>We hope you enjoy your experience with BALANCE, in partnership with <span class="text-info"><b>%site_title%</b></span>. We are here to help you achieve your financial success.</p>
				<p>Best,</p>
				<p>BALANCE in partnership with <span class="text-info"><b>%site_title%</b></span></p>';
        $output .= '            <p>';
        $output .= '              <label><b>' . __('Message Text', 'balance') . ':</b></label><br/>';
        $output .= textarea_field($data['thankYou_text'], 'thankYou_text', false, 15, $default_thankYou_text, '', array('media_buttons' => false, 'quicktags' => true));
        $output .= '            <span><b>[Note: %site_title%, %site_url% are variables used for your website title and website url .]</b>';
        $output .= '            </p>';
        $output .= '  </div>';
        $output .= '</div>';
        //END OF THANK YOU MESSAGE


//START OF WELCOME MESSAGE
        $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';
        $output .= '<h3>Welcome Message<a class="description fright section-expander" data-toggle-title="Expand" href="javascript:;">Collapse</a></h3> ';
        $output .= '  <div class="inside">';
        $output .= '     <p>';
        $output .= '          <label><b>Subject</b></label><br>';
        if (isset($data['message_subject']) && trim($data['message_subject']) != '') $message_subject = $data['message_subject'];
        else $message_subject = 'Welcome to BALANCE, in Partnership with %site_title%';
        $output .= '             <input type="text" class="" name="message_subject" value="' . $message_subject . '">';
        $output .= '            <span><b>[Note: %site_title%,%username% are variables used for your website title and User name]</b></span>';
        $output .= '      </p>';

        $output .= '     <p>';
        $output .= '          <label><b>Enable Dynamic Message</b></label><br>';
        if (isset($data['message_status']) && $data['message_status'] == 'on') {
            $output .= '             <input type="checkbox" class="toggleCheck" name="message_status" checked>';
        } else {
            $output .= '             <input type="checkbox" class="toggleCheck" name="message_status">';
        }
        $output .= '      </p>';
        //Textarea field

        $default_message_text = '<div><h1>Welcome to BALANCE, in partnership with %site_url%</h1>
					<p>Welcome and thank you for completing your registration with BALANCE, in partnership with %site_url%.</p>
					<p>
					You have taken a step to financial fitness by being an active participant in your own financial wellness.
					</p>
					<p>
					Your registration gives you access to more online education programs available on <a href="%site_url%">%site_url%</a> .
					</p>
					<p>
					Use your email address and BALANCE password to log in to a program when prompted.
					</p>
		<p>
					We hope you enjoy your experience with BALANCE, in partnership with %site_title% We are here to help you achieve your financial success.
				</p>
					<p>Start by visiting us at <a href="%site_url%">%site_url% </a> .
					</p>
				<p>	Best,</p>
				<p>	
					BALANCE, in partnership with %site_title%
					</p>
					</div>';

        $output .= '            <p>';
        $output .= '              <label><b>' . __('Message Text', 'balance') . ':</b></label><br/>';
        $output .= textarea_field($data['message_text'], 'message_text', false, 20, $default_message_text, '', array('media_buttons' => false, 'quicktags' => true));
        $output .= '            <span><b>[Note: %site_title%,%site_url% are variables used for your website title and website url]</b></span>';
        $output .= '            </p>';
        $output .= '  </div>';
        $output .= '</div>';
        //END OF WELCOME MESSAGE


//START OF CERTIFICATE MESSAGE
        $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';
        $output .= '<h3>Certificate Message<a class="description fright section-expander" data-toggle-title="Expand" href="javascript:;">Collapse</a></h3> ';
        //START OF CUSTOMER
        $output .= '  <div class="inside" style="display: inline-block;width: 45.5%;">';
        $output .= '     <p>';
        $output .= '          <label><b>Customer</b></label><br><br>';
        $output .= '      </p>';
        $output .= '     <p>';
        $output .= '          <label><b>Subject</b></label><br>';
        if (isset($data['certificate_subject']) && trim($data['certificate_subject']) != '') $certificate_subject = $data['certificate_subject'];
        else $certificate_subject = '%quiztitle%';

        $output .= '             <input type="text" class="" name="certificate_subject" value="' . $certificate_subject . '">';
        $output .= '            <span><b>[Note: %quiztitle% variable used for Quiz Title.]</b></span><br><br>';
        $output .= '      </p>';

        $output .= '     <p>';
        $output .= '          <label><b>Enable Dynamic Message</b></label><br>';
        if (isset($data['certificate_status']) && $data['certificate_status'] == 'on') {
            $output .= '             <input type="checkbox" class="toggleCheck" name="certificate_status" checked>';
        } else {
            $output .= '             <input type="checkbox" class="toggleCheck" name="certificate_status">';
        }
        $output .= '      </p>';
        //Textarea field
        $default_certificate_text = '<div><p>Congratulations, you successfully completed this module! Your score was %score% (%correct_val%/%total_questions%)</p></div>';
        $output .= '            <p>';
        $output .= '              <label><b>' . __('Message Text', 'balance') . ':</b></label><br/>';
        $output .= textarea_field($data['certificate_text'], 'certificate_text', false, 10, $default_certificate_text, '', array('media_buttons' => false, 'quicktags' => true));
        $output .= '            <span><br><br><b>[Note: %score%, %correct_val%, %total_questions% variable used for your quiz score,correct answers and total questions.]</b></span>';
        $output .= '            </p>';
        $output .= '  </div>';
        //END OF CUSTOMER

        //START OF ADMIN/SUPERADMIN
        $output .= '  <div class="inside" style="display: inline-block;width: 45.5%;">';
        $output .= '     <p>';
        $output .= '          <label><b>Admin / Superadmin</b></label><br><br>';
        $output .= '     </p>';
        $output .= '     <p>';
        $output .= '          <label><b>Subject</b></label><br>';
        if (isset($data['certificate_admin_subject']) && trim($data['certificate_admin_subject']) != '') $certificate_admin_subject = $data['certificate_admin_subject'];
        else $certificate_admin_subject = '%quiztitle%';
        $output .= '             <input type="text" class="" name="certificate_admin_subject" value="' . $certificate_admin_subject . '">';
        $output .= '            <span><b>[Note: %quiztitle% variable used for Quiz Title.]</b></span>';
        $output .= '      </p>';

        $output .= '      </p>';

        $output .= '     <p>';
        $output .= '          <label><b>Enable Dynamic Message</b></label><br>';
        if (isset($data['certificate_admin_status']) && $data['certificate_admin_status'] == 'on') {
            $output .= '             <input type="checkbox" class="toggleCheck" name="certificate_admin_status" checked>';
        } else {
            $output .= '             <input type="checkbox" class="toggleCheck" name="certificate_admin_status">';
        }
        $output .= '      </p>';
        //Textarea field
        $default_certificate_admin_text = '<div><p>%user% has completed the quiz successfully.</p><BR>
				The score was %score% (%correct_val%/%total_questions%)
				<BR><BR>Website : %site_title%<BR>
				<BR><BR>Thanks<BR>
				Balancepro.org<BR></div>';
        $output .= '            <p>';
        $output .= '              <label><b>' . __('Message Text', 'balance') . ':</b></label><br/>';
        $output .= textarea_field($data['certificate_admin_text'], 'certificate_admin_text', false, 10, $default_certificate_admin_text, '', array('media_buttons' => false, 'quicktags' => true));

        $output .= '        <span><b>[Note: %user%, %site_title%, %score%, %correct_val%, %total_questions% variable used for your quiz user name, website title, score,correct answers and total questions.]</b></span>';
        $output .= '            </p>';
        $output .= '  </div>';
        //END OF ADMIN/SUPERADMIN


        $output .= '</div>';
        //END OF CERTIFICATE MESSAGE


        $output .= '</div>';

        /*END OF MESSAGES*/
        $output .= '<script type="text/javascript">';
        $output .= 'jQuery(document).ready(function(){';
        $output .= 'jQuery(".box-div-settings input:radio").addClass("toggleCheck");';
        $output .= '});';
        $output .= '</script>';
        return $output;
    }

?>
