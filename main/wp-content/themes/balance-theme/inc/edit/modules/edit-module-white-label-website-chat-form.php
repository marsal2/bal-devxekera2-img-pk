<?php
/*
 *  WP Edit module: White Label Websites Chat
 */
function white_label_websites_chat_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
	global $data;

  /*START OF CHAT*/
  $output .= '<div class="module-wrapper wlw-programs-module-wrapper-0 hidden" data-visible-on="tab-10">';

  $output .= '<style type="text/css">
  .form-table th, .form-table td { padding: 10px 10px; border:1px solid #f3f0f0; } 
  input.toggleCheck { padding:14px 27px !important}';
$output .= '</style>';

  $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';
  $output .= '<h3>Chat<a class="description fright section-expander is-expanded" data-toggle-title="Expand" href="javascript:;">Collapse</a></h3> ';
  $output .= '  <div class="inside">';
  
/*
  $output .= '          <label><b>'. __( 'Disable chat', 'balance' ) .':</b></label><br>';
  $output .=              radiobuttonlist_field( $data['wlw_general_info_module'][ $key ]['disable_chat'], 'wlw_general_info_module['.$key.'][disable_chat]', array( 'y' => __( 'Yes', 'balance' ), 'n' => __( 'No', 'balance' ) ), 'n', true);
  $output .= '        </p>';
  $output .= '        <p>';
*/

   // Dynamic chat enable disable:
  $output .= '     <p>';
  $output .= '          <label><b>'. __( 'Do you want to show the CHAT Label', 'balance' ) .'</b></label><br>';
  $output .=  checkbox_field( $data['whitelabel_chat_enable_disable'], 'whitelabel_chat_enable_disable',false,'toggleCheck'); 
  $output .= '      </p>';


  $output .= '     <p>';

  $output .= '<table class="form-table">';
  // Days heading
  $output .= '<tr valign="top">';
  $output .= '<th scope="row">';
  $output .= __( 'Days', 'balance' );
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .= __( 'ON/OFF', 'balance' );
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .= __( 'Start Time', 'balance' );
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .= __( 'End Time', 'balance' );
  $output .= '</th>';
  $output .= '</tr>';
  // Days heading end

  // Sunday timing start
  $output .= '<tr valign="top">';
  $output .= '<th scope="row">';
  $output .= __( 'Sunday', 'balance_theme' );
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  checkbox_field( $data['whitelabel_chat_checkbox_sun'], 'whitelabel_chat_checkbox_sun',false,'toggleCheck','sun');	
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_sun_start'], 'whitelabel_chat_sun_start','','whitelabel_chat_sun_start');				
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_sun_end'], 'whitelabel_chat_sun_end','','whitelabel_chat_sun_end');					
  $output .= '</th>';
  $output .= '</tr>';
  // Sunday timing End

  // Monday timing start
  $output .= '<tr valign="top">';
  $output .= '<th scope="row">';
  $output .= __( 'Monday', 'balance' );
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  checkbox_field( $data['whitelabel_chat_checkbox_mon'], 'whitelabel_chat_checkbox_mon',false,'toggleCheck','mon');	
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_mon_start'], 'whitelabel_chat_mon_start','','whitelabel_chat_mon_start');				
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_mon_end'], 'whitelabel_chat_mon_end','','whitelabel_chat_mon_end');					
  $output .= '</th>';
  $output .= '</tr>';
  // Monday timing End

  // Tuesday timing start
  $output .= '<tr valign="top">';
  $output .= '<th scope="row">';
  $output .= __( 'Tuesday', 'balance' );
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  checkbox_field( $data['whitelabel_chat_checkbox_tue'], 'whitelabel_chat_checkbox_tue',false,'toggleCheck','tue');	
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_tue_start'], 'whitelabel_chat_tue_start','','whitelabel_chat_tue_start');				
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_tue_end'], 'whitelabel_chat_tue_end','','whitelabel_chat_tue_end');					
  $output .= '</th>';
  $output .= '</tr>';
  // Tuesday timing End

  // Wednesday timing start
  $output .= '<tr valign="top">';
  $output .= '<th scope="row">';
  $output .= __( 'Wednesday', 'balance' );
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  checkbox_field( $data['whitelabel_chat_checkbox_wed'], 'whitelabel_chat_checkbox_wed',false,'toggleCheck','wed');	
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_wed_start'], 'whitelabel_chat_wed_start','','whitelabel_chat_wed_start');				
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_wed_end'], 'whitelabel_chat_wed_end','','whitelabel_chat_wed_end');					
  $output .= '</th>';
  $output .= '</tr>';
  // Wednesday timing End

   // Thursday timing start
  $output .= '<tr valign="top">';
  $output .= '<th scope="row">';
  $output .= __( 'Thursday', 'balance' );
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  checkbox_field( $data['whitelabel_chat_checkbox_thu'], 'whitelabel_chat_checkbox_thu',false,'toggleCheck','thu');	
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_thu_start'], 'whitelabel_chat_thu_start','','whitelabel_chat_thu_start');				
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_thu_end'], 'whitelabel_chat_thu_end','','whitelabel_chat_thu_end');					
  $output .= '</th>';
  $output .= '</tr>';
  // Thursday timing End

   // Friday timing start
  $output .= '<tr valign="top">';
  $output .= '<th scope="row">';
  $output .= __( 'Friday', 'balance' );
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  checkbox_field( $data['whitelabel_chat_checkbox_fri'], 'whitelabel_chat_checkbox_fri',false,'toggleCheck','fri');	
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_fri_start'], 'whitelabel_chat_fri_start','','whitelabel_chat_fri_start');				
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_fri_end'], 'whitelabel_chat_fri_end','','whitelabel_chat_fri_end');					
  $output .= '</th>';
  $output .= '</tr>';
  // Friday timing End

   // Saturday timing start
  $output .= '<tr valign="top">';
  $output .= '<th scope="row">';
  $output .= __( 'Saturday', 'balance' );
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  checkbox_field( $data['whitelabel_chat_checkbox_sat'], 'whitelabel_chat_checkbox_sat',false,'toggleCheck','sat');	
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_sat_start'], 'whitelabel_chat_sat_start','','whitelabel_chat_sat_start');				
  $output .= '</th>';
  $output .= '<th scope="row">';
  $output .=  text_field( $data['whitelabel_chat_sat_end'], 'whitelabel_chat_sat_end','','whitelabel_chat_sat_end');					
  $output .= '</th>';
  $output .= '</tr>';
  // Saturday timing End

  $output .= '</table>';

  $output .= '     </p>';

  // Title tooltip This text will appear as hover on Chat menu
  $output .= '     <p>';
  $output .= '          <label><b>'. __( 'Title tooltip:', 'balance' ) .'</b></label><br>';
  $output .=                text_field( $data['whitelabel_chat_title'], 'whitelabel_chat_title','','whitelabel_chat_title');
  $output .= '      </p>';
  //$output .= '<span><b>This text will appear as hover on Chat menu.</b>';

    // Title field end

  // Description field start
  $output .= '     <p>';
  $output .= '          <label><b>'. __( 'Description:', 'balance' ) .'</b></label><br>';
  $output .=                textarea_field( $data['whitelabel_chat_description'], 'whitelabel_chat_description', false, 5,'<p>%sitetitle% chat is currently unavailable. Our normal business hours are in (UTC-08:00) Pacific Time (US),<br> %timesettings%</p><p> You may call us at 1-888-456-2227 during normal business hours.</p>' );
  $output .= '      </p>';
  $output .= '<span><b>The above description will appear in new window when we are offline. %sitetitle% and %timesettings% as specified in settings tab.</b>';
  // Description field end

  $output .= '  </div>';
  $output .= '</div>';
  $output .= '</div>';

  $output .= '<script src="'.get_template_directory_uri().'/js/admin/jquery.timepicker.min.js"></script>';
  $output .= '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/admin/jquery.timepicker.min.css" type="text/css"  />';
  $output .= '<script type="text/javascript">';
  $output .= 'jQuery(document).ready(function(){';
  /*$output .= 'jQuery("table .toggleCheck").on("click",function(){
  		if(jQuery(this).is(":checked")){
  			jQuery(".whitelabel_chat_sun_start,.whitelabel_chat_sun_end").attr({"disabled":false});
  		}else{
  			jQuery(".whitelabel_chat_sun_start,.whitelabel_chat_sun_end").attr({"disabled":true});
  		}
  });';*/
  $output .= 'jQuery(".whitelabel_chat_sun_start,.whitelabel_chat_sun_end").timepicker();';
  $output .= 'jQuery(".whitelabel_chat_mon_start,.whitelabel_chat_mon_end").timepicker();';
  $output .= 'jQuery(".whitelabel_chat_tue_start,.whitelabel_chat_tue_end").timepicker();';
  $output .= 'jQuery(".whitelabel_chat_wed_start,.whitelabel_chat_wed_end").timepicker();';
  $output .= 'jQuery(".whitelabel_chat_thu_start,.whitelabel_chat_thu_end").timepicker();';
  $output .= 'jQuery(".whitelabel_chat_fri_start,.whitelabel_chat_fri_end").timepicker();';
  $output .= 'jQuery(".whitelabel_chat_sat_start,.whitelabel_chat_sat_end").timepicker();';
  $output .= '});';
  $output .= '</script>';

  /*END OF CHAT*/

  return $output;
}
?>