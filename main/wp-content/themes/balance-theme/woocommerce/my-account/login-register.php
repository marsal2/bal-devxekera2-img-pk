<?php
global $my_account_action;
$my_account_page_permalink = get_the_permalink ( get_option('woocommerce_myaccount_page_id') );
$all_notices  = WC()->session->get( 'wc_notices', array() );
$show_default_message = (count($all_notices) > 0) ? false : true;
?>
<ul role="tablist" class="nav nav-tabs">
	<li role="presentation" class="<?php echo ($my_account_action == 'register') ? 'active' : ''; ?>"><a href="?action=register" aria-controls="tab1" role="tab"><span class="icon-user"></span><?php _e( 'Register', 'balance' ); ?></a></li>
       <li role="presentation" class="<?php echo ($my_account_action == 'login') ? 'active' : ''; ?>"><a href="?action=login" aria-controls="tab2" role="tab"><span class="icon-user"></span><?php _e( 'Log In', 'balance' ); ?></a></li>
</ul>
<div class="tab-content">
	<?php wc_print_notices(); ?>
	<?php if ( isset($_GET['status']) && $_GET['status'] == 'please-verify' ) { ?>
		<div class="woocommerce-message woocommerce-message-success"><?php _e( 'Account created successfully, <b>you need to verify it</b>. Please check email inbox for confirmation email.', 'balance' ); ?></div>
	<?php } else if ( isset($_GET['status']) && $_GET['status'] == 'invalid-verification-code' ) { ?>
		<div class="woocommerce-message woocommerce-message-error"><?php _e( 'Invalid account verification code.', 'balance' ); ?></div>
	<?php } ?>
	<div id="tab1" role="tabpanel" class="tab-panel <?php echo $my_account_action == 'register' ? 'active' : ''; ?>">
		<form method="post" class="register account-form" action="<?php echo $my_account_page_permalink . '?action=register'; ?>">
			<?php do_action( 'woocommerce_register_form_start' ); ?>
			<fieldset>
				<?php if ( $show_default_message ) { ?>
					<p><?php _e( 'Please register as a New User to access our library of financial resources, newsletters, and more!', 'balance' ); ?></p>
				<?php } ?>
				<div class="form-block">
					<div class="input-group">
						<span class="label">
							<label for="billing_first_name"><?php _e( 'First Name', 'balance' ); ?></label>
						</span>
						<input id="billing_first_name" type="text" autocomplete="off" placeholder="<?php _e( 'First Name', 'balance' ); ?>" name="billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" required>
					</div>
					<div class="input-group">
						<span class="label">
							<label for="billing_last_name"><?php _e( 'Last Name', 'balance' ); ?></label>
						</span>
						<input id="billing_last_name" type="text" autocomplete="off" placeholder="<?php _e( 'Last Name', 'balance' ); ?>" name="billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" required>
					</div>
					<div class="input-group">
						<span class="label">
							<label for="email"><?php _e( 'Email', 'balance' ); ?></label>
						</span>
						<input id="email" type="email" autocomplete="off" placeholder="<?php _e( 'Your email will be your username', 'balance' ); ?>" name="email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" required>
					</div>
					<div class="input-group">
						<span class="label">
							<label for="password"><?php _e( 'Password', 'balance' ); ?></label>
						</span>
						<input id="password" type="password" autocomplete="off" placeholder="<?php _e( '8 characters, no spaces', 'balance' ); ?>" name="password" value="<?php if ( ! empty( $_POST['password'] ) ) echo esc_attr( $_POST['password'] ); ?>" required>
					</div>
					<div class="input-group">
						<span class="label">
							<label for="password2"><?php _e( 'Confirm Password', 'balance' ); ?></label>
						</span>
						<input id="password2" type="password" autocomplete="off" placeholder="<?php _e( '8 characters, no spaces', 'balance' ); ?>" name="password2" value="<?php if ( ! empty( $_POST['password2'] ) ) echo esc_attr( $_POST['password2'] ); ?>" required>
					</div>
					<div class="input-group">
						<div class="g-recaptcha" data-validate="#submit-register"></div>
						<img alt="autocomplete loader" class="loading-recaptcha" src="<?php echo get_stylesheet_directory_uri() . '/images/autocomplete-loader.gif'; ?>">
					</div>
					<!-- Spam Trap -->
					<div style="<?php echo ( is_rtl() ) ? 'right' : 'left'; ?>: -999em; position: absolute;visibility: hidden;">
						<label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label>
						<input type="text" name="email_2" id="trap" tabindex="-1" />
					</div>
					<?php do_action( 'woocommerce_register_form' ); ?>
					<?php do_action( 'register_form' ); ?>
					<?php wp_nonce_field( 'woocommerce-register' ); ?>
					<div class="btn-holder">
						<input type="submit" class="btn btn-warning" name="register" id="submit-register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>" disabled>
					</div>
				</div>
			</fieldset>
			<?php do_action( 'woocommerce_register_form_end' ); ?>
		</form>

	</div>
	<div id="tab2" role="tabpanel" class="tab-panel <?php echo $my_account_action == 'login' ? 'active' : ''; ?>">
		<form method="post" class="login account-form" action="<?php echo $my_account_page_permalink . '?action=login'; ?>">
			<fieldset>
				<?php if ( $show_default_message ) { ?>
					<p><?php _e( 'Login to access our library of financial resources, newsletters, and more!', 'balance' ); ?>&nbsp;<a href="<?php echo $my_account_page_permalink . '?action=lost-password'; ?>"><?php _e( 'Lost your password?', 'balance' ); ?></a></p>
				<?php } ?>
				<div class="form-block">
					<div class="input-group">
						<span class="label">
							<label for="input6"><?php _e( 'Email / Username', 'balance' ); ?></label>
						</span>
						<input id="input6" type="text" name="username" placeholder="<?php _e( 'Your Email / Username', 'balance' ); ?>" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" required>
					</div>
					<div class="input-group">
						<span class="label">
							<label for="input7"><?php _e( 'Password', 'woocommerce' ); ?></label>
						</span>
						<input id="input7" type="password" name="password" placeholder="<?php _e( 'Password', 'balance' ); ?>" value="<?php if ( ! empty( $_POST['password'] ) ) echo esc_attr( $_POST['password'] ); ?>"  required>
					</div>
					<div class="input-group">
		                        	<span class="label">
		                        		<label>&nbsp;</label>
		                        	</span>
		                          	<div style="width:77%;text-align: left;float:right;">
		                          		<input id="rememberme" type="checkbox" name="rememberme">
		                          		<label for="rememberme"><?php _e( 'Remember me?', 'woocommerce' ); ?></label>
		                          	</div>
		                        </div>
		                        <div class="input-group">
		                        	<div class="g-recaptcha" data-validate="#submit-login"></div>
		                        	<img alt="autocomplete loader" class="loading-recaptcha" src="<?php echo get_stylesheet_directory_uri() . '/images/autocomplete-loader.gif'; ?>">
		                      	</div>
					<?php do_action( 'woocommerce_login_form' ); ?>
					<div class="btn-holder">
						<?php wp_nonce_field( 'woocommerce-login' ); ?>
						<input type="submit" class="btn btn-warning" name="login" id="submit-login" value="<?php esc_attr_e( 'Log In', 'woocommerce' ); ?>" disabled/>
					</div>
					<?php do_action( 'woocommerce_login_form_end' ); ?>
				</div>
			</fieldset>
		</form>
		<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
	</div>
</div>
