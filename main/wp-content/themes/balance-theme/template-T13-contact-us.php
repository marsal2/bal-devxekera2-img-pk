<?php
/*
Template Name: T13: Contact us
Modules: {"m1[0]":{"name":"M1: Hero"},"m34[0]":{"name":"M34: Section"},"m38a[0]":{"name":"M38a: Contact details"}}
*/

?>
<?php
global $additional_body_class, $data;
$additional_body_class = 'contact-us';
get_custom_data();

get_header();

/* M01/M57 renderer */
if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0] );
}

/* SECTION - TITLE, COPY */
if (!empty($data['m34_module']) && !empty($data['m34_module'][0])) {
    echo render_m34_section_title_copy($data['m34_module'][0]);
}

/* CONTACT FORM AND CONTACT DETAILS */
if (!empty($data['m38a_module']) && !empty($data['m38a_module'][0])) {
    if ($_SESSION['contact_us_page_html'] == 1) {
        echo render_m38a_contact_details($data['m38a_module'][0]);
    }
}

get_footer();
?>
