<?php
/*
Template Name: T02: About Us
Modules: {"m1[0]":{"name":"M1: Hero"},"m31[0]":{"name":"M31: Section"},"m56_m17[0]":{"name":"M17: Four columns"},"m31[1]":{"name":"M31: Section"},"m31[2]":{"name":"M31: Section"},"m31[3]":{"name":"M31: Section"}}
*/
?>
<?php
global $additional_body_class, $data;
$additional_body_class = 'about-us';
get_custom_data();

get_header();

/* M01/M57 renderer */
if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0], 'medium' );
}

/* SECTION - TITLE, COPY, BUTTON */
if (!empty($data['m31_module']) && !empty($data['m31_module'][0])) {
    echo render_m33_section_title_copy_button($data['m31_module'][0]);
}

/* FOUR/THREE MESSAGES */
if (!empty($data['m56_module']) && !empty($data['m56_module'][0])) {
		echo '<div class="border-divider"> </div>';
    echo render_m56_m17_three_four_messages($data['m56_module'][0]);
}

/* SECTION - TITLE, COPY, BUTTON */
if (!empty($data['m31_module']) && !empty($data['m31_module'][1])) {
	  echo '<div class="border-divider"> </div>';
    echo render_m33_section_title_copy_button($data['m31_module'][1]);
}

/* SECTION - TITLE, COPY, BUTTON */
if (!empty($data['m31_module']) && !empty($data['m31_module'][2])) {
	  echo '<div class="border-divider"> </div>';
    echo render_m33_section_title_copy_button($data['m31_module'][2]);
}

/* SECTION - TITLE, COPY, BUTTON */
if (!empty($data['m31_module']) && !empty($data['m31_module'][3])) {
	  echo '<div class="border-divider"> </div>';
    echo render_m33_section_title_copy_button($data['m31_module'][3]);
}

get_footer();

?>
