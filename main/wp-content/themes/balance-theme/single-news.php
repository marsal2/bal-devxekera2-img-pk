<?php
/*
Modules: {"m34[0]":{"name":"M34: Section","params":{"media_buttons":true,"quicktags":true}}}
*/

?>
<?php
global $additional_body_class;
$additional_body_class = 'single-news';
get_custom_data();
get_header();
get_template_part('partials/simple-content');

get_footer();
