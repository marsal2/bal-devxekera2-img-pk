<?php
    /**
     * Default Page Header
     */
    global $additional_body_class;
    global $show_header;
    $additional_body_class = empty($additional_body_class) ? '' : $additional_body_class;
    if (is_null($show_header)) {
        $show_header = true;
    }
    session_start();
    date_default_timezone_set('America/Los_Angeles');

?>
<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8 ie7" lang="en"><![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 ie8" lang="en"><![endif]-->
<!--[if gt IE 8]>
<html class="no-js" lang="en"><![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo get_bloginfo('description') . wp_title(' | ', false, 'left'); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="/apple-touch-icon.png"/>
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png"/>
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png"/>
    <?php wp_head(); ?>
    <?php echo get_option('header_scripts', ''); ?>
    <style type="text/css">
        .nav.navbar-nav li a span {
            color: #323232!important;
            background: transparent!important;
        }
    </style>
</head>
<body <?php body_class($additional_body_class); ?>>
<input type="hidden" name="base_site_url" class="base_site_url" value="<?php echo BASE_SIT_URL; ?>/">
<input type="hidden" name="site_key" class="site_key" value="<?php echo SITE_KEY; ?>">
<!--[if lt IE 10]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="//browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php
    //Fixed edit button
    if (current_user_can('edit_pages')):
        $edit_link = get_edit_post_link();
        if (is_archive('product')) {
            $page = get_page_by_path('shop');
            $edit_link = get_edit_post_link($page->ID);
        }
        ?>
        <div class="frontend-edit-link-wrapper">
            <a title="Edit this page/resource" href="<?php echo $edit_link; ?>" class="dashicons dashicons-edit"></a>
        </div>
    <?php endif; ?>

<header id="header" class="headerline nav-fixed">
    <div class="header-t">
        <div class="container">
            <div class="row">
                <!-- search form-->
                <!--<form action="#" class="search-form hidden-xs">
                  <fieldset><a href="/resources" class="icon-a-tag"><span class="icon-search"></span></a>
                  </fieldset>
                </form>-->
                <?php
                    //Top header 1-level menu
                    $top_header_menu = get_menu("topheader", 0, 1, 0);
                ?>


                <?php if (!empty($top_header_menu)): ?>
                    <ul class="contact-block">
                        <?php $timesettings = getChatSettingsMain();
                            $_SESSION['mainChatSettings'] = $timesettings;
                            $_SESSION['title'] = 'Balance Pro';
                            $show_chat = true;
                            $show_contact = true;

                            //print_r($timesettings);exit;
                            if (trim($timesettings['chat_status']) == 1) {
                                //if($_SESSION['accesschat'] == 1 || $_SESSION['accesschat'] === 'y'){
                                if ($timesettings['chat_title'] != '') $tooltip_text = ', ' . $timesettings['chat_title'];
                                else $tooltip_text = '';
                                $chat_day = date('D');
                                $chat_time = date('H:i:s');
                                $phone = '1-888-456-2227';
                                if (empty($phone)) $phone = '1-888-456-2227';
                                if (isset($timesettings) && !empty($timesettings)) {
                                    if ($chat_day == 'Mon') {
                                        $chat_time_start = $timesettings['mon_start'];
                                        $chat_time_start_24 = date('H:i:s', strtotime($chat_time_start));
                                        $chat_time_end = $timesettings['mon_end'];
                                        $chat_time_end_24 = date('H:i:s', strtotime($chat_time_end));
                                        if (isset($timesettings['mon_status']) && $timesettings['mon_status'] == 1) {
                                            if ($chat_time >= $chat_time_start_24 && $chat_time <= $chat_time_end_24)
                                                $chat_available = 1;
                                            else
                                                $chat_available = 0;
                                        } else {
                                            $chat_available = 0;
                                        }
                                    }
                                    if ($chat_day == 'Tue') {
                                        $chat_time_start = $timesettings['tue_start'];
                                        $chat_time_start_24 = date('H:i:s', strtotime($chat_time_start));
                                        $chat_time_end = $timesettings['tue_end'];
                                        $chat_time_end_24 = date('H:i:s', strtotime($chat_time_end));
                                        if (isset($timesettings['tue_status']) && $timesettings['tue_status'] == 1) {
                                            if ($chat_time >= $chat_time_start_24 && $chat_time <= $chat_time_end_24)
                                                $chat_available = 1;
                                            else
                                                $chat_available = 0;
                                        } else {
                                            $chat_available = 0;
                                        }
                                    }
                                    if ($chat_day == 'Wed') {
                                        $chat_time_start = $timesettings['wed_start'];
                                        $chat_time_start_24 = date('H:i:s', strtotime($chat_time_start));
                                        $chat_time_end = $timesettings['wed_end'];
                                        $chat_time_end_24 = date('H:i:s', strtotime($chat_time_end));
                                        if (isset($timesettings['wed_status']) && $timesettings['wed_status'] == 1) {
                                            if ($chat_time >= $chat_time_start_24 && $chat_time <= $chat_time_end_24)
                                                $chat_available = 1;
                                            else
                                                $chat_available = 0;
                                        } else {
                                            $chat_available = 0;
                                        }
                                    }
                                    if ($chat_day == 'Thu') {
                                        $chat_time_start = $timesettings['thu_start'];
                                        $chat_time_start_24 = date('H:i:s', strtotime($chat_time_start));
                                        $chat_time_end = $timesettings['thu_end'] . $timesettings['thu_end_ap'];
                                        $chat_time_end_24 = date('H:i:s', strtotime($chat_time_end));
                                        if (isset($timesettings['thu_status']) && $timesettings['thu_status'] == 1) {
                                            if ($chat_time >= $chat_time_start_24 && $chat_time <= $chat_time_end_24)
                                                $chat_available = 1;
                                            else
                                                $chat_available = 0;
                                        } else {
                                            $chat_available = 0;
                                        }
                                    }
                                    if ($chat_day == 'Fri') {
                                        $chat_time_start = $timesettings['fri_start'];
                                        $chat_time_start_24 = date('H:i:s', strtotime($chat_time_start));
                                        $chat_time_end = $timesettings['fri_end'];
                                        $chat_time_end_24 = date('H:i:s', strtotime($chat_time_end));
                                        if (isset($timesettings['fri_status']) && $timesettings['fri_status'] == 1) {
                                            if ($chat_time >= $chat_time_start_24 && $chat_time <= $chat_time_end_24)
                                                $chat_available = 1;
                                            else
                                                $chat_available = 0;
                                        } else {
                                            $chat_available = 0;
                                        }
                                    }
                                    if ($chat_day == 'Sat') {
                                        $chat_time_start = $timesettings['sat_start'];
                                        $chat_time_start_24 = date('H:i:s', strtotime($chat_time_start));
                                        $chat_time_end = $timesettings['sat_end'];
                                        $chat_time_end_24 = date('H:i:s', strtotime($chat_time_end));
                                        if (isset($timesettings['sat_status']) && $timesettings['sat_status'] == 1) {
                                            if ($chat_time >= $chat_time_start_24 && $chat_time <= $chat_time_end_24)
                                                $chat_available = 1;
                                            else
                                                $chat_available = 0;
                                        } else {
                                            $chat_available = 0;
                                        }
                                    }
                                    if ($chat_day == 'Sun') {
                                        $chat_time_start = $timesettings['sun_start'];
                                        $chat_time_start_24 = date('H:i:s', strtotime($chat_time_start));
                                        $chat_time_end = $timesettings['sun_end'];
                                        $chat_time_end_24 = date('H:i:s', strtotime($chat_time_end));
                                        if (isset($timesettings['sun_status']) && $timesettings['sun_status'] == 1) {
                                            if ($chat_time >= $chat_time_start_24 && $chat_time <= $chat_time_end_24)
                                                $chat_available = 1;
                                            else
                                                $chat_available = 0;
                                        } else {
                                            $chat_available = 0;
                                        }
                                    }
                                } else {
                                    if (in_array($chat_day, ['Mon', 'Tue', 'Wed', 'Thu'])) {
                                        if ($chat_time >= '07:30:00' && $chat_time <= '18:00:00') $chat_available = 1; else $chat_available = 0;
                                    } else if ($chat_day == 'Fri') {
                                        if ($chat_time >= '07:30:00' && $chat_time <= '17:00:00') $chat_available = 1; else $chat_available = 0;
                                    } else if ($chat_day == 'Sat') {
                                        if ($chat_time >= '09:00:00' && $chat_time <= '14:00:00') $chat_available = 1; else $chat_available = 0;
                                    }
                                }
                            }
                            else{
                                $show_chat = false;
                            }
                            //Phone Validation
                            if($timesettings['contact_number'] == 1){
                                $show_contact = true;
                            }
                            if(isset($timesettings['contact_form']) && $timesettings['contact_form'] == 1){
                                $_SESSION['contact_us_page_html'] = 1;
                            }
                            else{
                                $_SESSION['contact_us_page_html'] = '';
                            }
                            //Show Chat when true
                            if ($show_chat == true) {
                                if ($chat_available) { ?>
                                    <li>
                                        <a title='Online<?= $tooltip_text ?>'
                                           onclick="window.open('http://chat.balancepro.org/i3root/chat_cccs/index.html', 'Chat', 'resizable,height=500,width=500')"
                                           title='Online<?= $tooltip_text ?>' class="chat" href='javascript:void(0)'>
                                            <span>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/chat_icon.png"
                                     style="width: 23px;height: 24px;margin: 7px 2px 0 0;"
                                     alt='Available, <?= $tooltip_text ?>'>
                                </span>Chat <span style="color: #228104">Online</span>
                                        </a>
                                    </li>
                                <?php } else { ?>
                                    <li>
                                        <a title='Offline<?= $tooltip_text ?>' class="chat" href='javascript:void(0)'
                                           onclick="window.open('<?php echo get_site_url(); ?>/chatMainMessage.php', 'Chat', 'resizable,height=350,width=500')">
                                    <span>
                                        <img alt="menu icon"
                                             src="<?php echo get_template_directory_uri(); ?>/images/chat_icon.png"
                                             style="width: 23px;height: 24px;margin: 7px 2px 0 0;"
                                             alt='Offline, <?= $tooltip_text ?>'/>
                                    </span>Chat <span style="color: #9f6515">Offline</span>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }
                            if($timesettings['contact_number'] == 1 ) {?>
                                <li><a href="tel:<?= $phone ?>" title="Online"><span class="icon-phone"></span><?= $phone ?></a></li>
                            <?php }
                            if ($timesettings['contact_page'] == 1) {
                                if ($_SESSION['contact_menu'] != '') $contact_menu = $_SESSION['contact_menu'];
                                else $contact_menu = 'Contact'; ?>
                                <li>
                                    <a href="<?= home_url() ?>/about-us/contact-us" class="" title='<?= $contact_menu ?>'><?= $contact_menu ?></a>
                                </li>
                            <?php } ?>
                        <?php foreach ($top_header_menu as $menu_item):
                            $login = in_array('login', $menu_item['menu_classes']);
                            $icon = "";
                            foreach ($menu_item['menu_classes'] as $class) {
                                if (stripos($class, "icon") !== false) {
                                    $icon = $class;
                                }
                            }


                            if ($timesettings['contact_page'] == 'Yes') {
                            if ($_SESSION['contact_menu'] != '') $contact_menu = $_SESSION['contact_menu'];
                            else $contact_menu = 'Contact'; ?>
                            <li <?php if ($_GET['action'] == 'contact'){ ?>class='active'<?php } ?>>
                                <a href="<?= $base_site_url ?>/contact" class=""
                                   title='<?= $contact_menu ?>'><?= $contact_menu ?></a>
                            </li>
                            <?php } ?>
                            <!-- Below line is not clear -->




                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

            </div>
        </div>
    </div>
    <nav aria-label="main navbar header-menu" class="navbar navbar-default">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button aria-expanded="false" data-target="#bs-navbar-collapse-1" data-toggle="collapse"
                            type="button" class="navbar-toggle collapsed"><span
                                class="sr-only"><?php _e('Toggle Navigation', 'balance'); ?></span><span
                                class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                    <!-- page logo--><a href="<?php echo get_home_url(); ?>" class="navbar-brand"><img
                                src="<?php echo get_template_directory_uri(); ?>/images/logo.svg" width="238"
                                height="51" alt="balance"></a>
                </div>
                <div id="bs-navbar-collapse-1" class="collapse navbar-collapse">

                    <?php
                        //Top header 2-level menu
                        $header_menu = get_menu("header", 0, 2, 0);
                    ?>

                    <?php if (!empty($header_menu)): ?>
                        <ul class="nav navbar-nav navbar-right">
                            <?php

                                foreach ($header_menu as $menu_item):
                                    $classesparent_array = $menu_item['menu_classes'];
                                    $classesparent = implode(" ", $classesparent_array);
                                    ?>

                                    <li>
                                        <a href="<?php echo $menu_item['menu_url']; ?>" aria-label="header menu"
                                           class="<?php echo(strpos($menu_item['menu_flag'], 'active') !== false ? 'active' : ''); ?>" <?php echo(!empty($menu_item['menu_target']) ? ' target="' . $menu_item['menu_target'] . '"' : ''); ?>><?php echo $menu_item['menu_title']; ?>
                                            <?php if (count($menu_item['childs']) > 0): ?>
                                                <span class="dropdown-toggle custom-caret visible-xs">
                      <img src="<?php echo get_template_directory_uri(); ?>/images/drop-arrow.svg" width="19"
                           height="11" alt="">
                    </span>
                                            <?php endif; ?>
                                        </a>

                                        <!-- Second level -->
                                        <?php if (count($menu_item['childs']) > 0): ?>

                                            <!-- Mega menu -->
                                            <?php if (in_array('mega', $classesparent_array)):

                                                $countchild = 0;
                                                //left and right mega menu column
                                                $left = "";
                                                $right = "";
                                                foreach ($menu_item['childs'] as $menu_item_child):
                                                    if ($countchild++ % 2 == 0):
                                                        $left .= '<li><a href="' . $menu_item_child['menu_url'] . '"' . (!empty($menu_item_child['menu_target']) ? ' target="' . $menu_item_child['menu_target'] . '"' : '') . '><span class="' . $menu_item_child['menu_classes'][0] . '"></span><span class="menu-text">' . $menu_item_child['menu_title'] . '</span></a></li>';
                                                    elseif ($countchild++ % 2 == 1):
                                                        $right .= '<li><a href="' . $menu_item_child['menu_url'] . '"' . (!empty($menu_item_child['menu_target']) ? ' target="' . $menu_item_child['menu_target'] . '"' : '') . '><span class="' . $menu_item_child['menu_classes'][0] . '"></span><span class="menu-text">' . $menu_item_child['menu_title'] . '</span></a></li>';

                                                    else:
                                                        $left .= '<li><a href="' . $menu_item_child['menu_url'] . '"' . (!empty($menu_item_child['menu_target']) ? ' target="' . $menu_item_child['menu_target'] . '"' : '') . '><span class="' . $menu_item_child['menu_classes'][0] . '"></span><span class="menu-text">' . $menu_item_child['menu_title'] . '</span></a></li>';
                                                    endif;

                                                endforeach;

                                                ?>

                                                <div class="slide-drop">
                                                    <ul class="custom-dropdown-menu <?php echo str_replace("mega", "", $classesparent); ?>">
                                                        <li class="mega">
                                                            <ul><?php echo $left; ?></ul>
                                                            <ul><?php echo $right; ?></ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!-- End mega menu -->
                                            <?php else: ?>
                                                <div class="slide-drop">
                                                    <ul class="custom-dropdown-menu <?php echo str_replace("mega", "", $classesparent); ?>">
                                                        <?php foreach ($menu_item['childs'] as $menu_item_child): ?>
                                                            <?php echo '<li><a href="' . $menu_item_child['menu_url'] . '"' . (!empty($menu_item_child['menu_target']) ? ' target="' . $menu_item_child['menu_target'] . '"' : '') . '>' . $menu_item_child['menu_title'] . '</a></li>'; ?>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <!-- End Second level -->
                                    </li>
                                <?php endforeach; ?>
                            <?php
                                if (!empty(get_current_user_id())) {
                                    ?>
                                    <li class="visible-xs">
                                        <a href="/my-account/"><?php _e('My Account', 'balance'); ?></a>
                                    </li>
                                    <li class="visible-xs">
                                        <a href="/my-account/customer-logout"><?php _e('Log Out', 'balance'); ?></a>
                                    </li>
                                <?php } else { ?>
                                    <li class="visible-xs">
                                        <a aria-label="header menu" data-toggle="modal" data-target="#Modal1"
                                           href="#Modal1" class="dropdown-toggle"><?php _e('Log In', 'balance'); ?></a>
                                    </li>
                                <?php } ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </nav>
</header>

<!-- Include modal WC register/login -->
<?php require_once get_template_directory() . '/woocommerce/modal-login.php' ?>

<main id="main">

    <?php get_breadcrumbs(); ?>
