<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://roidna.com
 * @since      1.0.0
 *
 * @package    Balance_Access_Groups
 * @subpackage Balance_Access_Groups/admin/partials
 */


    $status = "publish";

    $post_type = "access_group";

    if (isset($_REQUEST['tableview']) && ($_REQUEST['tableview'] === "trash"))
    {
        $status = "trash";
    }

    $table = new access_group_List_Table();

    $table->prepare_items($status);

    ?>
    <div class="wrap">

        <h1>Resources Groups <a href="<?php echo admin_url("post-new.php?post_type=access_group")?>" class="page-title-action">Add New Resource Group</a></h1>

        <form id="access_groups" method="get">
            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <?php

            $publishClass = ($status=="publish")?('class="current"'):"";
            $trashClass = ($status=="trash")?('class="current"'):"";

            $countPublish = Balance_Access_Groups_DB::count("publish");
            $countTrash = Balance_Access_Groups_DB::count("trash");
            echo '<form id="search-access-groups" method="get" action="' . admin_url( 'admin.php' ) . '">';
            echo '<input type="hidden" name="page" value="access_groups_list" />';
            $table->search_box('Search Resource Groups' , 'search_id');
            echo '</form>';
            echo '<ul class="subsubsub">';
            echo '<li class="publish"><a '.$publishClass.' href="' . admin_url( 'admin.php?page=access_groups_list' ) . '">Published <span class="count">(' .$countPublish . ')</span></a> | </li>';
            echo '<li class="trash" ><a '.$trashClass .' href="'. admin_url( 'admin.php?page=access_groups_list&tableview=trash' ) . '">Trash <span class="count">(' . $countTrash . ')</span></a>  </li>';
            echo '</ul>';

            ?>

            <!-- Now we can render the completed list table -->
            <?php $table->display() ?>
        </form>

    </div>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
