<?php

if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * Extended WP_List_Table class for life stages overview  listing
 *
 *
 * @package    Balance_Life_Stages
 * @subpackage Balance_Life_Stages/admin
 * @author     ROIDNA <devs@roidna.com>
 */
class WP_Life_Stages_List_Table extends WP_List_Table {

   /**
   * The post_type of this class.
   *
   * @since    1.0.0
   * @access   private
   * @var      string    $post_type    The current post_type for which we are building the wp list table.
   */
  private $post_type;
  public  $post_status;
  private $orderby;
  private $orientation;

    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct( $post_type ){
        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => $post_type,     //singular name of the listed records
            'plural'    => $post_type . 's',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );

        $this->post_type = $post_type;
        $this->post_status = isset($_GET['post_status']) ? $_GET['post_status'] : 'publish';
        $this->orderby = isset($_GET['orderby']) ? $_GET['orderby'] : 'post_date';
        $this->orientation = isset($_GET['order']) ? $_GET['order'] : 'DESC';

    }

    /** ************************************************************************
     * Recommended. This is a custom column method and is responsible for what
     * is rendered in any column with a name/slug of 'title'. Every time the class
     * needs to render a column, it first looks for a method named
     * column_{$column_title} - if it exists, that method is run. If it doesn't
     * exist, column_default() is called instead.
     *
     * This example also illustrates how to implement rollover actions. Actions
     * should be an associative array formatted as 'slug'=>'link html' - and you
     * will need to generate the URLs yourself. You could even ensure the links
     *
     *
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/


    function column_title($item){

        //Build row actions
        if( $this->post_status != 'trash' ){
            $complete_url = wp_nonce_url( admin_url( 'post.php?post=' . $item['ID'] . '&action=trash'), 'trash-post_'.$item['ID'] );

            $actions = array(
                'edit'   => '<a href="' . admin_url( 'post.php?post=' . $item['ID'] . '&action=edit' ) . '">Edit</a>',
                'view'   => '<a href="' . get_permalink( $item['ID'] ) . '">View</a>',
                'trash'  => '<a href="' . $complete_url . '">Trash</a>',
            );

        } else { //if trash

            $complete_url = wp_nonce_url( admin_url( 'post.php?post=' . $item['ID'] . '&action=delete'), 'delete-post_'.$item['ID'] );
            $complete_url_restore = wp_nonce_url( admin_url( 'post.php?post=' . $item['ID'] . '&action=untrash'), 'untrash-post_'.$item['ID'] );

            $actions = array(
                'restore'   => '<a href="' . $complete_url_restore . '">Restore</a>',
                'delete'    => '<a href="' . $complete_url . '">Delete Permanently</a>',

            );

        }

        //Return the title contents
        return sprintf('%1$s %2$s',
            /*$1%s*/ '<a class="row-title" href="' . admin_url( 'post.php?post=' . $item['ID'] . '&action=edit' ) . '">' . $item['post_title'] . '</a>',
            /*$2%s*/ $this->row_actions($actions)
        );
    }

    function column_access_level($item) {
    	$access_level = get_post_meta( $item['ID'], 'access_level', true );
		switch ( $access_level ) {
		case '200':
			return 'Registered users';
			break;
		default:
			return  'Non registered users';
			break;
		}
    }

    /** ************************************************************************
     * REQUIRED! This method dictates the table's columns and titles. This should
     * return an array where the key is the column slug (and class) and the value
     * is the column's title text. If you need a checkbox for bulk actions, refer
     * to the $columns array below.
     *
     * The 'cb' column is treated differently than the rest. If including a checkbox
     * column in your table you must create a column_cb() method. If you don't need
     * bulk actions or checkboxes, simply leave the 'cb' entry out of your array.
     *
     * @see WP_List_Table::::single_row_columns()
     * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_columns(){
        $columns = array(
            'title'     => 'Title',
            'access_level' => 'Access Level'
        );
        return $columns;
    }

    function get_sortable_columns() {
      $sortable_columns = array();
      return $sortable_columns;
    }

    /**
     * Overrides core single row
     *
     * @since 3.1.0
     * @access public
     *
     * @param object $item The current item
     */
    public function single_row( $item ) {

        echo '<tr class="life-stage-list-parent" data-value="'.$item['ID'].'">';
        $this->single_row_columns( $item );
        echo '</tr>';

    }


    /**
     * OVERRIDE Generates the columns for a single row of the table
     *
     * Outputs a parent row and all it's children
     *
     * @since 3.1.0
     * @access protected
     *
     * @param object $item The current item
     */
    protected function single_row_columns( $item ) {
        global $wpdb; //This is used only if making any database queries

        list( $columns, $hidden, $sortable, $primary ) = $this->get_column_info();

        $primary = $column_name = "title";

        $classes = "title column-title";

        // Comments column uses HTML in the display name with screen reader text.
        // Instead of using esc_attr(), we strip tags to get closer to a user-friendly string.
        $data = 'data-colname="Title"';

        $attributes = "class='$classes' $data";

        if ( method_exists( $this, 'column_title' ) ) {
            echo "<td $attributes>";
            if( $this->post_status == 'publish' ){
                echo '<span class="dashicons dashicons-menu" title="Drag up or down to reorder its position"></span>';
            }
            echo call_user_func( array( $this, 'column_title' ), $item );
            echo $this->handle_row_actions( $item, $column_name, $primary );

            //only get posts that has current item as a parent
            $data_childs = $wpdb->get_results( "SELECT * FROM " . $wpdb->posts . " WHERE post_parent = ". $item['ID'] ." AND post_type = '$this->post_type' AND post_status = '$this->post_status' ORDER BY menu_order ASC", ARRAY_A);

            //if parent has children, assamble a list of children
            if( !empty( $data_childs ) ){
                echo "<ul>";
                //list all children under current parent
                foreach ($data_childs as $item_child) {
                    echo '<li class="'. $item['ID'] .'-child life-stage-list-child" data-value="'. $item_child['ID'] .'">';

                    echo '<span class="dashicons dashicons-editor-break"></span>';
                    echo '<span class="dashicons dashicons-menu" title="Drag up or down to reorder its position"></span>';

                    echo call_user_func( array( $this, 'column_title' ), $item_child );
                    echo $this->handle_row_actions( $item_child, $column_name, $primary );

                    echo '</li>';
                }
                echo "</ul>";
            }

            echo "</td>";
        }
        if ( method_exists( $this, 'column_access_level' ) ) {
        	echo "<td $attributes>";
        	echo call_user_func( array( $this, 'column_access_level' ), $item );
        	echo "</td>";
        }

    }

    /**
     * Function for getting all the unassigned life_experiences that do not have a parent
     *
     * @return array $data
     */
    public function get_unassigned_life_experiences(){
        global $wpdb; //This is used only if making any database queries

        //get unassigned life experiences
        $data = $wpdb->get_results( "SELECT " . $wpdb->posts . ".* FROM " . $wpdb->posts . "
                                     INNER JOIN " . $wpdb->postmeta . " ON " . $wpdb->postmeta . ".post_id = " . $wpdb->posts . ".ID AND " . $wpdb->postmeta . ".meta_value = 'life_experience'
                                     WHERE post_parent = '' AND post_type = 'life_stage' AND post_status = 'publish'
                                     ORDER BY $this->orderby $this->orientation", ARRAY_A);

        return $data;

    }

    /**
     * Function for getting all the life_stages that do not have a parent
     *
     * @return array $data
     */
    private function get_published_life_stages(){
        global $wpdb; //This is used only if making any database queries

        //get unassigned life experiences
        $data = $wpdb->get_results( "SELECT " . $wpdb->posts . ".* FROM " . $wpdb->posts . "
                                     INNER JOIN " . $wpdb->postmeta . " ON " . $wpdb->postmeta . ".post_id = " . $wpdb->posts . ".ID AND " . $wpdb->postmeta . ".meta_value = 'life_stage'
                                     WHERE ".( $this->post_status == 'publish' ? 'post_parent = 0 AND' : '' )." post_parent = '' AND post_type = '$this->post_type' AND post_status = '$this->post_status'
                                     ORDER BY menu_order ASC", ARRAY_A);

        return $data;

    }

    /**
     * Function for getting all the life_stages in draft
     *
     * @return array $data
     */
    private function get_drafted_life_stages(){
        global $wpdb; //This is used only if making any database queries
        //get unassigned life experiences
        $data = $wpdb->get_results( "SELECT * FROM " . $wpdb->posts . " WHERE post_type = '$this->post_type' AND ( post_status = '$this->post_status'  OR post_status = 'auto-draft' ) ORDER BY $this->orderby $this->orientation", ARRAY_A);

        return $data;

    }

    public function delete_revisions_auto_save(){
        global $wpdb; //This is used only if making any database queries

        //get unassigned life experiences
        $posts_to_delete = $wpdb->get_results( "SELECT " . $wpdb->posts . ".* FROM " . $wpdb->posts . " WHERE post_status = 'auto-draft' AND post_type = 'life_stage' ", ARRAY_A);

        if( !empty($posts_to_delete) ){
            //deleta all the posts
            foreach ($posts_to_delete as $post) {
                wp_delete_post( $post['ID'], true );
            }
        }

    }

    /** ************************************************************************
     * REQUIRED! This is where you prepare your data for display. This method will
     * usually be used to query the database, sort and filter the data, and generally
     * get it ready to be displayed. At a minimum, we should set $this->items and
     * $this->set_pagination_args(), although the following properties and methods
     * are frequently interacted with here...
     *
     * @global WPDB $wpdb
     * @uses $this->_column_headers
     * @uses $this->items
     * @uses $this->get_columns()
     * @uses $this->get_sortable_columns()
     * @uses $this->get_pagenum()
     * @uses $this->set_pagination_args()
     **************************************************************************/
    function prepare_items() {
        global $wpdb; //This is used only if making any database queries

        /**
         * First, lets decide how many records per page to show
         */
        $per_page = 50;

        /**
         * REQUIRED. Now we need to define our column headers. This includes a complete
         * array of columns to be displayed (slugs & titles), a list of columns
         * to keep hidden, and a list of columns that are sortable. Each of these
         * can be defined in another method (as we've done here) before being
         * used to build the value for our _column_headers property.
         */
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        /**
         * REQUIRED. Finally, we build an array to be used by the class for column
         * headers. The $this->_column_headers property takes an array which contains
         * 3 other arrays. One for all columns, one for hidden columns, and one
         * for sortable columns.
         */
        $this->_column_headers = array($columns, $hidden, $sortable);

        /**
         * Optional. You can handle your bulk actions however you see fit. In this
         * case, we'll handle them within our package just to keep things clean.
         */
        $this->process_bulk_action();

        /**
         * Instead of querying a database, we're going to fetch the example data
         * property we created for use in this plugin. This makes this example
         * package slightly different than one you might build on your own. In
         * this example, we'll be using array manipulation to sort and paginate
         * our data. In a real-world implementation, you will probably want to
         * use sort and pagination data to build a custom query instead, as you'll
         * be able to use your precisely-queried data immediately.
         */

        if ( $this->post_status == 'unassigned' ){
            $data = $this->get_unassigned_life_experiences();
        }
        elseif ( $this->post_status == 'draft' || $this->post_status == 'trash' ) {
            $data = $this->get_drafted_life_stages();
        }
        else {
            $data = $this->get_published_life_stages();
        }


        /**
         * REQUIRED for pagination. Let's check how many items are in our data array.
         * In real-world use, this would be the total number of items in your database,
         * without filtering. We'll need this later, so you should always include it
         * in your own package classes.
         */
        $current_page = $this->get_pagenum();
        $total_items = count($data);

        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'per_page'    => $per_page,
        ));

        $this->items = array_slice( $data, ( ($current_page - 1) * $per_page ), $per_page );

    }
}
