(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
   // hide the page template label
  $(function() {

    //if published view and if there are items
    if( $(".life-stages_page_life_stages_list #life-stages").hasClass("publish") && !$("#the-list > tr").hasClass("no-items") ){
      $(".life-stage-list-parent ul").each(function() {
          $(this).sortable({
            placeholder: "meta-box-sortables ui-sortable empty-container",
            stop: function(evt, ui) {
                
              var allChildsValues = [];

              $(this).children(".life-stage-list-child").each(function(index, el) {
                allChildsValues.push( $(el).attr("data-value") );
              });

              var data = {
                'action': 'set_posts_menu_order',
                'elements': allChildsValues,
              };

              $.post(ajaxurl, data, function(response) {});

            }
          });
          $(this).disableSelection();
      });

      $("#the-list").each(function() {
          $(this).sortable({
              placeholder: "meta-box-sortables ui-sortable empty-container",
              stop: function(evt, ui) {
                  
                var allParentValues = [];

                $(this).children(".life-stage-list-parent").each(function(index, el) {
                  allParentValues.push( $(el).attr("data-value") );
                });

                var data = {
                  'action': 'set_posts_menu_order',
                  'elements': allParentValues,
                };

                $.post(ajaxurl, data, function(response) {});

              }
          });
          $(this).disableSelection();
      });
    }
    

  });
})( jQuery );
