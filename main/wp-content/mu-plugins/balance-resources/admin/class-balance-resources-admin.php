<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://roidna.com
 * @since      1.0.0
 *
 * @package    Balance_Resources
 * @subpackage Balance_Resources/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Balance_Resources
 * @subpackage Balance_Resources/admin
 * @author     ROIDNA <devs@roidna.com>
 */
class Balance_Resources_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param string  $plugin_name The name of this plugin.
	 * @param string  $version     The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Balance_Resources_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Balance_Resources_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/balance-resources-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Balance_Resources_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Balance_Resources_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/balance-resources-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Creates a custom table for resources
	 *
	 * @since     1.0.0
	 * @uses      $wpdb
	 */
	public function resources_install() {
		global $wpdb;
		global $resources_db_version;

		$prefix = $wpdb->prefix;
		$resources_table_name = $prefix . 'resources';
		$resources_view_count_table_name = $resources_table_name . '_view_count';
		$resources_search_keywords_table_name = $resources_table_name . '_keyword_search';
		$post_table_name = $prefix . 'posts';
		$post_table_id = $prefix . 'post_id';

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $resources_table_name (
			      ID bigint(20) NOT NULL AUTO_INCREMENT,
			      title longtext DEFAULT '' NULL,
			      post_title longtext DEFAULT '' NULL,
			      slug longtext DEFAULT '' NULL,
			      hero_html longtext DEFAULT '' NULL,
			      chapter_select_html longtext DEFAULT '' NULL,
			      html longtext DEFAULT '' NULL,
			      pager_html longtext DEFAULT '' NULL,
			      type tinytext DEFAULT '' NULL,
			      level_of_access mediumint	 DEFAULT 100 NOT NULL,
			      list_in_search varchar(255) DEFAULT 'true'  NOT NULL,
			      status varchar(255) DEFAULT 'auto-draft' NOT NULL,
			      page_order bigint(20) DEFAULT 1 NOT NULL,
			      $post_table_id bigint(20) NOT NULL,
			      UNIQUE KEY ID (ID)
			    );
			    CREATE fulltext index content ON $resources_table_name (html, title);
					CREATE fulltext index content ON $post_table_name (post_excerpt);
			    CREATE TABLE $resources_view_count_table_name (
			      ID bigint(20) NOT NULL AUTO_INCREMENT,
			      white_label_website_id bigint(20) DEFAULT 0 NOT NULL,
			      $post_table_id bigint(20) NOT NULL,
			      page bigint(20) DEFAULT 1 NOT NULL,
			      view_count bigint(20) DEFAULT 0 NOT NULL,
			      UNIQUE KEY ID (ID)
			    );
			    CREATE TABLE $resources_search_keywords_table_name (
			      ID bigint(20) NOT NULL AUTO_INCREMENT,
			      keyword longtext DEFAULT '' NOT NULL,
			      inserted_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			      white_label_website_id bigint(20) DEFAULT 0 NOT NULL,
			      user_id bigint(20) DEFAULT 0 NOT NULL,
			      UNIQUE KEY ID (ID)
			    ) $charset_collate ENGINE=MyISAM;";

		require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		dbDelta( $sql );

		add_option( 'resources_db_version', $resources_db_version );
	}

	/**
	 * Called on save_post to insert or update the data in the resources table
	 *
	 * @since   1.0.0
	 * @access  public
	 * @param int     $post_id The post ID
	 * @param object  $object  The post object
	 * @return  void
	 */
	public function insert_update_resource( $post_id, $object ) {
		global $resources_cpts;
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return $post_id; }
		if ( ! current_user_can( 'edit_post', $post_id ) ) { return $post_id; }
		if ( ! current_user_can( 'edit_page', $post_id ) ) { return $post_id; }
		// if ( $object->post_status == 'auto-draft' ) { return $post_id; }
		if ( in_array( $object->post_type, $resources_cpts ) || $object->post_type == 'program' ) { // if the type is one of the resources
			global $wpdb;

			// expose for the partials
			global $data;
			global $additional_body_class;

			$prefix = $wpdb->prefix;
			$resources_table_name = $prefix . 'resources';
			$post_table_id = $prefix . 'post_id';

			$exists = $wpdb->get_results(
				"
				SELECT
					ID
				FROM
					$resources_table_name
				WHERE
					$post_table_id = $post_id
				"
			);

			$data = get_post_custom( $post_id );
			$unserialized_data = '';

			if ( isset( $data['_page_edit_data'] ) ) {
				$unserialized_data = @unserialize( $data['_page_edit_data'][0] );
			}
			if ( $unserialized_data === false ) {
				$unserialized_data = array();
			}

			$data = $unserialized_data;

			$hero_html = '';

			if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
				$hero_html = render_m1_hero( $data['m1_module'][0] );
			}

			$chapter_select_html = '';

			$html = '';
			$pager = '';
			$post_title = '';
			$post_name = '';

			if ( $object->post_type == 'article' ) {
				$pages = array();
				if ( !empty( $data['multipage_module_module'] ) && !empty( $data['multipage_module_module'][0]['pages'] ) ) {
					$pages = $data['multipage_module_module'][0]['pages'];
				}

				// cleanup any previous article data
				if ( $exists ) {
					$this->delete_resource( $post_id );
				}
				// when inserting for the multipage article insert each separate page as a resource in wp_resources table
				// store selector in case article is multipage
				global $total_article_pages; // need to expose the variable used by the partial
				global $article_page; // need to expose this so that correct page is selected
				$current_page_pos = 0;
				$page_count = count( $pages );
				$total_article_pages = $page_count;
				if ( $page_count > 1 && !empty( $data['multipage_module_module'] ) && !empty( $data['multipage_module_module'][0] )  && !empty( $data['multipage_module_module'][0]['last_page_quiz'] ) ) {
					$total_article_pages += 1;
				}
				$additional_body_class = 'single-article';

				foreach ( $pages as $key => $page ) {
					$current_page_pos++;
					$article_page = $current_page_pos;

					if ( $page_count > 1 ) {
						ob_start();
						// only if there are multiple pages
						get_template_part( 'partials/multipage-article-chapters' );
						$chapter_select_html = ob_get_contents();
						ob_clean();
						get_template_part( 'partials/multipage-article-content' );
						$html = ob_get_contents();
						ob_clean();
						get_template_part( 'partials/multipage-article-pager' );
						$pager = ob_get_contents();
						ob_end_clean();
					} else {
						ob_start();
						get_template_part( 'partials/simple-article' );
						$html = ob_get_clean();
						ob_end_clean();
					}

					$post_title = '';
					$post_name = '';
					$post_title = !empty( $page['title'] ) ? $page['title'] : ( $object->post_title . ' - ' . __( 'Page', 'balance' ) . ' ' . ( $key + 1 ) );
					$post_name = $object->post_name;
					$this->insert_row_resource( $wpdb, $resources_table_name, $object, $post_id, $post_title, $post_name, $post_table_id, $hero_html, $chapter_select_html, $html, $pager, $key + 1 );
				}
				// if quiz is inserted add it as last page in multi page article
				if ( !empty( $data['multipage_module_module'] ) && !empty( $data['multipage_module_module'] )  && !empty( $data['multipage_module_module'][0]['last_page_quiz'] ) ) {
					$quiz = get_post( $data['multipage_module_module'][0]['last_page_quiz'] );
					if ( !empty( $quiz ) ) {
						$article_page++;
						$post_title = $quiz->post_title;
						$post_name = $object->post_name;
						ob_start();
						get_template_part( 'partials/multipage-article-chapters' );
						$chapter_select_html = ob_get_contents();
						ob_clean();
						get_template_part( 'partials/multipage-article-content' );
						$html = ob_get_contents();
						ob_clean();
						get_template_part( 'partials/multipage-article-pager' );
						$pager = ob_get_contents();
						ob_end_clean();
						$this->insert_row_resource( $wpdb, $resources_table_name, $object, $post_id, $post_title, $post_name, $post_table_id, $hero_html, $chapter_select_html, $html, $pager, count( $pages ) +1 );
					}
				}
			} else {
				// insert for all other resources
				$html_tpl = false;
				switch ( $object->post_type ) {
				case 'calculator':
					$additional_body_class = 'single-calculator';
					$html_tpl = 'partials/calculator';
					break;
				case 'video':
					$additional_body_class = 'single-video';
					$html_tpl = 'partials/video-components';
					break;
				case 'newsletter':
					$additional_body_class = 'single-newsletter';
					$html_tpl = 'partials/simple-content';
					break;
				case 'checklist':
					$additional_body_class = 'single-checklist';
					$html_tpl = 'partials/downloadable-resource';
					break;
				case 'podcast':
					$additional_body_class = 'single-podcast';
					$html_tpl = 'partials/downloadable-resource';
					break;
				case 'toolkit':
					$additional_body_class = 'single-toolkit';
					$html_tpl = 'partials/program';
					break;
				case 'worksheet':
					$additional_body_class = 'single-worksheet';
					$html_tpl = 'partials/downloadable-resource';
					break;
				case 'booklet':
					$additional_body_class = 'single-booklet';
					$html_tpl = 'partials/downloadable-resource';
					break;
				case 'program':
					if ( !empty( $data['m56b_module'] ) && !empty( $data['m56b_module'][0] )
						&& !empty( $data['m56b_module'][0]['program_type'] ) && $data['m56b_module'][0]['program_type'] == 'program_resources' ) {
						$additional_body_class = 'single-program';
						$html_tpl = 'partials/program';
					}
					break;
				}

				if ( $html_tpl !== false ) {
					ob_start();
					get_template_part( $html_tpl );
					$html = ob_get_clean();
					ob_end_clean();
				}
				if ( $exists ) {
					$this->update_row_resource( $wpdb, $resources_table_name, $object, $post_id, $post_title, $post_name, $post_table_id, $hero_html, $chapter_select_html, $html, $pager, 1 );
				} else {
					$this->insert_row_resource( $wpdb, $resources_table_name, $object, $post_id, $post_title, $post_name, $post_table_id, $hero_html, $chapter_select_html, $html, $pager, 1 );
				}
			}
		}
	}

	private function insert_row_resource( $wpdb, $resources_table_name, $object, $post_id, $post_title = '', $post_name = '', $post_table_id, $hero_html = '', $chapter_select_html = '', $html = '', $pager ='', $order = 1 ) {
		$wpdb->insert(
			$resources_table_name,
			array(
				'title' => empty( $post_title ) ? $object->post_title : $post_title,
				'post_title' => $object->post_title,
				'slug' => empty( $post_name ) ? $object->post_name : $post_name,
				'hero_html' => $hero_html,
				'chapter_select_html' => $chapter_select_html,
				'html' => $html,
				'pager_html' => $pager,
				'type' => $object->post_type,
				'status' => $object->post_status,
				'page_order' => $order,
				'level_of_access' => !empty( get_post_meta( $post_id, 'access_level', true ) ) ? get_post_meta( $post_id, 'access_level', true ) : '100',
				'list_in_search' => !empty( get_post_meta( $post_id, 'list_in_search', true ) ) ? get_post_meta( $post_id, 'list_in_search', true ) : 'true',
				$post_table_id => $post_id
			),
			array( // format of the columns
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
				'%s',
				'%d',
			)
		);
	}

	private function update_row_resource( $wpdb, $resources_table_name, $object, $post_id, $post_title = '', $post_name = '', $post_table_id, $hero_html = '', $chapter_select_html = '', $html = '', $pager = '', $order = 1 ) {
		$update = $wpdb->update(
			$resources_table_name, // table
			array( // columns
				'title' => empty( $post_title ) ? $object->post_title : $post_title,
				'post_title' => $object->post_title,
				'slug' => empty( $post_name ) ? $object->post_name : $post_name,
				'hero_html' => $hero_html,
				'chapter_select_html' => $chapter_select_html,
				'html' => $html,
				'pager_html' => $pager,
				'type' => $object->post_type,
				'status' => $object->post_status,
				'page_order' => $order,
				'level_of_access' => get_post_meta( $post_id, 'access_level', true ),
				'list_in_search' => get_post_meta( $post_id, 'list_in_search', true ),
				$post_table_id => $post_id
			),
			array( // where
				$post_table_id => $post_id,
			),
			array( // format of the columns
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
				'%s',
				'%d',
			),
			array( // where format
				'%d'
			)
		);
	}

	/**
	 * Called on delete_post to cleanup the data in the resources table
	 *
	 * @since   1.0.0
	 * @access  public
	 * @param int     $post_id The post ID
	 * @return  void
	 */
	public function delete_resource( $post_id ) {
		global $wpdb;
		$prefix = $wpdb->prefix;
		$resources_table_name = $prefix . 'resources';
		$post_table_id = $prefix . 'post_id';

		$wpdb->delete(
			$resources_table_name,
			array( // where
				$post_table_id => $post_id
			),
			array( // where format
				'%d'
			)
		);
	}

	/**
	 * Creates a resources custom post types (multiple post types declaration)
	 *
	 * @since     1.0.0
	 * @uses      register_post_type()
	 */
	public function new_cpts_resources() {
		global $resources_cpts;
		$cap_type = 'post';
		// register each custom post type in the array
		foreach ( $resources_cpts as $cpt_name ) {
			$plural = ucfirst( $cpt_name  ) . 's';
			$single = ucfirst( $cpt_name  );
			$opts['can_export'] = TRUE;
			$opts['capability_type'] = $cap_type;
			$opts['description'] = '';
			$opts['exclude_from_search'] = FALSE;
			$opts['has_archive'] = FALSE;
			$opts['hierarchical'] = FALSE;
			$opts['map_meta_cap'] = TRUE;
			$opts['menu_icon'] = 'dashicons-admin-post';
			$opts['menu_position'] = 25;
			$opts['public'] = TRUE;
			$opts['publicly_querable'] = TRUE;
			$opts['query_var'] = TRUE;
			$opts['register_meta_box_cb'] = '';
			$opts['rewrite'] = array( 'slug' => 'resources/'.$cpt_name.'s', 'with_front' => true );
			$opts['show_in_admin_bar'] = TRUE;
			$opts['show_in_menu'] = FALSE;
			$opts['show_in_nav_menu'] = TRUE;
			$opts['show_ui'] = TRUE;
			$opts['supports'] = array( 'title', 'excerpt', 'thumbnail' );
			$opts['taxonomies'] = array();
			$opts['capabilities']['delete_others_posts'] = "delete_others_{$cap_type}s";
			$opts['capabilities']['delete_post'] = "delete_{$cap_type}";
			$opts['capabilities']['delete_posts'] = "delete_{$cap_type}s";
			$opts['capabilities']['delete_private_posts'] = "delete_private_{$cap_type}s";
			$opts['capabilities']['delete_published_posts'] = "delete_published_{$cap_type}s";
			$opts['capabilities']['edit_others_posts'] = "edit_others_{$cap_type}s";
			$opts['capabilities']['edit_post'] = "edit_{$cap_type}";
			$opts['capabilities']['edit_posts'] = "edit_{$cap_type}s";
			$opts['capabilities']['edit_private_posts'] = "edit_private_{$cap_type}s";
			$opts['capabilities']['edit_published_posts'] = "edit_published_{$cap_type}s";
			$opts['capabilities']['publish_posts'] = "publish_{$cap_type}s";
			$opts['capabilities']['read_post'] = "read_{$cap_type}";
			$opts['capabilities']['read_private_posts'] = "read_private_{$cap_type}s";
			$opts['labels']['add_new'] = __( "Add New {$single}", 'balance' );
			$opts['labels']['add_new_item'] = __( "Add New {$single}", 'balance' );
			$opts['labels']['all_items'] = __( $plural, 'balance' );
			$opts['labels']['edit_item'] = __( "Edit {$single}" , 'balance' );
			$opts['labels']['menu_name'] = __( $plural, 'balance' );
			$opts['labels']['name'] = __( $plural, 'balance' );
			$opts['labels']['name_admin_bar'] = __( $single, 'balance' );
			$opts['labels']['new_item'] = __( "New {$single}", 'balance' );
			$opts['labels']['not_found'] = __( "No {$plural} Found", 'balance' );
			$opts['labels']['not_found_in_trash'] = __( "No {$plural} Found in Trash", 'balance' );
			$opts['labels']['parent_item_colon'] = __( "Parent {$plural} :", 'balance' );
			$opts['labels']['search_items'] = __( "Search {$plural}", 'balance' );
			$opts['labels']['singular_name'] = __( $single, 'balance' );
			$opts['labels']['view_item'] = __( "View {$single}", 'balance' );
			register_post_type( strtolower( $cpt_name ), $opts );

			add_filter( 'manage_edit-'.$cpt_name.'_columns', array( $this, 'edit_cpts_resources_columns' ) );
			add_action( 'manage_' . $cpt_name . '_posts_custom_column', array( $this, 'manage_cpts_resources_column' ), 10, 2 );
			add_action( 'restrict_manage_posts', array( $this, 'add_resource_category_filters' ), 10, 1, $cpt_name );
		}
	} // new_cpts_resources()

	/**
	 * Creates additional filter on the list screen for tags
	 *
	 * @since     1.0.0
	 * @uses      get_posts(), get_taxonomy(), get_terms()
	 */
	function add_resource_category_filters( $post_type ) {
		global $typenow, $resources_cpts;

		// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
		$taxonomies = array( 'resource_tag' );

		// must set this to the post type you want the filter(s) displayed on
		if ( $typenow == $post_type && in_array( $post_type, $resources_cpts ) ) {

			foreach ( $taxonomies as $tax_slug ) {
				$tax_obj = get_taxonomy( $tax_slug );
				$tax_name = $tax_obj->labels->name;
				$terms = get_terms( $tax_slug, array( 'hide_empty' => false ) );
				if ( count( $terms ) > 0 ) {
					echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
					echo "<option value=''>All Tags</option>";
					foreach ( $terms as $term ) {
						$items = get_posts( array(
								'post_type'   => $post_type,
								'numberposts' => -1,
								'taxonomy'    => 'resource_tag',
								'term'        => $term->slug
							) );
						$count =  count( $items );
						echo '<option value='. $term->slug . ( !empty( $_GET[$tax_slug] ) ? ( $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '' ) : '' ) . '>' . $term->name . ' (' . $count . ')</option>';
					}
					echo "</select>";
				}
			}
		}
	}

	/**
	 * customizes the columns on edit listing screen
	 *
	 * @since     1.0.0
	 */
	function edit_cpts_resources_columns( $columns ) {
		global $post;

		$columns = array(
			'cb' => '',
			'title' => 'Title',
			'access_level' => 'Access Level',
			'tags_' => 'Tags',
			'author' => 'Author',
			'date' => 'Date'
		);
		return $columns;
	} // edit_cpts_resources_columns()

	/**
	 * display the data for custom columns on edit listing screen
	 *
	 * @since     1.0.0
	 */
	function manage_cpts_resources_column( $column, $post_id ) {
		global $post;
		switch ( $column ) {
		case 'access_level' :
			$access_level = get_post_meta( $post->ID, 'access_level', true );
			switch ( $access_level ) {
			case '200':
				echo 'Registered users';
				break;
			default:
				echo  'Non registered users';
				break;
			}
			break;
		case 'tags_' :
			$resource_tags = wp_get_post_terms( $post->ID, 'resource_tag' );
			$output = '';
			foreach ( $resource_tags as $resource_tag ) {
				$output .= $resource_tag->name . ', ';
			}
			echo substr( $output, 0, -2 );
			break;
		}
	} // manage_cpts_resources_column()

	/**
	 * Creates a new taxonomy for a resources custom post types
	 *
	 * @since   1.0.0
	 * @access  public
	 * @uses  register_taxonomy()
	 */

	public function new_taxonomy_resource_tag() {
		global $resources_cpts;
		$plural = 'Tags';
		$single = 'Tag';
		$tax_name = 'resource_tag';
		$opts['hierarchical'] = FALSE;
		//$opts['meta_box_cb'] = '';
		$opts['public'] = TRUE;
		$opts['query_var'] = $tax_name;
		$opts['show_admin_column'] = FALSE;
		$opts['show_in_nav_menus'] = TRUE;
		$opts['show_tagcloud'] = FALSE;
		$opts['show_ui'] = TRUE;
		$opts['sort'] = '';
		//$opts['update_count_callback'] = '';
		$opts['capabilities']['assign_terms'] = 'edit_posts';
		$opts['capabilities']['delete_terms'] = 'manage_categories';
		$opts['capabilities']['edit_terms'] = 'manage_categories';
		$opts['capabilities']['manage_terms'] = 'manage_categories';
		$opts['labels']['add_new_item'] = __( "Add New {$single}", 'balance' );
		$opts['labels']['add_or_remove_items'] = __( "Add or remove {$plural}", 'balance' );
		$opts['labels']['all_items'] = __( $plural, 'balance' );
		$opts['labels']['choose_from_most_used'] = __( "Choose from most used {$plural}", 'balance' );
		$opts['labels']['edit_item'] = __( "Edit {$single}" , 'balance' );
		$opts['labels']['menu_name'] = __( $plural, 'balance' );
		$opts['labels']['name'] = __( $plural, 'balance' );
		$opts['labels']['new_item_name'] = __( "New {$single} Name", 'balance' );
		$opts['labels']['not_found'] = __( "No {$plural} Found", 'balance' );
		$opts['labels']['parent_item'] = __( "Parent {$single}", 'balance' );
		$opts['labels']['parent_item_colon'] = __( "Parent {$single}:", 'balance' );
		$opts['labels']['popular_items'] = __( "Popular {$plural}", 'balance' );
		$opts['labels']['search_items'] = __( "Search {$plural}", 'balance' );
		$opts['labels']['separate_items_with_commas'] = __( "Separate {$plural} with commas", 'balance' );
		$opts['labels']['singular_name'] = __( $single, 'balance' );
		$opts['labels']['update_item'] = __( "Update {$single}", 'balance' );
		$opts['labels']['view_item'] = __( "View {$single}", 'balance' );
		$opts['rewrite']['ep_mask'] = EP_NONE;
		$opts['rewrite']['hierarchical'] = FALSE;
		$opts['rewrite']['with_front'] = FALSE;
		register_taxonomy( $tax_name, $resources_cpts, $opts );
	} // new_taxonomy_resource_tag()


	/**
	 * Creates resources admin menu
	 *
	 * @since   1.0.0
	 * @access  public
	 * @uses  add_menu_page(), remove_submenu_page, add_submenu_page
	 */
	public function resources_admin_menu() {
		global $resources_cpts;
		remove_menu_page( 'edit.php' );

		$resources_submenu_pages = array();

		// add also Tags taxonomy
		$resources_submenu_pages[] = array(
			'parent_slug'   => 'resources',
			'page_title'    => 'All Resources',
			'menu_title'    => 'All Resources',
			'capability'    => 'read',
			'menu_slug'     => 'resources&list=all',
			'function'      => array( $this, 'callback_resources_admin_page' ), // Doesn't need a callback function.
		);

		// add also Tags taxonomy
		$resources_submenu_pages[] = array(
			'parent_slug'   => 'resources',
			'page_title'    => '',
			'menu_title'    => '&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;',
			'capability'    => 'read',
			'menu_slug'     => '#resource-dummy-link',
			'function'      => null, // Doesn't need a callback function.
		);

		// add for each cpt that should go under resources
		foreach ( $resources_cpts as $cpt_name ) {
			$resources_submenu_pages[] = array(
				'parent_slug'   => 'resources',
				'page_title'    => ucfirst( $cpt_name  ) . 's',
				'menu_title'    => ucfirst( $cpt_name  ) . 's',
				'capability'    => 'read',
				'menu_slug'     => 'edit.php?post_type=' . $cpt_name,
				'function'      => null, // Doesn't need a callback function.
			);
		}

		// add also Tags taxonomy
		$resources_submenu_pages[] = array(
			'parent_slug'   => 'resources',
			'page_title'    => '',
			'menu_title'    => '&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;',
			'capability'    => 'read',
			'menu_slug'     => '#resource-dummy-link',
			'function'      => null, // Doesn't need a callback function.
		);

		// add also Tags taxonomy
		$resources_submenu_pages[] = array(
			'parent_slug'   => 'resources',
			'page_title'    => 'Tags',
			'menu_title'    => 'Tags',
			'capability'    => 'read',
			'menu_slug'     => '/edit-tags.php?taxonomy=resource_tag',
			'function'      => null, // Doesn't need a callback function.
		);

		// Settings for custom admin menu
		$page_title = 'Resources';
		$menu_title = 'Resources';
		$capability = 'read';
		$menu_slug  = 'resources';
		$function   = array( $this, 'callback_resources_admin_page' );// Callback function which displays the page content.
		$icon_url   = 'dashicons-media-interactive';
		$position   = 21;

		// Add custom admin menu and remove first child(same as parent: Resources>Resources) and new renamed item
		add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );

		// Add each submenu item to custom admin menu.
		foreach ( $resources_submenu_pages as $submenu ) {
			add_submenu_page(
				$submenu['parent_slug'],
				$submenu['page_title'],
				$submenu['menu_title'],
				$submenu['capability'],
				$submenu['menu_slug'],
				$submenu['function']
			);
		}

		remove_submenu_page( 'resources', 'resources' );
		add_filter( 'parent_file', array( $this, 'resources_set_current_menu' ) );
	}

	/**
	 * Callback for resources overview page
	 *
	 * @since   1.0.0
	 * @access  public
	 */
	public function callback_resources_admin_page() {
		include plugin_dir_path( __FILE__ ) . 'partials/balance-resources-admin-display.php';
	}

	/**
	 * Sets the curent menu to the overview resources one
	 *
	 * @since   1.0.0
	 * @access  public
	 */
	function resources_set_current_menu( $parent_file ) {
		global $submenu_file, $current_screen, $pagenow, $resources_cpts;
		if ( in_array( $current_screen->post_type, $resources_cpts ) ) {
			if ( $pagenow == 'post-new.php' ) {
				$submenu_file = 'edit.php?post_type='.$current_screen->post_type;
			}
			$parent_file = 'resources';
		} elseif ( $current_screen->taxonomy == 'resource_tag' ) {
			$parent_file = 'resources';
		} elseif ( $pagenow == 'admin.php' && $current_screen->base = 'toplevel_page_resources' ) {
			$submenu_file = 'resources&list=all';
		}

		return $parent_file;

	}

	/**
	 * Creates resources meta boxes
	 *
	 * @since   1.0.0
	 * @access  public
	 * @uses  add_meta_box()
	 */
	public function add_metaboxes( $post_type ) {
		global $resources_cpts;
		// add_meta_box( $id, $title, $callback, $screen, $context, $priority, $callback_args );
		if ( in_array( $post_type, $resources_cpts ) || $post_type == 'program' || $post_type == 'life_stage' ) {
			add_meta_box(
				'access_level',
				'Access Level',
				array( $this, 'callback_metabox_access_level' ),
				$post_type,
				'side',
				'default'
			);
			if ( in_array( $post_type, $resources_cpts ) ) {
				add_meta_box(
					'list_in_search',
					'List In Resources Search',
					array( $this, 'callback_metabox_list_in_search' ),
					$post_type,
					'side',
					'default'
				);
			}
		}
	} // add_metaboxes()

	/**
	 * Callback for access level metabox
	 *
	 * @since   1.0.0
	 * @access  public
	 */
	public function callback_metabox_access_level( $object, $box ) {
		include plugin_dir_path( __FILE__ ) . 'partials/balance-resources-admin-display-metabox-access-level.php';
	} // callback_metabox_access_level()

	/**
	 * Callback for list in search metabox
	 *
	 * @since   1.0.0
	 * @access  public
	 */
	public function callback_metabox_list_in_search( $object, $box ) {
		include plugin_dir_path( __FILE__ ) . 'partials/balance-resources-admin-display-metabox-list-in-search.php';
	} // callback_metabox_list_in_search()

	/**
	 * Saves metabox data
	 *
	 * @since   1.0.0
	 * @access  public
	 * @param int     $post_id The post ID
	 * @param object  $object  The post object
	 * @return  void
	 */
	public function save_meta( $post_id, $object ) {
		global $resources_cpts;
		$post_type = get_post_type( $post_id );
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return $post_id; }
		if ( !current_user_can( 'edit_post', $post_id ) ) { return $post_id; }
		if ( !current_user_can( 'edit_page', $post_id ) ) { return $post_id; }
		// if one of the resources or program or life stage check if access level nonce is present, return if not
		if ( ( in_array( $post_type, $resources_cpts ) || $post_type == 'program' || $post_type == 'life_stage' ) && !isset( $_POST['access_level_nonce'] ) ) { return $post_id; }
		// if one of the resources check if list in search nonce is present, return if not present
		if ( in_array( $post_type, $resources_cpts ) && !isset( $_POST['list_in_search_nonce'] ) ) { return $post_id; }
		// if one of the resources or program or life stage verify the access level nonce field, return if not valid
		if ( ( in_array( $post_type, $resources_cpts ) || $post_type == 'program' || $post_type == 'life_stage' ) && !wp_verify_nonce( $_POST['access_level_nonce'], 'verify_this_0789' ) ) { return $post_id; }
		// if one of the resources verify the list in search nonce field, return if not valid
		if ( in_array( $post_type, $resources_cpts ) && !wp_verify_nonce( $_POST['list_in_search_nonce'], 'verify_this_0790' ) ) { return $post_id; }

		$custom = get_post_custom( $post_id );
		$metas  = array( 'access_level', 'list_in_search' );
		foreach ( $metas as $meta ) {
			$value = ( empty( $custom[$meta][0] ) ? '' : $custom[$meta][0] );
			$new_value  = sanitize_text_field( $_POST[$meta] );
			if ( $new_value && $new_value != $value ) {
				// If the new meta value does not match the old value, update it.
				update_post_meta( $post_id, $meta, $new_value );
			} elseif ( '' == $new_value && $value ) {
				// If there is no new meta value but an old value exists, delete it.
				delete_post_meta( $post_id, $meta, $value );
			} elseif ( $new_value && '' == $value ) {
				// If a new meta value was added and there was no previous value, add it.
				add_post_meta( $post_id, $meta, $new_value, true );
			} // End of meta value checks
		} // End of foreach loop
	} // save_meta()

	/**
	 * Disable revisions for resources post types
	 *
	 * @since   1.0.0
	 * @access  public
	 * @uses  post_type_supports(), get_post(), get_post_type()
	 */
	public function resources_post_type_pre_update( $post_id ) {
		global $resources_cpts;
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;
		if ( in_array( get_post_type( $post_id ), $resources_cpts ) ) { // if the type is one of the resources
			remove_action( 'pre_post_update', 'wp_save_post_revision', 10 );
			if ( !$post = get_post( $post_id, ARRAY_A ) )
				return;
			if ( !post_type_supports( $post['post_type'], 'revisions' ) )
				return;
			$return = _wp_put_post_revision( $post );
		}
	}

}
