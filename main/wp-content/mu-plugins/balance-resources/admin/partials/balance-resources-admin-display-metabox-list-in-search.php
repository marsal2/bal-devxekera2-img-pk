<?php

/**
 * Provide the view for a list in search metabox
 *
 * @link       http://roidna.com
 * @since      1.0.0
 *
 * @package    Balance_Resources
 * @subpackage Balance_Resources/admin/partials
 */

$meta = get_post_custom( $object->ID );
$value = '';

if ( ! empty( $meta['list_in_search'][0] ) ) {
  $value = $meta['list_in_search'][0];
} else {
  $value = 'true'; // default value
}

wp_nonce_field( 'verify_this_0790', 'list_in_search_nonce' );

?>
<p>
  <input type="radio" name="list_in_search" id="list_in_search_1" value="true" <?php checked( esc_attr( $value ), 'true' ); ?>  />
  <label for="list_in_search_1">Yes</label>
</p>
<p>
  <input type="radio" name="list_in_search" id="list_in_search_0" value="false" <?php checked( esc_attr( $value ), 'false' ); ?> />
  <label for="list_in_search_0">No</label>
</p>
