<?php

/* On activation add table to store quiz results */
function quiz_results_create_table() {
  global $wpdb;

  $prefix = $wpdb->prefix;
  $quiz_results_table_name = $prefix . 'quiz_results';
  $post_table_name = $prefix . 'posts';
  $user_table_name = $prefix . 'users';

  $charset_collate = $wpdb->get_charset_collate();

  $sql = "CREATE TABLE $quiz_results_table_name (
            ID BIGINT NOT NULL AUTO_INCREMENT,
            score FLOAT NOT NULL,
            num_correct INT NOT NULL,
            answers_data LONGTEXT NOT NULL,
            timestamp TIMESTAMP NOT NULL DEFAULT NOW(),
            wp_user_id BIGINT UNSIGNED NOT NULL,
            wp_post_id BIGINT UNSIGNED NOT NULL,
            mssql_id BIGINT NULL,
            transferred BOOLEAN NOT NULL DEFAULT 1
          ) ENGINE = InnoDB; DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
        ";
  dbDelta( $sql );
}

add_action( 'init', 'quiz_results_create_table' );
