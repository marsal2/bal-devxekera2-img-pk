<?php
ini_set('display_errors', 1);
$balKeyIv 	    = '../../../../../usr/share/balancepro/bal_key_iv.txt'; // /usr/share/balancepro/
$balCredentials = '../../../balancepro/credentials/bal_credentials.txt'; // /var/www/
$balEnv         = '../../../balancepro/bal_env.txt'; // /var/www/ and bal_env_backup

require_once('includes/db/encrypt_decrypt.php');

if($fileKey == $key && $fileIv == $iv){
	echo "Encryption Key and IV are similer, Please try another.";
	exit;
}else if(!empty($iv) && strlen($iv) != 16){
	echo "Encryption IV length should be 16 bytes.";
	exit;
}else{

	$fileBalCredentials = file($balCredentials);
	$aesEcrypt  = aesEcrypt($fileBalCredentials,$key,$iv,'array');
	//$aesDecrypt = aesDecrypt($aesEcrypt,$key,$iv,$type='array');

	//Create backup of env file and key file
	$datetime		 = date('Y-m-d-H-i-s');

	$renameKeyIvFile = '../../../../../usr/share/balancepro/key_iv_backup/bal_key_iv_'.$datetime.'.txt';
	$renameEnvFile   = '../../../balancepro/env_backup/bal_env_'.$datetime.'.txt';

	// Env file backup and new data save
	fileContentGetPut($renameEnvFile,$balEnv,$aesEcrypt);
	// key and iv backup and new data save
	$keyIvNewValue = $key."   //this is encryption key \r\n".$iv."   //this is encryption iv";
	fileContentGetPut($renameKeyIvFile,$balKeyIv,$keyIvNewValue);

	echo "New encryption key and iv created with backup.";
	exit;
}