<?php
return array(
  // This BASE_URL is used to generate links - for dev set it to your local address
  'HOME_URL' => 'https://www.balancepro.org',
  'BASE_URL' => 'https://www.balancepro.org/payment',
  // DEFAULT is used for a reguest to cc.php and ach.php without a $_GET['preset'] variable
  'DEFAULT' => [
    'PMT_SUBJECT_ACH' => 'New ACH Request for Bankruptcy payment (ref: %s)',
    'PMT_SUBJECT_ACH_CLIENT' => 'Bankruptcy payment Transaction (id: %s)',
    'PMT_SUBJECT_CARD' => 'New Card Payment for Bankruptcy payment (id: %s)',
    'PMT_RECEPIENTS' => [
      'somebody@there.here'
    ],
    'DESCRIPTION' => '',
  ],
  // presets are used to create diferent forms in cc.php and ach.php,
  // the $_GET['preset'] value is used to use the correct key in the
  // presets array
  'presets' => [
    'presetname' => [
      'PMT_SUBJECT_ACH' => 'New ACH Request for Reverse Mortgage (ref: %s)',
      'PMT_SUBJECT_ACH_CLIENT' => 'Reverse Mortgage Transaction (id: %s)',
      'PMT_SUBJECT_CARD' => 'New Card Payment for Reverse Mortgage (id: %s)',
      'PMT_RECEPIENTS' => [
        'somebody@there.here'
      ],
      'DESCRIPTION' => 'Preset 2 description',
      'AMOUNT' => '200.00'
    ],
    'presetname2' => [
      'PMT_SUBJECT_ACH' => 'New Credit Reports and Student Loan Counseling request (ref: %s)',
      'PMT_SUBJECT_ACH_CLIENT' => 'ACH payment Transaction (id: %s)',
      'PMT_SUBJECT_CARD' => 'New Card Payment (id: %s)',
      'PMT_RECEPIENTS' => [
        'somebody@there.here'
      ],
      'DESCRIPTION' => 'Preset 2 description',
      'OPTIONS' => [[
        'AMOUNT' => '10.00',
        'DESCRIPTION' => 'Preset 2 option 1 description'
      ], [
        'AMOUNT' => '50.00',
        'DESCRIPTION' => 'Preset 2 option 2 description'
      ], [
        'AMOUNT' => '100.00',
        'DESCRIPTION' => 'Preset 2 option 3 description'
      ]]
    ]
  ],
  'PMT_RECEPIENTS' => [
    'somebody@there.here'
  ],
  'STATIC_PMT_RECEPIENTS' => [
    'somebody@there.here'
  ]
  'PAYMENT_FAILURES_RECIPIENTS' => [
    'somebody@there.here'
  ]
);
