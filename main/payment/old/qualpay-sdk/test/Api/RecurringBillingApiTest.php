<?php
/**
 * RecurringBillingApiTest
 * PHP version 5
 *
 * @category Class
 * @package  qpPlatform
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Qualpay Platform API
 *
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.2
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace qpPlatform;

use \qpPlatform\Configuration;
use \qpPlatform\ApiException;
use \qpPlatform\ObjectSerializer;

/**
 * RecurringBillingApiTest Class Doc Comment
 *
 * @category Class
 * @package  qpPlatform
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class RecurringBillingApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for addPlan
     *
     * Add a Recurring Plan.
     *
     */
    public function testAddPlan()
    {
    }

    /**
     * Test case for addSubscription
     *
     * Add a Subscription.
     *
     */
    public function testAddSubscription()
    {
    }

    /**
     * Test case for archivePlan
     *
     * Archive a Recurring Plan.
     *
     */
    public function testArchivePlan()
    {
    }

    /**
     * Test case for browsePlans
     *
     * Get all Recurring Plans.
     *
     */
    public function testBrowsePlans()
    {
    }

    /**
     * Test case for browseSubscriptions
     *
     * Get all Subscriptions.
     *
     */
    public function testBrowseSubscriptions()
    {
    }

    /**
     * Test case for cancelSubscription
     *
     * Cancel a Subscription.
     *
     */
    public function testCancelSubscription()
    {
    }

    /**
     * Test case for deletePlan
     *
     * Delete a Recurring Plan.
     *
     */
    public function testDeletePlan()
    {
    }

    /**
     * Test case for getAllSubscriptionTransactions
     *
     * Get all subscription transactions.
     *
     */
    public function testGetAllSubscriptionTransactions()
    {
    }

    /**
     * Test case for getPlan
     *
     * Find Recurring Plan by Plan Code.
     *
     */
    public function testGetPlan()
    {
    }

    /**
     * Test case for getSubscription
     *
     * Get Subscription by Subscription ID.
     *
     */
    public function testGetSubscription()
    {
    }

    /**
     * Test case for getSubscriptionTransactions
     *
     * Get transactions by Subscription ID.
     *
     */
    public function testGetSubscriptionTransactions()
    {
    }

    /**
     * Test case for pauseSubscription
     *
     * Pause a Subscription.
     *
     */
    public function testPauseSubscription()
    {
    }

    /**
     * Test case for resumeSubscription
     *
     * Resume a Subscription.
     *
     */
    public function testResumeSubscription()
    {
    }

    /**
     * Test case for updatePlan
     *
     * Update a Recurring Plan.
     *
     */
    public function testUpdatePlan()
    {
    }

    /**
     * Test case for updateSubscription
     *
     * Update a Subscription.
     *
     */
    public function testUpdateSubscription()
    {
    }
}
