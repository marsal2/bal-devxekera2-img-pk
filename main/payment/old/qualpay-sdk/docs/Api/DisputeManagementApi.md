# qpPlatform\DisputeManagementApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**acceptDispute**](DisputeManagementApi.md#acceptDispute) | **GET** /dispute/{recId}/accept | Accept Dispute
[**browseDisputes**](DisputeManagementApi.md#browseDisputes) | **GET** /dispute/browse | Get Disputes
[**createDispute**](DisputeManagementApi.md#createDispute) | **GET** /dispute/{merchantId}/create | Create Dispute Data
[**getCreditedTran**](DisputeManagementApi.md#getCreditedTran) | **GET** /dispute/{recId}/creditedtrans | Get Credited Transaction Detail
[**getDisputeResponse**](DisputeManagementApi.md#getDisputeResponse) | **GET** /dispute/{recId}/response | Get Submitted Dispute Response
[**getNonDisputedTran**](DisputeManagementApi.md#getNonDisputedTran) | **GET** /dispute/{recId}/nondisputedtrans | Get Non Disputed Transaction
[**resetDispute**](DisputeManagementApi.md#resetDispute) | **GET** /dispute/{recId}/reset | Reset Dispute Data
[**submitDisputesResponse**](DisputeManagementApi.md#submitDisputesResponse) | **POST** /dispute/{recId}/respond | Submit Dispute Response


# **acceptDispute**
> \qpPlatform\Model\QPApiResponse acceptDispute($rec_id)

Accept Dispute

Accept first time Chargeback and Pre-Arbitration Withdraw from Arbitration.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\DisputeManagementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$rec_id = 789; // int | Control Number

try {
    $result = $apiInstance->acceptDispute($rec_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputeManagementApi->acceptDispute: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rec_id** | **int**| Control Number |

### Return type

[**\qpPlatform\Model\QPApiResponse**](../Model/QPApiResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **browseDisputes**
> \qpPlatform\Model\DisputeDetail browseDisputes($count, $order_on, $order_by, $page, $filter)

Get Disputes

Request all disputes and their detail associated with a vendor or a node.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\DisputeManagementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$count = 10; // int | The number of records in the result.
$order_on = "date_payment"; // string | The field on which the results will be sorted on. Refer to the response model for available fields.
$order_by = "desc"; // string | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
$page = 0; // int | Zero-based page number, use this to choose a page when there are more results than the count parameter.
$filter = "filter_example"; // string | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.

try {
    $result = $apiInstance->browseDisputes($count, $order_on, $order_by, $page, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputeManagementApi->browseDisputes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **int**| The number of records in the result. | [optional] [default to 10]
 **order_on** | **string**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to date_payment]
 **order_by** | **string**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **int**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **string**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional]

### Return type

[**\qpPlatform\Model\DisputeDetail**](../Model/DisputeDetail.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createDispute**
> \qpPlatform\Model\QPApiListResponse createDispute($merchant_id, $reason_code)

Create Dispute Data

For testing purposes, create a dispute for a specific reason code or for all the reason codes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\DisputeManagementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$merchant_id = 789; // int | Merchant ID
$reason_code = "null"; // string | Reason Code

try {
    $result = $apiInstance->createDispute($merchant_id, $reason_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputeManagementApi->createDispute: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **int**| Merchant ID |
 **reason_code** | **string**| Reason Code | [optional] [default to null]

### Return type

[**\qpPlatform\Model\QPApiListResponse**](../Model/QPApiListResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCreditedTran**
> \qpPlatform\Model\CorrespondingTransactionResponse getCreditedTran($rec_id)

Get Credited Transaction Detail

Request the credited transactions associated with dispute.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\DisputeManagementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$rec_id = 789; // int | Control Number

try {
    $result = $apiInstance->getCreditedTran($rec_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputeManagementApi->getCreditedTran: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rec_id** | **int**| Control Number |

### Return type

[**\qpPlatform\Model\CorrespondingTransactionResponse**](../Model/CorrespondingTransactionResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDisputeResponse**
> \qpPlatform\Model\DisputeResponse getDisputeResponse($rec_id)

Get Submitted Dispute Response

Get previously submitted dispute response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\DisputeManagementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$rec_id = 789; // int | Control Number

try {
    $result = $apiInstance->getDisputeResponse($rec_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputeManagementApi->getDisputeResponse: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rec_id** | **int**| Control Number |

### Return type

[**\qpPlatform\Model\DisputeResponse**](../Model/DisputeResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getNonDisputedTran**
> \qpPlatform\Model\CorrespondingTransactionResponse getNonDisputedTran($rec_id)

Get Non Disputed Transaction

Request the non-disputed transactions associated with with dispute.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\DisputeManagementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$rec_id = 789; // int | Control Number

try {
    $result = $apiInstance->getNonDisputedTran($rec_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputeManagementApi->getNonDisputedTran: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rec_id** | **int**| Control Number |

### Return type

[**\qpPlatform\Model\CorrespondingTransactionResponse**](../Model/CorrespondingTransactionResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resetDispute**
> \qpPlatform\Model\QPApiResponse resetDispute($rec_id)

Reset Dispute Data

For non production enviornment, request to reset a dispute case to new status.For Production, request to reset dispute to new status till a dispute is in Qualpay Review (Status=Q)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\DisputeManagementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$rec_id = 789; // int | Control Number

try {
    $result = $apiInstance->resetDispute($rec_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputeManagementApi->resetDispute: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rec_id** | **int**| Control Number |

### Return type

[**\qpPlatform\Model\QPApiResponse**](../Model/QPApiResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **submitDisputesResponse**
> \qpPlatform\Model\QPApiResponse submitDisputesResponse($rec_id, $file, $response)

Submit Dispute Response

Submit dispute responses with supporting documentation. Response options are dynamic and are based on the reason code.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\DisputeManagementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$rec_id = 789; // int | Control Number
$file = "/path/to/file.txt"; // \SplFileObject | The file to upload.
$response = "response_example"; // string | Dispute response

try {
    $result = $apiInstance->submitDisputesResponse($rec_id, $file, $response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputeManagementApi->submitDisputesResponse: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rec_id** | **int**| Control Number |
 **file** | **\SplFileObject**| The file to upload. |
 **response** | **string**| Dispute response |

### Return type

[**\qpPlatform\Model\QPApiResponse**](../Model/QPApiResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

