# qpPlatform\RecurringBillingApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addPlan**](RecurringBillingApi.md#addPlan) | **POST** /plan | Add a Recurring Plan
[**addSubscription**](RecurringBillingApi.md#addSubscription) | **POST** /subscription | Add a Subscription
[**archivePlan**](RecurringBillingApi.md#archivePlan) | **POST** /plan/{plan_code}/archive | Archive a Recurring Plan
[**browsePlans**](RecurringBillingApi.md#browsePlans) | **GET** /plan | Get all Recurring Plans
[**browseSubscriptions**](RecurringBillingApi.md#browseSubscriptions) | **GET** /subscription | Get all Subscriptions
[**cancelSubscription**](RecurringBillingApi.md#cancelSubscription) | **POST** /subscription/{subscription_id}/cancel | Cancel a Subscription
[**deletePlan**](RecurringBillingApi.md#deletePlan) | **DELETE** /plan/{plan_id}/delete | Delete a Recurring Plan
[**getAllSubscriptionTransactions**](RecurringBillingApi.md#getAllSubscriptionTransactions) | **GET** /subscription/transactions | Get all subscription transactions
[**getPlan**](RecurringBillingApi.md#getPlan) | **GET** /plan/{plan_code} | Find Recurring Plan by Plan Code
[**getSubscription**](RecurringBillingApi.md#getSubscription) | **GET** /subscription/{subscription_id} | Get Subscription by Subscription ID
[**getSubscriptionTransactions**](RecurringBillingApi.md#getSubscriptionTransactions) | **GET** /subscription/transactions/{subscription_id} | Get transactions by Subscription ID
[**pauseSubscription**](RecurringBillingApi.md#pauseSubscription) | **POST** /subscription/{subscription_id}/pause | Pause a Subscription
[**resumeSubscription**](RecurringBillingApi.md#resumeSubscription) | **POST** /subscription/{subscription_id}/resume | Resume a Subscription
[**updatePlan**](RecurringBillingApi.md#updatePlan) | **PUT** /plan/{plan_code} | Update a Recurring Plan
[**updateSubscription**](RecurringBillingApi.md#updateSubscription) | **PUT** /subscription/{subscription_id} | Update a Subscription


# **addPlan**
> \qpPlatform\Model\RecurringPlanResponse addPlan($body)

Add a Recurring Plan

Adds a new Recurring Plan. Save the generated unique plan_id, which is required to delete a plan.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \qpPlatform\Model\AddRecurringPlanRequest(); // \qpPlatform\Model\AddRecurringPlanRequest | Recurring Plan Object that needs to be added

try {
    $result = $apiInstance->addPlan($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->addPlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\qpPlatform\Model\AddRecurringPlanRequest**](../Model/AddRecurringPlanRequest.md)| Recurring Plan Object that needs to be added |

### Return type

[**\qpPlatform\Model\RecurringPlanResponse**](../Model/RecurringPlanResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addSubscription**
> \qpPlatform\Model\SubscriptionResponse addSubscription($body)

Add a Subscription

Creates a new subscription on the specified start date. Returns the subscription_id; save this id to interact with this subscription using the API. When a subscription is added, with a one-time fee, a payment gateway sale request is  made immediately to bill the customer the one-time fee. Check the response in the return model to check the status of the payment gateway request. Note that the subscription remains active even if the payment gateway request for the one-time fee fails. An “off plan” subscription, a subscription without a plan, can be created by excluding the plan_code from your request and sending applicable fields

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \qpPlatform\Model\AddSubscriptionRequest(); // \qpPlatform\Model\AddSubscriptionRequest | Subscription Request

try {
    $result = $apiInstance->addSubscription($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->addSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\qpPlatform\Model\AddSubscriptionRequest**](../Model/AddSubscriptionRequest.md)| Subscription Request | [optional]

### Return type

[**\qpPlatform\Model\SubscriptionResponse**](../Model/SubscriptionResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **archivePlan**
> \qpPlatform\Model\RecurringPlanResponse archivePlan($plan_code, $body)

Archive a Recurring Plan

Archives a Plan. Only active plans can be archived. Note that if there are subscribers to this plan, then all subscriptions belonging to this plan will continue to be active. No updates can be made to an archived plan. New subscribers cannot be added to a archived plan. You can always pull up information on an archived plan from the system.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$plan_code = "plan_code_example"; // string | Plan Code that will be archived
$body = new \qpPlatform\Model\ArchiveRecurringPlanRequest(); // \qpPlatform\Model\ArchiveRecurringPlanRequest | Plan Name

try {
    $result = $apiInstance->archivePlan($plan_code, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->archivePlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plan_code** | **string**| Plan Code that will be archived |
 **body** | [**\qpPlatform\Model\ArchiveRecurringPlanRequest**](../Model/ArchiveRecurringPlanRequest.md)| Plan Name | [optional]

### Return type

[**\qpPlatform\Model\RecurringPlanResponse**](../Model/RecurringPlanResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **browsePlans**
> \qpPlatform\Model\RecurringPlanListResponse browsePlans($count, $order_on, $order_by, $page, $filter, $merchant_id)

Get all Recurring Plans

Gets a paginated list of recurring plans. Optional query parameters determines, size and sort order of returned array

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$count = 10; // int | The number of records in the result.
$order_on = "plan_code"; // string | The field on which the results will be sorted on. Refer to the response model for available fields.
$order_by = "asc"; // string | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
$page = 0; // int | Zero-based page number, use this to choose a page when there are more results than the count parameter.
$filter = "filter_example"; // string | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
$merchant_id = 0; // int | Unique ID assigned by Qualpay to a merchant.

try {
    $result = $apiInstance->browsePlans($count, $order_on, $order_by, $page, $filter, $merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->browsePlans: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **int**| The number of records in the result. | [optional] [default to 10]
 **order_on** | **string**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to plan_code]
 **order_by** | **string**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to asc]
 **page** | **int**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **string**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional]
 **merchant_id** | **int**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**\qpPlatform\Model\RecurringPlanListResponse**](../Model/RecurringPlanListResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **browseSubscriptions**
> \qpPlatform\Model\SubscriptionListResponse browseSubscriptions($count, $order_on, $order_by, $page, $filter, $merchant_id)

Get all Subscriptions

Gets an array of subscription objects. Optional query parameters determines, size and sort order of returned array

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$count = 10; // int | The number of records in the result.
$order_on = "date_next"; // string | The field on which the results will be sorted on. Refer to the response model for available fields.
$order_by = "desc"; // string | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
$page = 0; // int | Zero-based page number, use this to choose a page when there are more results than the count parameter.
$filter = "filter_example"; // string | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
$merchant_id = 0; // int | Unique ID assigned by Qualpay to a merchant.

try {
    $result = $apiInstance->browseSubscriptions($count, $order_on, $order_by, $page, $filter, $merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->browseSubscriptions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **int**| The number of records in the result. | [optional] [default to 10]
 **order_on** | **string**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to date_next]
 **order_by** | **string**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **int**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **string**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional]
 **merchant_id** | **int**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**\qpPlatform\Model\SubscriptionListResponse**](../Model/SubscriptionListResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cancelSubscription**
> \qpPlatform\Model\SubscriptionResponse cancelSubscription($subscription_id, $body)

Cancel a Subscription

Cancels a subscription. Only active, suspended or paused subscriptions can be cancelled. A cancelled subscription cannot be resumed.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$subscription_id = 789; // int | Subscription ID
$body = new \qpPlatform\Model\CancelSubscriptionRequest(); // \qpPlatform\Model\CancelSubscriptionRequest | Customer ID

try {
    $result = $apiInstance->cancelSubscription($subscription_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->cancelSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_id** | **int**| Subscription ID |
 **body** | [**\qpPlatform\Model\CancelSubscriptionRequest**](../Model/CancelSubscriptionRequest.md)| Customer ID |

### Return type

[**\qpPlatform\Model\SubscriptionResponse**](../Model/SubscriptionResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePlan**
> \qpPlatform\Model\RecurringPlanResponse deletePlan($plan_id, $merchant_id)

Delete a Recurring Plan

Deletes a Plan. Any plan, active or not can be deleted. If there are subscribers to the plan, then all subscriptions related to this plan will be cancelled. A deleted plan cannot be updated, neither can new subscrbers be added to a deleted plan. Even if a plan is deleted, you can query the system to get information about the deleted plan.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$plan_id = 789; // int | Plan ID that will flagged as deleted
$merchant_id = 0; // int | Unique ID assigned by Qualpay to a merchant.

try {
    $result = $apiInstance->deletePlan($plan_id, $merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->deletePlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plan_id** | **int**| Plan ID that will flagged as deleted |
 **merchant_id** | **int**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**\qpPlatform\Model\RecurringPlanResponse**](../Model/RecurringPlanResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllSubscriptionTransactions**
> \qpPlatform\Model\TransactionListResponse getAllSubscriptionTransactions($count, $order_on, $order_by, $page, $filter, $merchant_id)

Get all subscription transactions

Gets all subscription transactions. Optional Parameters will help filter and restrict the result.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$count = 10; // int | The number of records in the result.
$order_on = "tran_time"; // string | The field on which the results will be sorted on. Refer to the response model for available fields.
$order_by = "desc"; // string | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
$page = 0; // int | Zero-based page number, use this to choose a page when there are more results than the count parameter.
$filter = "filter_example"; // string | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
$merchant_id = 0; // int | Unique ID assigned by Qualpay to a merchant.

try {
    $result = $apiInstance->getAllSubscriptionTransactions($count, $order_on, $order_by, $page, $filter, $merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->getAllSubscriptionTransactions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **int**| The number of records in the result. | [optional] [default to 10]
 **order_on** | **string**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to tran_time]
 **order_by** | **string**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **int**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **string**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional]
 **merchant_id** | **int**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**\qpPlatform\Model\TransactionListResponse**](../Model/TransactionListResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPlan**
> \qpPlatform\Model\RecurringPlanListResponse getPlan($plan_code, $count, $order_on, $order_by, $page, $filter, $merchant_id)

Find Recurring Plan by Plan Code

Returns a list of recurring plans for the plan_code. Search result includes active, archived and deleted plans. Optional query parameters determines, size and sort order of returned array

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$plan_code = "plan_code_example"; // string | Plan Code
$count = 10; // int | The number of records in the result.
$order_on = "plan_code"; // string | The field on which the results will be sorted on. Refer to the response model for available fields.
$order_by = "asc"; // string | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
$page = 0; // int | Zero-based page number, use this to choose a page when there are more results than the count parameter.
$filter = "filter_example"; // string | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
$merchant_id = 0; // int | Unique ID assigned by Qualpay to a merchant.

try {
    $result = $apiInstance->getPlan($plan_code, $count, $order_on, $order_by, $page, $filter, $merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->getPlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plan_code** | **string**| Plan Code |
 **count** | **int**| The number of records in the result. | [optional] [default to 10]
 **order_on** | **string**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to plan_code]
 **order_by** | **string**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to asc]
 **page** | **int**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **string**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional]
 **merchant_id** | **int**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**\qpPlatform\Model\RecurringPlanListResponse**](../Model/RecurringPlanListResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSubscription**
> \qpPlatform\Model\SubscriptionResponse getSubscription($subscription_id, $merchant_id)

Get Subscription by Subscription ID

Gets details of a subscription.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$subscription_id = 789; // int | Subscription ID
$merchant_id = 0; // int | Unique ID assigned by Qualpay to a merchant.

try {
    $result = $apiInstance->getSubscription($subscription_id, $merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->getSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_id** | **int**| Subscription ID |
 **merchant_id** | **int**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**\qpPlatform\Model\SubscriptionResponse**](../Model/SubscriptionResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSubscriptionTransactions**
> \qpPlatform\Model\TransactionListResponse getSubscriptionTransactions($subscription_id, $count, $order_on, $order_by, $page, $filter, $merchant_id)

Get transactions by Subscription ID

Gets all transactions for a subscription. Optional parameters will help filter and restrict the result.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$subscription_id = 789; // int | Subscription ID
$count = 10; // int | The number of records in the result.
$order_on = "tran_time"; // string | The field on which the results will be sorted on. Refer to the response model for available fields.
$order_by = "desc"; // string | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
$page = 0; // int | Zero-based page number, use this to choose a page when there are more results than the count parameter.
$filter = "filter_example"; // string | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
$merchant_id = 0; // int | Unique ID assigned by Qualpay to a merchant.

try {
    $result = $apiInstance->getSubscriptionTransactions($subscription_id, $count, $order_on, $order_by, $page, $filter, $merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->getSubscriptionTransactions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_id** | **int**| Subscription ID |
 **count** | **int**| The number of records in the result. | [optional] [default to 10]
 **order_on** | **string**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to tran_time]
 **order_by** | **string**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **int**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **string**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional]
 **merchant_id** | **int**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**\qpPlatform\Model\TransactionListResponse**](../Model/TransactionListResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pauseSubscription**
> \qpPlatform\Model\SubscriptionResponse pauseSubscription($subscription_id, $body)

Pause a Subscription

Pauses an active subscription. Recurring payments will be skipped when a subscription is paused. Only active subscriptions can be paused.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$subscription_id = 789; // int | 
$body = new \qpPlatform\Model\PauseSubscriptionRequest(); // \qpPlatform\Model\PauseSubscriptionRequest | Customer ID

try {
    $result = $apiInstance->pauseSubscription($subscription_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->pauseSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_id** | **int**|  |
 **body** | [**\qpPlatform\Model\PauseSubscriptionRequest**](../Model/PauseSubscriptionRequest.md)| Customer ID |

### Return type

[**\qpPlatform\Model\SubscriptionResponse**](../Model/SubscriptionResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resumeSubscription**
> \qpPlatform\Model\SubscriptionResponse resumeSubscription($subscription_id, $body)

Resume a Subscription

Resumes a suspended or paused subscription. When a suspended subscription is resumed, Qualpay’s subscription engine will initiate all the missed subscription transactions. When a paused subscription is resumed, all missed payments are skipped.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$subscription_id = 789; // int | 
$body = new \qpPlatform\Model\ResumeSubscriptionRequest(); // \qpPlatform\Model\ResumeSubscriptionRequest | Customer ID

try {
    $result = $apiInstance->resumeSubscription($subscription_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->resumeSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_id** | **int**|  |
 **body** | [**\qpPlatform\Model\ResumeSubscriptionRequest**](../Model/ResumeSubscriptionRequest.md)| Customer ID |

### Return type

[**\qpPlatform\Model\SubscriptionResponse**](../Model/SubscriptionResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePlan**
> \qpPlatform\Model\RecurringPlanResponse updatePlan($plan_code, $body)

Update a Recurring Plan

Update an active recurring plan. Only the fields sent in the request body will be updated. Only an active plan can be updated. If there are subscribers to this plan, then this plan will be archived and a copy of the plan with a new plan_id will be generated. All updates will be made on the new plan. Save the new plan_id to manage a plan

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$plan_code = "plan_code_example"; // string | Plan Code of the plan that will be updated
$body = new \qpPlatform\Model\UpdateRecurringPlanRequest(); // \qpPlatform\Model\UpdateRecurringPlanRequest | Recurring Plan Object. Send only the fields that require an update. Read only fields will be ignored

try {
    $result = $apiInstance->updatePlan($plan_code, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->updatePlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plan_code** | **string**| Plan Code of the plan that will be updated |
 **body** | [**\qpPlatform\Model\UpdateRecurringPlanRequest**](../Model/UpdateRecurringPlanRequest.md)| Recurring Plan Object. Send only the fields that require an update. Read only fields will be ignored |

### Return type

[**\qpPlatform\Model\RecurringPlanResponse**](../Model/RecurringPlanResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSubscription**
> \qpPlatform\Model\SubscriptionResponse updateSubscription($subscription_id, $body)

Update a Subscription

Updates the start date of an existing subscription. Only subscriptions that has not yet started can be updated. Only start date can be updated.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\RecurringBillingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$subscription_id = 789; // int | 
$body = new \qpPlatform\Model\UpdateSubscriptionRequest(); // \qpPlatform\Model\UpdateSubscriptionRequest | Subscription Request

try {
    $result = $apiInstance->updateSubscription($subscription_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecurringBillingApi->updateSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_id** | **int**|  |
 **body** | [**\qpPlatform\Model\UpdateSubscriptionRequest**](../Model/UpdateSubscriptionRequest.md)| Subscription Request |

### Return type

[**\qpPlatform\Model\SubscriptionResponse**](../Model/SubscriptionResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

