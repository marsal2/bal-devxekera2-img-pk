# qpPlatform\EmbeddedFieldsApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEmbeddedTransientKey**](EmbeddedFieldsApi.md#getEmbeddedTransientKey) | **GET** /embedded | Get Transient Key


# **getEmbeddedTransientKey**
> \qpPlatform\Model\EmbeddedKeyResponse getEmbeddedTransientKey()

Get Transient Key

Gets a transient key for use with Qualpay Embedded Fields. This key will be invalidated in 12 hours or when a card is successfully verified using Embedded Fields. The transient key is a one time key that is used to invoke Embedded Fields.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\EmbeddedFieldsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getEmbeddedTransientKey();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmbeddedFieldsApi->getEmbeddedTransientKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\qpPlatform\Model\EmbeddedKeyResponse**](../Model/EmbeddedKeyResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

