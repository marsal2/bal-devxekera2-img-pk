# AusRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\qpPlatform\Model\AusRequestData[]**](AusRequestData.md) | An array of data fields to save to the Aus Requests. | 
**card_id** | **string** |  | 
**card_number** | **string** |  | 
**exp_date** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


