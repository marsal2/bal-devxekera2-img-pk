# DepositReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rec_id** | **int** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 18 N&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;Unique ID assigned by Qualpay to this deposit. | 
**merchant_id** | **int** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 16 N&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;Unique ID assigned by Qualpay to a merchant. | 
**dba_name** | **string** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 25 AN&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;The doing business as name of the merchant. | 
**reference_number** | **string** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 16 N&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;The bank reference number of the deposit. | 
**amt_ach** | **string** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 17,2 N&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;ACH amount. | 
**ach_description** | **string** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 18 AN&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;ACH description. | 
**post_date_actual** | **string** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 10 AN, in YYYY-MM-DD format&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;The date the ACH posted to the federal reserve. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


