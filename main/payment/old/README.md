### Basic setup (different payment presets)

- for any payments system to work you need to create, and fill the `config.php` file (from `config.example.php` template)
- you can define different presets (mortgage, dmppayment, ...), emails for admins, additional fields for forms, amounts for payments etc.
- check `config.example.php` template for which settings are possible

### Setup for PayPal

- for the script to work you need to configure `paypal.conf.php` (from `paypal.conf.example.php` template)
- you can switch between live and test environments with setting `$USE_LIVE = false/true;` flag in `paypal.conf.php`
- Paypal payments are done with the Paypal Hosted Pages through Payflow Gateway. For setting up Paypal follow the [the official documentation guide](https://developer.paypal.com/docs/classic/payflow/test_hosted_pages/)

### Setup for Dwolla

- for the script to work you need to configure `dwolla.conf.php` (from `dwolla.conf.example.php` template)
- you can switch between live and test environments with setting `$USE_LIVE = false/true;` flag in `paypal.conf.php`
- Dwolla ACH is done using the Dwolla SDK. See the [official Github repository](https://github.com/Dwolla/dwolla-swagger-php) for development information.
