<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// SET HEADER
header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
include('database.php');
// MAKE SQL QUERY
$personData = json_decode($_REQUEST['data']);
$rtypes = $personData->dvalue;
$page = $personData->pager;
$lifestage = $personData->lifestage;
$search = $personData->search;
$sort = $personData->sort;
$tags = $personData->tags;
if(empty($tags) or $tags == "" ){
	$tags = '0'	;
}
if($lifestage == 1 || $lifestage == '1'){
    $lifestage = 1; 
}
$keywords= explode(',', $tags);
$advancedkeywords = implode("', '", $keywords);
if(empty($tags) or $tags == "" ){
	$tags = '0'	;
}
		if(empty($sort)){
		$sort = "0";
		}
		if($rtypes == 'tags'){
			$rtypes = "0";
		}

		if(empty($search)){
			$search = "0";
			}

			if(empty($lifestage)){
				$lifestage = "0";
				}
				if(empty($rtypes)){
					$rtypes = "0";
					}
		$searchQuery = str_replace('\\', "", $search);
		$unquotedQuery = str_replace('"', "", $search);
		$limit = 9;
		if($page){
		    $start = ($page - 1) * $limit; 
		}else{
		    $start = 0; 
		}  
		$list = 'true';
		$level = '100';
		$pgorder = '1';
		$resources = 'resources/';

		$check = '0';
		if($search == ''){
			$searchvalue = '0';
		}else{
			$searchvalue = $search;
		}
		//order by title_match desc, title_rough_match desc, relevancy desc
		//we have life stage cat id and resource type category
		 if($lifestage == '0' AND $searchvalue != '0' AND $rtypes == '0'){
			// echo " line 33..." . $lifestage ." -> lifestage ". $search ." -> search ". $rtypes  ." -> rtypes "; 

		    if($sort == "0"){
		    $select = "SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id";
		    $from = " FROM wp_resources as w, wp_posts AS P";
		    $where = " WHERE w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100'";
		    if ($searchQuery != '') {
		    $where .= " and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
		    }
			if($tags !='0'){
				$from .= " , wp_term_relationships as wtr ";
				$where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
			 }
		    $order = " order by title_match desc, title_rough_match desc, relevancy desc";
		    $query = $select . $from . $where . $order;
		    $checkn = $db->prepare($query);
		    /*$result = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9");
		    $result1 = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");*/
		}else if($sort == "relevance"){
			$select = "SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id";
		    $from = " FROM wp_resources as w, wp_posts AS P";
		    $where = " WHERE w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' ";
		    if ($searchQuery != '') {
		    $where .= " and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
		    }
			if($tags !='0'){
				$from .= " , wp_term_relationships as wtr ";
				$where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
			 }
		    $order = " order by title_match desc, title_rough_match desc, relevancy desc";
		    $query = $select . $from . $where . $order;
		    $checkn = $db->prepare($query);

		/*$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9";
		    $result = $db->prepare($query);
		        $query1 = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'";
		    $result1 = $db->prepare($query1);*/
		    
		}else if($sort == "views"){
		    $select = "SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, wc.view_count";
		    $from = " FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc";
		    $where = " WHERE w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id";
		    if ($searchQuery != '') {
		    $where .= " and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
		    }
			if($tags !='0'){
				$from .= " , wp_term_relationships as wtr ";
				$where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
			 }
		    $order = " order by wc.view_count desc";
		    $query = $select . $from . $where . $order;
		    $checkn = $db->prepare($query);
		    /*$query = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC limit 9";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result = $db->prepare($query);
		    $query1 = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result1 = $db->prepare($query1);*/
		    
		}else if($sort == "date"){
		    $select = "SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, P.post_date_gmt";
		    $from = " FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc";
		    $where = " WHERE w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id";
		    if ($searchQuery != '') {
		    $where .= " and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
		    }
			if($tags !='0'){
				$from .= " , wp_term_relationships as wtr ";
				$where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
			 }
		    $order = " order by P.post_date_gmt desc";
		    $query = $select . $from . $where . $order;
		    $checkn = $db->prepare($query);
		    /*$query = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC limit 9";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result = $db->prepare($query);
		    $query1 = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result1 = $db->prepare($query1);*/

		}
		} else 
		if($lifestage != '0' AND $searchvalue == '0' AND $rtypes == '0'){
		    if($sort == "0"){
		    $select = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id";
		    $from = " FROM wp_resources as w, wp_posts AS P ";
		    $where = " WHERE w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100'  ";
		    if($lifestage !== '0'){
				$from .=", life_stage_type AS l";
				$where .= " AND l.lifestagetype = '$lifestage' AND l.postid = w.ID";
			}
			if($tags !='0'){
				$from .= " , wp_term_relationships as wtr ";
				$where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
			 }
		    $order = "  AND w.page_order = '$pgorder' ";
		    $query = $select . $from . $where . $order;
		    $checkn = $db->prepare($query);
		    /*$result = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9");
		    $result1 = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");*/
		}else if($sort == "relevance"){
		    $select = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id";
		    $from = " FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc";
		    $where = " WHERE w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id";
		   
			if($lifestage !== '0'){
				$from .=", life_stage_type AS l";
				$where .= " AND l.lifestagetype = '$lifestage' AND l.postid = w.ID";
			}
			if($tags !='0'){
				$from .= " , wp_term_relationships as wtr ";
				$where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
			 }
		    $order = "  AND w.page_order = '$pgorder' ";
		    $query = $select . $from . $where . $order;
		    $checkn = $db->prepare($query);

	/*
		    $select = "SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id";
		    $from = " FROM wp_resources as w, wp_resources_view_count as wc";
		    $where = " WHERE w.type = '$rtypes' AND w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id";
		    if ($searchQuery != '') {
		    $where .= " and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
		    }
		    if($rtypes != '0'){
		        $where .= " and w.type = '$rtypes'";
		        $where .= " and P.post_type = '$rtypes'";
		    }
		    if($rtypes != '0'){
		        $from .=", wp_posts AS P";
		        $where .= " and P.post_type = '$rtypes'";
		    }
		    if($lifestage !== '0'){
		        $from .=", life_stage_type AS l";
		        $where .= " AND l.lifestagetype = '$lifestage' AND l.postid = w.ID";
		    }
		    $order = " order by title_match desc, title_rough_match desc, relevancy desc";
		    //$limit = 'limt 0, 9';
		    $query = $select . $from . $where . $order . $limit;
		    $stmt = $db->prepare($query);*/
		/*$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9";
		    $result = $db->prepare($query);
		        $query1 = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'";
		    $result1 = $db->prepare($query1);*/
		    
		}else if($sort == "views"){
		    $select = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, wc.view_count";
		    $from = " FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc";
		    $where = " WHERE w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id";
		   
		    
			if($lifestage !== '0'){
				$from .=", life_stage_type AS l";
				$where .= " AND l.lifestagetype = '$lifestage' AND l.postid = w.ID";
			}
			if($tags !='0'){
				$from .= " , wp_term_relationships as wtr ";
				$where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
			 }
		    $order = " order by wc.view_count desc";
		    $query = $select . $from . $where . $order;
		    $checkn = $db->prepare($query);
		    /*$query = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC limit 9";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result = $db->prepare($query);
		    $query1 = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result1 = $db->prepare($query1);*/
		    
		}else if($sort == "date"){
		    $select = "SELECT Distinct w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, P.post_date_gmt";
		    $from = " FROM wp_resources as w, wp_posts AS P ";
		    $where = " WHERE w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100'";
		   

			if($lifestage !== '0'){
				$from .=", life_stage_type AS l";
				$where .= " AND l.lifestagetype = '$lifestage' AND l.postid = w.ID";
			}
		    
		    $order = " order by P.post_date_gmt desc";
			if($tags !='0'){
				$from .= " , wp_term_relationships as wtr ";
				$where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
			 }
		    $query = $select . $from . $where . $order;
		    $checkn = $db->prepare($query);
		    /*$query = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC limit 9";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result = $db->prepare($query);
		    $query1 = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result1 = $db->prepare($query1);*/

		}
		} else  

		if($searchvalue !== '0'){
			// echo " line 44..";
		 if($sort == "0"){
	     $select = "SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id";
	     $from = " FROM wp_resources as w, wp_posts AS P";
	     $where = " WHERE w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100'";
	     if ($searchQuery != '') {
	     $where .= " and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
	     }
	     if($rtypes != '0'){
	        
	         $where .= " and w.type = '$rtypes'";
	         $where .= " and P.post_type = '$rtypes'";
	     }

	     if($lifestage !== '0'){
	         $from .=", life_stage_type AS l";
	         $where .= " AND l.lifestagetype = '$lifestage' AND l.postid = w.ID";
	     }
		 if($tags !='0'){
            $from .= " , wp_term_relationships as wtr ";
            $where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
         }
	     $order = " order by title_match desc, title_rough_match desc, relevancy desc";
	     //$limit = 'limt 0, 9';
	     $query = $select . $from . $where . $order ;
	     $result = $db->prepare($query);
	     $query1 = $select . $from . $where . $order ;
	     $checkn = $db->prepare($query1);

	    /*$result = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9");
	    $result1 = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");*/
	}else if($sort == "relevance"){

	$select = "SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id";
	     $from = " FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc";
	     $where = " WHERE w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id";
	     if ($searchQuery != '') {
	     $where .= " and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
	     }
	     if($rtypes != '0'){
	        
	         $where .= " and w.type = '$rtypes'";
	         $where .= " and P.post_type = '$rtypes'";
	     }

	     if($lifestage !== '0'){
	         $from .=", life_stage_type AS l";
	         $where .= " AND l.lifestagetype = '$lifestage' AND l.postid = w.ID";
	     }
		 if($tags !='0'){
            $from .= " , wp_term_relationships as wtr ";
            $where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
         }
	     $order = " order by title_match desc, title_rough_match desc, relevancy desc";
	     //$limit = 'limt 0, 9';
	     $query = $select . $from . $where . $order ;
	     $result = $db->prepare($query);
	     $query1 = $select . $from . $where . $order ;
	     $checkn = $db->prepare($query1);

	/*$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9";
	    $result = $db->prepare($query);
	        $query1 = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'";
	    $result1 = $db->prepare($query1);*/
	    
	}else if($sort == "views"){
	    $select = "SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, wc.view_count";
	     $from = " FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc";
	     $where = " WHERE w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id";
	     if ($searchQuery != '') {
	     $where .= " and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
	     }
	     if($rtypes != '0'){
	        
	         $where .= " and w.type = '$rtypes'";
	         $where .= " and P.post_type = '$rtypes'";
	     }

	     if($lifestage !== '0'){
	         $from .=", life_stage_type AS l";
	         $where .= " AND l.lifestagetype = '$lifestage' AND l.postid = w.ID";
	     }
		 if($tags !='0'){
            $from .= " , wp_term_relationships as wtr ";
            $where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
         }
	     $order = " order by wc.view_count desc";
	     //$limit = 'limt 0, 9';
	     $query = $select . $from . $where . $order ;
	     $result = $db->prepare($query);
	     $query1 = $select . $from . $where . $order ;
	     $checkn = $db->prepare($query1);

	    
	    /*$query = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC limit 9";

	        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
	    $result = $db->prepare($query);
	    $query1 = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC";

	        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
	    $result1 = $db->prepare($query1);*/
	    
	}else if($sort == "date"){
	    $select = "SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, P.post_date_gmt";
	     $from = " FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc";
	     $where = " WHERE w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id";
	     if ($searchQuery != '') {
	     $where .= " and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
	     }
	     if($rtypes != '0'){
	        
	         $where .= " and w.type = '$rtypes'";
	         $where .= " and P.post_type = '$rtypes'";
	     }

	     if($lifestage !== '0'){
	         $from .=", life_stage_type AS l";
	         $where .= " AND l.lifestagetype = '$lifestage' AND l.postid = w.ID";
	     }
		 if($tags !='0'){
            $from .= " , wp_term_relationships as wtr ";
            $where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
         }
	     $order = " order by P.post_date_gmt desc";
	     //$limit = 'limt 0, 9';
	     $query = $select . $from . $where . $order;
	     $result = $db->prepare($query);
	     $query1 = $select . $from . $where . $order ;
	     $checkn = $db->prepare($query1);


	    /*$query = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC limit 9";

	        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
	    $result = $db->prepare($query);
	    $query1 = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC";

	        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
	    $result1 = $db->prepare($query1);*/

	}
		} else if ($searchvalue == '0'){
			// echo " line 68..";
			 if($sort == "0"){
	     $select = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id";
	         $from = " FROM wp_resources as w, wp_posts AS P";
	         $where = " WHERE P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100'";
	         
	         if($rtypes != '0'){
	             $where .= " and w.type = '$rtypes'";
	             
	             $where .= " and P.post_type = '$rtypes'";
	             
	         }
	        
	         if($lifestage !== '0'){
	                     $from .=", life_stage_type AS l";
	             $where .= " AND l.lifestagetype = '$lifestage' AND l.postid = w.ID";
	         }
			 if($tags !='0'){
				$from .= " , wp_term_relationships as wtr ";
				$where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
			 }
	         $order = " AND w.page_order = '$pgorder'";
	         //$limit = 'limt 0, 9';
	         $query = $select . $from . $where . $order ;
	         
	     $result = $db->prepare($query);
	     $query1 = $select . $from . $where . $order ;
	     $checkn = $db->prepare($query1);
	    /*$result = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9");
	    $result1 = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");*/
	}else if($sort == "relevance"){
	     $select = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id";
	         $from = " FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc";
	         $where = " WHERE P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id";
	         
	         if($rtypes != '0'){
	             $where .= " and w.type = '$rtypes'";
	             
	             $where .= " and P.post_type = '$rtypes'";
	             
	         }
	        
	         if($lifestage !== '0'){
	                     $from .=", life_stage_type AS l";
	             $where .= " AND l.lifestagetype = '$lifestage' AND l.postid = w.ID";
	         }
			 if($tags !='0'){
				$from .= " , wp_term_relationships as wtr ";
				$where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
			 }
	         $order = " AND w.page_order = '$pgorder'";
	         //$limit = 'limt 0, 9';
	         $query = $select . $from . $where . $order ;
	         
	     $result = $db->prepare($query);
	     $query1 = $select . $from . $where . $order ;
	     $checkn = $db->prepare($query1);

	/*$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9";
	    $result = $db->prepare($query);
	        $query1 = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'";
	    $result1 = $db->prepare($query1);*/
	    
	}else if($sort == "views"){
	     $select = "SELECT Distinct w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, wc.view_count";
	         $from = " FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc";
	         $where = " WHERE P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id";
	         
	         if($rtypes != '0'){
	             $where .= " and w.type = '$rtypes'";
	             
	             $where .= " and P.post_type = '$rtypes'";
	             
	         }
	        
	         if($lifestage !== '0'){
	                     $from .=", life_stage_type AS l";
	             $where .= " AND l.lifestagetype = '$lifestage' AND l.postid = w.ID";
	         }
			 if($tags !='0'){
				$from .= " , wp_term_relationships as wtr ";
				$where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
			 }
	         $order = " order by wc.view_count desc";
	         //$limit = 'limt 0, 9';
	         $query = $select . $from . $where . $order ;
	         
	     $result = $db->prepare($query);
	     $query1 = $select . $from . $where . $order ;
	     $checkn = $db->prepare($query1);


	    
	    /*$query = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC limit 9";

	        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
	    $result = $db->prepare($query);
	    $query1 = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC";

	        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
	    $result1 = $db->prepare($query1);*/
	    
	}else if($sort == "date"){
	     $select = "SELECT Distinct w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, P.post_date_gmt";
	         $from = " FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc";
	         $where = " WHERE P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id";
	         
	         if($rtypes != '0'){
	             $where .= " and w.type = '$rtypes'";
	             
	             $where .= " and P.post_type = '$rtypes'";
	             
	         }
	        
	         if($lifestage !== '0'){
	                     $from .=", life_stage_type AS l";
	             $where .= " AND l.lifestagetype = '$lifestage' AND l.postid = w.ID";
	         }
			 if($tags !='0'){
				$from .= " , wp_term_relationships as wtr ";
				$where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
			 }
	         $order = " order by P.post_date_gmt desc";
	         //$limit = 'limt 0, 9';
	         $query = $select . $from . $where . $order ;
	         
	     $result = $db->prepare($query);
	     $query1 = $select . $from . $where . $order ;
	     $checkn = $db->prepare($query1);

	    /*$query = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC limit 9";

	        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
	    $result = $db->prepare($query);
	    $query1 = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC";

	        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
	    $result1 = $db->prepare($query1);*/

	}
		} else 

		if($lifestage == '0' AND $searchvalue == '0' AND $rtypes == '0'){
			
			//$checkn = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");
		   if($sort == "0"){
			//echo '--line 233';
		   	$checkn = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");
		}else if($sort == "relevance"){
		        $query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'";
		    $checkn = $db->prepare($query);
		    
		}else if($sort == "views"){
		    $query = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $checkn = $db->prepare($query);
		    
		}else if($sort == "date"){
		    $query = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND wc.wp_post_id = w.wp_post_id ORDER BY P.post_date_gmt DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $checkn = $db->prepare($query);

		}


		} else



		if($lifestage == '0' AND $searchvalue == '0' AND $rtypes == '0'){
			//$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'";
		    //$checkn = $db->prepare($query);

		      if($sort == "0"){
		   	$checkn = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");
		}else if($sort == "relevance"){
		        $query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'";
		    $checkn = $db->prepare($query);
		    
		}else if($sort == "views"){
		    $query = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, wc.view_count from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $checkn = $db->prepare($query);
		    
		}else if($sort == "date"){
		    $query = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' ORDER BY P.post_date_gmt DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $checkn = $db->prepare($query);

		}
		}else if($lifestage == '0' AND $searchvalue == '0'AND $rtypes != '0'){
		    // life stage is 0 and search is 0 starts
		//	echo " line 42....";
		   //$checkn = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE type = '$rtypes' AND level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");
		   if($sort == "0"){
		        $checkn = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE type = '$rtypes' AND level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");
		    /*$result = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9");
		    $result1 = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");*/
		}else if($sort == "relevance"){
		    $checkn = $db->prepare("SELECT Distinct  l.postid, l.lifestagetype, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id 
		    FROM life_stage_type AS l, wp_resources AS w, wp_resources_view_count as wc
		    WHERE l.lifestagetype = '$lifestage' AND l.postid = w.ID AND w.type = '$rtypes' AND w.level_of_access = '$level' AND w.list_in_search = '$list' AND w.page_order = '$pgorder' AND wc.wp_post_id = w.wp_post_id");
		/*$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9";
		    $result = $db->prepare($query);
		        $query1 = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'";
		    $result1 = $db->prepare($query1);*/
		    
		}else if($sort == "views"){
		        //$checkn = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE type = '$rtypes' AND level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");
		    $checkn = $db->prepare("SELECT Distinct  l.postid, l.lifestagetype, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, wc.view_count 
		    FROM life_stage_type AS l, wp_resources AS w, wp_resources_view_count as wc
		    WHERE l.lifestagetype = '$lifestage' AND l.postid = w.ID AND w.type = '$rtypes' AND w.level_of_access = '$level' AND w.list_in_search = '$list' AND w.page_order = '$pgorder' AND wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC");
		    /*$query = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC limit 9";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result = $db->prepare($query);
		    $query1 = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result1 = $db->prepare($query1);*/
		    
		}else if($sort == "date"){
		        //$checkn = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE type = '$rtypes' AND level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");
		    $checkn = $db->prepare("SELECT Distinct  l.postid, l.lifestagetype, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, P.post_date_gmt 
		    FROM life_stage_type AS l, wp_resources AS w, wp_resources_view_count as wc
		    WHERE l.lifestagetype = '$lifestage' AND l.postid = w.ID AND w.type = '$rtypes' AND w.level_of_access = '$level' AND w.list_in_search = '$list' AND w.page_order = '$pgorder' AND wc.wp_post_id = w.wp_post_id ORDER BY P.post_date_gmt DESC");
		    /*$query = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC limit 9";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result = $db->prepare($query);
		    $query1 = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result1 = $db->prepare($query1);*/

		} 
		    // life stage is 0 and search is 0 ends
		}else if($lifestage != '0' AND $searchvalue == '0'){
		    // life stage is 'some value' and search is 0 starts
		    if($sort == "0"){
		        $checkn = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE type = '$rtypes' AND level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");
		    /*$result = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9");
		    $result1 = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");*/
		}else if($sort == "relevance"){
		    $checkn = $db->prepare("SELECT Distinct  l.postid, l.lifestagetype, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id 
		    FROM life_stage_type AS l, wp_resources AS w, wp_resources_view_count as wc
		    WHERE l.lifestagetype = '$lifestage' AND l.postid = w.ID AND w.type = '$rtypes' AND w.level_of_access = '$level' AND w.list_in_search = '$list' AND w.page_order = '$pgorder' AND wc.wp_post_id = w.wp_post_id");
		/*$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9";
		    $result = $db->prepare($query);
		        $query1 = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'";
		    $result1 = $db->prepare($query1);*/
		    
		}else if($sort == "views"){
		        //$checkn = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE type = '$rtypes' AND level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");
		    $checkn = $db->prepare("SELECT Distinct  l.postid, l.lifestagetype, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, wc.view_count 
		    FROM life_stage_type AS l, wp_resources AS w, wp_resources_view_count as wc
		    WHERE l.lifestagetype = '$lifestage' AND l.postid = w.ID AND w.type = '$rtypes' AND w.level_of_access = '$level' AND w.list_in_search = '$list' AND w.page_order = '$pgorder' AND wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC");
		    /*$query = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC limit 9";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result = $db->prepare($query);
		    $query1 = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result1 = $db->prepare($query1);*/
		    
		}else if($sort == "date"){
		        //$checkn = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE type = '$rtypes' AND level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");
		    $checkn = $db->prepare("SELECT Distinct  l.postid, l.lifestagetype, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, P.post_date_gmt 
		    FROM life_stage_type AS l, wp_resources AS w, wp_resources_view_count as wc
		    WHERE l.lifestagetype = '$lifestage' AND l.postid = w.ID AND w.type = '$rtypes' AND w.level_of_access = '$level' AND w.list_in_search = '$list' AND w.page_order = '$pgorder' AND wc.wp_post_id = w.wp_post_id ORDER BY P.post_date_gmt DESC");
		    /*$query = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC limit 9";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result = $db->prepare($query);
		    $query1 = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result1 = $db->prepare($query1);*/

		} 
		    // life stage is 'some value' and search is 0 ends
		}else if($lifestage == '0' AND $searchvalue != '0'){
		    // life stage is 0 and search is 'some value' starts
		    if($sort == "0"){
		   $checkn = $db->prepare("SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id FROM wp_resources as w, wp_posts AS P WHERE w.type = '$rtypes' AND w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode)) order by title_match DESC, title_rough_match desc, relevancy desc");
		    /*$result = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9");
		    $result1 = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");*/
		}else if($sort == "relevance"){
		   $checkn = $db->prepare("SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc WHERE w.type = '$rtypes' AND w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode)) order by title_match DESC, title_rough_match desc, relevancy desc");
		/*$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9";
		    $result = $db->prepare($query);
		        $query1 = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'";
		    $result1 = $db->prepare($query1);*/
		    
		}else if($sort == "views"){
		   $checkn = $db->prepare("SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, wc.view_count FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc WHERE w.type = '$rtypes' AND w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode)) order by title_match DESC, title_rough_match desc, relevancy desc, wc.view_count desc");
		    /*$query = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC limit 9";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result = $db->prepare($query);
		    $query1 = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result1 = $db->prepare($query1);*/
		    
		}else if($sort == "date"){
		   $checkn = $db->prepare("SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, P.post_date_gmt FROM wp_resources as w, wp_posts AS P, wp_resources_view_count as wc WHERE w.type = '$rtypes' AND w.status = 'publish' AND P.ID = w.wp_post_id AND w.list_in_search = 'true' AND w.level_of_access = '100' AND wc.wp_post_id = w.wp_post_id and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode)) order by title_match DESC, title_rough_match desc, relevancy desc, P.post_date_gmt DESC");
		    /*$query = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC limit 9";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result = $db->prepare($query);
		    $query1 = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result1 = $db->prepare($query1);*/

		}
		   //echo 'im reached here';
		   //$return_arr['query'] = $stmt;
		    // life stage is 0 and search is 'some value' ends
		}else if($lifestage != '0' AND $searchvalue != '0'){
		    // life stage is 'some value' and search is 'some value' starts
		    if($sort == "0"){
		    $checkn = $db->prepare("SELECT Distinct  l.postid, l.lifestagetype, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, (w.title = '{$unquotedQuery}') AS title_match 
		    FROM life_stage_type AS l, wp_resources AS w
		    WHERE title LIKE '%{$unquotedQuery}%' AND l.lifestagetype = '$lifestage' AND l.postid = w.ID AND w.type = '$rtypes' AND status = 'publish' AND list_in_search = 'true' AND level_of_access = '100' order by title_match DESC");  
		    /*$result = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9");
		    $result1 = $db->prepare("SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'");*/
		}else if($sort == "relevance"){
		    $checkn = $db->prepare("SELECT Distinct  l.postid, l.lifestagetype, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, (w.title = '{$unquotedQuery}') AS title_match 
		    FROM life_stage_type AS l, wp_resources AS w, wp_resources_view_count as wc
		    WHERE title LIKE '%{$unquotedQuery}%' AND l.lifestagetype = '$lifestage' AND l.postid = w.ID AND w.type = '$rtypes' AND status = 'publish' AND list_in_search = 'true' AND level_of_access = '100' AND wc.wp_post_id = w.wp_post_id order by title_match DESC");  
		/*$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit 9";
		    $result = $db->prepare($query);
		        $query1 = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder'";
		    $result1 = $db->prepare($query1);*/
		    
		}else if($sort == "views"){
		    $checkn = $db->prepare("SELECT Distinct  l.postid, l.lifestagetype, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, (w.title = '{$unquotedQuery}') AS title_match , wc.view_count
		    FROM life_stage_type AS l, wp_resources AS w, wp_resources_view_count as wc
		    WHERE title LIKE '%{$unquotedQuery}%' AND l.lifestagetype = '$lifestage' AND l.postid = w.ID AND w.type = '$rtypes' AND status = 'publish' AND list_in_search = 'true' AND level_of_access = '100' AND wc.wp_post_id = w.wp_post_id order by title_match DESC, wc.view_count desc");  
		    /*$query = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC limit 9";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result = $db->prepare($query);
		    $query1 = "SELECT Distinct   w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id from wp_resources as w, wp_resources_view_count as wc  where  wc.wp_post_id = w.wp_post_id ORDER BY wc.view_count DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result1 = $db->prepare($query1);*/
		    
		}else if($sort == "date"){
		    $checkn = $db->prepare("SELECT Distinct  l.postid, l.lifestagetype, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id, (w.title = '{$unquotedQuery}') AS title_match, P.post_date_gmt
		    FROM life_stage_type AS l, wp_resources AS w, wp_resources_view_count as wc
		    WHERE title LIKE '%{$unquotedQuery}%' AND l.lifestagetype = '$lifestage' AND l.postid = w.ID AND w.type = '$rtypes' AND status = 'publish' AND list_in_search = 'true' AND level_of_access = '100' AND wc.wp_post_id = w.wp_post_id order by title_match DESC, P.post_date_gmt DESC");  
		    /*$query = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC limit 9";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result = $db->prepare($query);
		    $query1 = "SELECT Distinct  * FROM wp_resources as w, wp_posts AS P WHERE w.level_of_access = '$level' AND P.ID = w.wp_post_id AND w.list_in_search = '$list' AND ORDER BY P.post_date_gmt DESC";

		        //$query = "SELECT Distinct  * FROM wp_resources WHERE level_of_access = '$level' AND list_in_search = '$list' AND page_order = '$pgorder' limit $start, $limit";
		    $result1 = $db->prepare($query1);*/

		}
		    // life stage is 0 and search is 'some value' ends
		}else{}
		//check 
		$checkn->execute();
		//now count row 
		$checkcountn = $checkn->rowCount();
		//$return_arr['countnow'] = $checkcountn;
		$totalpages = ceil( $checkcountn / $limit );
		$output = '';
		$output .='<nav aria-label="balance pager m14-m15" balance-pager="" class="paging-holder clear">
		<ul class="pagination">';
		if(empty($page) or $page == '' or $page == 0 or $page == '0'){
			$page = 1;
		}							
			if ($page > 1) {
				if($totalpages != 1){
					$output .='<li>
					<div class="prv-btn" lifestage="'.$lifestage.'" type="'.$rtypes.'" pager="'.($page-1).'"  search="'.$searchvalue.'"  sort="'.$sort.'">
						<div style="float:left;margin-right: 5px;margin-left: 10px;margin-top: 11px; cursor: pointer;">
							<span class="btn-prev"></span>
						</div>
						<div style="float:left;margin-top: 7px;  cursor: pointer; margin-right: 22px;">
							<span class="hidden-xs">Prev</span>
						</div>
					</div>
				 </li>';
				}
				
			}
										
			if($page == $totalpages){
				 if($totalpages<= 6) {
					for ($i= 1 ; $i <= $totalpages; $i++) {
						$output .='<li class="pg-btn '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" lifestage="'.$lifestage.'" typevalue="'.$rtypes.'" pagerv="'.$i.'"  search="'.$searchvalue.'"  sort="'.$sort.'">'.$i.'</li>';
					}
				} else if((6 + $page -1)<$totalpages){
					for ($i= (1 + $page -1) ; $i <= (6 + $page -1) ; $i++) {
						$output .='<li class="pg-btn '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" lifestage="'.$lifestage.'" typevalue="'.$rtypes.'" pagerv="'.$i.'"  search="'.$searchvalue.'"  sort="'.$sort.'">'.$i.'</li>';
					}
				} else{
					for ($i= ($totalpages-5); $i <=  $totalpages ; $i++)
					{
						$output .='<li class="pg-btn '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" lifestage="'.$lifestage.'" typevalue="'.$rtypes.'" pagerv="'.$i.'"  search="'.$searchvalue.'"  sort="'.$sort.'">'.$i.'</li>';
					}
				}
				

			if ($page < $totalpages) {
				$output .='<li><div class="next-btn" search="'.$searchvalue.'"  sort="'.$sort.'" lifestage="'.$lifestage.'" type="'.$rtypes.'" pager="'.($page+1).'">
					<div style="float:left;margin-right: 5px;margin-left: 22px;margin-top: 4px; cursor: pointer;"><span class="hidden-xs">Next</span></div>
					<div style="float:left;margin-top: 10px;  cursor: pointer;"><span class="btn-next"></span></div>
				</div></li>';
			}
			$output .='</ul>
				<p>
					<span>of&nbsp;</span>
					<span class="ng-binding">'.$totalpages.'</span>
					<span>&nbsp;pages</span>
				</p>
		</nav>';

			}else{
				if($totalpages<= 6) {
					for ($i= 1 ; $i <= $totalpages; $i++) {
						$output .='<li class="pg-btn '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" lifestage="'.$lifestage.'" typevalue="'.$rtypes.'" pagerv="'.$i.'"  search="'.$searchvalue.'"  sort="'.$sort.'">'.$i.'</li>';
					}
				} else if((6 + $page -1)<$totalpages){
					for ($i= (1 + $page -1) ; $i <= (6 + $page -1) ; $i++) {
						$output .='<li class="pg-btn '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" lifestage="'.$lifestage.'" typevalue="'.$rtypes.'" pagerv="'.$i.'"  search="'.$searchvalue.'"  sort="'.$sort.'">'.$i.'</li>';
					}
				} else{
					for ($i= ($totalpages-5); $i <=  $totalpages ; $i++) 
					{
						$output .='<li class="pg-btn '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" lifestage="'.$lifestage.'" typevalue="'.$rtypes.'" pagerv="'.$i.'"  search="'.$searchvalue.'"  sort="'.$sort.'">'.$i.'</li>';
					}
				}

			if ($page < $totalpages) {
				$output .='<li><div class="next-btn"  sort="'.$sort.'" search="'.$searchvalue.'" lifestage="'.$lifestage.'" type="'.$rtypes.'" pager="'.($page+1).'">
					<div style="float:left;margin-right: 5px;margin-left: 22px;margin-top: 4px; cursor: pointer;"><span class="hidden-xs">Next</span></div>
					<div style="float:left;margin-top: 10px;  cursor: pointer;"><span class="btn-next"></span></div>
				</div></li>';
			}
		if ($totalpages > 1) {
		$output .='</ul>
			<p>
				<span>of&nbsp;</span>
				<span class="ng-binding">'.$totalpages.'</span>
				<span>&nbsp;pages</span>
			</p>
	</nav>';
		} else {
		$output .='</ul>
			<p>
				<span>of&nbsp;</span>
				<span class="ng-binding">'.$totalpages.'</span>
				<span>&nbsp;page</span>
			</p>
	</nav>';
		}
		}
										
			$return_arr['message'] = $output;
			echo json_encode($return_arr);
		?>