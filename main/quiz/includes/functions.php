<?php
include('smtp/PHPMailerAutoload.php');

function userLogin($user,$pass){
	global $conn;
	$user = trim($user);
	$pass = md5(trim($pass));
	$sql="SELECT ID,user_login,user_nicename,user_email,display_name FROM wp_users where user_email='$user' AND user_pass='$pass' AND user_status='1'";
	
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_array($res);		
		$_SESSION['loggedIn']=$row['ID'];
		$_SESSION['user_name']=$row['user_login'];
		$_SESSION['user_contact']=$row['user_email'];
		//$_SESSION['white_label_website_id']=$row['white_label_website_id'];
		$_SESSION['display_name']=$row['display_name'];
		return true;
	}
	return false;	

}

function userRegistration($post){
	global $conn,$base_url;
	
	$firstname = trim($post['firstname']);
	$lastname = trim($post['lastname']);
	$email = trim($post['email']);
	$streetAddress = trim($post['streetAddress']);
	$city = trim($post['city']);
	$state = trim($post['state']);
	$zip = trim($post['zip']);
	$memberNumber = trim($post['memberNumber']);
	$password = md5(trim($post['password']));
	$is_employee = trim($post['is_employee']);
	$firstname = trim($post['firstname']);
	$date=date('Y-m-d H:i:s');
	$wlw = $_SESSION['white_label_website_id'];
	$token = generateToken();
	$sql="INSERT INTO `whitelabel_users` (`firstname`, `lastname`, `white_label_website_id`, `email`, `status`, `password`, `avatar`, `confirmed`, `confirmation_code`, `remember_token`, `created_at`, `updated_at`, `streetAddress`, `city`, `state`, `zip`, `memberNumber`, `is_employee`, `is_super_admin`) VALUES ('$firstname', '$lastname', '$wlw', '$email', 'active', '$password', '', '0', NULL, NULL, '$date', '$date', '$streetAddress', '$city', '$state', '$zip', NULL, '$is_employee', '0')";

	
	mysqli_query($conn,$sql);
	
	$sqluser ="INSERT INTO wp_users SET
	user_login='$firstname $lastname',
	user_pass='$password',	
	user_email='$email',
	user_url='',
	user_registered='$date',
	user_activation_key='$token',
	user_status='0',
	display_name='$firstname $lastname'
	";
	if(mysqli_query($conn,$sqluser)){
		$last_id = mysqli_insert_id($conn);
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='wlw_id',
		meta_value=''
		";
		
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='nickname',
		meta_value='$firstname $lastname'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='first_name',
		meta_value='$firstname'
		";
		mysqli_query($conn,$sqlmeta1);
		
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='last_name',
		meta_value='$lastname'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='description',
		meta_value=''
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='rich_editing',
		meta_value='true'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='comment_shortcuts',
		meta_value='false'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='admin_color',
		meta_value='fresh'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='use_ssl',
		meta_value=0
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='show_admin_bar_front',
		meta_value='true'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='wp_capabilities',
		meta_value=".serialize(array('cu_partner' => 1));
		
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='wp_user_level',
		meta_value='0'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='dismissed_wp_pointers',
		meta_value=''
		";
		mysqli_query($conn,$sqlmeta1);
		
		$link = $base_url.'index.php?action=verify&token='.$token;
		//$_SESSION['reg_link']=$link;
		$body="<p>Thank you $firstname for creating your new profile with BALANCE, in partnership with ".$_SESSION['title'].". Please click the link below to verify your ownership of $email.</p>

		<p>CLICK THIS LINK TO VERIFY:<BR>
		<BR><BR>
		Best,<BR>

		BALANCE in partnership with ".$_SESSION['title']."<BR><BR>$link
		</p>";

		$sendname = $firstname.' '.$lastname;		
		sendMail($email,'BALANCE Account Email Verification!',$body,$sendname);

		return true;	
	}
	else return false;	

}

function check_user_exist($user){
	global $conn;
	$sql="SELECT ID FROM wp_users where user_email='$user'";	
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		return true;
	}
	else{
		return false;
	}	
	
}
function logout(){
	session_destroy();
	session_unset();
	header("location:index.php");exit;
}

function sendMail($to,$subject,$msg,$name,$attached_file=''){
	//echo 'filename:: ',$attached_file;exit;
	global $smtp,$mailsendfrom;
	$mail = new PHPMailer(); 
	$mail->SMTPDebug  = false;
	$mail->IsSMTP(); 
	$mail->SMTPAuth = true; 
	$mail->SMTPSecure = 'tls'; 
	$mail->Host = $smtp['Host'];
	$mail->Port = 587; 
	$mail->IsHTML(true);
	$mail->CharSet = 'UTF-8';
	$mail->Username = $smtp['Username'];
	$mail->Password = $smtp['Password'];
	$mail->SetFrom($mailsendfrom,'Balance');
	$mail->Subject = $subject;
	$mail->Body =$msg;
	$mail->AddAddress($to,$name);
	if($attached_file!='')
	$mail->addAttachment($attached_file);
	$mail->SMTPOptions=array('ssl'=>array(
		'verify_peer'=>false,
		'verify_peer_name'=>false,
		'allow_self_signed'=>false
	));
	$mail->Send();
	/*if(!$mail->Send()){
		echo $mail->ErrorInfo;
	}else{
		return 'Sent';
	}*/
}

function generateToken(){
	$token = openssl_random_pseudo_bytes(16);
	$token = bin2hex($token);
	return $token;
}

function verifyToken(& $token){
	global $conn,$base_url;
	$sql="SELECT ID,user_login,user_nicename,user_email,display_name,user_status FROM wp_users where user_activation_key='$token'";	
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row=mysqli_fetch_array($res);
		$status = $row['user_status'];
		if($status=='1') return false;
		else{
			$_SESSION['loggedIn']=$row['ID'];
			$_SESSION['user_name']=$row['user_login'];
			$_SESSION['user_contact']=$row['user_email'];
			$_SESSION['display_name']=$row['display_name'];
		
			$sql="UPDATE wp_users set user_status='1' where user_activation_key	='$token'";	
			mysqli_query($conn,$sql);
			
			$body = '<div class="alert " role="alert">                   
                    <h1 class="text-info text-center">Welcome to BALANCE, in partnership with '.$_SESSION['title'].'</h1>

                    <p>Welcome m and thank you for completing your registration with BALANCE, in partnership with '.$_SESSION['title'].'.</p>

                    <p>You\'\ve taken a step to financial fitness by being an active participant in your own financial wellness.</p>

                    <p>Your registration gives you access to more online edcation programs available on <a href="'.$base_url.'">'.$base_url.'</a> .</p>

                    <p>Use your email address and BALANCE password to log in to a program when prompted.</p>
                    <p>We hope you enjoy your experience with BALANCE, in partnership with '.$_SESSION['title'].'. We are here to help you achieve your financial success!</p>

                    <p>Start by visiting us at <a href="'.$base_url.'">'.$base_url.' </a> .</p>
                    <p>Best,</p>
                    <p>BALANCE, in partnership with '.$_SESSION['title'].'</p>
                </div>';
			sendMail($row['user_email'],'Welcome to BALANCE, in Partnership with '.$_SESSION['title'],$body,$_SESSION['display_name']);
			return true;
		}
	}
	else{
		return false;
	}
}

function sendmailForgotPassword($email){
	global $base_url,$conn;
	$token = generateToken();
	$tmppwd = bin2hex(openssl_random_pseudo_bytes(5));
	
	
	$link = $base_url.'index.php?action=forgot_password&token='.$token.'&mail='.$email;
	//$_SESSION['reg_link']=$link;
	$body="<p>Hello!<BR>
	You are receiving this email because we received a password reset request for your account.<BR>
	<BR><BR>
	Your temparory password is $tmppwd
	<BR><BR>
	<a href='$link'>Reset Password</a>
	<BR><BR>
	If you did not request a password reset, no further action is required.<BR>

	<hr>

	If you're having trouble clicking the Reset Password button, copy and paste the URL below into your web browser: $link";
//echo $body;
		
	sendMail($email,$_SESSION['title'].' Password Reset',$body,'');
	$sql="UPDATE wp_users set user_url='$token' where user_email='$email'";	
	mysqli_query($conn,$sql);
	$sql = "UPDATE whitelabel_users SET remember_token='$tmppwd' WHERE email='$email'";
	mysqli_query($conn,$sql);
}


function checkPasswordExists($data){
	global $conn;
	//echo 'pass',$data['pass'];
	$pass=md5($data['pass']);
	$id=$data['id'];
	$sql="SELECT ID FROM wp_users where user_pass='$pass' AND ID='$id'";	
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0) return 1; 
	else return 0;
		
}	
function change_password(& $post){
	global $conn,$base_url;
	$token = trim($post['token']);
	$email = trim($post['email']);
	$password = md5(trim($post['password_confirmation']));
	$tmppassword = trim($post['password']);
	
	$sql = "SELECT w.ID,user_login,user_nicename,user_email,display_name,user_status,remember_token FROM wp_users w, whitelabel_users u where w.user_email=u.email AND w.user_url='$token' AND w.user_email='$email' AND u.remember_token='$tmppassword'";	
	//echo $sql;exit;
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){		
		$row = mysqli_fetch_array($res);
		$_SESSION['loggedIn']=$row['ID'];
		$_SESSION['user_name']=$row['user_login'];
		$_SESSION['user_contact']=$row['user_email'];
		$_SESSION['display_name']=$row['display_name'];
		$name = $row['user_login'];
		$sql="UPDATE wp_users set user_pass='$password' where user_email='$email'";	
		mysqli_query($conn,$sql);
		
		$sql="UPDATE whitelabel_users set remember_token='' where email='$email'";	
		mysqli_query($conn,$sql);
		$link = $base_url.'index.php?action=login';
		$body="<p> Dear $name,<BR>
		Your password has been changed successfully. <BR>		
		</p>";
		sendMail($email,'BALANCE Password!',$body,$name);
		return true;
	}
	
	else{
		return false;
	}
	
}

function getUserData(){	
	global $conn;	
	$id = $_SESSION['loggedIn'];
	$sql="SELECT * FROM wp_users  where ID='$id' limit 1";	//exit;
	$sql="SELECT user_login,w.ID,user_email,city,state,zip,streetAddress,is_employee,memberNumber FROM wp_users w , whitelabel_users u where w.user_email=u.email AND w.ID='$id'";
	//$sql="SELECT user_login,ID,user_email,city,state,zip,is_employee,memberNumber FROM wp_users w , whitelabel_users u where w.user_email=u.email AND w.ID='$id'";
	$res = mysqli_query($conn,$sql);
	return mysqli_fetch_array($res);	
}




function updateUser($post){		
	global $conn,$base_url;
	$firstname = trim($post['firstname']);
	$lastname = trim($post['lastname']);
	$id = $post['id'];	
	$emailid = $post['emailid'];	
	$streetAddress = trim($post['streetAddress']);
	$city = trim($post['city']);
	$state = trim($post['state']);
	$zip = trim($post['zip']);
	$memberNumber = trim($post['memberNumber']);	
	$is_employee = trim($post['is_employee']);	
	if($memberNumber=='') $memberNumber=0;
	
	$sql ="UPDATE wp_users SET
	user_login='$firstname $lastname',
	user_nicename='$firstname',
	display_name='$firstname $lastname' WHERE ID='$id'";
	if(mysqli_query($conn,$sql)){
		$sql ="UPDATE whitelabel_users SET
		city='$city',
		state='$state',
		zip='$zip',
		streetAddress='$streetAddress',
		memberNumber='$memberNumber',
		is_employee='$is_employee'
		WHERE email='$emailid'";//exit;
		mysqli_query($conn,$sql);
		$_SESSION['user_name']="$firstname $lastname";
		$_SESSION['user_contact']=$emailid;
		//$_SESSION['white_label_website_id']=$row['white_label_website_id'];
		$_SESSION['display_name']="$firstname $lastname";
	}	
}


function updateEmail($post){		
	global $conn,$base_url;
	$id = $post['id'];	
	$email = trim($post['email']);
	$oldemail = $_SESSION['user_contact'];
	$token = generateToken();
	$sql ="UPDATE wp_users SET user_email='$email' WHERE ID='$id'";
	if(mysqli_query($conn,$sql)){
		$sql ="UPDATE whitelabel_users SET email='$email' WHERE email='$oldemail'";
		mysqli_query($conn,$sql);
		session_destroy();
		session_unset();
		
	}	
}


function updatePassword($post){		
	global $conn,$base_url;
	//print_r();
	$password = md5(trim($post['password']));
	$id = $post['id'];
	
	$sql="SELECT user_pass FROM wp_users where ID='$id'";	
	$res = mysqli_query($conn,$sql);
	$row = mysqli_fetch_array($res);
	$oldpass = $row['user_pass'];
	//echo '<BR>new ',$password;	
	//exit;
	if($oldpass==$password){
		return 1;
	}
	else{	
	$sql="UPDATE wp_users SET user_pass='$password' WHERE ID='$id'";
	mysqli_query($conn,$sql);
	return 2;
	}
}

function getHomePage($domain){
	global $conn,$base_url;
	$html = '<article aria-label="article for Quiz" class="text-block default-content-style article default-content-style quiz-header"><div class="container"><div class="row">No Data available</div></div></article>';
	$sql = "SELECT title,domain,homepage_html,white_label_website_id,logo FROM wp_white_label_websites WHERE domain LIKE '%$domain%' LIMIT 1";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_array($res);
		$_SESSION['domainhtml'] = $row['homepage_html'];
		$_SESSION['title'] = $row['title'];
		$_SESSION['domain'] = $row['domain'];
		$_SESSION['logo'] = $row['logo'];
		$_SESSION['white_label_website_id'] = $row['white_label_website_id'];
		$html = $_SESSION['domainhtml'];
	}		
	return $html;
}

function submitQuiz(){
	global $conn,$mailsendfrom,$adminmail,$superadminmail,$base_url;
	$post = $_POST;
	$quizid = $post['quizid'];
	$wlw = $post['wlw'];
	$max_tries = $post['max_tries'];
	$cooldown = $post['cooldown'];
	$success_rate = $post['success_rate'];
	$certificate_title = $post['certificate_title'];
	$email_subject = $post['email_subject'];
	$email_body = $post['email_body'];
	$message_success = $post['message_success'];
	$has_certificate = $post['has_certificate'];
	$post_title = $post['post_title'];
	$newArr = getQuizQuestions($quizid);
	$wp_user_id = $_SESSION['loggedIn'];
	unset($post['wlw']);
	unset($post['action']);
	unset($post['quizid']);
	unset($post['max_tries']);
	unset($post['cooldown']);
	unset($post['success_rate']);
	unset($post['certificate_title']);
	unset($post['email_subject']);
	unset($post['email_body']);
	unset($post['message_success']);
	unset($post['has_certificate']);
	unset($post['post_title']);
	
	
	$newpost = array_values($post);
	
	$cntans = count($newpost);
	$cntsess = count($_SESSION['correctquiz']);
	$sess = $_SESSION['correctquiz'];
	$correct_val=0;
	for($s=0;$s<$cntsess;$s++){
		if(isset($post['answer-'.$s])){
			//echo '<BR>',$sess[$s],'==',$post['answer-'.$s];//$newpost[$s];
			if($sess[$s]==$post['answer-'.$s]) {
				$correct_val=$correct_val+1;
			}	
		}		
	}
	
	$perscore = ($correct_val*100)/$cntsess;
	/** count numer of attemt make for this quiz by user **/	
	
	$chk = checkUserCanAnswer($wp_user_id,$quizid,$max_tries,$cooldown);
	
	/** count numer of attemt make for this quiz by user **/
	
	
	if($chk=='error'){
		$_SESSION['message'] = 'You have tried maximum times. Please try later';
		header("location:$base_url".'index.php?action=quizresult');die;
	}
	
	//if user not given quiz or time interval is more than 12 hours
	if($chk=='insert'){
		$answer = addslashes(json_encode($post));		
		$sql="INSERT INTO `wp_quiz_results` SET 
		score='$perscore',
		num_correct='$correct_val',
		answers_data='$answer',
		wp_user_id='$wp_user_id',
		wp_post_id ='$quizid',
		mssql_id ='1234',
		timestamp=now(),
		transferred ='1'";
		
		//echo $sql;exit;
		if(mysqli_query($conn,$sql)){
			$last_id = mysqli_insert_id($conn);	
			$success_rate=50;
			if($perscore>=$success_rate){
				$display_name = $_SESSION['display_name'];
				$post_titlex = explode(':',$post_title);
				$html=file_get_contents('BALANCE_certificate.html');		
				$date = date('F d, Y',strtotime(date('Y-m-d')));
				$html = str_replace(array('%date%','%user%','%post_title1%','%post_title2%'),array($date,$display_name,$post_titlex[0],$post_titlex[1]),$html);
				
				$filenme = 'cert_'.$wp_user_id.'-'.$quizid.time().'.pdf';
				$attached_file = 'uploads/'.$filenme;
				
				createPDF($html,$attached_file);
				$sqlc="INSERT INTO wp_quiz_certificates SET quiz_result_id='$last_id',crtf_name='$filenme'";
				mysqli_query($conn,$sqlc);
				
				$to = $_SESSION['user_contact'];
				//isset($_SESSION['user_contact']) ? $_SESSION['user_contact'] : 'dhirajuphat@gmail.com';
				$data = $newArr['email_body'];
				$data .= " Your score was ".$perscore."% ($correct_val/$cntsess)";
				//if($email_subject=='') $email_subject='Certification From Balance';
				$email_subject=$post_title;
				$adminbody = $data."<BR><BR>User : ".$display_name;
				$adminbody .= "<BR><BR>Website : ".$_SESSION['title'].'<BR>';
				sendMail($to,$email_subject,$data,'',$attached_file);
				sendMail($adminmail,$email_subject,$adminbody,'',$attached_file);
				sendMail($superadminmail,$email_subject,$adminbody,'',$attached_file);
			}
		}
	}
	//if user again tries within time interval 12 and not reached upto max tries
	
	return $last_id;
}


function checkUserCanAnswer($wp_user_id,$quizid,$max_tries,$cooldown){
	global $conn;
	
	$sql = "SELECT ID,score FROM wp_quiz_results WHERE wp_user_id='$wp_user_id' AND wp_post_id='$quizid' AND timestamp > NOW() - INTERVAL $cooldown HOUR ORDER BY ID DESC";
	$res = mysqli_query($conn,$sql);
	$numrows = mysqli_num_rows($res);
	
	if($numrows>0){		
		while($row=mysqli_fetch_array($res)){			
			$score = $row['score'];
			continue;			
		}
		if($max_tries>$numrows){
			return 'insert';		
		} else if($max_tries<=$numrows){
			return 'error';	
		}
		else {
			return 'insert';		
		}
	}	
	else return 'insert';	
}	
function getLatestQuizResult(){
	global $conn,$base_url;
	$userid = $_SESSION['loggedIn'];
	///$userid='509';
	
	$sql = "SELECT * FROM wp_quiz_results WHERE wp_user_id='$userid' ORDER BY ID DESC LIMIT 1";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		return mysqli_fetch_object($res);		
	}
	return '';	
}

function getPastQuizes(){
	global $conn,$base_url;
	$userid = $_SESSION['loggedIn'];
	//$userid='509';	
	$sql = "SELECT r.*,crtf_name FROM wp_quiz_results r, wp_quiz_certificates c WHERE quiz_result_id=r.ID AND wp_user_id='$userid' ORDER BY ID DESC";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		return mysqli_fetch_all($res,MYSQLI_ASSOC);		
	}
	return '';	
}


function sendmailContact(){
	global $base_url,$mailsendfrom,$superadminmail,$adminmail;
	///$to='dhirajuphat@gmail.com';	
	$from = $_POST['email']; // this is the sender's Email address
	$request_type = $_POST['request_type'];
	$primary_concern = $_POST['primary_concern'];
	$secondary_concern = $_POST['secondary_concern'];
	$name = $_POST['name'];
	$email = $_POST['email'];
	$messagep= $_POST['message'];
	
	$name = ($name!='') ? $name : 'Name not captured';
	
	$primary_concern = ($primary_concern!='') ? ucfirst(str_replace('-',' ',$primary_concern)) : 'None stated';
	
	$secondary_concern = ($secondary_concern!='') ? ucfirst(str_replace('-',' ',$secondary_concern)) : 'None stated';
	
	$email = ($email!='') ? $email : 'Email not captured';
	$messagep = ($messagep!='') ? $messagep : 'Email not captured';
	
	
	if(isset($_POST['request_type']) && $_POST['request_type']=='counselor')
	$message = "Request For Counselor"."<br />";
	else $message = "General Inquiry"."<br />";
	
	$message .= '<BR>'.$_SESSION['title'];
	$message .= '<BR>Message from:  '.$name;
	$message .= '<BR>Sender email: '.$email ;
	$message .= '<BR>Primary Concern: '. $primary_concern;
	$message .= '<BR>Secondary Concern: '. $secondary_concern;
	//$message .= 'Referred By: ' .($name!='') ? ucfirst(str_replace('-',' ',$name) : 'None stated';
	$message .= '<br /><br />Message: ' .$messagep;
	$message .= '<br /><br />Website: ' .$$_SESSION['title'];
	
	sendMail($adminmail,'Thanks for contacting us!', $message,'');
	sendMail($superadminmail,'Thanks for contacting us!', $message,'');

	$_SESSION['message']="Your form is submitted successfully!";
	header("location:$base_url".'index.php?action=contact');
	die();	
}


function getQuizQuestions($postid){
	global $conn,$base_url;	
	$getQuestionDataQuery = mysqli_query($conn,"SELECT * FROM `wp_postmeta` WHERE `post_id`='$postid' AND `meta_key`='_page_edit_data'");

	$questionData = mysqli_fetch_array($getQuestionDataQuery);
	
	 $str = substr($questionData[3],5);
	/***test**/
	//$ansd = (array)json_decode($str);
	//print_r($ansd);exit;
	/***test**/
	
	$pattern = '/^";s(.+):"$/';
	$replacement = '';
	$result = preg_replace($pattern, $replacement, $str);

	$search = "/[^;s](.*)[^:]/";
	$replace = "";
	$string = substr($questionData[3],5);
	$getString = preg_replace('#;s(.*?):#e', "strlen('\\1')", $str);
	$str = '<a href="http://example.com/xyz">
		Series Hell In Heaven information
	</a>
	<a href="http://example.com/123">
		Series What is going information
	</a>';
	$newArr=array();
	preg_match_all('/".*?"/', $questionData[3], $matches);	
	foreach($matches[0] as $key => $value){
	  $newArr["question"] = $newArr["question"]+1;
	  $newArr["answer"] = $newArr["answer"]+1;	  
	  if($value == "question"){
		$newarrays[$value] = $value+1;
		if($value == "answer"){
		  $newarrays[$value] = $value+1;
		}
	  }
	}
	$match = $matches[0];
	$newArr=array();
	$i=0;
	$j=0;
	$m=0;
	///////////echo'<pre>',print_r($match),'</pre>';
	foreach($match as $key => $value){
		$newval = str_replace('"','',$value);
		if($newval=='question'){
			$newArr['question'][$i]=$match[$key+1];
			$k=$i;		
			$i++;	
		}
		if($newval=='answer'){		
			$newArr['answer'][$k][]=$match[$key+1];
			$cans =  str_replace('"','',$match[$key+2]);		 
			if($cans=='is_correct'){			
				$g=$k;
				$newArr['correct'][]=count($newArr['answer'][$k])-1;
			}		
		}
		if($newval=='copy'){
			$newArr['copy']=str_replace('"','',$match[$key+1]);
		}
		if($newval=='max_tries'){
			$newArr['max_tries']=str_replace('"','',$match[$key+1]);
		}
		if($newval=='cooldown'){
			$newArr['cooldown']=str_replace('"','',$match[$key+1]);
		}
		if($newval=='success_rate'){
			$newArr['success_rate']=str_replace('"','',$match[$key+1]);
		}
		if($newval=='certificate_title'){
			$newArr['certificate_title']=str_replace('"','',$match[$key+1]);
		}
		if($newval=='email_subject'){
			$newArr['email_subject']=str_replace('"','',$match[$key+1]);
		}
		if($newval=='email_body'){
			$newArr['email_body']=str_replace('"','',$match[$key+1]);
		}
		if($newval=='message_success'){
			$newArr['message_success']=str_replace('"','',$match[$key+1]);
		}
		if($newval=='has_certificate'){
			$newArr['has_certificate']=str_replace('"','',$match[$key+1]);
		}
		
		if($newval=='post_title'){
			$newArr['post_title']=str_replace('"','',$match[$key+1]);
		}
		if($newval=='message_fail'){
			$newArr['message_fail']=str_replace('"','',$match[$key+1]);
		}
		
			
		$j++;
	}
	//echo'<pre>',print_r($newArr);
	return $newArr;	
}

function createPDF($html,$filename){
	require('pdf/vendor/autoload.php');
	$mpdf=new \Mpdf\Mpdf();
	$mpdf->WriteHTML($html);
	//$file=$filename;
	$mpdf->output($filename,'F');	
}


function m24_check_quiz_validity( $max_tries,$cooldown,$postid ) {
	global $conn;

	//if ( is_user_logged_in() ) {
		$user_id = $_SESSION['loggedIn'];	
		

		if ( isset( $max_tries ) && isset( $cooldown ) ) {
			$cooldown_period = ( int ) $cooldown;
			$max_tries = ( int ) $max_tries;
			$cooldown = $last_try = false;
			$date_try_again = new DateTime('now');

			// check if user has used up all attempts in the last 12 hours (with insufficient score)
			//$table_name = $wpdb->prefix . 'quiz_results';
			//$sql = $wpdb->prepare( "SELECT * FROM $table_name WHERE wp_user_id = %d AND wp_post_id = %d AND timestamp > NOW() - INTERVAL 12 HOUR AND score < %f ", $user_id, $current_post_id, $quiz_data['success_rate'] );
			//$results = $wpdb->get_results( $sql, ARRAY_A );

			$sql= "SELECT * FROM wp_quiz_results WHERE wp_user_id = '$user_id' AND wp_post_id = '$postid' AND timestamp > NOW() - INTERVAL 12 HOUR AND score < 80 ";
			$result = mysqli_query($conn,$sql);	
			$numrows = mysqli_num_rows($result);
			if ( $numrows > 1 ) {
				$row = mysqli_fetch_array($result);
				$last_try = $row[ $numrows - 1 ]['timestamp'];
				$last_try = new DateTime( $last_try );
				// if a cooldown period has not yet passed from the last try, do not allow reattempting the quiz
				$date_try_again = clone $last_try;
				$date_try_again->add(new DateInterval('PT'.$cooldown_period.'H'));
				$diff = $date_try_again->diff(new DateTime('now'));
				if ( count( $results ) >= $max_tries ) {
					$now = new DateTime('now');
					if ( $date_try_again > $now ) {
						$cooldown = true;
					}
				}
			}
			return array(
				'reattempt'       => true,
				'cooldown'        => $cooldown,
				'last_try'        => $last_try,
				'try_again'       => $date_try_again,
				'attempts_left'   => $max_tries -  $numrows,			
			);
		}
		return true;
	//}
	//return true;
}

?>