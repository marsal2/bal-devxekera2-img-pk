<!DOCTYPE html>
<html>

<head>
    <!-- set the encoding of your site -->
    <meta charset="utf-8">
    <!-- set the viewport width and initial-scale on mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$_SESSION['title']?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <!-- include the site stylesheet -->
    <link rel="stylesheet" href="<?=$base_url?>assets/css/main.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js' async defer ></script>
</head>
<style>

@font-face {
    font-family: dinot;
    src: url(<?=$base_url?>assets/fonts/DINOT-CondBold.eot);
    src: url(<?=$base_url?>assets/fonts/DINOT-CondBold.eot?#iefix) format("embedded-opentype"), url(<?=$base_url?>assets/fonts/DINOT-CondBold.svg) format("svg"), url(<?=$base_url?>assets/fonts/DINOT-CondBold.woff) format("woff"), url(<?=$base_url?>assets/fonts/DINOT-CondBold.ttf) format("truetype");
    font-weight: 700;
    font-style: normal
}

@font-face {
    font-family: dinot;
    src: url(<?=$base_url?>assets/fonts/DINOT-CondMedium.eot);
    src: url(<?=$base_url?>assets/fonts/DINOT-CondMedium.eot?#iefix) format("embedded-opentype"), url(<?=$base_url?>assets/fonts/DINOT-CondMedium.svg) format("svg"), url(<?=$base_url?>assets/fonts/DINOT-CondMedium.woff) format("woff"), url(<?=$base_url?>assets/fonts/DINOT-CondMedium.ttf) format("truetype");
    font-weight: 600;
    font-style: normal
}

@font-face {
    font-family: dinot;
    src: url(<?=$base_url?>assets/fonts/DINOT-CondRegular.eot);
    src: url(<?=$base_url?>assets/fonts/DINOT-CondRegular.eot?#iefix) format("embedded-opentype"), url(<?=$base_url?>assets/fonts/DINOT-CondRegular.svg) format("svg"), url(<?=$base_url?>assets/fonts/DINOT-CondRegular.woff) format("woff"), url(<?=$base_url?>assets/fonts/DINOT-CondRegular.ttf) format("truetype");
    font-weight: 400;
    font-style: normal
}

@font-face {
    font-family: Gotham;
    src: url(<?=$base_url?>assets/fonts/Gotham-Bold.eot);
    src: url(<?=$base_url?>assets/fonts/Gotham-Bold.eot?#iefix) format("embedded-opentype"), url(<?=$base_url?>assets/fonts/Gotham-Bold.svg) format("svg"), url(<?=$base_url?>assets/fonts/Gotham-Bold.woff) format("woff"), url(<?=$base_url?>assets/fonts/Gotham-Bold.ttf) format("truetype");
    font-weight: 700;
    font-style: normal
}

@font-face {
    font-family: Gotham;
    src: url(<?=$base_url?>assets/fonts/Gotham-Book.eot);
    src: url(<?=$base_url?>assets/fonts/Gotham-Book.eot?#iefix) format("embedded-opentype"), url(<?=$base_url?>assets/fonts/Gotham-Book.svg) format("svg"), url(<?=$base_url?>assets/fonts/Gotham-Book.woff) format("woff"), url(<?=$base_url?>assets/fonts/Gotham-Book.ttf) format("truetype");
    font-weight: 400;
    font-style: normal
}

@font-face {
    font-family: Gotham;
    src: url(<?=$base_url?>assets/fonts/Gotham-Medium.eot);
    src: url(<?=$base_url?>assets/fonts/Gotham-Medium.eot?#iefix) format("embedded-opentype"), url(<?=$base_url?>assets/fonts/Gotham-Medium.svg) format("svg"), url(<?=$base_url?>assets/fonts/Gotham-Medium.woff) format("woff"), url(<?=$base_url?>assets/fonts/Gotham-Medium.ttf) format("truetype");
    font-weight: 600;
    font-style: normal
}

.wcolor{color:white;}

.alert {
  padding: 20px; 
  opacity: 1;
  transition: opacity 0.6s;
  margin-bottom: 15px;
}

.alert.success {background-color: #04AA6D;}
.alert.info {background-color: #2196F3;}
.alert.warning {background-color: #ff9800;}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

.closebtn:hover {
  color: black;
}
</style>

<body>
    <!-- header of the page -->
   <?php include 'templates/header.php'?>
    <!-- contain main informative part of the site -->
        <!-- footer of the page -->
    <?php include $file?>
    <?php include 'templates/footer.php'?>
    <?php include 'templates/onlyregistration.php'?>
   
	
    
	
    <script src="assets/js/vendor.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/ajax.js"></script>
	<script src="assets/js/validation.js"></script>
	<script src="assets/js/custom.js"></script>
<script>
var close = document.getElementsByClassName("closebtn");
var i;
for (i = 0; i < close.length; i++) {
  close[i].onclick = function(){
    var div = this.parentElement;
    div.style.opacity = "0";
    setTimeout(function(){ div.style.display = "none"; }, 600);
  }
}
</script>
<?php if(isset($_SESSION['reg_link'])){?>
				<a href=<?=$_SESSION['reg_link']?>>link</a><BR>
				<?php } ?>
</body>

</html>