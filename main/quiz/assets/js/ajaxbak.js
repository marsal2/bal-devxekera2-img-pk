$( document ).ready(function() {
	$("#frm_register").on('submit', function(e) {
		
		e.preventDefault();		
		if(validRegister()){
		$('#frmsubmitbtn').hide();	
		var email = $('#email').val();
		let success_msg = `
			<div class="container">
			<div class="row">
				<div class="col-md-7">
					<div class="alert alert-success">
			Your Email verification has been sent to: <a href=" " class="__cf_email__" >[${email}]</a>
		</div>
					<div class="panel panel-default">
						<div class="panel-heading">Email Verification</div>
						<div class="panel-body">
							Once you have verified your email, you may login and reset password. If you have not received this email yet, first check your spam folder and if it's still not found you can <a href="">resend verification</a>.
						</div>
					</div>
				</div>
			</div>
		</div>
		`;
				
		var formData = new FormData(this);
		$.ajax({
			url: 'index.php?action=do_register',
			data: formData,
			type: 'POST',
			success: function(data) {
				///alert(data)
				$('#frmsubmitbtn').show();
				if(data=='1'){
					 $("#Modal1").removeClass("fade").modal("hide");
					 $("#Modalsuccess").modal("show").addClass("fade");
					 $("#log_reg_msg_id").html(success_msg);
					 $("#frm_register").trigger("reset");
				}
				else if(data=='2'){					
					$("#emailmsg").html('This user is already exists');
					$('#email').focus();		
				}	
				else{
					$("#Modalerror").modal("show").addClass("fade");
				}
			},
			cache: false,
			contentType: false,
			processData: false
		});
		}
	})	
});


$( document ).ready(function() {
	$("#frm_register_mob").on('submit', function(e) {
		alert(success_msg)
		
		e.preventDefault();		
		if(validRegister()){
		$('#mob_frmsubmitbtn').hide();		
		var formData = new FormData(this);
		$.ajax({
			url: 'index.php?action=do_register',
			data: formData,
			type: 'POST',
			success: function(data) {
				$('#mob_frmsubmitbtn').show();
				if(data=='1'){
					 $("#Modal1").removeClass("fade").modal("hide");
					 $("#Modalsuccess").modal("show").addClass("fade");
					 $("#log_reg_msg_id").html(success_msg);
					 $("#frm_register").trigger("reset");
				}
				else{
					$("#Modalerror").modal("show").addClass("fade");
				}
			},
			cache: false,
			contentType: false,
			processData: false
		});
		}
	})	
});


$( document ).ready(function() {
	$("#frm_login").on('submit', function(e) {
		$('#mob_frmsubmitbtn').hide();
		e.preventDefault();		
		if(validlogin()){			
		var formData = new FormData(this);
		$.ajax({
			url: 'index.php?action=do_login',
			data: formData,
			type: 'POST',
			success: function(data) {
				$('#frmsubmitbtnlogin').show();
				//alert(data)
				if(data=='0'){
					 $("#Modalerror").modal("show").addClass("fade");
				}
				else{
					$("#Modal1").removeClass("fade").modal("hide");
					 $("#logout_hdr").show();
					 $("#logged_name_hdr").show();
					 $("#nologin_hdr").hide();
					 $("#logged_name_hdr").html('<span class="icon-user"></span>'+data);
					 $("#log_reg_msg_id").html("Logged in Successfully!");
					 $("#Modal1").removeClass("fade").modal("hide");
					 $("#Modalsuccess").modal("show").addClass("fade");
					 $("#frm_login").trigger("reset");
					
				}
			},
			cache: false,
			contentType: false,
			processData: false
		});
		}
	})	
});
