   <?php
   //$html=file_get_contents('BALANCE_certificate.html');
   //createPDF($html);
  // print_r($_SESSION);
  $newArr = getQuizQuestions($data->wp_post_id);
   ?> 
	
	<main id="main">
        <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                        <!-- Link to home -->
                        <li><a href="<?=$base_url?>">Home</a></li>
                        <!-- Link to resource page -->
                        <li>Quizzes</li>
                        <li><?=$newArr['post_title']?></li>
                    </ol>
                </div>
            </div>
        </div>
     
        <article aria-label="article for Quiz" class="text-block default-content-style article default-content-style quiz-header" style="display: none;">
            <div class="container">
                <div class="row">
                    <div class="">
                        <h1 class="text-info text-center">Quiz</h1>
                        <p><span style="line-height: 30px;">Take the quiz to check your understanding of this&nbsp;module. Submit your answers to view your results.&nbsp;Your results will be shared with your&nbsp;referring organization.</span></p>
                    </div>
                </div>
            </div>
        </article>

		
		<?php if(!empty($data)){
			
			
			$total = (100*$data->num_correct)/$data->score;
			//echo $data->answers_data;
			$ansd = (array)json_decode(str_replace('answer-','',$data->answers_data));
			//print_r($ansd);
			
			$correctans = $newArr['correct'];
			///echo'<pre>',print_r($newArr),'</pre>';
			//$valid = m24_check_quiz_validity( $newArr['max_tries'],$newArr['cooldown'],$data->wp_post_id );
			//echo '<br>';
			//print_r($valid);
			?>
        <article name="balance_article" class="quiz-results text-block article background-white default-content-style" style="" aria-label="article module print score">
            <div class="container">
                <div class="row">
                    <h1 class="text-info text-center">Your Results</h1>
                    <p>Your score was <b class="quiz-percent"><?=$data->score?> %</b> <?php if($data->num_correct>0){?>(<span class="quiz-number-of-correct"><?=$data->num_correct?></span>/<span class="quiz-number-of-questions"><?=$total?></span>).<?php } ?></p>
					<?php if($data->score<$newArr['success_rate']){?>
                    <p><span style="line-height: 25.7143px;"><?=$newArr['message_fail']?></span></p>
					<?php } else {?>
					 <p><span style="line-height: 25.7143px;"><?=$newArr['message_success']?></span></p>
					<?php } ?>
                    <div class="btn-holder clearfix" style="margin-bottom: 30px;"><a href="<?=$base_url?>index.php?action=loginquiz1&postid=<?=$data->wp_post_id?>" class="btn btn-warning pull-left quiz-retry">Retry quiz</a></div>
                </div>
            </div>
        </article>
		
		<section aria-label="quiz questions" class="quiz-questions modules-form-section">
            <div class="container">
                <div class="row">
                    <form id="quiz-form" method="post" class="form-wrap">
					<input type='hidden' name='action' value='submit_query'>
                        <div class="quiz-questions">
                            <ol class="questions">
							<?php for($i=0;$i<count($newArr['question']);$i++){
								$question = $newArr['question'][$i];
								$answers = $newArr['answer'][$i];							
								
								?>
                                <li><strong class="h3"><?php echo str_replace('"','',$question); ?></strong>
                                    <ul class="choose-list">
									<?php 
									for($b=0;$b<count($answers);$b++){
										if(isset($ansd[$i]) && $ansd[$i] == $b) {
											$checked='checked';
											if($correctans[$i]==$b){
												$icon='icon-check';
											} else{
												$icon='icon-close';
											}	
											
											$istyle="margin-left: -30px;";
											
											
										}else {
											$checked='';
											$icon='';
											$istyle='';
										}
										if(isset($ansd[$i])){
											if($correctans[$i]==$b){
												$src= $base_url.'assets/img/green.png';
												$img = "<img src='$src' style='width:40px;padding-left:10px;'/>";
											}
											else {
												$src= $base_url.'assets/img/wrong.png';
												$img = "<img src='$src' style='width:40px;padding-left:10px;' />";
											}	
										}
										else{
											$img = "";
										}
										
										//echo $checked;
									?>
                                        <li> 
										  
										  
										  <label> 
										  <!--span class="<?=$icon?>" style="<?=$istyle?>"></span--> 
										  <input type="radio" name="answer-<?=$i?>" value="<?=$b?>" <?=$checked?>> 
										  <span class="fake-input"></span>
										  <span class="fake-label" style="margin-left: 10px;"><?php echo str_replace('"','',$answers[$b]); ?> </span>
										  <?=$img?></label>
										  
										  
                                        </li>                  
									<?php } ?>                                        
                                    </ul>
                                </li>
                                <?php } ?>
                            </ol>
                           
                        </div>
                    </form>
                </div> 
            </div>
        </section>
		<?php } ?>
    </main>
	<script>
	$(':radio,:checkbox').click(function(){
    return false;
});

	</script>
   