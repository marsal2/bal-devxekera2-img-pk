    <!-- login quiz code start -->
    <main id="main">
        <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                        <!-- Link to home -->
                        <li><a href="<?=$base_url?>">Home</a></li>
                        <!-- Link to resource page -->
                        <li>Past Quizzes</li>
                       
                    </ol>
                </div>
            </div>
        </div>
   <?php ///echo '<pre>',print_r($newArr);echo '</pre>';?>
        <article aria-label="article for Quiz" class="text-block default-content-style article default-content-style quiz-header">
            <div class="container">
                <div class="row">
                    <div class="">
                        <h1 class="text-info text-center">Past Quizes</h1>                        
                    </div>
                </div>
            </div>
        </article>
        <section aria-label="quiz questions" class="quiz-questions modules-form-section">
            <div class="container">
                <div class="row">
					<?php if(!empty($data)){?>
					   <div class="quiz-questions">
                            <ol class="questions">
							<?php for($i=0;$i<count($data);$i++){								
								$quizdata=getQuizQuestions($data[$i]['wp_post_id']);
								$sdata = & $data[$i];
							?>
                                <li><strong class="h3"><?php echo $quizdata['post_title']; ?></strong>
                                    <ul class="choose-list">
									
                                        <li>
										<div style=''>
										<div>Date <?=date('d F Y, h:i A',strtotime($sdata['timestamp']))?></div>
										<div>Your Score is <?=$sdata['score']?>%</div>
										<div>Correct Answers are <?=$sdata['num_correct']?></div>
										<div>Certificate <a href="<?=$base_url?>uploads/<?=$sdata['crtf_name']?>" target='_blank' download><?=$sdata['crtf_name']?></a></div>
										<hr>
										</div>
                                          


										 </li>                  
									
                                <?php } ?>
                            </ol>
                           
                        </div>
					<?php } else {?>
					No quiz is given by you till date.		
					<?php } ?>	
                </div> 
            </div>
        </section>
        </main>

<script>
function validQuiz(){
	var x = $("input[type = 'radio']:checked");  
	if (x.length<2) {
		alert('Please answer minimum 2 questions');
		return false;
	} 
	return true;
}
</script>
 <!-- login quiz code end -->

