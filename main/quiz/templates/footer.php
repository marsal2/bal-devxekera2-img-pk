<footer class="footer">
        <div class="container">
            <div class="row">
                <div class="logo-holder">
                    <a href="<?=$base_url?>"><img src="<?=$base_url?>assets/img/logo-primary.svg" width="198" height="44" alt="Advantage Home Plus
"></a>
                </div>
                <ul class="menu-f">
                    <li><a href="<?=$base_url?>resources?sortBy=-views&pager=1">RESOURCES</a></li>
                    <li><a href="<?=$base_url?>index.php?action=programs">PROGRAMS</a></li>
                    <li><a href="<?=$base_url?>index.php?action=contact">CONTACT US</a></li>
                </ul>
                <ul class="social-links">
                    <li><a href="https://www.facebook.com/BALANCEFinFit/"><span class="icon-facebook"></span></a></li>
                    <li><a href="https://twitter.com/BAL_Pro"><span class="icon-twitter"></span></a></li>
                </ul>
                <div class="copyright-footer">
                    &copy; <?=date('Y')?> BALANCE. All rights reserved.
                </div>
            </div>
        </div>
    </footer>