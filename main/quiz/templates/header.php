 <?php if(isset($_SESSION['user_name']) && $_SESSION['user_name']!='')
 {
	 $displaynamex=explode(' ',$_SESSION['user_name']);
	 $displayname = substr($displaynamex[1],0,8);
	 $logged_style="";
	 $nolog_style="display:none"; 
 }	 
 else{
	$logged_style="display:none"; 
	$nolog_style='';
 }
 //print_r($_SESSION);
	/*$domain_slug = isset($_GET['domain'])?$_GET['domain']:'demo';
	if(isset($_SESSION['domainhtml']) && !isset($_GET['domain'])){		
		$mainhtml = $_SESSION['domainhtml'];
	}
	else {
		echo'mainhtml';
		$mainhtml = getHomePage($domain_slug);
		
	}*/	

	
	?>
 <header id="header" class="nav-fixed">
        <div class="header-t private-header-t no-chat" style="background-image: url(<?=$base_url?>assets/img/img01.png);">
            <div class="container">
                <div class="row">
                    <!-- search form -->
                    <ul class="contact-block">
                        <li><a href="tel:8005112197"><span class="icon-phone"></span>800-511-2197</a>
                        </li>
						
                        <li class="hidden-xs" id='logged_name_hdr' style="<?=$logged_style?>"><a href="<?=$base_url?>index.php?action=account"><span class="icon-user"></span><?php echo $displayname?></a>
                        </li>
						<li class="hidden-xs" id='logout_hdr' style="<?=$logged_style?>"><a  href="index.php?action=logout" title='Logout'><span class="icon-user"></span>LOGOUT</a>
                        </li>
						
						<!--li class="hidden-xs" id='nologin_hdr' style="<?=$nolog_style?>"><a data-toggle="modal" data-target="#Modal1" href="#Modal1"><span class="icon-user"></span>LOG IN</a>
                        </li-->
						<li class="hidden-xs" id='nologin_hdr' style="<?=$nolog_style?>"><a data-toggle="modal"  href="index.php?action=login"><span class="icon-user"></span>LOG IN</a>
                        </li>
						
                    </ul>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-default private-header">
            <div class="container">
                <div class="row">
                    <div class="navbar-header private-header">
                        <button aria-expanded="false" data-target="#bs-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- page logo -->
						<?php $logo = isset($_SESSION['logo']) ? $_SESSION['logo'] : $base_url.'assets/img/advantagehp_logo.jpg'?>
						<?php $title = isset($_SESSION['title']) ? $_SESSION['title'] : ''?>
                        <a href="<?=$base_url?>" class="navbar-brand navbar-promotion" title="<?=$title?>"><img src="<?=$logo?>" alt="<?=$title?>"></a>
                        <!-- promotion banner -->
                        <a href="<?=$base_url?>" class="navbar-brand navbar-promotion" title="<?=$title?>"><img src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/2020/11/29100513/Home-1.png" alt="Home-1.png"></a>
                    </div>
                    <!-- main navigation of the page -->
                    <div id="bs-navbar-collapse-1" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="<?=$base_url?>index.php?action=resources&sortBy=-views&pager=1" title='Resources'>Resources</a>
                            </li>
                            <li>
                                <a href="<?=$base_url?>index.php?action=programs" title='Programs'>Programs</a>
                            </li>
							<li id='li_account' style="<?=$logged_style?>">
                                <a href="<?=$base_url?>index.php?action=account" title='My Account'>Account</a>
                            </li>
                            <li>
                                <a href="<?=$base_url?>index.php?action=contact" class="" title='Contact Balance'>Contact Balance</a>
                            </li>
							<?php if($nolog_style==''){?>
                            <li class="visible-xs" id='nologin_hdrmob'><a href="index.php?action=login" class="dropdown-toggle">Log In<span class="custom-caret visible-xs"><img src="/images/drop-arrow.svg" width="19" height="11" alt=" "></span></a></li>
							<?php } else { ?>
							<li class="visible-xs">
                                <a href="<?=$base_url?>index.php?action=logout" title='Logout'>Logout</a>
                            </li>
							<?php }  ?>
							
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
				
            </div>
        </nav>
    </header>