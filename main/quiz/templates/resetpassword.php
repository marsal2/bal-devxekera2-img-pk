 <!-- contain main informative part of the site -->
 <?php
 $token = isset($_GET['token'])? $_GET['token'] : '';
 $mail = isset($_GET['mail'])? $_GET['mail'] : '';
 
 
 
 ?>
 <main id="main" role="main">
	
    <div class="container">
        <div class="row">
            <div class="">
			<?php
	if(isset($_SESSION['chpass_msg']) && $_SESSION['chpass_msg']!=''){
		?>
		<div class="alert warning">
		<span class="closebtn">&times;</span>  
	<?php echo $_SESSION['chpass_msg']?>
		</div>
	<?php
	unset($_SESSION['chpass_msg']);	
	}
	?>
                <div class="panel panel-default">
                    <div class="panel-heading">Reset Password</div>
                    <div class="panel-body">

                        <form class="login-form" role="form" method="POST" action="index.php?action=changepassword" name='frm_reset' onsubmit="return validReset()">
                            <input type='hidden' name='token' value="<?=$token?>">
                            <div class="input-group">
                                <label class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input type="email" name="email" id="email" value="<?=$mail?>" onfocusout="validEmail('email','emailmsg')">
									<span id='emailmsg'></span>
                                </div>
                            </div>

                            <div class="input-group">
                                <label class="col-md-4 control-label">Temparory Password</label>

                                <div class="col-md-6">
                                    <input type="password" name="password" id="password" >
									<span id='msg_pass'></span>
                                </div>
								
                            </div>

                            <div class="input-group">
                                <label class="col-md-4 control-label">New Password</label>

                                <div class="col-md-6">
                                    <input type="password" name="password_confirmation" id="password_confirmation_reset" onfocusout="validPassReset(this.value,'msg_cpass')">
									<span id='msg_cpass'></span>
                                </div>
								
                            </div>

                             
                            <div class="input-group">
                                <div class="col-md-4 col-md-offset-5">
                                    <button type="submit" class="btn btn-warning">
                                        Reset Password
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

</main>