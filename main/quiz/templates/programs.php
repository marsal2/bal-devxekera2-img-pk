<main id="main" role="main">
    <style>
    .report-block img {
        height: 315px;
        width: auto;
    }

    .report-block .image-holder,
    .report-block .copy-holder {
        width: 50%;
    }

    .report-block .copy-holder {
        position: relative;
        padding: 0 38px;
        text-align: left;
    }

    @media  screen and (max-width: 1024px) {
        .report-block .image-holder {
            width: 100%;
        }

        .report-block .image-holder img {
            width: 100%;
            height: auto !important;
        }

        .report-block .copy-holder {
            width: 100%;
            padding-top: 25px;
            padding-bottom: 25px;
            top: 0;

            -webkit-transform: translateY(0%);
            -ms-transform: translateY(0%);
            transform: translateY(0%);
        }
    }
</style>

    <div class="banner" style="background-image: url('https://arrowheadcu.balancepro.org/images/T52_header_desktop.jpg');">
    <div class="container">
        <div class="row">
            <div class="banner-block short">
                <div class="banner-text">
                    <h1 style="font-size: 44px;">Programs to guide you towards financial success</h1>
                    <p>Check out our available programs below</p>
                </div>
            </div>
        </div>
    </div>
</div>
    
            <div class = "block report-holder background-white default-content-style">
            <section class = "report-block">
                <div class = "container">
                    <div class = "row">
                        <div class = "img-holder goofy">
                            <img src="https://www.balancepro.org/wp-content/uploads/program_BT.jpg" width="700" height="410" alt="image description" class = "height-calc">
                        </div>

                        <div class = "text-holder height-calc goofy">
                            <div class = "text">
                                <h1 class = "text-info">BalanceTrack</h1>
                                <p>Learn the basics of personal finance with the BalanceTrack educational modules. Register for a free user account or log in to get started.</p><a href="<?=$base_url?>index.php?action=programs-balancetrack" class="btn btn-warning">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

                    <div class = "block report-holder background-white default-content-style">
            <section class = "report-block">
                <div class = "container">
                    <div class = "row">
                        <div class = "img-holder ">
                            <img src="https://www.balancepro.org/wp-content/uploads/program-credit-matters.jpg" width="700" height="410" alt="image description" class = "height-calc">
                        </div>

                        <div class = "text-holder height-calc ">
                            <div class = "text">
                                <h1 class = "text-info">BalanceTrack (En Español)</h1>
                                <p></p><a href="<?=$base_url?>programs/balancetrack-en-espanol" class="btn btn-warning">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

                    <div class = "block report-holder background-white default-content-style">
            <section class = "report-block">
                <div class = "container">
                    <div class = "row">
                        <div class = "img-holder goofy">
                            <img src="https://www.balancepro.org/wp-content/uploads/program-MB.jpg" width="700" height="410" alt="image description" class = "height-calc">
                        </div>

                        <div class = "text-holder height-calc goofy">
                            <div class = "text">
                                <h1 class = "text-info">MyBalance</h1>
                                <p>Use MyBalance to create a budget plan that helps you achieve your financial goals. </p><a href="<?=$base_url?>programs/mybalance-budget" class="btn btn-warning">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

                    <div class = "block report-holder background-white default-content-style">
            <section class = "report-block">
                <div class = "container">
                    <div class = "row">
                        <div class = "img-holder ">
                            <img src="https://www.balancepro.org/wp-content/uploads/program-GB.jpg" width="700" height="410" alt="image description" class = "height-calc">
                        </div>

                        <div class = "text-holder height-calc ">
                            <div class = "text">
                                <h1 class = "text-info">Get in Balance</h1>
                                <p>Learn to manage your checking account with the Get in Balance educational module. Register for a free user account or log in to get started.</p><a href="<?=$base_url?>programs/get-in-balance" class="btn btn-warning">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

            </main>