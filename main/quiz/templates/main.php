<?php 
//$style_pop='display:none';
//$_SESSION['firstlogin']=1;
if(isset($_SESSION['firstlogin']) && $_SESSION['firstlogin']==1){
	 //$style_pop='';
?>
<script>
$(document).ready(function(){
    $("#modalThank").modal();   
});
</script>
<?php	 
}


?>

<main id="main" role="main">
<?php
if(isset($_SESSION['firstlogin_err']) && $_SESSION['firstlogin_err']!=''){
?>
<div class="alert wcolor">
  <span class="closebtn">&times;</span>  
  <?php echo $_SESSION['firstlogin_err']?>
</div>

<?php
}
if(isset($_SESSION['message']) && $_SESSION['message']!=''){
?>
	<div class="alert success wcolor">
  <span class="closebtn">&times;</span>  
  <?php echo $_SESSION['message']?>
	</div>
<?php
}	
unset($_SESSION['message']);
unset($_SESSION['firstlogin']);
unset($_SESSION['firstlogin_err']);
?>
<div id="modalThank" tabindex="-1" role="dialog" class="modal fade">
	<div  class="modal-dialog">
		<div class="modal-content">
			 <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">X</span></button>    <!-- &#215; -->
			<div class="alert " role="alert">                   
				<h1 class="text-info text-center">Thank you for verifying your email address</h1>
				<p>Thank you for completing your registration with BALANCE, in partnership with <?=$_SESSION['title']?>. You’ve taken a step to financial fitness by being an active participant in your own financial wellness.</p>
				<p>Your registration gives you access to more BALANCE online education programs available on this website. Use your email address and BALANCE password to log in to a program when prompted.</p>
				<p>We hope you enjoy your experience with BALANCE, in partnership with <?=$_SESSION['title']?>. We are here to help you achieve your financial successes!</p>
				<p>Best,</p>
				<p>BALANCE in partnership with <?=$_SESSION['title']?></p>
			</div>
		</div>
	</div>
</div>
<?php
echo $mainhtml;

?>
</main>
