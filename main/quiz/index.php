<?php
session_start();

include 'includes/config.php';
include 'includes/functions.php';
error_reporting(E_ALL);
error_reporting(0);
$message = '';
$action = '';


$domain_slug = isset($_GET['domain'])?$_GET['domain']:'demo';
if(isset($_SESSION['domainhtml']) && !isset($_GET['domain'])){		
	$mainhtml = $_SESSION['domainhtml'];
}
else {
	//echo'mainhtml';
	$mainhtml = getHomePage($domain_slug);	
}


if(isset($_REQUEST['action']) && $_REQUEST['action']!=''){
	$action=$_REQUEST['action'];
}

switch ($action) {
  case 'do_register':
    
	include  'php/registration.php';
		exit;
	
    break;
	
  case 'do_login':
  
  
    include  'php/login.php'; exit;
	
    break;
	
	case 'login':
		if(isset($_SESSION['loggedIn'])){
		  header("location:$base_url");die;
		}
    $file = 'templates/mainlogin.php';
	
    break;
	
	case 'thankyou':
  
    $file = 'templates/thankyou.php';
	
    break;
	
	case 'error':
  
    $file = 'templates/error.php';
	
    break;
	
	case 'verify':
	//echo 'hello';exit;
    if(verifyToken($_GET['token']))  { 
	//echo 'success';exit;
	$_SESSION['firstlogin']=1; header("location:$base_url");die;
	}
	else {
		//echo 'error';exit;
		$_SESSION['firstlogin_err']='Invalid confirmation code, please check your email or register a new account.';
		 header("location:$base_url");die;
	}
	
	
    break;
	
	
  case 'logout':
  
    logout();
	
    break;
	
   case 'contact':
  
    $file = 'templates/contact.php';
	
    break;	
	
	case 'sendmail_forgot':
		
		sendmailForgotPassword(trim($_POST['email']));
		if(isset($_POST['actiontype'])){
			header("location:$base_url".'index.php?action=login');die;
		} else	exit;		
	
    break;	
	
	case 'forgot_password':
		
		$file = 'templates/resetpassword.php';		
	
    break;

	case 'changepassword':
		
		if(change_password($_POST)){
			$_SESSION['message']='Your password has been changed successfully!';
			header("location:$base_url");die;
		}
		else{
			$link=$base_url."index.php?action=forgot_password&token=".$_POST['token'].'&mail='.$_POST['email'];
			$_SESSION['chpass_msg']="Something is wrong! Please try again.";
			header("location:$link");die;
		}
	
    break;	
	
	case 'check_password_exists':
		echo checkPasswordExists($_GET);exit;
	
    break;

	case 'chkuserexits':
		if(check_user_exist($_GET['email'])){
			echo 1;exit;
		}
		else{
			echo 2;exit;
		}
	
    break;	
	
	
	case 'account':
		if(isset($_SESSION['loggedIn'])){
			$data = getUserData();
			$file = 'templates/accountupdate.php';
		}
		else { header("location:$base_url");die;}
	
    break;
	
	case 'update_user':
		
		updateUser($_POST);
		$_SESSION['acc_msg']='Your account is updated';
		header("location:$base_url".'index.php?action=account');die;	
	
    break;

	case 'update_email':
		
		updateEmail($_POST);
		session_start();
		$_SESSION['newmailid']=$_POST['email'];
		$_SESSION['email_msg']='Your email address is updated.';
		header("location:$base_url".'index.php?action=login');die;
	
    break;

	case 'update_pass':
		
		$flag = updatePassword($_POST);	
		if($flag==1) $_SESSION['pass_msg']="Please don't enter same password, try another";	
		else $_SESSION['pass_msg']='Your password has been updated';	
		header("location:$base_url".'index.php?action=account');die;
    break;

	case 'programs':
		
		$file = 'templates/programs.php';		
	
    break;
	
	case 'loginquiz':
	
		
	$file = 'templates/loginquiz.php';		
	
    break;
	
	case 'loginquiz1':
		if(isset($_SESSION['loggedIn'])){
			$file = 'templates/loginquiz1.php';	
		}
		else{
			header("location:$base_url".'index.php?action=login');die;
		}
		//$file = 'templates/loginquiz1.php';			
	
    break;
	
	case 'submit_query':
	
		$id = submitQuiz();
		if($id!='') {
			//$_SESSION['message'] = 'Quiz submitted successfully';
			header("location:$base_url".'index.php?action=quizresult');die;
		}	
		else {
			$_SESSION['message'] = 'Something is wrong';
			header("location:$base_url".'index.php?action=loginquiz1');die;
		}	
		
		exit;
	
    break;	
	
	
	case 'quizresult':
		if(isset($_SESSION['loggedIn'])){
			$data = getLatestQuizResult();
			$file = 'templates/quizresult.php';		
		}
		else{
			header("location:$base_url".'index.php?action=login');die;
		}
		
	
    break;
	
	case 'pastquiz':
	
		if(isset($_SESSION['loggedIn'])){
			$data = getPastQuizes();
			$file = 'templates/pastquiz.php';		
		}
		else{
			header("location:$base_url".'index.php?action=login');die;
		}
	
		
    break;

	case 'withoutloginquiz':
		
		$file = 'templates/quizwithoutLogin.php';		
	
    break;


	case 'resources':
		
		$file = 'templates/render-search-main-design.php';		
	
    break;	
	
	case 'programs-balancetrack':
		
		$file = 'templates/balance-track.php';		
	
    break;	
	
	
	case 'sendmailContact':
		
		sendmailContact();;		
	
    break;	
	
	
	default:
	
	$file = 'templates/main.php';
	
	break;
  
}

include 'main.php';
if(isset($_SESSION['loggedIn'])) echo $_SESSION['loggedIn'];
if(isset($_SESSION['white_label_website_id'])) echo '<BR>',$_SESSION['white_label_website_id'];
//echo session_id();
?>
