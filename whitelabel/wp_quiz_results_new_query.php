<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include 'includes/config_2.php';
//general query
//$sql = $db->prepare("SELECT ID,wp_user_id,wlwid FROM wp_quiz_results WHERE wlwid > 0");

function quiz_update_log($log_msg, $logtype)
{
    $log_filename = "log";
    if (!file_exists($log_filename)) 
    {
        // create directory/folder uploads.
        mkdir($log_filename, 0777, true);
    }
    if($logtype == 'success'){
        $log_file_data = $log_filename.'/updated_wp_quiz_result_data.log';
    }else{
        $log_file_data = $log_filename.'/updated_error_wp_quiz_result_data.log';
    }
    
    // if you don't add `FILE_APPEND`, the file will be erased each time you add a log
    file_put_contents($log_file_data, $log_msg . "\n", FILE_APPEND);
} 

//timestamp between query
$sql = $db->prepare("SELECT ID,wp_user_id,wlwid FROM wp_quiz_results WHERE timestamp between '2022-01-29 08:00:00' AND '2022-02-19 11:04:00'");
$sql->execute();
$countsql = $sql->rowCount();
echo '<b>total wlwid > 0 count:</b> '.$countsql.'<br/><br/>';
  if($countsql >  0){
        $i = 0;
        while($row = $sql->fetch(PDO::FETCH_ASSOC)){
        $wp_user_id = $row['wp_user_id'];
        $qid = $row['ID'];
        $wlwid = $row['wlwid'];
        $sqlWP = $db->prepare("SELECT wpusers.ID,wpusers.user_email,wlusers.email,wlusers.white_label_website_id FROM wp_users as wpusers, whitelabel_users as wlusers WHERE wlusers.id = '$wp_user_id' AND wlusers.email = wpusers.user_email");
        $sqlWP->execute();
        $countsqlWP = $sqlWP->rowCount();
        //echo $countsqlWP.'-- total result will be updated in result table <br/>';
            if($countsqlWP > 0){
                $rowWP = $sqlWP->fetch(PDO::FETCH_ASSOC);
                $wid = $rowWP['ID'];
                $user_email = $rowWP['user_email'];
                $email = $rowWP['email'];
                $white_label_website_id = $rowWP['white_label_website_id'];

                $sqlUpdate = $db->prepare("UPDATE wp_quiz_results SET wp_user_id = '$wid', mssql_id = NULL, transferred = 0 WHERE ID = '$qid'");
                $sqlUpdate->execute();
                $countsqlUpdate = $sqlUpdate->rowCount();
                if($countsqlUpdate){ 
                        echo "Updated Successfully: wp_result id : <b>".$qid.'</b><br/><br/>';
                        $lgtype = 'success';
                        $log_msgs=date('Y-m-d H:i:s').' :: Wordpress User Email ID :'.$user_email.' :: Whitelabel User Email ID :'.$email.' :: Whitelabel User ID :'.$wp_user_id.' :: Wordpress ID :'.$wid;
                        quiz_update_log($log_msgs, $lgtype); 
                     } else { 
                        echo "Could not update data: wp_result id : <b>".$qid.'</b><br/><br/>';
                        $lgtype = 'error';
                        $log_msgs=date('Y-m-d H:i:s').' :: Wordpress User Email ID :'.$user_email.' :: Whitelabel User Email ID :'.$email.' :: Whitelabel Userid :'.$wp_user_id.' :: Wordpress User ID:'.$wid;
                        quiz_update_log($log_msgs, $lgtype);
                    } 
            }else{
                echo 'whitelabel user which has no id in wp_user: <b>'.$wp_user_id.'</b><br/><br/>';
                       // echo "Could not update data: wp_result id : <b>".$wp_user_id.'</b><br/><br/>';
                        $lgtype = 'error';
                        $log_msgs=date('Y-m-d H:i:s').' :: Whitelabel Email ID :'.$email.' :: Whitelabel User ID :'.$wp_user_id;
                        quiz_update_log($log_msgs, $lgtype);
            }
        }
        $i++;
}else{
   echo 'No wp quiz results which has wlwid > 0 <br/>';
}
?>
