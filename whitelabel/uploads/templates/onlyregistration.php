<style>
                #registerLegend {
                    text-align: left;
                    padding-top: 15px;
                }

                #registerLegend ul {
                    padding-left: 0;
                }

                .formRequired, .formOptional { font-size: 30px; vertical-align: text-top; }

                .formRequired,
                .formOptional {
                    color:red;
                }

                .label {
                    white-space: normal;
                    text-align: left;
                }

                .login-modal .login-form .checkbox label {
                    padding-left: 0;
                }

                .jcf-select {
                    text-align: left;
                    width: 100%;
                    height: 40px;
                    border: 1px solid #bbb;
                }

                .jcf-select .jcf-select-opener {
                    background: none;
                }

                .jcf-select-opener:before {
                    content: "\25be";
                    font-style: normal;
                    font-weight: normal;
                    text-decoration: inherit;
                    color: #000;
                    font-size: 18px;
                    padding-right: 0.5em;
                    position: absolute;
                    top: 6px;
                    right: 0;
                }

                .jcf-select .jcf-select-text {
                    line-height: 40px;
                }

                .counselingAndPrivacy { padding-right: 5rem; padding-left: 0 !important; }
                .counselingAndPrivacy .fake-input { left: inherit; right: -5rem; }
                @media (min-width: 1200px) {
                    .fake-label { text-align: left; }
                }
            </style>

            <?php
                if(!isset($base_site_url) ){
                  require_once('../../includes/db/common.php');
                }
            ?>
    
    <div class="login-modal">
        <div id="myModal" aria-label="register dialog modal" tabindex="-1" role="dialog" class="modal fade">
            <div role="document" class="modal-dialog">
                <div class="modal-content">
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">X</span></button>    <!-- &#215; -->
                    <div class="login-form">
                        <fieldset>
                            <div class="tab-holder">
                                <div class="pop-up-tab visible-xs">
                                    <ul role="tablist" class="nav nav-tabs">
                                        <li role="presentation" class="active"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Register</a></li>                                       
                                    </ul>
                                    <button aria-label="Close" data-dismiss="modal" type="button" class="close visible-xs"><span aria-hidden="true">x</span></button>
                                </div>
                                <div class="tab-content col-holder">
                                    <div id="tab4" role="tabpanel" class="tab-panel active">
                                        <div class="form-content">
                                            <h1 class="text-info" style="text-align: center;">Sign up for your BALANCE account.</h1>
                                            <p style="text-align: center;">Please register as a new user to access our online education programs.</p>
                                            <div class="form-fields hidden-xs">
                                                <form class="login-form" enctype= "multipart/form-data" autocomplete="off" id='frm_register'>
                                                    <fieldset>
                                                        <input type="hidden" name="registerfrm" value="1">
                                                        <input type="hidden" name="action" value="do_register">
                                                        <div class="form-block">
                                                        	<div class="row">
                                                        		<div class="col-md-6">
                                                            <div class="input-group">
                                                                <span class="label"><label for="firstname">First Name<span class="formRequired">*</span></label></span>
                                                                <input type="text" id="firstname" name="firstname" placeholder="" value="">
																<span id='firstnamemsg'></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="input-group">
                                                                <span class="label"><label for="lastname">Last Name<span class="formRequired">*</span></label></span>
                                                                <input type="text" id="lastname" name="lastname" placeholder="" value="" >
																<span id='lastnamemsg'></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        		<div class="col-md-6">
                                                            <div class="input-group">
                                                                <span class="label"><label for="email">Email<span class="formRequired">*</span></label></span>
                                                                <input type="Email" id="emailweb" name="email" placeholder="Your email will be your username." value=""  onfocusout="chkUserExits(this.value,'web')"><span id='emailmsg'></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
																<input type='hidden' id='mailexistsid' value='0'>
                                                            <div class="input-group">
                                                                <span class="label"><label for="streetAddress" style="margin-bottom: 18px;">Street Address</label></span>
                                                                <input type="text" id="streetAddress" name="streetAddress" placeholder="" value="">
																
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        		<div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="label"><label for="city">City</label></span>
                                                                <input type="text" id="city" name="city" placeholder="" value="">
																
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="label"><label for="state">State</label></span>
                                                                <input type="text" id="state" name="state" placeholder="" value="">
																
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="label"><label for="zip">Zip</label></span>
                                                                <input type="text" id="zip" name="zip" placeholder="" value="" maxlength=5 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
																

																<p id='zipmsg'></p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        		<div class="col-md-6">

                                                    	<div class="input-group">
                                                                <span class="label"><label for="password">Password<span class="formRequired">*</span></label></span>
                                                                <input type="password" name="password" placeholder="8 characters, no spaces" id="passwordweb" onfocusout="validPass(this.value)" >
																<span id='passwordmsg'></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">

                                                            <div class="input-group">
                                                                <span class="label"><label for="password_confirmation">Confirm Password<span class="formRequired">*</span></label></span>
                                                                <input type="password" name="password_confirmation" placeholder="" id="password_confirmation" onfocusout="cvalidPass(this,'passwordweb')" >
																<span id='password_confirmationmsg'></span>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    	 <div class="row">
                                                        		<div class="col-md-6">

                                                            <div class="input-group">
                                                                <span class="label"><label for="memberNumber" style="white-space: break-spaces;                                                                 
                                                                    text-align: initial; margin-bottom: 9px;">Last four digits of account/member number</label></span>
                                                                <input type="text" id="memberNumber" name="memberNumber" placeholder="" value="">
																
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">

                                                            
                                                            <div class="input-group checkbox">
                                                                <span class="label"><label style="white-space: break-spaces;                                                                 
                                                                    text-align: initial; padding-left: 0px;">Are you a member/client or employee?</label></span>
                                                                <select name="is_employee" id="is_employee">
                                                                    <option value="0">Member/client</option>
                                                                    <option value="1">Employee</option>
                                                                </select>
																
                                                            </div>
                                                        </div>
                                                    </div>
                                                            <div class="input-group checkbox counselingAndPrivacy check" style="text-align: center;">
                                                                <label  style="position: relative;">
                                                                    <input type="checkbox" name="counseling_and_privacy_agreement" id="counseling_and_privacy_agreement">
                                                                    <span class="fake-input"></span>
                                                                    <span class="fake-label ">
                                                                        I agree with
                                                                        <a href="<?php echo $parent_url; ?>/counseling-agreement-and-privacy-policy/" target="_blank">
                                                                            Counseling Agreement and Privacy Policy
                                                                        </a><span class="formRequired">*</span><BR>
                                                                        <span id="counseling_and_privacy_agreementmsg"></span>
                                                                        
                                                                        
                                                                    <!-- <input type="checkbox" name="counseling_and_privacy_agreement" id="input12"> -->
                                                                    </span>
                                                                   
                                                                </label>
                                                            </div>
                                                          <div id="registerLegend">
                                                                <ul style="list-style-type: none;">
                                                                    <li><span class="formRequired">*</span>Required</li>
                                                                </ul>
                                                            </div> 
                                                          <div class="g-recaptcha" data-sitekey="<?=$site_key?>"></div> <p id='grecaptchamsg'></p>
                                                            
                                                            <div class="btn-holder" id='frmsubmitbtn'>
                                                                <!--input type="submit" class="but btn btn-primary" value="Register"-->
																<button type="submit" class="btn btn btn-primary" id='frmsubmitbtn'>Register</button>
                                                            </div>
															<span id='load_msg'><span>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="form-fields visible-xs">
                                                <form class="login-form" id='frm_register_mob' method="POST" autocomplete="off" >
												<input type="hidden" name="registerfrm" value="1">
                                                <input type="hidden" name="action" value="do_register">
                                                    <fieldset>
                                                        <input type="hidden" name="_token" value=" ">
                                                        <div class="form-block">
                                                            <div class="input-group">
                                                                <input type="text" id="firstnamemob" name="firstname" placeholder="First name" value="">
																<span id='firstnamemsgmob'></span>
																
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="text" id="lastnamemob" name="lastname" placeholder="Last name" value="">
																<span id='lastnamemsgmob'></span>
																
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="Email" id="emailmob" name="email" placeholder="Email" value="" onfocusout="chkUserExits(this.value,'mob')">
																<span id='emailmsgmob'></span>
																
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="text" id="Street Address" name="streetAddress" placeholder="Street Address" value="">
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="text" id="city" name="city" placeholder="City" value="">
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="text" id="state" name="state" placeholder="State" value="">
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="text" id="zipmob" name="zip" placeholder="Zip Code" value="" maxlength=5 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"><span id='zipmsgmob'></span>
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="text" id="memberNumber" name="memberNumber" placeholder="Last 4 Member #" value="">
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="password" name="password" placeholder="Password" id="passwordmob" onfocusout="validPassMob(this.value)" ><span id='passwordmsgmob'></span>
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="password" name="password_confirmation" placeholder="Confirm password" 
																id="password_confirmationmob" onfocusout="cvalidPassMob(this)" ><span id='password_confirmationmsgmob'></span>
                                                            </div>
                                                            <div class="input-group">
                                                                <select name="is_employee" id="is_employee">
                                                                    <option value="0">Member/client</option>
                                                                    <option value="1">Employee</option>
                                                                </select>
                                                            </div>
                                                            <div class="input-group checkbox counselingAndPrivacy">
                                                                <label style="position: relative;">
                                                                    <input type="checkbox" name="counseling_and_privacy_agreement" id="counseling_and_privacy_agreementmob">
                                                                   
                                                                        I agree with
                                                                        <a href="" target="_blank">
                                                                            Counseling Agreement and Privacy Policy
                                                                        </a>
																		<span class='fake-input'></span>
																		<span id="counseling_and_privacy_agreementmsgmob"></span>
                                                                        <span class="formRequired">*</span>
                                                                   
                                                                </label>
                                                            </div>
															<div class="g-recaptcha" data-sitekey="<?=$site_key?>"></div> 
															<p id='grecaptchamsgmob'></p>
															<div class="btn-holder" id='mob_frmsubmitbtn'>
                                                                <!--input type="submit" class="but btn btn-primary" value="Register"-->
																<button type="submit" class="btn btn btn-primary">Register</button>
																<span id='load_msgmob'><span>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
		
			<div id="Modalsuccess" tabindex="-1" role="dialog" aria-label="Forgotten Password dialog" class="modal fade inner">
				<div role="document" class="modal-dialog">
					<div class="modal-content">
						<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
						<div class="pop-up-tab visible-xs">
							<button aria-label="Close" data-dismiss="modal" type="button" class="close visible-xs"><span aria-hidden="true">&#215;</span></button>
						</div>
						<div class="form-content" id='log_reg_msg_id'>						
							<p style="text-align:left;" >Registration successfully done</p>						
						</div>
					</div>
				</div>
			</div>
		
		
		
		<div id="Modalerror" tabindex="-1" role="dialog" aria-label="Forgotten Password dialog" class="modal fade inner">
			<div role="document" class="modal-dialog">
				<div class="modal-contentv">
					<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
					<div class="pop-up-tab visible-xs">
						<button aria-label="Close" data-dismiss="modal" type="button" class="close visible-xs"><span aria-hidden="true">&#215;</span></button>
					</div>
					<div class="form-content">
						
						<p style="text-align:left;" id='err_msg'>Somethis went wrong. Please try again!</p>
						
					</div>
				</div>
			</div>
		</div>
