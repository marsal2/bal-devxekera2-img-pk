<?php
/**** Function for users,quiz,contact,homepage ***/
/**** Author: Dhiraj uphat***/

include('smtp/PHPMailerAutoload.php');


/** function for user login **/
function userLogin($user,$pass){
	global $conn;
	$user = trim($user);
	$pass = trim($pass);
	$wlw = $_SESSION['white_label_website_id'];
	$sql="select * from whitelabel_users where email='$user' and white_label_website_id='$wlw' and status='active'";
		
		$res = mysqli_query($conn,$sql);
		if(mysqli_num_rows($res)>0){
			//$_SESSION['login_error_new']='As system is updated, you have to reset the password.';	
			$row = mysqli_fetch_array($res);
			$hash = $row['password'];	
			
			if (password_verify($pass, $hash)) {
			  // echo '<br/>Password is valid!';
				$_SESSION['loggedIn']=$row['id'];
				$_SESSION['user_name']=$row['firstname'].' '.$row['lastname'];
				$_SESSION['user_contact']=$row['email'];
				$_SESSION['display_name']=$row['firstname'].' '.$row['lastname'];
				return true;
				//echo'password valid';
			} else {
			   return false;
				//echo'password invalid';		
			}
			//exit;
		}
		
	
	return false;	

}

/** function for user registration **/
function userRegistration($post){
	global $conn,$base_url;
	
	$firstname = trim($post['firstname']);
	$lastname = trim($post['lastname']);
	$email = trim($post['email']);
	$streetAddress = trim($post['streetAddress']);
	$city = trim($post['city']);
	$state = trim($post['state']);
	$zip = trim($post['zip']);
	$memberNumber = trim($post['memberNumber']);
	$pasword = trim($post['password']);
	$cost=10; // Default cost
	$password = password_hash($pasword, PASSWORD_BCRYPT, ['cost' => $cost]);
	$is_employee = trim($post['is_employee']);
	$firstname = trim($post['firstname']);
	$date=date('Y-m-d H:i:s');
	$wlw = $_SESSION['white_label_website_id'];
	$token = generateToken();
	$sql="INSERT INTO `whitelabel_users` (`firstname`, `lastname`, `white_label_website_id`, `email`, `status`, `password`, `avatar`, `confirmed`, `confirmation_code`, `remember_token`, `created_at`, `updated_at`, `streetAddress`, `city`, `state`, `zip`, `memberNumber`, `is_employee`, `is_super_admin`) VALUES ('$firstname', '$lastname', '$wlw', '$email', 'inactive', '$password', '', '0', '$token', NULL, '$date', '$date', '$streetAddress', '$city', '$state', '$zip', NULL, '$is_employee', '0')";

	
	mysqli_query($conn,$sql);
	
	$sqluser ="INSERT INTO wp_users SET
	user_login='$firstname $lastname',
	user_pass='$password',	
	user_email='$email',
	user_url='',
	user_registered='$date',
	user_activation_key='$token',
	user_status='0',
	display_name='$firstname $lastname'
	";
	if(mysqli_query($conn,$sqluser)){
		$last_id = mysqli_insert_id($conn);
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='wlw_id',
		meta_value=''
		";
		
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='nickname',
		meta_value='$firstname $lastname'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='first_name',
		meta_value='$firstname'
		";
		mysqli_query($conn,$sqlmeta1);
		
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='last_name',
		meta_value='$lastname'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='description',
		meta_value=''
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='rich_editing',
		meta_value='true'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='comment_shortcuts',
		meta_value='false'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='admin_color',
		meta_value='fresh'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='use_ssl',
		meta_value=0
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='show_admin_bar_front',
		meta_value='true'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='wp_capabilities',
		meta_value=".serialize(array('cu_partner' => 1));
		
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='wp_user_level',
		meta_value='0'
		";
		mysqli_query($conn,$sqlmeta1);
		
		$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
		meta_key='dismissed_wp_pointers',
		meta_value=''
		";
		mysqli_query($conn,$sqlmeta1);
		
		$link = $base_url.'index.php?action=verify&token='.$token;
		//$_SESSION['reg_link']=$link;
		$body="<p>Thank you $firstname for creating your new profile with BALANCE, in partnership with ".$_SESSION['title'].". Please click the link below to verify your ownership of $email.</p>

		<p>CLICK THIS LINK TO VERIFY: <a href='$link'>Link</a> <BR>
		<BR><BR>
		Best,<BR>

		BALANCE in partnership with ".$_SESSION['title']."<BR><BR>$link
		</p>";

		$sendname = $firstname.' '.$lastname;
		$log = array('Registration');	
		sendMail($email,'BALANCE Account Email Verification!',$body,$sendname,$log);

		return true;	
	}
	else return false;	

}

/** function for check user is already exists or not **/
function check_user_exist($user){
	global $conn;
	$wlw = $_SESSION['white_label_website_id'];
	//$sql="SELECT id FROM whitelabel_users where email='$user' AND white_label_website_id='$wlw'";
	$sql="SELECT id FROM whitelabel_users where email LIKE '%$user%' AND white_label_website_id='$wlw'";
	print_r($sql);


	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		return true;
	}
	else{
		return $sql;
		return false;
	}	
	
}

/** function for use logout **/
function logout(){
	global $base_url;
	session_destroy();
	session_unset();
	header("location:$base_url");exit;
}


/** function for sending mail **/
function sendMail($to,$subject,$msg,$name,$log,$attached_file=''){
	global $smtp,$mailsendfrom;
	$mail = new PHPMailer(); 
	$mail->SMTPDebug  = false;
	$mail->IsSMTP(); 
	$mail->SMTPAuth = true; 
	$mail->SMTPSecure = 'tls'; 
	$mail->Host = $smtp['Host'];
	$mail->Port = 587; 
	$mail->IsHTML(true);
	$mail->CharSet = 'UTF-8';
	$mail->Username = $smtp['Username'];
	$mail->Password = $smtp['Password'];
	$mail->SetFrom($mailsendfrom,'Balance');
	$mail->Subject = $subject;
	$mail->Body =$msg;
	$mail->AddAddress($to,$name);
	if($attached_file!='')
	$mail->addAttachment($attached_file);
	$mail->SMTPOptions=array('ssl'=>array(
		'verify_peer'=>false,
		'verify_peer_name'=>false,
		'allow_self_signed'=>false
	));
	$logmsg = implode(' | ',$log);
	$domain = $_SESSION['domain'];
	if($mail->Send()){
		//$msg = $logmsg. " | Site: $domain | To: $to | Subject: $subject".' | Mail sent successfully | Date '.date('d,M Y H:i A');
	}
	else {
		$msg = $logmsg. " | Site: $domain | To: $to | Subject: $subject".' | Mail not sent successfully | Date '.date('d,M Y H:i A');
		wh_log($msg);
	}
	
	
}

/** function to create log files **/
function wh_log($log_msg) {
    $log_filename = $_SERVER['DOCUMENT_ROOT']."/log";
    if (!file_exists($log_filename))
    {
        // create directory/folder uploads.
        mkdir($log_filename, 0777, true);
    }
    $log_file_data = $log_filename.'/log_' . date('d-M-Y') . '.log';
    file_put_contents($log_file_data, $log_msg . "\r\n", FILE_APPEND);
}

/** function for generating tokens **/
function generateToken(){
	$token = openssl_random_pseudo_bytes(32);
	$token = bin2hex($token);
	return $token;
}


/** function to very token for user confirmation and password reset **/
function verifyToken(& $token){
	global $conn,$base_url;
	$sql="SELECT ID,user_login,user_nicename,user_email,display_name,user_status FROM wp_users where user_activation_key='$token'";	
	$res = mysqli_query($conn,$sql);
	
	if(mysqli_num_rows($res)>0){
		$row=mysqli_fetch_array($res);
		$status = $row['user_status'];
		$user = $row['user_email'];
		if($status=='1') return false;
		else{
			$wlw = $_SESSION['white_label_website_id'];
			$sqlu="select * from whitelabel_users where email='$user' and white_label_website_id='$wlw'";
	
			$resu = mysqli_query($conn,$sqlu);
			if(mysqli_num_rows($resu)>0){
				$rowu = mysqli_fetch_array($resu);
				$wid = $rowu['id'];		
				$_SESSION['loggedIn']=$rowu['id'];
				$_SESSION['user_name']=$rowu['firstname'].' '.$rowu['lastname'];
				$_SESSION['user_contact']=$rowu['email'];
				$_SESSION['display_name']=$rowu['firstname'].' '.$rowu['lastname'];
			}			
			
			$sql="UPDATE wp_users set user_status='1' where user_activation_key	='$token'";	
			mysqli_query($conn,$sql);
			
			$sql="UPDATE whitelabel_users set status='active' where id='$wid'";	
			mysqli_query($conn,$sql);
			
			$body = '<div class="alert " role="alert">                   
                    <h1 class="text-info text-center">Welcome to BALANCE, in partnership with '.$_SESSION['title'].'</h1>

                    <p>Welcome and thank you for completing your registration with BALANCE, in partnership with '.$_SESSION['title'].'.</p>

                    <p>You\'\ve taken a step to financial fitness by being an active participant in your own financial wellness.</p>

                    <p>Your registration gives you access to more online edcation programs available on <a href="'.$base_url.'">'.$base_url.'</a> .</p>

                    <p>Use your email address and BALANCE password to log in to a program when prompted.</p>
                    <p>We hope you enjoy your experience with BALANCE, in partnership with '.$_SESSION['title'].'. We are here to help you achieve your financial success!</p>

                    <p>Start by visiting us at <a href="'.$base_url.'">'.$base_url.' </a> .</p>
                    <p>Best,</p>
                    <p>BALANCE, in partnership with '.$_SESSION['title'].'</p>
                </div>';
				$log = array('Verify Token');
			sendMail($row['user_email'],'Welcome to BALANCE, in Partnership with '.$_SESSION['title'],$body,$_SESSION['display_name'],$log);
			return true;
		}
	}
	else{
		return false;
	}
}


/** function for sneding mail for forgot password **/
function sendmailForgotPassword($email){
	global $base_url,$conn;
	$token = generateToken();
	$tmppwd = bin2hex(openssl_random_pseudo_bytes(3));
	
	$link = $base_url.'forgot_password/'.$token.'/'.$email;
	//$_SESSION['reg_link']=$link;
	$body="<p>Hello!<BR>
	You are receiving this email because we received a password reset request for your account.<BR>
	<BR><BR>
	Your temparory password is $tmppwd
	<BR><BR>
	<a href='$link'>Reset Password</a>
	<BR><BR>
	If you did not request a password reset, no further action is required.<BR>

	Regards,<BR><BR>
	<hr>

	If you're having trouble clicking the Reset Password button, copy and paste the URL below into your web browser: $link";
	
	$wlw = $_SESSION['white_label_website_id'];	
	$log = array('Forgot Password');
	sendMail($email,$_SESSION['title'].' Password Reset',$body,'',$log);
	$sql="UPDATE wp_users set user_url='$token' where user_email='$email'";	
	mysqli_query($conn,$sql);
	$sql = "UPDATE whitelabel_users SET remember_token='$tmppwd' WHERE email='$email' and white_label_website_id='$wlw'";
	mysqli_query($conn,$sql);
}


/** function for checking password **/
function checkPasswordExists($data){
	global $conn;
	$pass=trim($data['pass']);
        $cost=10; // Default cost
        $password = password_hash($pass, PASSWORD_BCRYPT, ['cost' => $cost]);
	$id=$data['id'];
	$sql="SELECT id FROM whitelabel_users where password='$password' AND id='$id'";	
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0) return 1; 
	else return 0;
		
}	

/** function to change password **/
function change_password(& $post){
	global $conn,$base_url;
	$token = trim($post['token']);
	$email = trim($post['email']);
	$pasword = trim($post['password_confirmation']);
	$cost=10; // Default cost
	$password = password_hash($pasword, PASSWORD_BCRYPT, ['cost' => $cost]);
	$tmppassword = trim($post['password']);
	$wlw = $_SESSION['white_label_website_id'];	
	
	$sql= "select * from whitelabel_users where email='$email' and remember_token='$tmppassword' and white_label_website_id='$wlw'";	
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){		
		$row = mysqli_fetch_array($res);
		
		$_SESSION['loggedIn']=$row['id'];
		$_SESSION['user_name']=$row['firstname'].' '.$row['lastname'];
		$_SESSION['user_contact']=$row['email'];
		$_SESSION['display_name']=$row['firstname'].' '.$row['lastname'];
		
		$name = $row['firstname'].' '.$row['lastname'];
		
		
		$sql="UPDATE whitelabel_users set remember_token='', password='$password' where email='$email' and white_label_website_id='$wlw'";	
		mysqli_query($conn,$sql);
		$link = $base_url.'login';
		$body="<p> Dear $name,<BR>
		Your password has been changed successfully. <BR>		
		</p>";
		$log = array('Change Password');
		sendMail($email,'BALANCE Password!',$body,$name,$log);
		return true;
	}
	
	else{
		return false;
	}
	
}

/** function to get user's iinformation **/
function getUserData(){	
	global $conn;	
	$id = $_SESSION['loggedIn'];	
	$sql="SELECT * FROM whitelabel_users where id='$id'";
	$res = mysqli_query($conn,$sql);
	return mysqli_fetch_array($res);	
}



/** function to update user information **/
function updateUser($post){		
	global $conn,$base_url;
	$firstname = trim($post['firstname']);
	$lastname = trim($post['lastname']);
	$id = $post['id'];	
	$emailid = $post['emailid'];	
	$streetAddress = trim($post['streetAddress']);
	$city = trim($post['city']);
	$state = trim($post['state']);
	$zip = trim($post['zip']);
	$memberNumber = trim($post['memberNumber']);	
	$is_employee = trim($post['is_employee']);	
	if($memberNumber=='') $memberNumber=0;
	
	$sql ="UPDATE whitelabel_users SET
	firstname='$firstname',
	lastname='$lastname',
	city='$city',
	state='$state',
	zip='$zip',
	streetAddress='$streetAddress',
	memberNumber='$memberNumber',
	is_employee='$is_employee'
	WHERE email='$emailid'";//exit;
	mysqli_query($conn,$sql);
	$_SESSION['user_name']="$firstname $lastname";
	$_SESSION['user_contact']=$emailid;
	$_SESSION['display_name']="$firstname $lastname";
	
}

/** function for update email id **/
function updateEmail($post){		
	global $conn,$base_url;
	$id = $post['id'];	
	$wlw = $_SESSION['white_label_website_id'];
	$email = trim($post['email']);
	$oldemail = $_SESSION['user_contact'];
	//$token = generateToken();
	$sqlc="select id from whitelabel_users where email='$email' and white_label_website_id='$wlw'";
	$resc = mysqli_query($conn,$sqlc);
	if(mysqli_num_rows($resc)>0){
		$_SESSION['email_msg']='This email id is already exists. Please try another.';
		header("location:$base_url".'account');die;
	}
	else{		
	$sql ="UPDATE whitelabel_users SET email='$email' WHERE id='$id'";
		mysqli_query($conn,$sql);
		//echo $sql;exit;
		session_destroy();
		session_unset();
	
	}
}

/** function to update password **/
function updatePassword($post){		
	global $conn,$base_url;
	//print_r();
	$pasword = trim($post['password']);
        $cost=10; // Default cost
        $password = password_hash($pasword, PASSWORD_BCRYPT, ['cost' => $cost]);
	$id = $post['id'];
	
	$sql="SELECT user_pass FROM wp_users where ID='$id'";	
	$sql="SELECT password FROM whitelabel_users where id='$id'";	
	$res = mysqli_query($conn,$sql);
	$row = mysqli_fetch_array($res);
	$oldpass = $row['user_pass'];
	
	if($oldpass==$password){
		return 1;
	}
	else{	
	$sql="UPDATE whitelabel_users SET password='$password' WHERE id='$id'";
	mysqli_query($conn,$sql);
	return 2;
	}
}

/** function to get home page data **/
function getHomePage($domain){
	global $conn,$base_url;
	$url=$_SERVER['HTTP_HOST'];//$domain.'.balancepro.org';
	$html = '<article aria-label="article for Quiz" class="text-block default-content-style article default-content-style quiz-header"><div class="container"><div class="row">No Data available</div></div></article>';
	$sql = "SELECT title,domain,homepage_html,resources_page_html,webinars_page_html,contact_us_page_html,white_label_website_id,logo,phone,wp_post_id FROM wp_white_label_websites WHERE domain = '$url' and status='publish' LIMIT 1";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_array($res);
		$_SESSION['domainhtml'] = str_replace('#b3481b','',stripslashes($row['homepage_html']));
		$_SESSION['webinars_page_html'] = $row['webinars_page_html'];
		$_SESSION['contact_us_page_html'] = str_replace(array('{{contact_us_form}}','#b3481b'),array('',''),$row['contact_us_page_html']);
		$_SESSION['resources_page_html'] = $row['resources_page_html'];
		$_SESSION['phone'] = $row['phone'];
		$_SESSION['title'] = $row['title'];
		$_SESSION['domain'] = $row['domain'];
		$_SESSION['logo'] = $row['logo'];
		$_SESSION['white_label_website_id'] = $row['white_label_website_id'];
		$_SESSION['wlw_wp_post_id'] = $row['wp_post_id'];
		$html = $_SESSION['domainhtml'];
	}
	$wid = $_SESSION['wlw_wp_post_id'];
	$sqlp = "SELECT meta_value FROM wp_postmeta WHERE post_id = '$wid' and meta_key='_page_edit_data' LIMIT 1";
	$resp = mysqli_query($conn,$sqlp);
	if(mysqli_num_rows($resp)>0){
		$rowp = mysqli_fetch_array($resp);
		$meta_value = unserialize($rowp['meta_value']);
		//print_r($meta_value);
		if(isset($meta_value['wlw_general_info_module'][0]['promotion_logo']['image']['fullpath'])){
			//echo'promo_logo';
			$_SESSION['promo_logo'] = $meta_value['wlw_general_info_module'][0]['promotion_logo']['image']['fullpath'];			
			$_SESSION['promo_logo_name'] = $meta_value['wlw_general_info_module'][0]['promotion_logo']['image']['filename'];						
			$_SESSION['promotion_logo_url'] = $meta_value['wlw_general_info_module'][0]['promotion_logo_url'];		
		}
		$_SESSION['accesschat'] = $meta_value['wlw_general_info_module'][0]['disable_chat'];
	}
	$wlw_id = $_SESSION['white_label_website_id'];
	$sqla = "SELECT * FROM `whitelabel_menu_access` WHERE wlw_id='$wlw_id'";
	$resa = mysqli_query($conn,$sqla);
	if(mysqli_num_rows($resa)>0){
		$rowa = mysqli_fetch_array($resa);
		$_SESSION['accesstoresources'] = $rowa['resources_access'];
		$_SESSION['resources_menu'] = $rowa['resources'];
		$_SESSION['accesstocontact'] = $rowa['contact_access'];
		$_SESSION['contact_menu'] = $rowa['contact'];
		$_SESSION['accesstoprograms'] = $rowa['programs_access'];
		$_SESSION['programs_menu'] = $rowa['programs'];
		$_SESSION['accesstowebinars'] = $rowa['webinars_access'];
		$_SESSION['webinars_menu'] = $rowa['webinars'];		
	}	
	
	return $html;
}


/** Function for menu name and access **/

function getMenuAccess(){
	global $conn;
	$sql = "";
}



/** function to check if site is published or not **/
function checkSitePublish($domain){
	global $conn,$base_url;
	$url=$_SERVER['HTTP_HOST'];
	$sql = "SELECT white_label_website_id FROM wp_white_label_websites WHERE domain = '$url' and status='publish' LIMIT 1";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		return true;
	}
	else{
		return false;
	}	
}

/** function to submit quiz **/
function submitQuiz(){
	global $conn,$mailsendfrom,$adminmail,$superadminmail,$base_url;
	$post = $_POST;
	$quizid = $post['quizid'];
	$wlw = $post['wlw'];
	
	$newArr = getQuizQuestions($quizid);
	$max_tries = $newArr['max_tries'];
	$cooldown = $newArr['cooldown'];
	$success_rate = $newArr['success_rate'];
	$certificate_title = $newArr['certificate_title'];
	$email_subject = $newArr['email_subject'];
	$email_body = $newArr['email_body'];
	$message_success = $newArr['message_success'];
	$has_certificate = $newArr['has_certificate'];
	$post_title = $newArr['post_title'];
	
	$wp_user_id = $_SESSION['loggedIn'];
	$white_label_website_id = $_SESSION['white_label_website_id'];
	
	unset($post['wlw']);
	unset($post['action']);
	unset($post['quizid']);
	unset($post['max_tries']);
	unset($post['cooldown']);
	
	
	
	$newpost = array_values($post);
	
	$cntans = count($newpost);
	$cntsess = count($newArr['correct']);
	$sess = $newArr['correct'];
	$correct_val=0;
	for($s=0;$s<$cntsess;$s++){
		if(isset($post['answer-'.$s])){
			if($sess[$s]==$post['answer-'.$s]) {
				$correct_val=$correct_val+1;
			}	
		}		
	}
	$perscore = ($correct_val*100)/$cntsess;
	$chk = chkIntervalForQuiz($quizid);

	if($chk=='insert'){
		$answer = addslashes(json_encode($post));
		$sql="INSERT INTO `wp_quiz_results` SET 
		score='$perscore',
		num_correct='$correct_val',
		answers_data='$answer',
		wp_user_id='$wp_user_id',
		wp_post_id ='$quizid',
		mssql_id ='1234',
		timestamp=now(),
		transferred ='1',
		wlwid ='$white_label_website_id'";

		if(mysqli_query($conn,$sql)){
			$last_id = mysqli_insert_id($conn);	
			$success_rate=80;
			if($perscore>=$success_rate){
				$display_name = ucwords($_SESSION['display_name']);
				$post_titlex = explode(':',$post_title);
				if(isset($post_titlex[1]) && trim($post_titlex[1])!='')
				$sectitle = $post_titlex[1];
				else $sectitle ='&nbsp;';
				$html=file_get_contents('balance.html');		
				$date = date('F d, Y',strtotime(date('Y-m-d')));
				$html = str_replace(array('%date%','%user%','%post_title1%','%post_title2%'),array($date,$display_name,$post_titlex[0],$sectitle),$html);
				
				$filenme = 'cert_'.$wp_user_id.'-'.$quizid.time().'.pdf';
				$attached_file = 'uploads/'.$filenme;
				
				createPDF($html,$attached_file);
				$sqlc="INSERT INTO wp_quiz_certificates SET quiz_result_id='$last_id',crtf_name='$filenme'";
				mysqli_query($conn,$sqlc);
				
				$to = $_SESSION['user_contact'];
				$data = $newArr['email_body'];
				$data .= " Your score was ".$perscore."% ($correct_val/$cntsess)";
				$email_subject=$post_title;
				$adminbody = "<p>$display_name has completed the quiz successfully.<BR>";
				$adminbody .= "The score was ".$perscore."% ($correct_val/$cntsess)";
				$adminbody .= "<BR><BR>Website : ".$_SESSION['title'].'<BR>';
				$adminbody .= "<BR><BR>Thanks<BR>";
				$adminbody .= "Balancepro Team<BR>";
				$log = array('Quiz',$display_name,'Score '.$perscore.'%');
				sendMail($to,$email_subject,$data,'',$log,$attached_file);
				if(isset($_SESSION['siteadmin'])){
					for($m=0;$m<count($_SESSION['siteadmin']); $m++){
						$smailid = $_SESSION['siteadmin'][$m];
						//sendMail($smailid,$email_subject,$adminbody,'',$log,$attached_file);
					}
				}
				//sendMail($superadminmail,$email_subject,$adminbody,'',$log,$attached_file);
			}
		}
	}

	return $last_id;
}

/** function to check interval of 24 hours for vacu.balancepro.org **/
function chkIntervalForQuiz($quizid){
	global $conn;
	$chk='insert';
	$wp_user_id = $_SESSION['loggedIn'];
	$white_label_website_id = $_SESSION['white_label_website_id'];
	if($white_label_website_id=='456'){
		$sql = "SELECT ID FROM wp_quiz_results WHERE wp_user_id='$wp_user_id' AND wp_post_id='$quizid' AND timestamp > NOW() - INTERVAL 24 HOUR ORDER BY ID DESC";
		$res = mysqli_query($conn,$sql);
		$numrows = mysqli_num_rows($res);
		if($numrows>0){
			$chk='noinsert';
		}		
	}
	return $chk;	
}


/** function to get result of quiz ***/
function getLatestQuizResult(){
	global $conn,$base_url;
	$cond='';
	$userid = $_SESSION['loggedIn'];
	$wlwid = $_SESSION['white_label_website_id'];
	if(isset($_GET['rid']) && $_GET['rid']!=''){
		$rid = $_GET['rid'];
		$cond=" AND ID='$rid'";
	}
	$sql = "SELECT * FROM wp_quiz_results WHERE wp_user_id='$userid' AND wlwid='$wlwid' $cond ORDER BY ID DESC LIMIT 1";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		return mysqli_fetch_object($res);		
	}
	return '';	
}

/** function to get all past quizes  ***/
function getPastQuizes(){
	global $conn,$base_url;
	$userid = $_SESSION['loggedIn'];
	$wid = $_SESSION['white_label_website_id'];
	$sql = "SELECT r.*,crtf_name FROM wp_quiz_results r, wp_quiz_certificates c WHERE quiz_result_id=r.ID AND wp_user_id='$userid' AND wlwid='$wid' AND timestamp > NOW() - INTERVAL 90 DAY ORDER BY ID DESC";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		return mysqli_fetch_all($res,MYSQLI_ASSOC);		
	}
	return '';	
}

/** function to send contact mail to user, site admin and wordpress admin ***/
function sendmailContact(){
	global $base_url,$mailsendfrom,$superadminmail,$adminmail;
	$from = $_POST['email']; // this is the sender's Email address
	$request_type = $_POST['request_type'];
	$primary_concern = $_POST['primary_concern'];
	$secondary_concern = $_POST['secondary_concern'];
	$name = $_POST['name'];
	$email = $_POST['email'];
	$messagep= $_POST['message'];
	
	$name = ($name!='') ? $name : 'Name not captured';
	
	$primary_concern = ($primary_concern!='') ? ucfirst(str_replace('-',' ',$primary_concern)) : 'None stated';
	
	$secondary_concern = ($secondary_concern!='') ? ucfirst(str_replace('-',' ',$secondary_concern)) : 'None stated';
	
	$email = ($email!='') ? $email : 'Email not captured';
	$messagep = ($messagep!='') ? $messagep : 'Email not captured';
	
	$message = "<style>th, td {padding: 0px;font-family: Arial, Helvetica, sans-serif;height:25px;}</style>";
	$message .= '<table>';
	$message .= "<tr><th style='font-size:25px'>".$_SESSION['title']."</th></tr>";
	$message .= "<tr><td>&nbsp;</td></tr>";
	if(isset($_POST['request_type']) && $_POST['request_type']=='counselor')
	$message .="<tr><td><b>Request For Counselor</b></td></tr>";
	else 
	$message .="<tr><td><b>General Inquiry</b></td></tr>";
	$message .= "<tr><td>&nbsp;</td></tr>";
	
	$message .= "<tr><td><b>Message from</b></td></tr>";
	
	$message .= "<tr><td>$name</td></tr>";
	$message .= "<tr><td>&nbsp;</td></tr>";
	$message .= "<tr><td><b>Sender email</b></td></tr>";
	$message .= "<tr><td>$email</td></tr>";
	$message .= "<tr><td>&nbsp;</td></tr>";
	$message .= "<tr><td><b>Primary Concern</b></td></tr>";
	$message .= "<tr><td>$primary_concern</td></tr>";
	$message .= "<tr><td>&nbsp;</td></tr>";
	$message .= "<tr><td><b>Secondary Concern</b></td></tr>";
	$message .= "<tr><td>$secondary_concern</td></tr>";
	$message .= "<tr><td>&nbsp;</td></tr>";
	$message .= "<tr><td><b>Message</b></td></tr>";
	$message .= "<tr><td>$messagep</td></tr>";	
	$message .= "</table>";
	
	$sendermessage="Dear $name,<BR>your contact details has been submitted, our team will get back to you as soon as possible.<BR><BR>Thanks,<BR>Team BalancePro<BR>";
	$log = array('Contact',$name);
	if(isset($_SESSION['siteadmin'])){
		for($m=0;$m<count($_SESSION['siteadmin']); $m++){
			$smailid = $_SESSION['siteadmin'][$m];
			//sendMail($smailid,'Thanks for contacting us!', $message,'',$log);
		}
	}	
	//sendMail($adminmail,'Thanks for contacting us!', $message,'',$log);
	//sendMail($superadminmail,'Thanks for contacting us!', $message,'',$log);
	sendMail($email,'BalancePro Contact', $sendermessage,'',$log);

	$_SESSION['message']="Your form is submitted successfully!";
	header("location:$base_url".'contact');
	die();	
}

/** function to get quiz questions **/
function getQuizQuestions($postid){
	global $conn,$base_url;	
	$getQuestionDataQuery = mysqli_query($conn,"SELECT * FROM `wp_postmeta` WHERE `post_id`='$postid' AND `meta_key`='_page_edit_data'");

	$questionData = mysqli_fetch_array($getQuestionDataQuery);
	$data = unserialize($questionData[3]);
	
	
	$newArr=array();
	preg_match_all('/".*?"/', $questionData[3], $matches);	
	foreach($matches[0] as $key => $value){
	  $newArr["question"] = $newArr["question"]+1;
	  $newArr["answer"] = $newArr["answer"]+1;	  
	  if($value == "question"){
		$newarrays[$value] = $value+1;
		if($value == "answer"){
		  $newarrays[$value] = $value+1;
		}
	  }
	}
	$match = $matches[0];
	$newArr=array();
	$i=0;
	$j=0;
	$m=0;
	
	foreach($match as $key => $value){
		$newval = str_replace('"','',$value);
		if($newval=='question'){
			$newArr['question'][$i]=$match[$key+1];
			$k=$i;		
			$i++;	
		}
		if($newval=='answer'){		
			$newArr['answer'][$k][]=$match[$key+1];
			$cans =  str_replace('"','',$match[$key+2]);		 
			if($cans=='is_correct'){			
				$g=$k;
				$newArr['correct'][]=count($newArr['answer'][$k])-1;
			}		
		}
		
			
		$j++;
	}
	$newArr['copy']=$data['m34_module'][0]['copy'];
	$newArr['max_tries'] = $data['quiz_module'][0]['max_tries'];
	$newArr['cooldown'] = $data['quiz_module'][0]['cooldown'];
	$newArr['success_rate'] = $data['quiz_module'][0]['success_rate'];
	$newArr['certificate_title'] = $data['quiz_module'][0]['certificate_title'];
	$newArr['email_subject'] = $data['quiz_module'][0]['email_subject'];
	$newArr['email_body'] = $data['quiz_module'][0]['email_body'];
	$newArr['message_success'] = $data['quiz_module'][0]['message_success'];
	$newArr['post_title'] = $data['post_title'];
	$newArr['message_fail'] = $data['quiz_module'][0]['message_fail'];
	$newArr['has_certificate'] = $data['quiz_module'][0]['has_certificate'];
	//echo'<pre>',print_r($data),'</pre>';
	return $newArr;	
}

/** function to create PDF file for quiz ***/
function createPDF($html,$filename){
	require('pdf/vendor/autoload.php');
	$mpdf=new \Mpdf\Mpdf();
	$mpdf->WriteHTML($html);
	$mpdf->output($filename,'F');	
}



/** function to get super admin email id and site admin email id ***/
function getAdminEmailIds(){
	global $conn;
	$wlw = $_SESSION['white_label_website_id'];
	$sql="SELECT email FROM whitelabel_admins WHERE white_label_website_id='$wlw'";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		
		while($row = mysqli_fetch_array($res)){
			$_SESSION['siteadmin'][] = $row['email'];
		}
	}
	$sql = "SELECT option_value FROM `wp_options` WHERE option_name='admin_email'";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_array($res);
		$_SESSION['superadmin'] = $row['option_value'];
	}	
}

/** function to fetch programs ***/
function getPrograms(){
	global $conn;
	$postid = $_SESSION['wlw_wp_post_id'];
	$getQuestionDataQuery = mysqli_query($conn,"SELECT * FROM `wp_postmeta` WHERE `post_id`='$postid' AND `meta_key`='_page_edit_data'");
	$questionData = mysqli_fetch_array($getQuestionDataQuery);
	$str = 	(substr($questionData[3],5));
	$res = unserialize($questionData[3]);
	$ids = $res['wlw_programs_module'][0]['programs'];
	$sql="select * from wp_posts where post_status='publish' and post_type='program' and ID in ($ids) order by menu_order asc";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_all($res,MYSQLI_ASSOC);
		
	}
	return $row;
}

/** function to get program image ***/
function getProgramFeaturedImage($programId){
	global $conn; 
	$sql="select TN.* from wp_postmeta as PM left join wp_postmeta as TN on TN.post_id=PM.meta_value where PM.meta_key='_thumbnail_id' and PM.post_id='$programId'";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		return $row = mysqli_fetch_all($res,MYSQLI_ASSOC);
	}
}


/** functions for program page **/
function getProgram($id) {
	global $conn; 
	 
	$sql="select * from wp_resources as P join wp_postmeta as PM on PM.post_id=P.wp_post_id where status='publish' and PM.meta_key='_page_edit_data' and P.type='program' and P.slug='$id'";//exit;
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_all($res,MYSQLI_ASSOC);
		$program = setProgramModuleData($row[0]);
		///echo'<pre>',print_r($program['level_of_access']);exit;
		$checkAuth = checkAuthProgram($program);
        if ( $checkAuth ) {
            return $checkAuth;
        }
		
		$template = determineProgramTemplate($program['m56bModule']['program_type'],$program['m56bModule']['link_to_old_site']);
		//exit;
		list($hero, $html, $programResources) = generateProgramTemplateData($template, $program, 1);
	}
	return $hero.'<br>'.$html;
	
}

/** functions for program templates **/
function generateProgramTemplateData($template, $program, $pageNum)  {
	//print_r($program);
	$programResources = '';
	$heroButton = createProgramHeroButton($program['m1Module']);	
	$heroBackgroundImage = $program['m1Module']['bgimage']['image']['fullpath'];
	$heroTitle = $program['m1Module']['title'];
	$heroCopy = $program['m1Module']['copy'];
	$hero = '<div style="background-image: url('.$heroBackgroundImage.');" class="banner banner-hero text-center">
    <div class="container">
        <div class="row">
            <div class="banner-block short">
                <div class="banner-text">
                    <h1>'.$heroTitle.'</h1>
                    <p>'.$heroCopy.'</p>'.$heroButton .'
                </div>
            </div>
        </div>
    </div>
	</div>';
	
	if ($template === 'T26') {
		$html = $program['html'];
	} else {
		$programResources = getProgramPageInfo($program['m56bModule'][$program['m56bModule']['program_type']], $program['slug'], $pageNum);
		$html = getProgramResource($programResources['resource_id'], $programResources['page']);
	}
	return [$hero, $html, $programResources];
	
}

/** functions for program module data **/
function setProgramModuleData($program) {
	$meta = unserialize($program['meta_value']);
	$program['meta']= $meta;
	$program['m56bModule'] = $meta['m56b_module'][0];
	$program['m1Module'] = $meta['m1_module'][0];
	unset($program['meta_key'], $program['meta_value']);
	return $program;
}

/** functions to check program template page **/
function determineProgramTemplate($m56bModule, $redirectLocation) {
	global $base_url;
	switch ($m56bModule) {
		case 'link_to_old_site':			
			header("Location: $redirectLocation");exit;
			break;
		case 'program_resources':
			return 'T26';
			break;
		case 'resource_as_program':
		default:
			return 'T27';
			break;
	}
}

/** functions for program page information **/
function getProgramPageInfo($id, $programSlug, $currentPage=1)   {
	list($postId, $programResources) = getProgramsResourcesData($id);
	$pageInfo = generateProgramPageInfo($currentPage, $programSlug, $programResources, $postId);
	return $pageInfo;
}

/** functions for program resource data **/
function getProgramsResourcesData($id) {
	global $conn;
	if (is_array($id)) {
		$id = $id[0]['list'];
	}
	
	$sql = "SELECT title, post_title, wp_post_id, status, count('wp_post_id') as pages FROM wp_resources WHERE wp_post_id IN ($id) AND status = 'publish' GROUP BY wp_post_id ORDER BY field(wp_post_id, $id)";
	
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_all($res,MYSQLI_ASSOC);
	}
	return ['wp_post_id', $row];
}

/** functions for program page information **/
function generateProgramPageInfo($currentPage, $programSlug, $programResources, $postId) {
	$pageInfo = ['page' => 0, 'total_pages' => 0, 'resource_id' => 0, 'chapters' => []];

	foreach ($programResources as $idx => $resource) {
		$prev = $pageInfo['total_pages'];
		$pageInfo['total_pages'] += $resource->pages;
		$chapter = ['title' => $resource->post_title, 'slug' => createProgramUrl($programSlug, (int)$prev + 1), 'selected' => false];
		if ($pageInfo['total_pages'] >= $currentPage && empty($pageInfo['page'])) {
			$pageInfo['page'] = $currentPage - $prev;
			$pageInfo['resource_id'] = $resource->$postId;
			$chapter['selected'] = 'selected';
		}
		$pageInfo['chapters'][] = $chapter;
	}	

	return $pageInfo;
}

/** functions for program button **/
function createProgramHeroButton($m1Module) {
	$return = '';
	if (!empty($m1Module['buttontext'])) {
		$return .= !empty($m1Module['linkfield']) ? sprintf($this->build_link($m1Module['linkfield']), 'btn btn-default', $m1Module['buttontext']) : '';
	}
	return $return;
}

/** functions for program page url **/
function createProgramUrl($programSlug, $pageNum)  {
    return '/programs/' . $programSlug . '/page/'.$pageNum;
}

/** functions for program page resource **/
function getProgramResource($resourceId, $resourcePage)  {
	global $conn;
	
	$sql = "SELECT * FROM wp_resources WHERE wp_post_id = '$resourceId' AND status = 'publish' AND page_order = '$resourcePage' LIMIT 1";
	
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_all($res,MYSQLI_ASSOC);
		return $row[0]['html'];
	}	
}

/** functions to generate resource url **/
function resourcesUrl($redirectLocation){
	global $base_url;
	$new = str_replace('/resources/','',$redirectLocation);
	$newx = explode('/',$new);
	$id = str_replace('/','',$newx[1]);	
	$type=$newx[0];
	$newsite=$base_url.'index.php?action=resources1&type='.$type.'&id='.$id;
	return $newsite;
}

?>
