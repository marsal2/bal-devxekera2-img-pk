<?php
	$data = $dataq;
	$newArr = getQuizQuestions($data->wp_post_id);
	
?> 
	
	<main id="main">
        <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                        <!-- Link to home -->
                        <li><a href="<?=$base_url?>">Home</a></li>
                        <!-- Link to resource page -->
                        <li>Quizzes</li>
                        <li><?=$newArr['post_title']?></li>
                    </ol>
                </div>
            </div>
        </div>
     
        <article aria-label="article for Quiz" class="text-block default-content-style article default-content-style quiz-header" style="display: none;">
            <div class="container">
                <div class="row">
                    <div class="">
                        <h1 class="text-info text-center">Quiz</h1>
                        <p><span style="line-height: 30px;">Take the quiz to check your understanding of this&nbsp;module. Submit your answers to view your results.&nbsp;Your results will be shared with your&nbsp;referring organization.</span></p>
                    </div>
                </div>
            </div>
        </article>

		
		<?php 
		if(!empty($data)){
			$total = round((100*$data->num_correct)/$data->score);
			$ansd = (array)json_decode(str_replace('answer-','',$data->answers_data));
			//Correct answers
			$correctans = $newArr['correct'];
			?>
        <article name="balance_article" class="quiz-results text-block article background-white default-content-style" style="" aria-label="article module print score">
            <div class="container">
                <div class="row">
                    <h1 class="text-info text-center">Your Results</h1>
                    <p>Your score was <b class="quiz-percent"><?php echo round($data->score,2)?> %</b> <?php if($data->num_correct>0){?>(<span class="quiz-number-of-correct"><?=$data->num_correct?></span>/<span class="quiz-number-of-questions"><?=$total?></span>).<?php } ?></p>
					<?php if($data->score<$_SESSION['passing_percent']){?>
                    <p><span style="line-height: 25.7143px;">You need to score at least <?=$_SESSION['passing_percent']?>% on the quiz to successfully complete this module and receive a Certificate of Completion. Please review the module and try the quiz again.</span></p>
					<?php } else {?>
					 <p><span style="line-height: 25.7143px;"><?php //$newArr['message_success']?>
                            Congratulations! you successfully completed this module, please check certificate in your email or <a href='<?=$base_url?>pastcertificate'>'Past Certificate under'</a> Accounts</span></p>
					<?php } 
					//if($chkFlag=='insert'){
					?>
                    <div class="btn-holder clearfix" style="margin-bottom: 30px;"><a href="#" onclick="GoBackWithRefresh();return false;" class="btn btn-warning pull-left quiz-retry">Retry quiz</a></div>
					<?php //} ?>
                </div>
            </div>
        </article>
            <?php if($_SESSION['show_quiz_results'] == 'Yes'){ ?>
		        <section aria-label="quiz questions" class="quiz-questions modules-form-section">
            <div class="container">
                <div class="row">
                    <form id="quiz-form" method="post" class="form-wrap">
					<input type='hidden' name='action' value='submit_query'>
                        <div class="quiz-questions">
                            <ol class="questions">
							<?php for($i=0;$i<count($newArr['question']);$i++){
								$question = $newArr['question'][$i];
								$answers = $newArr['answer'][$i];
                                $src= $base_url.'assets/img/wrong.png';
                                $img = "<img src='$src' style='width:25px;' />";
                                if(isset($ansd[$i])){
                                        if($ansd[$i] == $newArr['correct'][$i]){
                                        $src= $base_url.'assets/img/green.png';
                                        $img = "<img src='$src' style='width:25px;'/>";
                                    }
                                    else {
                                        $src= $base_url.'assets/img/wrong.png';
                                        $img = "<img src='$src' style='width:25px;' />";
                                    }
                                }
                                else{

                                }

								?>
                                <li><strong class="h3"><?php echo str_replace('"','',$question); ?><span><?php echo $img;?></span></strong>
                                    
                                    <ul class="choose-list">
									<?php
									    for($b=0;$b<count($answers);$b++){
                                            if(isset($ansd[$i]) && $ansd[$i] == $b) {
                                                $checked='checked';
                                                if($ansd[$i]==$newArr['correct'][$i]){
                                                    $icon='icon-check';
                                                }
                                                else{
                                                    $icon='icon-close';
                                                }
                                                $istyle="margin-left: -30px;";
                                            }else {
                                                $checked='';
                                                $icon='';
                                                $istyle='';
                                            }
//                                            if(isset($ansd[$i])){
//                                                if($ansd[$i] == $newArr['correct'][$i]){
//                                                    $src= $base_url.'assets/img/green.png';
//                                                    $img = "<img src='$src' style='width:40px;padding-left:10px;'/>";
//                                                }
//                                                else {
//                                                    $src= $base_url.'assets/img/wrong.png';
//                                                    $img = "<img src='$src' style='width:40px;padding-left:10px;' />";
//                                                }
//                                            }
//                                            else{
//                                                $img = "";
//                                            }
									?>
                                        <li>
										  <label>
                                              <!--span class="<?=$icon?>" style="<?=$istyle?>"></span-->
                                              <input type="radio" name="answer-<?=$i?>" value="<?=$b?>">
                                              <span class="fake-input"></span>
                                              <span class="fake-label" style="margin-left: 10px;"><?php echo str_replace('"','',$answers[$b]); ?> </span>

                                          </label>
                                        </li>                  
									<?php } ?>                                        
                                    </ul>
                                </li>
                                <?php } ?>
                            </ol>
                           
                        </div>
                    </form>
                </div> 
            </div>
        </section>
            <?php }?>
		<?php } ?>
    </main>
	<script>
	$(':radio,:checkbox').click(function(){
    return false;
});

    function GoBackWithRefresh(event) {
        if ('referrer' in document) {
            window.location = document.referrer;
            /* OR */
            //location.replace(document.referrer);
        } else {
            window.history.back();
        }
    }
	</script>
   