<main id="main" role="main">
    <style>
    .report-block img {
        height: 315px;
        width: auto;
    }

    .report-block .image-holder,
    .report-block .copy-holder {
        width: 50%;
    }

    .report-block .copy-holder {
        position: relative;
        padding: 0 38px;
        text-align: left;
    }

    @media  screen and (max-width: 1024px) {
        .report-block .image-holder {
            width: 100%;
        }

        .report-block .image-holder img {
            width: 100%;
            height: auto !important;
        }

        .report-block .copy-holder {
            width: 100%;
            padding-top: 25px;
            padding-bottom: 25px;
            top: 0;

            -webkit-transform: translateY(0%);
            -ms-transform: translateY(0%);
            transform: translateY(0%);
        }
    }
</style>

   
	<?php
	
	echo $_SESSION['webinars_page_html'];
	
	
	?>
	
	
</main>