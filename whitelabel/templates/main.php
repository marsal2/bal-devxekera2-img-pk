<?php 
//$style_pop='display:none';
//$_SESSION['firstlogin']=1;
if(isset($_SESSION['firstlogin']) && $_SESSION['firstlogin']==1){
	 //$style_pop='';
?>
<script>
$(document).ready(function(){
 $("#modalThank").modal();   
});
</script>
<?php	 
}
?>

<main id="main" role="main">
<?php
if(isset($_SESSION['firstlogin_err']) && $_SESSION['firstlogin_err']!=''){
?>
<div class="alert alert-warning">
  <span class="closebtn">&times;</span>  
  <?php echo $_SESSION['firstlogin_err']?>
</div>

<?php
}
if(isset($_SESSION['message']) && $_SESSION['message']!=''){
?>
	<div class="alert alert-success">
  <span class="closebtn">&times;</span>  
  <?php echo $_SESSION['message']?>
	</div>
<?php
}	
unset($_SESSION['message']);
unset($_SESSION['firstlogin']);
unset($_SESSION['firstlogin_err']);
?>
<div id="modalThank" tabindex="-1" role="dialog" class="modal fade">
	<div  class="modal-dialog">
		<div class="modal-content">
			 <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">X</span></button>    <!-- &#215; -->
			<div class="alert " role="alert">  
				<?php 
				if(isset($_SESSION['thankYou_text']) && $_SESSION['thankYou_text'] !=''){
					echo str_replace(array('%site_url%','%site_title%'),array($base_url,$_SESSION['title']),$_SESSION['thankYou_text']);
				} else {
				?>
				<h1 class="text-info text-center">Thank you for verifying your email address</h1>
				<p>Thank you for completing your registration with BALANCE, in partnership with <span class="text-info"><b><?=$_SESSION['title']?></b></span>. You’ve taken a step to financial fitness by being an active participant in your own financial wellness.</p>
				<p>Your registration gives you access to more BALANCE online education programs available on this website. Use your email address and BALANCE password to log in to a program when prompted.</p>
				<p>We hope you enjoy your experience with BALANCE, in partnership with <span class="text-info"><b><?=$_SESSION['title']?></b></span>. We are here to help you achieve your financial success.</p>
				<p>Best,</p>
				<p>BALANCE in partnership with <span class="text-info"><b><?=$_SESSION['title']?></b></span></p>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php
echo removeResourseUrl($mainhtml);
//echo $mainhtml;
//echo removeProgramUrl($mainhtml);

?>
</main>
