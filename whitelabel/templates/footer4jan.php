<footer class="footer">
        <div class="container">
            <div class="row">
                <div class="logo-holder">
                    <a href="<?=$base_url?>"><img src="<?=$base_url?>assets/img/logo-primary.svg" width="198" height="44" alt="Advantage Home Plus
"></a>
                </div>
                <ul class="menu-f">
				 <?php if(isset($_SESSION['accesstoresources']) && trim($_SESSION['accesstoresources'])!=''){ ?>
                    <li><a href="<?=$base_url?>index.php?action=resources" title='RESOURCES'>RESOURCES</a></li>
					<?php } ?>
					
					<?php if(isset($_SESSION['accesstoprograms']) && trim($_SESSION['accesstoprograms'])!=''){ ?>
                    <li><a href="<?=$base_url?>programs" title='PROGRAMS'>PROGRAMS</a></li>
					<?php } ?>
					<?php if(isset($_SESSION['webinars_page_html']) && trim($_SESSION['webinars_page_html'])!=''){ ?>
							
							 <li>
                                <a href="<?=$base_url?>index.php?action=webinars" title='Webinars'>Webinars</a>
                            </li>
							<?php } ?>
                    <li><a href="<?=$base_url?>contact" title='CONTACT US'>CONTACT US</a></li>
                </ul>
                <ul class="social-links">
                    <li><a href="https://www.facebook.com/BALANCEFinFit/"><span class="icon-facebook"></span></a></li>
                    <li><a href="https://twitter.com/BAL_Pro"><span class="icon-twitter"></span></a></li>
                </ul>
                <div class="copyright-footer">
                    &copy; <?=date('Y')?> BALANCE. All rights reserved.
                </div>
            </div>
        </div>
    </footer>