 <!-- contain main informative part of the site -->
 <?php
 
 $name = explode(' ',$data['user_login']);
 ?>
 <main id="main" role="main">
    <div class="container">
    <div class="row">
        <div class="main-tab tab-holder col-sm-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><span class="icon-user"></span>My Account</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
			
                <div role="tabpanel" class="tab-panel active" id="tab1">
                   
                    <form class="login-form" role="form" method="POST" action="index.php?action=update_user" autocomplete='off' onsubmit='return validChnageUser()'>
					<?php if(isset($_SESSION['acc_msg']) && $_SESSION['acc_msg']!=''){?>
					<div class="alert alert-success">
						<?php echo $_SESSION['acc_msg']?>
					</div>
					<?php 
					unset($_SESSION['acc_msg']);
					} ?>
					<input type='hidden' name='id' value="<?=$data['id']?>">
					<input type='hidden' name='emailid' value="<?=$data['email']?>">

					<div class="input-group">
						<input type="text" id="firstname" name="firstname" placeholder="First Name" value="<?=$data['firstname']?>" onfocusout="if(this.value!='')document.getElementById('ulastmsg').html=''"><span id='ufirstmsg'></span>
					</div>
					<div class="input-group">
						<input type="text" id="lastname" name="lastname" placeholder="Last Name" value="<?=$data['lastname']?>" ><span id='ulastmsg'></span>
					</div>
					<div class="input-group">
						<input type="text" id="streetAddress" name="streetAddress" placeholder="Street Address" value="<?=$data['streetAddress']?>">
					</div>
					<div class="input-group">
						<input type="text" id="city" name="city" placeholder="City" value="<?=$data['city']?>">
					</div>
					<div class="input-group">
						<input type="text" id="state" name="state" placeholder="State" value="<?=$data['state']?>">
					</div>
					<div class="input-group">
						<input type="text" id="zip" name="zip" placeholder="Zip Code" value="<?=$data['zip']?>" maxlength=5 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"><span id='uzipmsg'></span>
					</div>

						<div class="input-group">
						<input type="text" id="memberNumber" name="memberNumber" placeholder="Last four digits of your account/member number" value="<?=$data['memberNumber']?>" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
					</div>
						
					<div class="input-group">
						<select name = "is_employee" id = "is_employee">
							<option value = "0" <?php if($data['is_employee']==0) echo'selected'?>>Member/client</option>
							<option value = "1" <?php if($data['is_employee']==1) echo'selected'?>>Employee</option>
						</select>
					</div>
					<div class="input-group">
						<div class="col-md-4 col-md-offset-4">
							<button type="submit" class="btn but btn-warning">
								Update User
							</button>
						</div>
					</div>
					</form>
                    <form class="login-form" role="form" method="POST" action="index.php?action=update_email" onsubmit="return validEmail('email','emailmsg')" autocomplete='off'>
					<?php if(isset($_SESSION['email_msg']) && $_SESSION['email_msg']!=''){?>
					<div class="alert alert-success">
						<?php echo $_SESSION['email_msg']?>
					</div>
					<?php 
					unset($_SESSION['email_msg']);
					} ?>
					<input type='hidden' name='id' value="<?=$data['id']?>">
					<div class="input-group">
						<input type="Email" id="email" name="email" placeholder="Email" value="<?=$data['email']?>" onfocusout="validEmail('email','emailmsg')">
						<span id='emailmsg'></span>
						<BR><span class='pmsg'>[Disclaimer: Changing email address causes logout to re-login.]</span>
						
					</div>
					
					<div class="input-group">
						<div class="col-md-4 col-md-offset-4">
							<button type="submit" class="btn but btn-warning">
								Update Email
							</button>
						</div>
					</div>
					</form>
					 <form class="login-form" role="form" method="POST" onsubmit="return changePassword()">
					 <input type='hidden' name='action' value='update_pass'> 
					 <?php if(isset($_SESSION['pass_msg']) && $_SESSION['pass_msg']!=''){?>
					<div class="alert alert-success">
						<?php echo $_SESSION['pass_msg']?>
					</div>
					<?php 
					unset($_SESSION['pass_msg']);
					} 
					
					?>
					<input type='hidden' name='id' value="<?=$data['id']?>">
					<input type='hidden' id='isloggedin' value="<?=$data['id']?>">
					<div class="input-group">
						<input type="password" id="password" name="password" placeholder="Password" onfocusout="checkPassExists(<?=$data['ID']?>,this.value)"><span id='passwordmsg'></span>
					</div>
					<div class="input-group">
						<input type="password" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password" onfocusout="cvalidPass(this,'password')" >
						<span id='password_confirmationmsg'></span>
					</div>

					<div class="form-group">
						<div class="col-md-4 col-md-offset-4">
							<button type="submit" class="btn but btn-warning">
								Update Password
							</button>
						</div>
					</div>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>
</main>