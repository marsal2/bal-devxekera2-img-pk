<main id="main" role="main">
        <div style="background-image: url('https://balancepro.s3-us-west-2.amazonaws.com/wp-content/uploads/26192854/T26-1400x415.png');" class="banner banner-hero text-center">
			<div class="container">
				<div class="row">
					<div class="banner-block short">
						<div class="banner-text">
							<h1>BalanceTrack</h1>
							<p>Personal Finance Education Center</p>
							
						</div>
					</div>
				</div>
			</div>
		</div>

    <article class="text-block article default-content-style" aria-label="m38 article module ">
      <div class="container">
        <div class="row">
          <p style="padding: 54px 0 36px !important;">Welcome to BalanceTrack, a personal finance education program brought to you by your financial institution. These modules will guide you through core aspects of personal finance management. Completing these modules will help you move toward sound money management and financial success. Click on a topic below to get started!</p>

        </div>
      </div>
    </article>
        <div aria-label="Financial Basics" class="border-divider"></div>
        <section aria-label="Financial Basics" class="inner-block same-height-holder">
          <div class="container">
            <div class="row">
              <h1 class="text-center text-info">Financial Basics</h1>
              <div class="col-holder">
                
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: High-Cost Financial Services Section:Financial Basics" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: High-Cost Financial Services" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154759/T26-icon-121x121-HomeOwnership-2.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: High-Cost Financial Services</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1368" href="/resources/articles/balancetrack-high-cost-financial-services/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: The Psychology of Spending Section:Financial Basics" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: The Psychology of Spending" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154758/T26-icon-121x121-FinancialBasics-6.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: The Psychology of Spending</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1362" href="/resources/articles/the-psychology-of-spending/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Drive Away Happy Section:Financial Basics" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Drive Away Happy" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154757/T26-icon-121x121-FinancialBasics-2.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Drive Away Happy</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1335" href="/resources/articles/balancetrack-drive-away-happy/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Credit Matters Section:Financial Basics" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Credit Matters" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154757/T26-icon-121x121-FinancialBasics-4.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Credit Matters</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1265" href="/resources/articles/balancetrack-credit-matters/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: The World of Credit Reports Section:Financial Basics" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: The World of Credit Reports" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154756/T26-icon-121x121-FinancialBasics-1.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: The World of Credit Reports</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1262" href="/resources/articles/balancetrack-the-world-of-credit-reports/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Money Management Section:Financial Basics" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Money Management" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154758/T26-icon-121x121-FinancialBasics-5.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Money Management</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1256" href="/resources/articles/balancetrack-money-management/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Checking Account Management Section:Financial Basics" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Checking Account Management" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154757/T26-icon-121x121-FinancialBasics-3.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Checking Account Management</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 118" href="/resources/articles/balancetrack-checking-account-management/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
              </div>
            </div>
          </div>
        </section>
        <div aria-label="Planning for Success" class="border-divider"></div>
        <section aria-label="Planning for Success" class="inner-block same-height-holder">
          <div class="container">
            <div class="row">
              <h1 class="text-center text-info">Planning for Success</h1>
              <div class="col-holder">
                
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Smart Tax Planning Section:Planning for Success" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Smart Tax Planning" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154801/T26-icon-121x121-PlanningForSuccess-1.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Smart Tax Planning</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1666" href="/resources/articles/balancetrack-smart-tax-planning/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Financial Planning Section:Planning for Success" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Financial Planning" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154804/T26-icon-121x121-PlanningForSuccess-4.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Financial Planning</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1610" href="/resources/articles/balancetrack-financial-planning/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: The Basics of Investing Section:Planning for Success" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: The Basics of Investing" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154802/T26-icon-121x121-PlanningForSuccess-2.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: The Basics of Investing</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1428" href="/resources/articles/balancetrack-the-basics-of-investing/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Ten Steps to Financial Success Section:Planning for Success" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Ten Steps to Financial Success" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154804/T26-icon-121x121-PlanningForSuccess-3.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Ten Steps to Financial Success</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1345" href="/resources/articles/balancetrack-ten-steps-to-financial-success/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
              </div>
            </div>
          </div>
        </section>
        <div aria-label="Homeownership" class="border-divider"></div>
        <section aria-label="Homeownership" class="inner-block same-height-holder">
          <div class="container">
            <div class="row">
              <h1 class="text-center text-info">Homeownership</h1>
              <div class="col-holder">
                
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Using Home Equity Section:Homeownership" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Using Home Equity" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154800/T26-icon-121x121-HomeOwnership-3.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Using Home Equity</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1319" href="/resources/articles/using-home-equity/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: The Road to Homeownership Section:Homeownership" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: The Road to Homeownership" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154759/T26-icon-121x121-HomeOwnership-1.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: The Road to Homeownership</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1306" href="/resources/articles/balancetrack-the-road-to-homeownership/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
              </div>
            </div>
          </div>
        </section>
        <div aria-label="Financial Crises" class="border-divider"></div>
        <section aria-label="Financial Crises" class="inner-block same-height-holder">
          <div class="container">
            <div class="row">
              <h1 class="text-center text-info">Financial Crises</h1>
              <div class="col-holder">
                
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Rebuilding After a Financial Crisis Section:Financial Crises" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Rebuilding After a Financial Crisis" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154759/T26-icon-121x121-FinancialCrisis-2.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Rebuilding After a Financial Crisis</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1667" href="/resources/articles/balancetrack-rebuilding-after-a-financial-crisis/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Financial First Aid Section:Financial Crises" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Financial First Aid" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154759/T26-icon-121x121-FinancialCrisis-3.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Financial First Aid</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1613" href="/resources/articles/balancetrack-financial-first-aid/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Identity Theft Section:Financial Crises" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Identity Theft" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154758/T26-icon-121x121-FinancialCrisis-1.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Identity Theft</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1342" href="/resources/articles/identity-theft/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
              </div>
            </div>
          </div>
        </section>
        <div aria-label="Teens and College Students" class="border-divider"></div>
        <section aria-label="Teens and College Students" class="inner-block same-height-holder">
          <div class="container">
            <div class="row">
              <h1 class="text-center text-info">Teens and College Students</h1>
              <div class="col-holder">
                
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: The Art of Saving Money Section:Teens and College Students" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: The Art of Saving Money" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/13204118/T26-icon-121x121-ArtSaving.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: The Art of Saving Money</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 3943" href="/resources/articles/balancetrack-the-art-of-saving-money/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Cash Flow Section:Teens and College Students" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Cash Flow" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/13204118/T26-icon-121x121-MoneyFlow.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Cash Flow</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 3934" href="/resources/articles/balancetrack-cash-flow/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Repaying Student Loans Section:Teens and College Students" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Repaying Student Loans" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154805/T26-icon-121x121-TeensandStudents-3.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Repaying Student Loans</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1616" href="/resources/articles/balancetrack-student-loan-repayment/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Finances for College Students Section:Teens and College Students" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Finances for College Students" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154805/T26-icon-121x121-TeensandStudents-2.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Finances for College Students</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1615" href="/resources/articles/balancetrack-finances-for-college-students/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail BalanceTrack: Teens and Money Section:Teens and College Students" style="height: 320px;">
            
          <span class="icon">
            <img alt="BalanceTrack: Teens and Money" src="https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/01154804/T26-icon-121x121-TeensandStudents-1.png">
          </span>
        
            <div class="text-holder">
              <h2>BalanceTrack: Teens and Money</h2>
              <p class="height-finance-wrap" style="height: 0px;"></p>
              <a aria-label="Read more resource 1614" href="/resources/articles/balancetrack-teens-and-money/" class="text-read-more text-warning">READ MORE</a>
            </div>
          </article>
        </div>
      
              </div>
            </div>
          </div>
        </section>
</main>