<footer class="footer">
        <div class="container">
            <div class="row">
                <div class="logo-holder">
                    <a href="<?=$base_url?>"><img src="<?=$base_url?>assets/img/logo-primary.svg" width="198" height="44" alt="Advantage Home Plus
"></a>
                </div>
                <ul class="menu-f">
				 <?php if(isset($_SESSION['accesstoresources']) && $_SESSION['accesstoresources']=='1'){
								if($_SESSION['resources_menu']!='') $resources_menu = $_SESSION['resources_menu'];
								else $resources_menu ='Resources';		
								?>
				
				
                    <li><a href="<?=$base_url?>index.php?action=resources" title='RESOURCES'><?=$resources_menu?></a></li>
					<?php } ?>
					
					<?php if(isset($_SESSION['accesstoprograms']) && $_SESSION['accesstoprograms']=='1'){ 
								if($_SESSION['programs_menu']!='') $programs_menu = $_SESSION['programs_menu'];
								else $programs_menu ='Programs';		
							
							?>
                    <li><a href="<?=$base_url?>programs" title='PROGRAMS'><?=$programs_menu?></a></li>
					<?php } ?>
					<?php if(isset($_SESSION['accesstowebinars']) && $_SESSION['accesstowebinars']=='1'){
								if($_SESSION['webinars_menu']!='') $webinars_menu = $_SESSION['webinars_menu'];
								else $webinars_menu ='Webinars';

							?>
							
							 <li>
                                <a href="<?=$base_url?>index.php?action=webinars" title='Webinars'><?=$webinars_menu?></a>
                            </li>
							<?php } ?>


                            <?php if(isset($_SESSION['accesstocontact']) && $_SESSION['accesstocontact']=='1'){
                                if($_SESSION['contact_menu']!='') $contact_menu = $_SESSION['contact_menu'];
                                else $contact_menu ='Contact';

                            ?>
                            <li><a href="<?=$base_url?>contact" title='<?=$contact_menu?>'><?=$contact_menu?></a></li>
                            <?php } ?>
                    
                </ul>
                <ul class="social-links">
                    <li><a href="https://www.facebook.com/BALANCEFinFit/"><span class="icon-facebook"></span></a></li>
                    <li><a href="https://twitter.com/BAL_Pro"><span class="icon-twitter"></span></a></li>
                </ul>
                <div class="copyright-footer">
                    &copy; <?=date('Y')?> BALANCE. All rights reserved.
                </div>
            </div>
        </div>
    </footer>
