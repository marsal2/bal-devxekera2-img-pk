<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include 'includes/config_2.php';
//general query
//$sql = $db->prepare("SELECT ID,wp_user_id,wlwid FROM wp_quiz_results WHERE wlwid > 0");

function quiz_update_log($log_msg, $logtype)
{
    $log_filename = "log";
    if (!file_exists($log_filename))
    {
        // create directory/folder uploads.
        mkdir($log_filename, 0777, true);
    }
    if($logtype == 'success'){
        $log_file_data = $log_filename.'/updated_success_mssql_id.log';
    }else{
        $log_file_data = $log_filename.'/updated_error_mssql_id.log';
    }

    // if you don't add `FILE_APPEND`, the file will be erased each time you add a log
    file_put_contents($log_file_data, $log_msg . "\n", FILE_APPEND);
}

//get txt file data
$handle = @fopen("mssql_data.txt", "r");

while (!feof($handle)) // Loop til end of file.
{
        $buffer = fgets($handle, 4096);
        echo $buffer.'<br/>';
        //list($a,$b,$c)=explode("|",$buffer);
        $sql = $db->prepare("SELECT *  FROM wp_quiz_results WHERE ID = '$buffer'");
        $sql->execute();
        //check row count
        $count = $sql->rowCount();
        //fetch
        $fetch = $sql->fetch(PDO::FETCH_ASSOC);
        $wid = $fetch['ID'];
        $mssql_id = $fetch['mssql_id'];
        if($count > 0){
        //success msg and log
             echo 'Found Successfully: quiz result id: <b>'.$wid.'</b><br/><br/>';
             $lgtype = 'success';
             $log_msgs=date('Y-m-d H:i:s').' :: MSSQL ID :'.$mssql_id;
             quiz_update_log($log_msgs, $lgtype);
       }else{
        // error msg and log
             echo 'Error: quiz result id: <b>'.$wid.'</b><br/><br/>';
             $lgtype = 'error';
             $log_msgs = date('Y-m-d H:i:s').' :: MSSQL ID :'.$mssql_id;
             //$log_msgs=date('Y-m-d H:i:s').' :: Wordpress User Email ID :'.$user_email.' :: Whitelabel User Email ID :'.$email.' :: Whitelabel Website Id in Whitelabel User Table :'.$whi$
             quiz_update_log($log_msgs, $lgtype);
       }
}

?>

