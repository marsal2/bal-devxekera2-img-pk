<?php
    $meta = get_post_meta( get_the_ID(), '_page_edit_data', true );
    if ($meta == '') {
        echo '&nbsp;';
    } else {
        echo '<h2>' . $meta . '</h2>';
    }
