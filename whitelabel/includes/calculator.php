<?php
if(!isset($base_site_url) ){
  require_once('./db/database.php');
  $staging_url = "https://staging.".$parent_url;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>
Enhanced Loan Calculator
</title>
<meta http-equiv="X-UA-Compatible" content="IE=Edge"><meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<link rel="shortcut icon" href="favicon.ico" >
<meta name="viewport" content="height=device-height,width=device-width,initial-scale=1">
<META NAME="Description" CONTENT="Enhanced Loan Calculator: Use this calculator to look at a variety of possible loans. Change your monthly payment, loan amount, interest rate or term. Press the &quot;View Report&quot; button to see a complete amortization schedule, either by month or by year.">



<script language="JavaScript">
function navigateLink(form) {
var destination = form.selectNav[form.selectNav.selectedIndex].value;
if (destination != '*') {location.href = destination; }
else {  return false; }
}
/*
 * github: https://github.com/dominicg666,
   Blog:https://dominicdomu.blogspot.com/
   source:https://github.com/dominicg666/KJE-Calculator
 */
</script>
<link type='text/css' rel='StyleSheet' href='KJE.css' />
<link type='text/css' rel='StyleSheet' href='KJESiteSpecific.css' />


<script language="JavaScript">    
function navigateLink(form) {
var destination = form.selectNav[form.selectNav.selectedIndex].value;
if (destination != '*') {location.href = destination; }
else {  return false; }
}
</script>
</head>
<body>
<div align=center><div class=KJEWrapper id="KJEPageHeader">
<div class=container>
<div class=row>
<div class="DinkytownCalc last">
<div id="KJEAllContent"></div>
<!-- Get a Enhanced Loan Calculator you can put on your website! Many more colorful, interactive, graphing financial calculators.  Financial Calculators, ©1998-2016 KJE Computer Solutions, LLC.  For more information please see:   <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A> -->
<!--[if lt IE 9]>
<script language="JavaScript" SRC="excanvas.js"></script>
<![endif]-->
<script language="JavaScript" type="text/javascript" SRC="KJE.js"></script>
<!--<script language="JavaScript" type="text/javascript" src="https://balancepro.s3.amazonaws.com/calculators/2019/KJE.js"></script> -->
<script language="JavaScript" type="text/javascript" src="<?php echo $staging_url; ?>/wp-content/themes/balance-theme/js/M02a.js?v=2019"></script>
<!--<script language="JavaScript" type="text/javascript" SRC="KJESiteSpecific.js"></script>-->
<!-- <script language="JavaScript" type="text/javascript" SRC="EnhancedLoan.js"></script> -->
<script language="JavaScript" type="text/javascript" src="<?php echo $staging_url; ?>/wp-content/themes/balance-theme/calculator/2021/AutoLoan.js"></script>
<script language="JavaScript" type="text/javascript" src="<?php echo $staging_url; ?>/wp-content/themes/balance-theme/calculator/2021/AutoLoanParams.js"></script>
<!--CALC_PARAM_START-->

<!--CALC_PARAM_END-->
</div>
</div>
</div>

</div>
</div>
<script>if (KJE.IE7and8) KJE.init();</script>
 <script>  
 KJ=new Object();
   KJ.KJECalcNavigation = document.getElementById("KJECalcNavigation");
   KJ.KJECalcNavigation2 = document.getElementById("KJECalcNavigation2");
   KJ.KJENavButton = document.getElementById("KJENavButton");
   KJ.ShowNav = false;

   KJ.KJECalcNav = function(){
     if (KJ.ShowNav) {
       KJ.KJECalcNavigation.style.display='none';
       if (window.innerWidth < 500) KJ.KJECalcNavigation2.style.display='none';
       KJ.KJENavButton.innerHTML='<br><br>More&darr;';
       KJ.ShowNav = false;
     }
     else    {
       KJ.KJECalcNavigation.style.display='block';
       KJ.KJECalcNavigation2.style.display='inline-block';
       KJ.KJENavButton.innerHTML='<br><br>Less&uarr;';
       KJ.ShowNav = true;
     }
   };


   KJ.KJECalcNavResize = function(){
     var iNavOldWidth = window.innerWidth;
     if (iNavOldWidth > 900)   KJ.KJECalcNavigation.style.display='block';
     else  KJ.KJECalcNavigation.style.display='none';

     if (iNavOldWidth > 500)   KJ.KJECalcNavigation2.style.display='inline-block';
     else  KJ.KJECalcNavigation2.style.display='none';

     if (iNavOldWidth < 900) {KJ.ShowNav = false;  KJ.KJENavButton.innerHTML='<br><br>More&darr;';}

   };

   if (window.addEventListener) {
     window.addEventListener('resize', KJ.KJECalcNavResize ,false);
   }
 </script>

<script>
  function KJESetRecents(rvt_div){
    if (!rvt_div) return;

    var aList = new Array();
    for (var i = 0; i < localStorage.length; i++){
      aList[i] = localStorage.key(i) ;
    }
    aList = aList.sort();

    var iList = rvt_div;
    var sList = "";
    for (var i = 0; i < aList.length; i++){
      //  only care about entries without a "_" in them.
      var i_ = aList[i].indexOf("_");
      if (i_ < 0) {
        var name = (aList[i]+"_name");
        var iCutOff = aList[i].indexOf('#');
        if (iCutOff>0) sIndex = aList[i].substring(0,iCutOff);
        else sIndex =  aList[i];

        var sHref = sIndex +"_href";
        var sTitle = sIndex +"_title";
  if (localStorage.getItem(sTitle)!=null) {
    sList += "<dt style='line-height:0px'><div class='rvt_line' style='line-height:0px'><div class='rvt_line_lead' style='line-height:0px'>&#9829;</div></dt><dd><a href='"+ localStorage.getItem(sHref)+"?KJEData="+ localStorage.getItem(aList[i]) +"'>"+localStorage.getItem(sTitle)+(iCutOff>0?" : "+localStorage.getItem(name) :"")+"</a></div></dd>";
     }
      }
    }
    iList.innerHTML = '<dl><div class="rvt_title">Recently saved:</div>'+sList+'</dl>';
  }
  KJESetRecents(document.getElementById("KJERecents"));

</script>
</body>
</html>
