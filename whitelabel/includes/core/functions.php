<?php

function resourcesUrl($redirectLocations){
	global $base_url;
	//$new = str_replace('/resources/','',$redirectLocation);
	//$newx = explode('/',$new);
	//print_r($newx);
	//$id = str_replace('/','',$newx[1]);
	//$type='articles';//$newx[0];
	//$type=$newx[0];
	///resources/articles/credit-union-programs-debt-and-budget-coaching/
	//$newsite=$base_url.'index.php?action=resources1&type='.$type.'&id='.$id;
	//return $newsite;
       return $base_url.'resources/'.$redirectLocations;
}


function resourcesWhitelabelLifeStages($lifestage){
    	global $db;
		$lsWhere  = '';

		if($lifestage == '0' OR $lifestage == 0 OR $lifestage == ''){
			//$lifestage = "SELECT  P.ID AS id FROM wp_posts AS P WHERE P.post_type = 'life_stage' AND P.post_status = 'publish' AND P.`post_parent` = 0 ";
			
	 		$lifestage = getParentLifeStages();
	 		$lifestage = getSubLifeStages($lifestage);

    		$lsSelectSql =" SELECT GROUP_CONCAT(gc.`post_type`) AS post_type,
			  GROUP_CONCAT(gc.`term__in`) AS term__in,
			  GROUP_CONCAT(gc.`term__not_in`) AS term__not_in,
			  GROUP_CONCAT(gc.`post__in`) AS post__in,
			  GROUP_CONCAT(gc.`post__not_in`) AS post__not_in  FROM (SELECT 
			  ag.`post_type`,
			  ag.`term__in`,
			  ag.`term__not_in`,
			  ag.`post__in`,
			  ag.`post__not_in` 
			FROM
			  `wp_access_groups` ag INNER JOIN wp_white_label_website_access_group wwag ON ag.`wp_post_id` = wwag.`access_group_id`
			  INNER JOIN `wp_white_label_websites` ww ON wwag.`white_label_website_id` = ww.`white_label_website_id`
			WHERE ww.`domain` = '{$_SERVER['HTTP_HOST']}'

			UNION 

			SELECT  
			  ag.`post_type`,
			  ag.`term__in`,
			  ag.`term__not_in`,
			  ag.`post__in`,
			  ag.`post__not_in` 

			FROM `wp_postmeta` wpm INNER JOIN `wp_access_groups` ag WHERE wpm.`post_id` in ({$lifestage}) AND wpm.`meta_key`='_page_edit_data' 
        AND FIND_IN_SET(ag.`wp_post_id`,(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( wpm.`meta_value`, '\"accessgroups\"' , -1), '\";}}s:',1),'\"',-1)))
			        ) AS gc ";
			}else{

				$lifestage = getSubLifeStages($lifestage);

			$lsSelectSql =" SELECT  GROUP_CONCAT(ag.`post_type`) AS post_type,GROUP_CONCAT(ag.`term__in`) AS term__in,GROUP_CONCAT(ag.`term__not_in`) AS term__not_in,
    GROUP_CONCAT(ag.`post__in`) AS post__in,GROUP_CONCAT(ag.`post__not_in`) AS post__not_in FROM `wp_postmeta` wpm INNER JOIN `wp_access_groups` ag WHERE wpm.`post_id` in ({$lifestage}) AND wpm.`meta_key`='_page_edit_data' 
        AND FIND_IN_SET(ag.`wp_post_id`,(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( wpm.`meta_value`, '\"accessgroups\"' , -1), '\";}}s:',1),'\"',-1))) ";
			}
			// echo $lsSelectSql;
			// exit;
            $lsResult = $db->prepare($lsSelectSql);
            $lsResult->execute();
            $lsRow =$lsResult->fetch(PDO::FETCH_ASSOC);
            if($lsRow){
                
                $post_type = "'".str_replace(",", "','",delComma($lsRow['post_type'])). "'";
                $term__in = delComma($lsRow['term__in']);
                $term__in = ($term__in)?$term__in:0;
                $term__not_in = delComma($lsRow['term__not_in']);
                $term__not_in = ($term__not_in)?$term__not_in:0;
                $post__in = delComma($lsRow['post__in']);
                $post__in = ($post__in)?$post__in:0;
                $post__not_in = delComma($lsRow['post__not_in']);
                $post__not_in = ($post__not_in)?$post__not_in:0;

                $lsWhere .= " AND (";
                $lsWhere .= " (P.post_type in ({$post_type}) ";
                $lsWhere .= "  OR P.ID in (SELECT object_id FROM `wp_term_relationships` WHERE `term_taxonomy_id` in ({$term__in})) ";
                $lsWhere .= " OR P.ID in ({$post__in}) )  ";

                $lsWhere .= " AND (P.ID not in (SELECT object_id FROM `wp_term_relationships` WHERE `term_taxonomy_id` in ({$term__not_in})) ";
                $lsWhere .= " and P.ID not in ({$post__not_in})) ";
                $lsWhere .= " )";

            }
    // print_r($lsWhere);
    // exit;
    return $lsWhere;
}

function getParentLifeStages(){
	global $db;
	$lsSelectSqlParent = "SELECT  GROUP_CONCAT(P.ID) AS id FROM wp_posts AS P WHERE P.post_type = 'life_stage' AND P.post_status = 'publish' AND P.`post_parent` = 0 ";

	$lsResultParent = $db->prepare($lsSelectSqlParent);
    $lsResultParent->execute();
    $lsRowParent =$lsResultParent->fetch(PDO::FETCH_ASSOC);
    //$lifestage = '';
    if($lsRowParent){
    	$lifestage .=  ','.$lsRowParent['id'];
    }
    $lifestage = trim($lifestage,',');
    return $lifestage;
}

function getSubLifeStages($lifestage){
	global $db;
	$lsSelectSqlSub = "SELECT  GROUP_CONCAT(P.ID) AS id FROM wp_posts AS P WHERE P.post_type = 'life_stage' AND P.post_status = 'publish' AND P.`post_parent` in ($lifestage) ";
	$lsResultSub = $db->prepare($lsSelectSqlSub);
    $lsResultSub->execute();
    $lsRowSub =$lsResultSub->fetch(PDO::FETCH_ASSOC);
    if($lsRowSub){
    	$lifestage .=  ','.$lsRowSub['id'];
    }
    $lifestage = trim($lifestage,',');

    return $lifestage;
}

function delComma($strVal){
	$string = preg_replace("/,+/", ",", $strVal);
	return trim($string,',');
}
?>
