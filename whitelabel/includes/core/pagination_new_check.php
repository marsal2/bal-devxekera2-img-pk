<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
// SET HEADER
header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
include('database.php');include('functions.php');
// MAKE SQL QUERY
$personData = json_decode($_REQUEST['data']);

$rtypes = $personData->dvalue;
$page = $personData->pager;
$lifestage = $personData->lifestage;
$search = $personData->search;
$sort = $personData->sort;
$tags = $personData->tags;
if(empty($tags) or $tags == "" ){
	$tags = '0'	;
}
if($lifestage == 1 || $lifestage == '1'){
    $lifestage = 1; 
}
$keywords= explode(',', $tags);
$advancedkeywords = implode("', '", $keywords);
if(empty($tags) or $tags == "" ){
	$tags = '0'	;
}
if(empty($sort)){
	$sort = "0";
}
if($rtypes == 'tags'){
	$rtypes = "0";
}
if(empty($search)){
	$search = "0";
}
if(empty($lifestage)){
	$lifestage = "0";
}

if(!empty($personData->rtypes)){
    $resourcetypes = $personData->rtypes;
    $rtypes = $personData->rtypes;
    $rtype = $personData->rtypes;
    $rt = $personData->rtypes;
} 

if(!empty($personData->rtype)){
    $rt = $personData->rtype;
    $resourcetypes = $personData->rtype;
    $rtypes = $personData->rtype;
    $rtype = $personData->rtype; 
} 


$searchQuery = str_replace('\\', "", $search);
$unquotedQuery = str_replace('"', "", $search);

$searchQuery = preg_replace('/[#\@\.\*\%\;\$\&\^\-]+/', '', $searchQuery);
$unquotedQuery = str_replace("'", "", $unquotedQuery);
$unquotedQuery = preg_replace('/[#\@\.\*\%\;\$\&\^\-]+/', '', $unquotedQuery);


$limit = 9;
if($page){
	$start = ($page - 1) * $limit; 
}else{
	$start = 0; 
}  
$list = 'true';
$level = '100';
$pgorder = '1';
$resources = 'resources/';
$check = '0';
if($search == ''){
	$searchvalue = '0';
}else{
	$searchvalue = $search;
}

/// ================ Start adding new query =====
/**
     * Optimise code
     */
    $select = "select distinct RS.wp_post_id, P.post_title as title, P.post_date_gmt as `date`, P.post_type, P.post_name, RS.slug, match (RS.html, RS.title) against ('{$unquotedQuery}') AS relevancy, (P.post_title = '{$unquotedQuery}') AS title_match, (P.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, wc.view_count, RS.page_order,P.post_date_gmt";
    $selectCount = "select Count(DISTINCT P.id) AS pages";
    $join = " join wp_posts P on P.ID = RS.wp_post_id ";

    // if ($lifestage != 0) {
    //     $join .= " join life_stage_type AS l ON (l.lifestagetype in ({$lifestage}) AND l.postid = RS.ID) ";
    // }

    $join .= "  left join wp_postmeta PM on PM.post_id = RS.wp_post_id AND PM.meta_value <='{$searchQuery}' AND PM.meta_key = 'access_level' AND PM.meta_key = 'list_in_search' ";
    $join .= "  left join wp_postmeta MM on MM.post_id = RS.wp_post_id AND MM.meta_value = 'true'";
    $join .= "  left join wp_postmeta MD on MD.post_id = RS.wp_post_id AND MD.meta_value = '_page_edit_data'";
    $join .= "  left join wp_resources_view_count as wc on wc.wp_post_id = RS.wp_post_id ";

    $from = " from wp_resources as RS";
    $where = " where P.post_status = 'publish' AND RS.level_of_access = '$level' AND RS.list_in_search = '$list' AND RS.page_order = '$pgorder'";
    if ($searchQuery != '0') {
        $where .= " and (match (RS.html, RS.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
    }
    if ($rtypes != '0') {
        $where .= " and P.post_type='{$rtypes}' ";
    }
//    echo $tags;
    if ($tags != '0') {
        $tags = rtrim($tags, ',');
        $where .= " and  P.ID in (select TR.object_id from wp_term_taxonomy as TT  join wp_term_relationships as TR on TR.term_taxonomy_id = TT.term_taxonomy_id where TT.taxonomy = 'resource_tag' and TT.term_id in ({$tags})) ";
    }
    //Custom Pagination
    $order = '';
    // if($sort === 0 || $sort == '0'){
    //     $order .= " order by title_match desc, title_rough_match desc, relevancy desc";
    // }
    // else 
    
    if ($sort == 'views') {
        $order .= " order by wc.view_count DESC";
    }
    else if  ($sort == 'relevance') {
        $order .= " AND RS.page_order = '$pgorder' ";
    }
    else if ($sort == 'date') {
        $order .= " order by P.post_date_gmt desc";
    }
//    $order .=  " limit {$start},{$limit}";
    $where .= resourcesWhitelabelLifeStages($lifestage);
    $sql = $select . $from . $join . $where . $order;
        //echo $sql; die;
    $checkn = $db->prepare($sql);
/// ================ End adding new query =====
	$checkn->execute();
	//now count row 
	$checkcountn = $checkn->rowCount();
	//$return_arr['countnow'] = $checkcountn;
	$totalpages = ceil( $checkcountn / $limit );
	$output = '';
	$output .='<nav aria-label="balance pager m14-m15" balance-pager="" class="paging-holder clear" totalcheckcount="'.$checkcountn.'" totalpages="'.$totalpages.'">
	<ul class="pagination">';
	if(empty($page) or $page == '' or $page == 0 or $page == '0'){
		$page = 1;
	}							
		if ($page > 1) {
			if($totalpages != 1){
				$output .='<li>
				<div class="prv-btn" lifestage="'.$lifestage.'" type="'.$rtypes.'" pager="'.($page-1).'"  search="'.$searchvalue.'"  sort="'.$sort.'">
					<div style="float:left;margin-right: 5px;margin-left: 10px;margin-top: 11px; cursor: pointer;">
						<span class="btn-prev"></span>
					</div>
					<div style="float:left;margin-top: 7px;  cursor: pointer; margin-right: 22px;">
						<span class="hidden-xs">Prev</span>
					</div>
				</div>
				</li>';
			}
		}							
		if($page == $totalpages){
			if($totalpages == 1){
				$output .='<li class="active" style="padding:5px 6px; cursor: pointer"  totalpages="'.$totalpages.'"  resourcetypes="'.$rtypes.'" lifestage="'.$lifestage.'" queryvalue="'.$searchvalue.'" pagerv="1"  sort="'.$sort.'">1</li>';
			} else if($totalpages<= 6) {
				for ($i= 1 ; $i <= $totalpages; $i++) {
					$output .='<li class="pg-btn '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" lifestage="'.$lifestage.'" typevalue="'.$rtypes.'" pagerv="'.$i.'"  search="'.$searchvalue.'"  sort="'.$sort.'">'.$i.'</li>';
				}
			} else if((6 + $page -1)<$totalpages){
				for ($i= (1 + $page -1) ; $i <= (6 + $page -1) ; $i++) {
					$output .='<li class="pg-btn '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" lifestage="'.$lifestage.'" typevalue="'.$rtypes.'" pagerv="'.$i.'"  search="'.$searchvalue.'"  sort="'.$sort.'">'.$i.'</li>';
				}
			} else{
				for ($i= ($totalpages-5); $i <=  $totalpages ; $i++)
				{
					$output .='<li class="pg-btn '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" lifestage="'.$lifestage.'" typevalue="'.$rtypes.'" pagerv="'.$i.'"  search="'.$searchvalue.'"  sort="'.$sort.'">'.$i.'</li>';
				}
			}

			if ($page < $totalpages) {
				$output .='<li><div class="next-btn" search="'.$searchvalue.'"  sort="'.$sort.'" lifestage="'.$lifestage.'" type="'.$rtypes.'" pager="'.($page+1).'">
					<div style="float:left;margin-right: 5px;margin-left: 22px;margin-top: 4px; cursor: pointer;"><span class="hidden-xs">Next</span></div>
					<div style="float:left;margin-top: 10px;  cursor: pointer;"><span class="btn-next"></span></div>
				</div></li>';
			}
			if($totalpages<2) $pgn = 'page'; else $pgn='pages';
			$output .='</ul>
				<p>
					<span>of&nbsp;</span>
					<span class="ng-binding">'.$totalpages.'</span>
					<span>&nbsp;'.$pgn.'</span>
				</p>
		</nav>';

			}else{
				if($totalpages == 1){
				$output .='<li class="active" style="padding:5px 6px; cursor: pointer"  totalpages="'.$totalpages.'"  resourcetypes="'.$rtypes.'" lifestage="'.$lifestage.'" queryvalue="'.$searchvalue.'" pagerv="1"  sort="'.$sort.'">1</li>';
				} else if($totalpages<= 6) {
					for ($i= 1 ; $i <= $totalpages; $i++) {
						$output .='<li class="pg-btn '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" lifestage="'.$lifestage.'" typevalue="'.$rtypes.'" pagerv="'.$i.'"  search="'.$searchvalue.'"  sort="'.$sort.'">'.$i.'</li>';
					}
				} else if((6 + $page -1)<$totalpages){
					for ($i= (1 + $page -1) ; $i <= (6 + $page -1) ; $i++) {
						$output .='<li class="pg-btn '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" lifestage="'.$lifestage.'" typevalue="'.$rtypes.'" pagerv="'.$i.'"  search="'.$searchvalue.'"  sort="'.$sort.'">'.$i.'</li>';
					}
				} else{
					for ($i= ($totalpages-5); $i <=  $totalpages ; $i++) 
					{
						$output .='<li class="pg-btn '.($page == $i ? 'active' : '').'" style="padding:5px 6px; cursor: pointer" lifestage="'.$lifestage.'" typevalue="'.$rtypes.'" pagerv="'.$i.'"  search="'.$searchvalue.'"  sort="'.$sort.'">'.$i.'</li>';
					}
				}

			if ($page < $totalpages) {
				$output .='<li><div class="next-btn"  sort="'.$sort.'" search="'.$searchvalue.'" lifestage="'.$lifestage.'" type="'.$rtypes.'" pager="'.($page+1).'">
					<div style="float:left;margin-right: 5px;margin-left: 22px;margin-top: 4px; cursor: pointer;"><span class="hidden-xs">Next</span></div>
					<div style="float:left;margin-top: 10px;  cursor: pointer;"><span class="btn-next"></span></div>
				</div></li>';
			}
		if ($totalpages > 1) {
		$output .='</ul>
			<p>
				<span>of&nbsp;</span>
				<span class="ng-binding">'.$totalpages.'</span>
				<span>&nbsp;pages</span>
			</p>
	</nav>';
        } else if ($totalpages == 1)  {
        $output .='</ul>
            <p>
                <span>of&nbsp;</span>
                <span class="ng-binding">'.$totalpages.'</span>
                <span>&nbsp;page</span>
            </p>
    </nav>';
        } else {
		$output .='</ul>
			<p>
				<span>of&nbsp;</span>
				<span class="ng-binding">'.$totalpages.'</span>
				<span>&nbsp;page</span>
			</p>
	</nav>';
		}
		}
										
			$return_arr['message'] = $output;
			echo json_encode($return_arr);
		?>