<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
// SET HEADER
header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
include('database.php');include('functions.php');
 // MAKE SQL QUERY
    $personData = json_decode($_REQUEST['data']);
    $dtype = $personData->type;  // Resource type
    $dpager = $personData->pager;  // Page number
    $lifestage = $personData->lifestage;
    $search = $personData->search;

    //29/10/21 Sorting by Swati
    $sort = $personData->sort;


    if(empty($lifestage)){
        $lifestage = "0";
    }

    if(empty($dtype)){
        //$dtype = "0";
        $dtype = '';
    }

    if($dtype == 'tags'){
        $dtype = "";
    }
    if(!isset($personData->search)){
        $search = $personData->dvalue;
    }
//    if(empty($sort)){
//        $sort = "0";
//    }
    $newsearch='';
    if(strpos($search ,'(' )!='' &&  strpos($search ,')') ==''){
        $searchx = explode('(',$search);
        $newsearch = trim($searchx[0]);
    }
    else if(strpos($search ,'(' )!='' &&  strpos($search ,')') !=''){
        $search = trim(preg_replace('/\s*\([^)]*\)/', '', $search));
    }
    if($newsearch!=''){
        $search=$newsearch;
    }

    if($search == '0'){
        $search = '';
        $searchQuery = '';
        $unquotedQuery = '';
    }
    $searchQuery = str_replace('\\', "", $search);
    $unquotedQuery = str_replace('"', "", $search);
    $searchQuery = preg_replace('/[#\@\.\*\%\;\$\&\^\-]+/', '', $searchQuery);
    $unquotedQuery = str_replace("'", "", $unquotedQuery);
    $unquotedQuery = preg_replace('/[#\@\.\*\%\;\$\&\^\-]+/', '', $unquotedQuery);


   // if($searchQuery == 0){
   //     $searchQuery = '';
   //     $unquotedQuery = '';
   // }


    $return_arr = array();
    $limit = '9';
    // $limitn = ' limit 9';
    if($dpager){
        $start = ($dpager - 1) * $limit;
    }else{
        $start = 0;
    }
    $tags = $personData->tags;
    if(empty($tags) or $tags == "" ){
        $tags = '0' ;
    }
    $keywords= explode(',', $tags);
    $advancedkeywords = implode("', '", $keywords);
//    if(empty($tags) or $tags == "" ){
//        $tags = '0'   ;
//    }
    $list = 'true';
    $level = '100';
    $pgorder = '1';
    /**
     * Optimise code
     */
    $select = "select distinct RS.wp_post_id, P.post_title as title, P.post_date_gmt as `date`, P.post_type, P.post_name, RS.slug, match (RS.html, RS.title) against ('{$unquotedQuery}') AS relevancy, (P.post_title = '{$unquotedQuery}') AS title_match, (P.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, wc.view_count, RS.page_order, P.post_date_gmt";
    $join = " join wp_posts P on P.ID = RS.wp_post_id ";

    // if($lifestage != 0){
    //     $join .=" join life_stage_type AS l ON (l.lifestagetype in ({$lifestage}) AND l.postid = RS.ID) ";
    // }

    $join .= "  left join wp_postmeta PM on PM.post_id = RS.wp_post_id AND PM.meta_value <='{$searchQuery}' AND PM.meta_key = 'access_level' AND PM.meta_key = 'list_in_search' ";
    $join .= "  left join wp_postmeta MM on MM.post_id = RS.wp_post_id AND MM.meta_value = 'true'";
    $join .= "  left join wp_postmeta MD on MD.post_id = RS.wp_post_id AND MD.meta_value = '_page_edit_data'";
    $join .= "  left join wp_resources_view_count as wc on wc.wp_post_id = RS.wp_post_id ";

    $from = " from wp_resources as RS";
    $where = " where P.post_status = 'publish' AND RS.level_of_access = '$level' AND RS.list_in_search = '$list' AND RS.page_order = '$pgorder'" ;
    if ($searchQuery != '') {
        $where .= " and (match (RS.html, RS.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
    }


    if($dtype !== ''){
        $where .= " and P.post_type='{$dtype}' ";
    }
//    echo $tags;
    if($tags != 0){
        $tags = rtrim($tags,',');
        $where .= " and  P.ID in (select TR.object_id from wp_term_taxonomy as TT  join wp_term_relationships as TR on TR.term_taxonomy_id = TT.term_taxonomy_id where TT.taxonomy = 'resource_tag' and TT.term_id in ({$tags})) ";
    }
    //Custom Pagination
    $order = '';
    // if($sort === 0 || $sort == '0'){
    //     $order .= " order by title_match desc, title_rough_match desc, relevancy desc";
    // }
    // else 

    if ($sort == 'views') {
        $order .= " order by wc.view_count DESC";
    }
    else if  ($sort == 'relevance') {
        $order .= " AND RS.page_order = '$pgorder' ";
    }
    else if ($sort == 'date') {
        $order .= " order by P.post_date_gmt desc";
    }
    $order .=  " limit {$start},{$limit}";
    //echo $order;
    $where .= resourcesWhitelabelLifeStages($lifestage);
    $sql = $select . $from . $join . $where . $order;
    //echo $sort; echo '<br>';
    //echo $sql; die;

    $result = $db->prepare($sql);
    $result->execute();
    $countthem = $result->rowCount();

    $url =  (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
$escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
$table_name = "wp_term_relationships";
$table_name2 = "wp_terms";
$resources = '/resources/';

while($row = $result->fetch(PDO::FETCH_ASSOC)){
    $titlen = $row['title'];
        $titlesmall = substr_replace($titlen, "...", 150);
    $dvaluen = $row['post_type'];
    $slug = $row['slug'];
        $postid = $row['wp_post_id'];
    if($dvaluen == 'article'){
    $seo_dvalue = 'articles/';
    }else if($dvaluen == 'calculator'){
     $seo_dvalue = 'calculators/';
    }else if($dvaluen == 'video'){
     $seo_dvalue = 'videos/';
    }else if($dvaluen == 'newsletter'){
     $seo_dvalue = 'newsletters/';
    }else if($dvaluen == 'podcast'){
     $seo_dvalue = 'podcasts/';
    }else if($dvaluen == 'toolkit'){
     $seo_dvalue = 'toolkits/';
    }else if($dvaluen == 'booklet'){
     $seo_dvalue = 'booklets/';
    }else if($dvaluen == 'worksheet'){
         $seo_dvalue = 'worksheets/';
        }else if($dvaluen == 'checklist'){
         $seo_dvalue = 'checklists/';
        }else{}
        $output = '';
        //$output .= '<div class="resource-column same-height-holder content-inner-page">';
        $output .= '<!-- resource resource in resources starts -->
            <div class="col-sm-6 col-md-4">
             <!-- resource block starts -->
             <div class="resource-block">';
        $output .= '<div class="img-holder same-height"><span class="icon-'.$dvaluen.'"></span></div>
           <div class="text-holder"><p class="dot-holder">'.$titlesmall.'</p>';
        $output .= '<div class="btn-tag same-height  same-height-left same-height-right" style="height: 103px;" dvalue="'.$countthem.'">';
        $resultss = $db->prepare("SELECT Distinct  * FROM $table_name WHERE object_id='$postid' limit 3");
    $resultss->execute();
        while($resultn = $resultss->fetch(PDO::FETCH_ASSOC)) {
         $tagid = $resultn['term_taxonomy_id'];
           $output .= $tagid;
           $gettagname = $db->prepare("SELECT Distinct  * FROM $table_name2 WHERE term_id='$tagid'");
           $gettagname->execute();
           while($resultnn = $gettagname->fetch(PDO::FETCH_ASSOC)){
               $output .='<a class="tag ng-binding ng-scope tag-click" url="'.$escaped_url.'" tagname="'.$resultnn['name'].'" pager="'.$pager.'" type="'.$type.'" dvalue="'.$tagid.'"  tags="'.$tagid.'"  tagid="'.$tagid.'" sort="'.$sort.'">';
               $output .= $resultnn['name'];
               $output .='</a>';
           }
        }
    $viewUrl = $seo_dvalue.$slug;$variable = resourcesUrl($viewUrl);
        $output .='</div>';
        $output .='<a href="'.$variable.'" target="_self" class="text-view btn btn-warning btn-block">VIEW</a>';
        $output .='</div>';
        $output .= '<span class="icon-lock" style="display: none;"></span>';
        $output .= '</div>
                    <!-- resource block ends -->
                    </div>
                    <!-- resource resource in resources starts -->';
        //$output .= '</div>';
        $return_arr[] = array("message" => $output);
        
        //echo $output;
        //return $output;
}
// Encoding array in JSON format
echo json_encode($return_arr);
?>