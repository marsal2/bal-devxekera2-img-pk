<?php
    session_start();
    $timesettings = $_SESSION['timesettings'];

    if(isset($timesettings['chat_description']) && trim($timesettings['chat_description'])!=''){
                $chatmessage = $timesettings['chat_description'];
    }
    else {
        $chatmessage ='<p>'.$_SESSION['title'].' chat is currently unavailable. Our normal business hours are in (UTC-08:00) Pacific Time (US):<br> %timesettings%.</p><p> You may call us at 1-888-456-2227 during normal business hours.</p>';
    }
    if(isset($timesettings) && !empty($timesettings)){
        $chat_time_msg ='<BR><table border="0">
	<tr>
		<td>Monday</td>';
        if(isset($timesettings['mon_status']) && $timesettings['mon_status'] == 1){
            $chat_time_msg.='<td>'.$timesettings['mon_start'].' to '.$timesettings['mon_end'].' </td>';
        }
        else $chat_time_msg.='<td>Closed</td>';
        $chat_time_msg.='</tr>
	<tr>
		<td>Tuesday</td>';
        if(isset($timesettings['tue_status']) && $timesettings['tue_status'] == 1){
            $chat_time_msg.='<td>'.$timesettings['tue_start'].' to '.$timesettings['tue_end'].' </td>';
        }
        else $chat_time_msg.='<td>Closed</td>';
        $chat_time_msg.='</tr>
	<tr>
		<td>Wednesday</td>';
        if(isset($timesettings['wed_status']) && $timesettings['wed_status'] == 1){
            $chat_time_msg.='<td>'.$timesettings['wed_start'].' to '.$timesettings['wed_end'].'</td>';
        }
        else $chat_time_msg.='<td>Closed</td>';
        $chat_time_msg.='</tr>
	<tr>
		<td>Thursday</td>';
        if(isset($timesettings['thu_status']) && $timesettings['thu_status'] == 1){
            $chat_time_msg.='<td>'.$timesettings['thu_start'].' to '.$timesettings['thu_end'].' </td>';
        } else $chat_time_msg.='<td>Closed</td>';
        $chat_time_msg.='</tr>
	<tr>
		<td>Friday</td>';
        if(isset($timesettings['fri_status']) && $timesettings['fri_status'] == 1){
            $chat_time_msg.='<td>'.$timesettings['fri_start'].' to '.$timesettings['fri_end'].' </td>';
        } else $chat_time_msg.='<td>Closed</td>';
        $chat_time_msg.='</tr>
	<tr>
		<td>Saturday</td>';
        if(isset($timesettings['sat_status']) && $timesettings['sat_status'] == 1){
            $chat_time_msg.='<td>'.$timesettings['sat_start'].' to '.$timesettings['sat_end'].'<td>';
        } else $chat_time_msg.='<td>Closed</td>';
        $chat_time_msg.='</tr>
	<tr>
		<td>Sunday</td>';
        if(isset($timesettings['sun_status']) && $timesettings['sun_status'] == 1){
            $chat_time_msg.='<td>'.$timesettings['sun_start'].' to '.$timesettings['sun_end'].'</td>';
        } else $chat_time_msg.='<td>Closed</td>';
        $chat_time_msg.='</tr>
</table>';
        $chat_title = str_replace(array('%sitetitle%','%timesettings%'),array($_SESSION['title'],$chat_time_msg),$chatmessage);
    } else {
        $chat_title = '<p>'.$_SESSION['title'].' chat is currently unavailable. Our normal business hours are, Monday to Thursday from 7:30am—6:00pm, Friday 7:30am—5:00pm, and Saturday 9:00am—2:00pm (PST). You may call us at 1-888-456-2227 during normal business hours.</p>';
    }
?>
<div style="font-family: 'Arial', sans-serif;"><?php echo $chat_title ;?></div>
